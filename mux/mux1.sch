EESchema Schematic File Version 2  date 19/07/2012 9:42:41 AM
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:chrisj1
LIBS:diy_connectors
LIBS:diy_diodes
LIBS:diy_ics
LIBS:diy_rcl
LIBS:diy_regulators
LIBS:diy_switches
LIBS:diy_transformers
LIBS:diy_transistors
LIBS:diy_triac
LIBS:diy_tubes
LIBS:mux1-cache
EELAYER 25  0
EELAYER END
$Descr A3 16535 11700
encoding utf-8
Sheet 1 1
Title "Multiplexer for IV curve tracer and low power PV panels"
Date "18 jul 2012"
Rev ""
Comp "CSES, CECS, ANU"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 8600 10400 0    30   ~ 0
PC_CTS
Text Notes 8800 10200 0    30   ~ 0
PC_RTS
Connection ~ 10550 9350
Wire Wire Line
	10550 9350 10950 9350
Wire Wire Line
	10950 9350 10950 9300
Connection ~ 11550 8750
Wire Wire Line
	11550 8750 11550 8850
Wire Wire Line
	11550 8850 11500 8850
Connection ~ 10550 7350
Wire Wire Line
	10550 7350 10950 7350
Wire Wire Line
	10950 7350 10950 7300
Connection ~ 10550 5350
Wire Wire Line
	10550 5350 10950 5350
Wire Wire Line
	10950 5350 10950 5300
Connection ~ 11550 4750
Wire Wire Line
	11550 4750 11550 4850
Wire Wire Line
	11550 4850 11500 4850
Wire Wire Line
	10550 9500 10550 9350
Wire Wire Line
	10550 9350 10550 7350
Wire Wire Line
	10550 7350 10550 5350
Wire Wire Line
	10550 5350 10550 3350
Wire Wire Line
	10550 3350 10550 900 
Connection ~ 11550 2750
Wire Wire Line
	11550 2750 11550 2850
Wire Wire Line
	11550 2850 11500 2850
Wire Wire Line
	2400 8700 2350 8700
Wire Wire Line
	2350 8700 2000 8700
Wire Wire Line
	2000 8700 1850 8700
Wire Wire Line
	1850 8700 1600 8700
Connection ~ 1650 9200
Wire Wire Line
	3350 6450 2750 6450
Wire Wire Line
	3350 6250 2750 6250
Wire Wire Line
	3350 6150 2750 6150
Wire Wire Line
	3350 6350 2750 6350
Wire Wire Line
	3350 6550 2750 6550
Wire Wire Line
	15150 8550 15150 6550
Wire Wire Line
	15150 6550 15150 4550
Wire Wire Line
	15150 4550 15150 2550
Wire Wire Line
	15150 2550 15150 1200
Connection ~ 14850 1500
Wire Wire Line
	14850 7500 14850 5500
Wire Wire Line
	14850 5500 14850 3500
Wire Wire Line
	14850 3500 14850 1500
Wire Wire Line
	14850 1500 14850 1300
Connection ~ 15150 6550
Wire Wire Line
	15150 6550 14450 6550
Connection ~ 15250 5850
Wire Wire Line
	15250 5850 14450 5850
Connection ~ 15150 4550
Wire Wire Line
	15150 4550 14450 4550
Connection ~ 15250 3850
Wire Wire Line
	15250 3850 14450 3850
Connection ~ 11050 8750
Wire Wire Line
	11050 8700 11050 8750
Wire Wire Line
	11050 4700 11050 4750
Connection ~ 11400 6750
Wire Wire Line
	15250 7850 15250 5850
Wire Wire Line
	15250 5850 15250 3850
Wire Wire Line
	15250 3850 15250 1850
Wire Wire Line
	15250 1850 15250 1400
Wire Wire Line
	12850 6250 14500 6250
Wire Wire Line
	12850 6050 13400 6050
Wire Wire Line
	12850 5850 13550 5850
Wire Notes Line
	12000 6050 12000 6100
Wire Notes Line
	11950 6150 12000 6100
Wire Notes Line
	11950 6150 11950 6200
Wire Notes Line
	11950 6200 12250 6200
Wire Notes Line
	12250 6200 12250 6150
Wire Notes Line
	12250 6150 12400 6150
Wire Notes Line
	12050 5850 12000 5900
Wire Notes Line
	12050 5850 12200 5850
Wire Notes Line
	12200 5850 12200 5950
Wire Notes Line
	12200 5950 12400 5950
Wire Wire Line
	11600 6650 13650 6650
Wire Wire Line
	13400 6050 13400 6500
Wire Wire Line
	13400 6500 13600 6500
Wire Wire Line
	13600 6500 13600 5750
Wire Wire Line
	13600 5750 13650 5750
Wire Wire Line
	13650 6450 13450 6450
Wire Wire Line
	13450 6450 13450 6150
Wire Wire Line
	13500 5900 13500 5750
Wire Wire Line
	13500 5900 14500 5900
Wire Wire Line
	14500 5900 14500 6000
Wire Wire Line
	14500 6000 14450 6000
Wire Wire Line
	14450 6350 14500 6350
Wire Wire Line
	14500 6350 14500 6250
Wire Wire Line
	13650 6100 13550 6100
Wire Wire Line
	13550 6100 13550 5850
Wire Wire Line
	13650 5400 13450 5400
Wire Wire Line
	13450 5400 13450 5950
Wire Notes Line
	12400 5750 11750 5750
Wire Notes Line
	11750 5750 11750 5850
Wire Notes Line
	11750 5850 11700 5900
Wire Notes Line
	11700 5900 11800 5950
Wire Notes Line
	11800 5950 11700 6000
Wire Notes Line
	11700 6000 11800 6050
Wire Notes Line
	11800 6050 11700 6100
Wire Notes Line
	11700 6100 11750 6150
Wire Notes Line
	11750 6150 11750 6250
Wire Notes Line
	11750 6250 12400 6250
Wire Notes Line
	12400 5850 12250 5850
Wire Notes Line
	12250 5850 12250 5800
Wire Notes Line
	12250 5800 11950 5800
Wire Notes Line
	11950 5800 11950 5850
Wire Notes Line
	11950 5850 12000 5900
Wire Notes Line
	12400 6050 12200 6050
Wire Notes Line
	12200 6050 12200 6150
Wire Notes Line
	12200 6150 12050 6150
Wire Notes Line
	12050 6150 12000 6100
Wire Notes Line
	12000 5900 12000 5950
Wire Notes Line
	11950 6050 11950 5950
Wire Notes Line
	11950 6050 12050 6050
Wire Notes Line
	12050 6050 12050 5950
Wire Notes Line
	12050 5950 11950 5950
Wire Wire Line
	13500 5750 12850 5750
Wire Wire Line
	13450 5950 12850 5950
Wire Wire Line
	13450 6150 12850 6150
Wire Wire Line
	12850 4250 14500 4250
Wire Wire Line
	12850 4050 13400 4050
Wire Wire Line
	12850 3850 13550 3850
Wire Notes Line
	12000 4050 12000 4100
Wire Notes Line
	11950 4150 12000 4100
Wire Notes Line
	11950 4150 11950 4200
Wire Notes Line
	11950 4200 12250 4200
Wire Notes Line
	12250 4200 12250 4150
Wire Notes Line
	12250 4150 12400 4150
Wire Notes Line
	12050 3850 12000 3900
Wire Notes Line
	12050 3850 12200 3850
Wire Notes Line
	12200 3850 12200 3950
Wire Notes Line
	12200 3950 12400 3950
Wire Wire Line
	11600 4650 13650 4650
Wire Wire Line
	13400 4050 13400 4500
Wire Wire Line
	13400 4500 13600 4500
Wire Wire Line
	13600 4500 13600 3750
Wire Wire Line
	13600 3750 13650 3750
Wire Wire Line
	13650 4450 13450 4450
Wire Wire Line
	13450 4450 13450 4150
Wire Wire Line
	13500 3900 13500 3750
Wire Wire Line
	13500 3900 14500 3900
Wire Wire Line
	14500 3900 14500 4000
Wire Wire Line
	14500 4000 14450 4000
Wire Wire Line
	14450 4350 14500 4350
Wire Wire Line
	14500 4350 14500 4250
Wire Wire Line
	13650 4100 13550 4100
Wire Wire Line
	13550 4100 13550 3850
Wire Wire Line
	13650 3400 13450 3400
Wire Wire Line
	13450 3400 13450 3950
Wire Notes Line
	12400 3750 11750 3750
Wire Notes Line
	11750 3750 11750 3850
Wire Notes Line
	11750 3850 11700 3900
Wire Notes Line
	11700 3900 11800 3950
Wire Notes Line
	11800 3950 11700 4000
Wire Notes Line
	11700 4000 11800 4050
Wire Notes Line
	11800 4050 11700 4100
Wire Notes Line
	11700 4100 11750 4150
Wire Notes Line
	11750 4150 11750 4250
Wire Notes Line
	11750 4250 12400 4250
Wire Notes Line
	12400 3850 12250 3850
Wire Notes Line
	12250 3850 12250 3800
Wire Notes Line
	12250 3800 11950 3800
Wire Notes Line
	11950 3800 11950 3850
Wire Notes Line
	11950 3850 12000 3900
Wire Notes Line
	12400 4050 12200 4050
Wire Notes Line
	12200 4050 12200 4150
Wire Notes Line
	12200 4150 12050 4150
Wire Notes Line
	12050 4150 12000 4100
Wire Notes Line
	12000 3900 12000 3950
Wire Notes Line
	11950 4050 11950 3950
Wire Notes Line
	11950 4050 12050 4050
Wire Notes Line
	12050 4050 12050 3950
Wire Notes Line
	12050 3950 11950 3950
Wire Wire Line
	13500 3750 12850 3750
Wire Wire Line
	13450 3950 12850 3950
Wire Wire Line
	13450 4150 12850 4150
Wire Wire Line
	12850 8150 13450 8150
Wire Wire Line
	12850 7950 13450 7950
Wire Wire Line
	12850 7750 13500 7750
Wire Notes Line
	12050 7950 11950 7950
Wire Notes Line
	12050 7950 12050 8050
Wire Notes Line
	12050 8050 11950 8050
Wire Notes Line
	11950 8050 11950 7950
Wire Notes Line
	12000 7950 12000 7900
Wire Notes Line
	12050 8150 12000 8100
Wire Notes Line
	12050 8150 12200 8150
Wire Notes Line
	12200 8150 12200 8050
Wire Notes Line
	12200 8050 12400 8050
Wire Notes Line
	11950 7850 12000 7900
Wire Notes Line
	11950 7850 11950 7800
Wire Notes Line
	11950 7800 12250 7800
Wire Notes Line
	12250 7800 12250 7850
Wire Notes Line
	12250 7850 12400 7850
Wire Notes Line
	12400 8250 11750 8250
Wire Notes Line
	11750 8250 11750 8150
Wire Notes Line
	11750 8150 11700 8100
Wire Notes Line
	11700 8100 11800 8050
Wire Notes Line
	11800 8050 11700 8000
Wire Notes Line
	11700 8000 11800 7950
Wire Notes Line
	11800 7950 11700 7900
Wire Notes Line
	11700 7900 11750 7850
Wire Notes Line
	11750 7850 11750 7750
Wire Notes Line
	11750 7750 12400 7750
Wire Wire Line
	15150 8550 14450 8550
Wire Wire Line
	15250 7850 14450 7850
Wire Wire Line
	13450 7950 13450 7400
Wire Wire Line
	13450 7400 13650 7400
Wire Wire Line
	13550 8100 13550 7850
Wire Wire Line
	13550 8100 13650 8100
Wire Wire Line
	14500 8350 14500 8250
Wire Wire Line
	14500 8350 14450 8350
Wire Wire Line
	14450 8000 14500 8000
Wire Wire Line
	14500 8000 14500 7900
Wire Wire Line
	14500 7900 13500 7900
Wire Wire Line
	13500 7900 13500 7750
Wire Wire Line
	13450 8150 13450 8450
Wire Wire Line
	13450 8450 13650 8450
Wire Wire Line
	13650 7750 13600 7750
Wire Wire Line
	13600 7750 13600 8500
Wire Wire Line
	13600 8500 13400 8500
Wire Wire Line
	13400 8500 13400 8050
Wire Wire Line
	14850 7500 14450 7500
Wire Wire Line
	14450 8200 14950 8200
Wire Wire Line
	13650 8650 11600 8650
Wire Notes Line
	12400 7950 12200 7950
Wire Notes Line
	12200 7950 12200 7850
Wire Notes Line
	12200 7850 12050 7850
Wire Notes Line
	12050 7850 12000 7900
Wire Notes Line
	12400 8150 12250 8150
Wire Notes Line
	12250 8150 12250 8200
Wire Notes Line
	12250 8200 11950 8200
Wire Notes Line
	11950 8200 11950 8150
Wire Notes Line
	11950 8150 12000 8100
Wire Notes Line
	12000 8100 12000 8050
Wire Wire Line
	13550 7850 12850 7850
Wire Wire Line
	13400 8050 12850 8050
Wire Wire Line
	14500 8250 12850 8250
Wire Wire Line
	12850 2250 14500 2250
Wire Wire Line
	12850 2050 13400 2050
Wire Wire Line
	12850 1850 13550 1850
Wire Notes Line
	12000 2050 12000 2100
Wire Notes Line
	11950 2150 12000 2100
Wire Notes Line
	11950 2150 11950 2200
Wire Notes Line
	11950 2200 12250 2200
Wire Notes Line
	12250 2200 12250 2150
Wire Notes Line
	12250 2150 12400 2150
Wire Notes Line
	12050 1850 12000 1900
Wire Notes Line
	12050 1850 12200 1850
Wire Notes Line
	12200 1850 12200 1950
Wire Notes Line
	12200 1950 12400 1950
Connection ~ 2200 4450
Wire Wire Line
	2200 4450 2200 4350
Connection ~ 1850 8700
Wire Wire Line
	1850 8650 1850 8700
Connection ~ 4250 3350
Wire Wire Line
	4250 3350 4250 2550
Connection ~ 4150 3350
Wire Wire Line
	4150 3350 4150 3450
Wire Wire Line
	2450 8750 2400 8750
Connection ~ 2350 8700
Wire Wire Line
	2350 8700 2350 8750
Wire Wire Line
	2400 8750 2400 8700
Connection ~ 2000 9200
Wire Wire Line
	2000 9150 2000 9200
Wire Wire Line
	2000 8700 2000 8750
Wire Wire Line
	1600 8700 1600 8650
Connection ~ 1950 9200
Wire Wire Line
	1950 9200 1950 9250
Wire Wire Line
	2450 9350 2100 9350
Wire Wire Line
	2100 9350 2100 9200
Wire Wire Line
	1650 9300 1650 9200
Connection ~ 11600 8250
Wire Wire Line
	11600 8250 11400 8250
Wire Wire Line
	11400 8250 11400 8300
Connection ~ 11600 6250
Wire Wire Line
	11600 6250 11400 6250
Wire Wire Line
	11400 6250 11400 6300
Connection ~ 11600 4250
Wire Wire Line
	11600 4250 11400 4250
Wire Wire Line
	11400 4250 11400 4300
Connection ~ 11600 2250
Wire Wire Line
	11600 2250 11400 2250
Wire Wire Line
	11400 2250 11400 2300
Wire Wire Line
	11050 2700 11050 2750
Wire Wire Line
	1100 6950 1850 6950
Wire Wire Line
	1100 6750 1850 6750
Wire Wire Line
	1100 6350 1850 6350
Wire Wire Line
	1100 5550 1850 5550
Wire Wire Line
	1100 6150 1850 6150
Wire Wire Line
	1100 5750 1850 5750
Wire Wire Line
	1100 7150 1850 7150
Wire Wire Line
	8800 900  8300 900 
Wire Wire Line
	1600 8100 1600 8150
Wire Wire Line
	4250 2550 3800 2550
Wire Wire Line
	4050 9950 4450 9950
Wire Wire Line
	4450 9950 4450 10700
Wire Wire Line
	4450 10700 8750 10700
Wire Wire Line
	8750 10700 8750 10400
Wire Wire Line
	8750 10400 8600 10400
Wire Wire Line
	2350 9950 2350 10500
Wire Wire Line
	2350 9950 2450 9950
Wire Wire Line
	1950 3800 1950 3350
Connection ~ 3000 3350
Wire Wire Line
	2700 3350 2850 3350
Wire Wire Line
	2850 3350 3000 3350
Wire Wire Line
	3000 3350 3200 3350
Wire Wire Line
	4650 4400 4650 4450
Wire Wire Line
	4650 4000 4650 3900
Wire Wire Line
	4650 3400 4650 3350
Wire Wire Line
	3000 3350 3000 3450
Connection ~ 3600 4450
Wire Wire Line
	3600 4450 3600 3650
Wire Wire Line
	9100 1900 8900 1900
Wire Wire Line
	9100 1700 8900 1700
Wire Wire Line
	9100 1500 8900 1500
Connection ~ 6600 8350
Wire Wire Line
	6600 9200 6600 8350
Wire Wire Line
	6600 8350 6600 7650
Wire Wire Line
	6600 7650 6850 7650
Wire Wire Line
	6850 7650 6850 7700
Wire Wire Line
	8300 2200 8300 2100
Wire Wire Line
	7250 1700 7700 1700
Wire Wire Line
	7250 1500 7700 1500
Wire Wire Line
	9050 10100 8600 10100
Wire Wire Line
	9050 9900 8600 9900
Wire Wire Line
	9050 9700 8600 9700
Wire Wire Line
	4650 10200 5400 10200
Wire Wire Line
	4650 10000 5400 10000
Wire Wire Line
	4950 10100 5400 10100
Wire Wire Line
	4650 10300 5400 10300
Wire Wire Line
	4650 9900 5400 9900
Wire Wire Line
	7200 9200 7200 8450
Wire Wire Line
	7200 8450 6500 8450
Connection ~ 7200 9200
Connection ~ 5400 9200
Wire Wire Line
	5400 9100 5400 9200
Wire Wire Line
	5400 9200 5400 9500
Wire Wire Line
	7650 8650 6500 8650
Wire Wire Line
	7350 8250 6850 8250
Wire Wire Line
	6850 8250 6500 8250
Wire Wire Line
	8900 1400 10050 1400
Wire Wire Line
	10050 1400 10050 7450
Wire Wire Line
	10050 7450 10300 7450
Wire Wire Line
	10300 7450 10300 9100
Wire Wire Line
	10300 9100 10650 9100
Wire Wire Line
	8900 1200 10250 1200
Wire Wire Line
	10250 1200 10250 5100
Wire Wire Line
	10250 5100 10650 5100
Wire Wire Line
	11050 8200 11050 8150
Wire Wire Line
	11050 6200 11050 6150
Wire Wire Line
	11050 4200 11050 4150
Wire Wire Line
	11050 2200 11050 2150
Wire Wire Line
	11050 1750 11050 1700
Connection ~ 11600 1700
Wire Wire Line
	11050 1700 11600 1700
Connection ~ 11600 900 
Wire Wire Line
	11600 900  11350 900 
Wire Wire Line
	15250 1400 15350 1400
Connection ~ 8650 9200
Wire Wire Line
	8650 9200 8650 9500
Wire Wire Line
	8650 9500 8600 9500
Wire Wire Line
	4650 9700 5400 9700
Wire Wire Line
	4050 10050 4350 10050
Wire Wire Line
	4350 10050 4350 10800
Wire Wire Line
	4350 10800 8850 10800
Wire Wire Line
	8850 10800 8850 10300
Wire Wire Line
	8850 10300 8600 10300
Wire Notes Line
	1700 10125 1750 10100
Wire Notes Line
	1750 10100 1700 10075
Wire Notes Line
	1700 10025 1750 10000
Connection ~ 1850 10100
Wire Wire Line
	1200 9800 1850 9800
Wire Wire Line
	1850 9800 1850 10100
Wire Wire Line
	1850 10100 1850 10300
Wire Wire Line
	1200 9900 2200 9900
Wire Wire Line
	2200 9900 2200 9850
Wire Wire Line
	2200 9850 2450 9850
Wire Wire Line
	1750 10700 1750 10200
Wire Wire Line
	1750 10200 1200 10200
Connection ~ 11600 6650
Connection ~ 11600 2650
Wire Wire Line
	11600 2650 13650 2650
Connection ~ 15250 1850
Wire Wire Line
	15350 1100 14950 1100
Wire Wire Line
	14950 2200 14450 2200
Wire Wire Line
	14450 1500 14850 1500
Wire Wire Line
	13400 2050 13400 2500
Wire Wire Line
	13400 2500 13600 2500
Wire Wire Line
	13600 2500 13600 1750
Wire Wire Line
	13600 1750 13650 1750
Wire Wire Line
	13650 2450 13450 2450
Wire Wire Line
	13450 2450 13450 2150
Wire Wire Line
	13500 1900 13500 1750
Wire Wire Line
	13500 1900 14500 1900
Wire Wire Line
	14500 1900 14500 2000
Wire Wire Line
	14500 2000 14450 2000
Wire Wire Line
	14450 2350 14500 2350
Wire Wire Line
	14500 2350 14500 2250
Wire Wire Line
	13650 2100 13550 2100
Wire Wire Line
	13550 2100 13550 1850
Wire Wire Line
	13650 1400 13450 1400
Wire Wire Line
	13450 1400 13450 1950
Wire Wire Line
	14450 1850 15250 1850
Wire Wire Line
	14450 2550 15150 2550
Wire Wire Line
	14850 1300 15350 1300
Wire Wire Line
	15150 1200 15350 1200
Connection ~ 15150 2550
Wire Wire Line
	11600 8650 11600 8250
Wire Wire Line
	11600 8250 11600 7700
Wire Wire Line
	11600 7700 11600 6650
Wire Wire Line
	11600 6650 11600 6250
Wire Wire Line
	11600 6250 11600 5700
Wire Wire Line
	11600 5700 11600 4650
Wire Wire Line
	11600 4650 11600 4250
Wire Wire Line
	11600 4250 11600 3700
Wire Wire Line
	11600 3700 11600 2650
Wire Wire Line
	11600 2650 11600 2250
Wire Wire Line
	11600 2250 11600 1700
Wire Wire Line
	11600 1700 11600 900 
Wire Wire Line
	11600 900  11600 750 
Connection ~ 11600 4650
Wire Wire Line
	11050 2750 11400 2750
Wire Wire Line
	11400 2750 11550 2750
Wire Wire Line
	11550 2750 13650 2750
Wire Wire Line
	1200 10000 2200 10000
Wire Wire Line
	2200 10000 2200 10050
Wire Wire Line
	2200 10050 2450 10050
Wire Wire Line
	1200 10100 1850 10100
Wire Wire Line
	1850 10300 1200 10300
Wire Notes Line
	1750 9875 1700 9900
Wire Notes Line
	1700 9900 1750 9925
Wire Wire Line
	4050 9850 4550 9850
Wire Wire Line
	4550 9850 4550 10400
Wire Wire Line
	4550 10400 5400 10400
Wire Wire Line
	4650 9600 5400 9600
Wire Wire Line
	8900 9300 8900 9200
Wire Wire Line
	8900 9200 8650 9200
Wire Wire Line
	8650 9200 7200 9200
Wire Wire Line
	7200 9200 7100 9200
Wire Wire Line
	6700 9200 6600 9200
Wire Wire Line
	6600 9200 5400 9200
Wire Wire Line
	10550 900  10950 900 
Wire Wire Line
	10650 3100 10350 3100
Wire Wire Line
	10350 3100 10350 1100
Wire Wire Line
	10350 1100 8900 1100
Wire Wire Line
	10650 7100 10150 7100
Wire Wire Line
	10150 7100 10150 1300
Wire Wire Line
	10150 1300 8900 1300
Wire Wire Line
	7650 8550 6500 8550
Connection ~ 6600 9200
Wire Wire Line
	6500 8350 6600 8350
Wire Wire Line
	4950 9800 5400 9800
Wire Wire Line
	9050 9600 8600 9600
Wire Wire Line
	9350 9800 8600 9800
Wire Wire Line
	9050 10000 8600 10000
Wire Wire Line
	8600 10200 8950 10200
Wire Wire Line
	7250 1400 7700 1400
Wire Wire Line
	7250 1600 7700 1600
Wire Wire Line
	8300 900  8300 1000
Wire Wire Line
	6850 8200 6850 8250
Connection ~ 6850 8250
Wire Wire Line
	9100 1600 8900 1600
Wire Wire Line
	9100 1800 8900 1800
Wire Wire Line
	3500 4450 3500 4550
Connection ~ 3500 4450
Wire Wire Line
	3000 4450 3000 3850
Wire Wire Line
	4150 3850 4150 4450
Connection ~ 4150 4450
Wire Wire Line
	1950 3350 2200 3350
Wire Wire Line
	2200 3350 2300 3350
Wire Wire Line
	4650 4450 4150 4450
Wire Wire Line
	4150 4450 3600 4450
Wire Wire Line
	3600 4450 3500 4450
Wire Wire Line
	3500 4450 3000 4450
Wire Wire Line
	3000 4450 2200 4450
Wire Wire Line
	2200 4450 1950 4450
Connection ~ 3000 4450
Wire Wire Line
	1950 4450 1950 4000
Wire Wire Line
	1200 10400 2250 10400
Wire Notes Line
	2050 10375 2100 10400
Wire Notes Line
	2100 10400 2050 10425
Wire Notes Line
	1750 10000 1700 9975
Wire Notes Line
	2100 10525 2050 10500
Wire Notes Line
	2050 10500 2100 10475
Wire Wire Line
	2350 10500 1200 10500
Wire Wire Line
	2450 10150 2250 10150
Wire Wire Line
	2250 10150 2250 10400
Wire Notes Line
	4150 10175 4200 10150
Wire Notes Line
	4200 10150 4150 10125
Wire Notes Line
	4150 10025 4200 10050
Wire Notes Line
	4200 10050 4150 10075
Wire Notes Line
	4200 9975 4150 9950
Wire Notes Line
	4150 9950 4200 9925
Wire Notes Line
	4200 9875 4150 9850
Wire Notes Line
	4150 9850 4200 9825
Wire Wire Line
	4050 10150 4250 10150
Wire Wire Line
	4250 10150 4250 10900
Wire Wire Line
	4250 10900 8950 10900
Wire Wire Line
	8950 10900 8950 10200
Wire Notes Line
	5250 10375 5200 10400
Wire Notes Line
	5200 10400 5250 10425
Wire Notes Line
	8550 10675 8500 10700
Wire Notes Line
	8500 10700 8550 10725
Wire Notes Line
	8500 10825 8550 10800
Wire Notes Line
	8550 10800 8500 10775
Wire Notes Line
	8500 10875 8550 10900
Wire Notes Line
	8550 10900 8500 10925
Wire Notes Line
	8750 10225 8700 10200
Wire Notes Line
	8700 10200 8750 10175
Wire Notes Line
	8650 10425 8700 10400
Wire Notes Line
	8700 10400 8650 10375
Wire Notes Line
	8750 10275 8700 10300
Wire Notes Line
	8700 10300 8750 10325
Wire Wire Line
	2850 3350 2850 2550
Wire Wire Line
	2850 2550 3400 2550
Connection ~ 2850 3350
Wire Wire Line
	9300 1000 9300 900 
Wire Wire Line
	9300 900  9200 900 
Wire Wire Line
	11400 2750 11400 2700
Connection ~ 11400 2750
Wire Wire Line
	11400 4700 11400 4750
Wire Wire Line
	11400 6700 11400 6750
Wire Wire Line
	11400 8750 11400 8700
Wire Wire Line
	11050 7750 11050 7700
Wire Wire Line
	11050 7700 11600 7700
Connection ~ 11600 7700
Wire Wire Line
	11050 3750 11050 3700
Wire Wire Line
	11050 3700 11600 3700
Connection ~ 11600 3700
Wire Wire Line
	1950 9650 2450 9650
Connection ~ 2000 8700
Wire Wire Line
	2350 9150 2450 9150
Wire Wire Line
	4650 3350 4400 3350
Wire Wire Line
	4400 3350 4250 3350
Wire Wire Line
	4250 3350 4150 3350
Wire Wire Line
	4150 3350 4000 3350
Wire Wire Line
	4400 3350 4400 3300
Connection ~ 4400 3350
Wire Wire Line
	11050 5750 11050 5700
Wire Wire Line
	11050 5700 11600 5700
Connection ~ 11600 5700
Wire Wire Line
	2200 3250 2200 3350
Connection ~ 2200 3350
Wire Notes Line
	12400 1750 11750 1750
Wire Notes Line
	11750 1750 11750 1850
Wire Notes Line
	11750 1850 11700 1900
Wire Notes Line
	11700 1900 11800 1950
Wire Notes Line
	11800 1950 11700 2000
Wire Notes Line
	11700 2000 11800 2050
Wire Notes Line
	11800 2050 11700 2100
Wire Notes Line
	11700 2100 11750 2150
Wire Notes Line
	11750 2150 11750 2250
Wire Notes Line
	11750 2250 12400 2250
Wire Notes Line
	12400 1850 12250 1850
Wire Notes Line
	12250 1850 12250 1800
Wire Notes Line
	12250 1800 11950 1800
Wire Notes Line
	11950 1800 11950 1850
Wire Notes Line
	11950 1850 12000 1900
Wire Notes Line
	12400 2050 12200 2050
Wire Notes Line
	12200 2050 12200 2150
Wire Notes Line
	12200 2150 12050 2150
Wire Notes Line
	12050 2150 12000 2100
Wire Notes Line
	12000 1900 12000 1950
Wire Notes Line
	11950 2050 11950 1950
Wire Notes Line
	11950 2050 12050 2050
Wire Notes Line
	12050 2050 12050 1950
Wire Notes Line
	12050 1950 11950 1950
Wire Wire Line
	13500 1750 12850 1750
Wire Wire Line
	13450 1950 12850 1950
Wire Wire Line
	13450 2150 12850 2150
Connection ~ 11400 4750
Wire Wire Line
	11050 8750 11400 8750
Wire Wire Line
	11400 8750 11550 8750
Wire Wire Line
	11550 8750 13650 8750
Connection ~ 11400 8750
Wire Wire Line
	11050 6700 11050 6750
Wire Wire Line
	14850 3500 14450 3500
Connection ~ 14850 3500
Wire Wire Line
	14950 4200 14450 4200
Connection ~ 14950 4200
Wire Wire Line
	14850 5500 14450 5500
Connection ~ 14850 5500
Wire Wire Line
	14450 6200 14950 6200
Connection ~ 14950 6200
Wire Wire Line
	14950 1100 14950 2200
Wire Wire Line
	14950 2200 14950 4200
Wire Wire Line
	14950 4200 14950 6200
Wire Wire Line
	14950 6200 14950 8200
Connection ~ 14950 2200
Connection ~ 1600 8700
Wire Wire Line
	1650 9200 1950 9200
Wire Wire Line
	1950 9200 2000 9200
Wire Wire Line
	2000 9200 2100 9200
Wire Wire Line
	10950 2900 10950 2850
Wire Wire Line
	10950 2850 11000 2850
Wire Wire Line
	10950 3300 10950 3350
Wire Wire Line
	10950 3350 10550 3350
Connection ~ 10550 3350
Wire Wire Line
	11050 4750 11400 4750
Wire Wire Line
	11400 4750 11550 4750
Wire Wire Line
	11550 4750 13650 4750
Wire Wire Line
	11000 4850 10950 4850
Wire Wire Line
	10950 4850 10950 4900
Wire Wire Line
	11500 6850 11550 6850
Wire Wire Line
	11550 6850 11550 6750
Connection ~ 11550 6750
Wire Wire Line
	11050 6750 11400 6750
Wire Wire Line
	11400 6750 11550 6750
Wire Wire Line
	11550 6750 13650 6750
Wire Wire Line
	10950 6900 10950 6850
Wire Wire Line
	10950 6850 11000 6850
Wire Wire Line
	11000 8850 10950 8850
Wire Wire Line
	10950 8850 10950 8900
$Comp
L CP_EU C11
U 1 1 4FEBD096
P 2000 8950
F 0 "C11" H 1850 8850 50  0000 L CNN
F 1 "10uF 10V" H 1650 9100 50  0000 L CNN
	1    2000 8950
	1    0    0    -1  
$EndComp
NoConn ~ 2750 6450
NoConn ~ 2750 6550
NoConn ~ 2750 6350
NoConn ~ 2750 6250
NoConn ~ 2750 6150
Text Label 3350 6150 2    40   ~ 0
SEL4
Text Label 3350 6250 2    40   ~ 0
SEL5
Text Label 3350 6350 2    40   ~ 0
SEL6
Text Label 3350 6450 2    40   ~ 0
SEL7
Text Label 3350 6550 2    40   ~ 0
SEL8
NoConn ~ 1850 6350
NoConn ~ 1850 6150
NoConn ~ 1850 7150
NoConn ~ 1850 6950
NoConn ~ 1850 6750
NoConn ~ 1850 6150
NoConn ~ 1850 5750
NoConn ~ 1850 5550
$Comp
L R R11
U 1 1 4FEBBDB3
P 11250 8850
F 0 "R11" V 11330 8850 50  0000 C CNN
F 1 "0R" V 11250 8850 50  0000 C CNN
	1    11250 8850
	0    -1   -1   0   
$EndComp
$Comp
L R R10
U 1 1 4FEBBDB0
P 11250 6850
F 0 "R10" V 11330 6850 50  0000 C CNN
F 1 "0R" V 11250 6850 50  0000 C CNN
	1    11250 6850
	0    -1   -1   0   
$EndComp
$Comp
L R R9
U 1 1 4FEBBDA8
P 11250 4850
F 0 "R9" V 11330 4850 50  0000 C CNN
F 1 "0R" V 11250 4850 50  0000 C CNN
	1    11250 4850
	0    -1   -1   0   
$EndComp
$Comp
L R R8
U 1 1 4FEBBD41
P 11250 2850
F 0 "R8" V 11330 2850 50  0000 C CNN
F 1 "0R" V 11250 2850 50  0000 C CNN
	1    11250 2850
	0    -1   -1   0   
$EndComp
Text Label 12900 6050 0    40   ~ 0
V_plus_2
Text Label 12900 6150 0    40   ~ 0
I_plus_2
Text Label 12900 6250 0    40   ~ 0
RL_plus_2
Text Label 12900 5750 0    40   ~ 0
RL_minus_2
Text Label 12900 5850 0    40   ~ 0
I_minus_2
Text Label 12900 5950 0    40   ~ 0
V_minus_2
Text Notes 11800 6400 0    40   ~ 0
PV Panel and\nLoad Resistor
$Comp
L CHRISJ1_RAV14202 K3
U 1 1 4FEB99A1
P 14050 6400
F 0 "K3" H 14300 6050 70  0000 C CNN
F 1 "RAV14202" H 14200 5900 70  0000 C CNN
	1    14050 6400
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_CONN_6 P8
U 1 1 4FEB99A0
P 12500 6000
F 0 "P8" V 12450 6000 60  0000 C CNN
F 1 "CHRISJ1_CONN_6" V 12550 6000 60  0000 C CNN
	1    12500 6000
	-1   0    0    -1  
$EndComp
Text Label 12900 4050 0    40   ~ 0
V_plus_1
Text Label 12900 4150 0    40   ~ 0
I_plus_1
Text Label 12900 4250 0    40   ~ 0
RL_plus_1
Text Label 12900 3750 0    40   ~ 0
RL_minus_1
Text Label 12900 3850 0    40   ~ 0
I_minus_1
Text Label 12900 3950 0    40   ~ 0
V_minus_1
Text Notes 11800 4400 0    40   ~ 0
PV Panel and\nLoad Resistor
$Comp
L CHRISJ1_RAV14202 K2
U 1 1 4FEB9997
P 14050 4400
F 0 "K2" H 14300 4050 70  0000 C CNN
F 1 "RAV14202" H 14200 3900 70  0000 C CNN
	1    14050 4400
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_CONN_6 P7
U 1 1 4FEB9996
P 12500 4000
F 0 "P7" V 12450 4000 60  0000 C CNN
F 1 "CHRISJ1_CONN_6" V 12550 4000 60  0000 C CNN
	1    12500 4000
	-1   0    0    -1  
$EndComp
$Comp
L CHRISJ1_CONN_6 P9
U 1 1 4FEB9984
P 12500 8000
F 0 "P9" V 12450 8000 60  0000 C CNN
F 1 "CHRISJ1_CONN_6" V 12550 8000 60  0000 C CNN
	1    12500 8000
	-1   0    0    -1  
$EndComp
$Comp
L CHRISJ1_RAV14202 K4
U 1 1 4FEB9983
P 14050 8400
F 0 "K4" H 14300 8050 70  0000 C CNN
F 1 "RAV14202" H 14200 7900 70  0000 C CNN
	1    14050 8400
	1    0    0    -1  
$EndComp
Text Notes 11800 8400 0    40   ~ 0
PV Panel and\nLoad Resistor
Text Label 12900 7950 0    40   ~ 0
V_minus_3
Text Label 12900 7850 0    40   ~ 0
I_minus_3
Text Label 12900 7750 0    40   ~ 0
RL_minus_3
Text Label 12900 8250 0    40   ~ 0
RL_plus_3
Text Label 12900 8150 0    40   ~ 0
I_plus_3
Text Label 12900 8050 0    40   ~ 0
V_plus_3
$Comp
L PWR_FLAG #FLG01
U 1 1 4FEAA649
P 1850 8650
F 0 "#FLG01" H 1850 8745 30  0001 C CNN
F 1 "PWR_FLAG" H 1850 8830 30  0000 C CNN
	1    1850 8650
	1    0    0    -1  
$EndComp
NoConn ~ 1200 10600
Text Label 1100 7150 0    40   ~ 0
RA2
Text Label 1100 6950 0    40   ~ 0
RA5
Text Label 1100 6750 0    40   ~ 0
RA4
Text Label 1100 6350 0    40   ~ 0
RC5
Text Label 1100 6150 0    40   ~ 0
RC4
Text Label 1100 5750 0    40   ~ 0
RC6
Text Label 1100 5550 0    40   ~ 0
RC7
Text Label 4650 10300 0    40   ~ 0
RC7
Text Label 4650 10200 0    40   ~ 0
RC6
Text Label 4650 10000 0    40   ~ 0
RC4
Text Label 4650 9900 0    40   ~ 0
RC5
Text Label 4650 9700 0    40   ~ 0
RA4
Text Label 4650 9600 0    40   ~ 0
RA5
Text Label 9350 9800 2    40   ~ 0
RA2
$Comp
L GND #PWR02
U 1 1 4FEA93BA
P 9300 1000
F 0 "#PWR02" H 9300 1000 30  0001 C CNN
F 1 "GND" H 9300 930 30  0001 C CNN
	1    9300 1000
	1    0    0    -1  
$EndComp
$Comp
L C C9
U 1 1 4FEA9399
P 9000 900
F 0 "C9" H 9050 1000 50  0000 L CNN
F 1 "100nF 16V" V 9200 700 50  0000 L CNN
	1    9000 900 
	0    -1   -1   0   
$EndComp
$Comp
L R R1
U 1 1 4FEA92CF
P 1600 8400
F 0 "R1" V 1680 8400 50  0000 C CNN
F 1 "10R" V 1600 8400 50  0000 C CNN
	1    1600 8400
	1    0    0    -1  
$EndComp
$Comp
L DIODE D2
U 1 1 4FEA91DA
P 3600 2550
F 0 "D2" H 3600 2650 40  0000 C CNN
F 1 "1N4007" H 3600 2450 40  0000 C CNN
	1    3600 2550
	-1   0    0    1   
$EndComp
Text Notes 1300 10200 0    40   ~ 0
GND
Text Notes 1300 10300 0    40   ~ 0
DSR
Text Notes 1300 9800 0    40   ~ 0
DCD
Text Label 1300 10100 0    40   ~ 0
DTR
Text Label 1300 10500 0    40   ~ 0
CTS
Text Label 1300 10400 0    40   ~ 0
RTS
Text Label 1300 9900 0    40   ~ 0
RD
Text Label 1300 10000 0    40   ~ 0
TD
$Comp
L CP_EU C10
U 1 1 4FEA7CF6
P 11150 900
F 0 "C10" H 11200 1000 50  0000 L CNN
F 1 "470uF 16V" H 11200 800 50  0000 L CNN
	1    11150 900 
	0    1    1    0   
$EndComp
$Comp
L CP_EU C3
U 1 1 4FEA7CE3
P 2350 8950
F 0 "C3" H 2200 8850 50  0000 L CNN
F 1 "1uF 10V" H 2400 8850 50  0000 L CNN
	1    2350 8950
	1    0    0    1   
$EndComp
$Comp
L CP_EU C6
U 1 1 4FEA7CD7
P 4050 9450
F 0 "C6" H 4100 9550 50  0000 L CNN
F 1 "1uF 10V" H 4100 9350 50  0000 L CNN
	1    4050 9450
	1    0    0    -1  
$EndComp
$Comp
L CP_EU C5
U 1 1 4FEA7CD3
P 4050 8950
F 0 "C5" H 4100 9050 50  0000 L CNN
F 1 "1uF 10V" H 4100 8850 50  0000 L CNN
	1    4050 8950
	1    0    0    -1  
$EndComp
$Comp
L CP_EU C2
U 1 1 4FEA7CB2
P 1950 9450
F 0 "C2" H 1800 9350 50  0000 L CNN
F 1 "1uF 10V" H 2000 9350 50  0000 L CNN
	1    1950 9450
	1    0    0    -1  
$EndComp
$Comp
L CP_EU C7
U 1 1 4FEA7CAC
P 4150 3650
F 0 "C7" H 4200 3750 50  0000 L CNN
F 1 "470uF 16V" H 4200 3550 50  0000 L CNN
	1    4150 3650
	1    0    0    -1  
$EndComp
$Comp
L CP_EU C4
U 1 1 4FEA7C9F
P 3000 3650
F 0 "C4" H 3050 3750 50  0000 L CNN
F 1 "470uF 35V" H 3050 3550 50  0000 L CNN
	1    3000 3650
	1    0    0    -1  
$EndComp
Text Notes 650  4300 0    60   ~ 0
Power input DC 9V - 25V
Text Notes 3200 3100 0    60   ~ 0
Requires heatsink
$Comp
L CHRISJ1_CONN_2 P2
U 1 1 4FEA7B1A
P 1600 3900
F 0 "P2" V 1550 3900 40  0000 C CNN
F 1 "CHRISJ1_CONN_2" V 1650 3900 40  0000 C CNN
	1    1600 3900
	-1   0    0    -1  
$EndComp
$Comp
L DIODE D1
U 1 1 4FEA70AF
P 2500 3350
F 0 "D1" H 2500 3450 40  0000 C CNN
F 1 "1N4007" H 2500 3250 40  0000 C CNN
	1    2500 3350
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 4FEA707B
P 4650 3650
F 0 "R2" V 4730 3650 50  0000 C CNN
F 1 "390R" V 4650 3650 50  0000 C CNN
	1    4650 3650
	1    0    0    -1  
$EndComp
$Comp
L LED D3
U 1 1 4FEA707A
P 4650 4200
F 0 "D3" H 4650 4300 50  0000 C CNN
F 1 "LED" H 4650 4100 50  0000 C CNN
	1    4650 4200
	0    1    1    0   
$EndComp
$Comp
L GND #PWR03
U 1 1 4FEA6F95
P 3500 4550
F 0 "#PWR03" H 3500 4550 30  0001 C CNN
F 1 "GND" H 3500 4480 30  0001 C CNN
	1    3500 4550
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR04
U 1 1 4FEA6F8B
P 4400 3300
F 0 "#PWR04" H 4400 3390 20  0001 C CNN
F 1 "+5V" H 4400 3390 30  0000 C CNN
	1    4400 3300
	1    0    0    -1  
$EndComp
$Comp
L LM7805 U2
U 1 1 4FEA6F44
P 3600 3400
F 0 "U2" H 3750 3204 60  0000 C CNN
F 1 "LM7805" H 3600 3600 60  0000 C CNN
	1    3600 3400
	1    0    0    -1  
$EndComp
Text Label 9100 1900 2    40   ~ 0
SEL8
Text Label 9100 1800 2    40   ~ 0
SEL7
Text Label 9100 1700 2    40   ~ 0
SEL6
Text Label 9100 1600 2    40   ~ 0
SEL5
Text Label 9100 1500 2    40   ~ 0
SEL4
Text Label 9500 1400 0    40   ~ 0
SEL3
Text Label 9500 1300 0    40   ~ 0
SEL2
Text Label 9500 1200 0    40   ~ 0
SEL1
Text Label 9500 1100 0    40   ~ 0
SEL0
Text Notes 8950 2050 0    40   ~ 0
Use when no relay selected
NoConn ~ 8900 2000
Text Label 4950 10100 0    40   ~ 0
CH3
Text Label 9050 10100 2    40   ~ 0
CH2
Text Label 9050 10000 2    40   ~ 0
CH1
Text Label 9050 9900 2    40   ~ 0
CH0
Text Label 7250 1700 0    40   ~ 0
CH3
Text Label 7250 1600 0    40   ~ 0
CH2
Text Label 7250 1500 0    40   ~ 0
CH1
Text Label 7250 1400 0    40   ~ 0
CH0
$Comp
L R R3
U 1 1 4FEA69AE
P 6850 7950
F 0 "R3" V 6930 7950 50  0000 C CNN
F 1 "4k7" V 6850 7950 50  0000 C CNN
	1    6850 7950
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR05
U 1 1 4FEA6935
P 8300 900
F 0 "#PWR05" H 8300 990 20  0001 C CNN
F 1 "+5V" H 8300 990 30  0000 C CNN
	1    8300 900 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 4FEA692B
P 8300 2200
F 0 "#PWR06" H 8300 2200 30  0001 C CNN
F 1 "GND" H 8300 2130 30  0001 C CNN
	1    8300 2200
	1    0    0    -1  
$EndComp
Text Label 12900 2050 0    40   ~ 0
V_plus_0
Text Label 12900 2150 0    40   ~ 0
I_plus_0
Text Label 12900 2250 0    40   ~ 0
RL_plus_0
Text Label 12900 1750 0    40   ~ 0
RL_minus_0
Text Label 12900 1850 0    40   ~ 0
I_minus_0
Text Label 12900 1950 0    40   ~ 0
V_minus_0
Text Label 15250 1450 3    40   ~ 0
V_plus
Text Label 15150 1300 2    40   ~ 0
V_minus
Text Label 15250 1200 2    40   ~ 0
I_plus
Text Label 15000 1100 0    40   ~ 0
I_minus
Text Notes 7750 2350 0    40   ~ 0
Hardware decoder prevents damage\ndue to multiple relays being active\neven in the event of software errors.
Text Notes 11800 2400 0    40   ~ 0
PV Panel and\nLoad Resistor
Text Notes 15900 1500 1    40   ~ 0
To IV curve tracer
Text Notes 700  10450 1    40   ~ 0
RS-232 from PC
Text Notes 6000 8850 1    40   ~ 0
In-Circuit Programming
Text Label 9050 9600 2    40   ~ 0
PGD
Text Label 9050 9700 2    40   ~ 0
PGC
Text Label 4950 9800 0    40   ~ 0
MCLR
NoConn ~ 6500 8750
Text Label 7650 8650 2    40   ~ 0
PGC
Text Label 7650 8550 2    40   ~ 0
PGD
Text Label 7350 8250 2    40   ~ 0
MCLR
$Comp
L CHRISJ1_CONN_6 P4
U 1 1 4FEA6269
P 6150 8500
F 0 "P4" V 6100 8500 60  0000 C CNN
F 1 "CHRISJ1_CONN_6" V 6200 8500 60  0000 C CNN
	1    6150 8500
	-1   0    0    -1  
$EndComp
$Comp
L LED D12
U 1 1 4FEA5EB0
P 11050 7950
F 0 "D12" H 11050 8050 50  0000 C CNN
F 1 "LED" H 11050 7850 50  0000 C CNN
	1    11050 7950
	0    1    1    0   
$EndComp
$Comp
L R R7
U 1 1 4FEA5EAF
P 11050 8450
F 0 "R7" V 11130 8450 50  0000 C CNN
F 1 "390R" V 11050 8450 50  0000 C CNN
	1    11050 8450
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 4FEA5EA8
P 11050 6450
F 0 "R6" V 11130 6450 50  0000 C CNN
F 1 "390R" V 11050 6450 50  0000 C CNN
	1    11050 6450
	1    0    0    -1  
$EndComp
$Comp
L LED D11
U 1 1 4FEA5EA7
P 11050 5950
F 0 "D11" H 11050 6050 50  0000 C CNN
F 1 "LED" H 11050 5850 50  0000 C CNN
	1    11050 5950
	0    1    1    0   
$EndComp
$Comp
L LED D10
U 1 1 4FEA5E9F
P 11050 3950
F 0 "D10" H 11050 4050 50  0000 C CNN
F 1 "LED" H 11050 3850 50  0000 C CNN
	1    11050 3950
	0    1    1    0   
$EndComp
$Comp
L R R5
U 1 1 4FEA5E9E
P 11050 4450
F 0 "R5" V 11130 4450 50  0000 C CNN
F 1 "390R" V 11050 4450 50  0000 C CNN
	1    11050 4450
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 4FEA5E7B
P 11050 2450
F 0 "R4" V 11130 2450 50  0000 C CNN
F 1 "390R" V 11050 2450 50  0000 C CNN
	1    11050 2450
	1    0    0    -1  
$EndComp
$Comp
L LED D9
U 1 1 4FEA5DE6
P 11050 1950
F 0 "D9" H 11050 2050 50  0000 C CNN
F 1 "LED" H 11050 1850 50  0000 C CNN
	1    11050 1950
	0    1    1    0   
$EndComp
$Comp
L CHRISJ1_CONN_4 P10
U 1 1 4FCEEF28
P 15700 1250
F 0 "P10" V 15650 1250 50  0000 C CNN
F 1 "CHRISJ1_CONN_4" V 15750 1250 50  0000 C CNN
	1    15700 1250
	1    0    0    -1  
$EndComp
$Comp
L 4028 U3
U 1 1 4FEA5D05
P 8300 1550
F 0 "U3" H 8300 1550 60  0000 C CNN
F 1 "4028" H 8250 1250 60  0000 C CNN
	1    8300 1550
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 4FEA5A33
P 6900 9200
F 0 "C8" H 6950 9300 50  0000 L CNN
F 1 "100nF 16V" H 6950 9100 50  0000 L CNN
	1    6900 9200
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR07
U 1 1 4FEA5A13
P 8900 9300
F 0 "#PWR07" H 8900 9300 30  0001 C CNN
F 1 "GND" H 8900 9230 30  0001 C CNN
	1    8900 9300
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR08
U 1 1 4FEA5A05
P 5400 9100
F 0 "#PWR08" H 5400 9190 20  0001 C CNN
F 1 "+5V" H 5400 9190 30  0000 C CNN
	1    5400 9100
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR09
U 1 1 4FEA59F9
P 1600 8100
F 0 "#PWR09" H 1600 8190 20  0001 C CNN
F 1 "+5V" H 1600 8190 30  0000 C CNN
	1    1600 8100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 4FE95704
P 1750 10700
F 0 "#PWR010" H 1750 10700 30  0001 C CNN
F 1 "GND" H 1750 10630 30  0001 C CNN
	1    1750 10700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 4FE956D1
P 1650 9300
F 0 "#PWR011" H 1650 9300 30  0001 C CNN
F 1 "GND" H 1650 9230 30  0001 C CNN
	1    1650 9300
	1    0    0    -1  
$EndComp
$Comp
L MAX232 U1
U 1 1 4FE952F5
P 3250 9450
F 0 "U1" H 3250 10300 70  0000 C CNN
F 1 "MAX232" H 3250 8600 70  0000 C CNN
	1    3250 9450
	-1   0    0    -1  
$EndComp
$Comp
L CHRISJ1_CONN_9 P1
U 1 1 4FE9527B
P 850 10200
F 0 "P1" V 800 10200 60  0000 C CNN
F 1 "CHRISJ1_CONN_9" V 900 10200 60  0000 C CNN
	1    850  10200
	-1   0    0    -1  
$EndComp
$Comp
L PIC16F690 U4
U 1 1 4FE94FF4
P 7000 9950
F 0 "U4" H 7700 10550 60  0000 C CNN
F 1 "PIC16F690" H 7500 9350 60  0000 C CNN
	1    7000 9950
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q9
U 1 1 4FE94F33
P 10850 9100
F 0 "Q9" H 10860 9270 60  0000 R CNN
F 1 "2N7000 / WM209" H 11800 9100 60  0000 R CNN
	1    10850 9100
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q8
U 1 1 4FE94F2B
P 10850 7100
F 0 "Q8" H 10860 7270 60  0000 R CNN
F 1 "2N7000 / WM209" H 11800 7100 60  0000 R CNN
	1    10850 7100
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q7
U 1 1 4FE94F22
P 10850 5100
F 0 "Q7" H 10860 5270 60  0000 R CNN
F 1 "2N7000 / WM209" H 11800 5100 60  0000 R CNN
	1    10850 5100
	1    0    0    -1  
$EndComp
$Comp
L DIODE D16
U 1 1 4FE94F13
P 11400 8500
F 0 "D16" H 11400 8600 40  0000 C CNN
F 1 "1N4007" H 11400 8400 40  0000 C CNN
	1    11400 8500
	0    -1   -1   0   
$EndComp
$Comp
L DIODE D15
U 1 1 4FE94F0B
P 11400 6500
F 0 "D15" H 11400 6600 40  0000 C CNN
F 1 "1N4007" H 11400 6400 40  0000 C CNN
	1    11400 6500
	0    -1   -1   0   
$EndComp
$Comp
L DIODE D14
U 1 1 4FE94EFE
P 11400 4500
F 0 "D14" H 11400 4600 40  0000 C CNN
F 1 "1N4007" H 11400 4400 40  0000 C CNN
	1    11400 4500
	0    -1   -1   0   
$EndComp
$Comp
L PWR_FLAG #FLG012
U 1 1 4FCEEEDA
P 2200 4350
F 0 "#FLG012" H 2200 4445 30  0001 C CNN
F 1 "PWR_FLAG" H 2200 4530 30  0000 C CNN
	1    2200 4350
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG013
U 1 1 4FCEEED6
P 2200 3250
F 0 "#FLG013" H 2200 3345 30  0001 C CNN
F 1 "PWR_FLAG" H 2200 3430 30  0000 C CNN
	1    2200 3250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 4FCEE975
P 10550 9500
F 0 "#PWR014" H 10550 9500 30  0001 C CNN
F 1 "GND" H 10550 9430 30  0001 C CNN
	1    10550 9500
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q6
U 1 1 4FCEE957
P 10850 3100
F 0 "Q6" H 10860 3270 60  0000 R CNN
F 1 "2N7000 / WM209" H 11800 3100 60  0000 R CNN
	1    10850 3100
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR015
U 1 1 4FCEE911
P 11600 750
F 0 "#PWR015" H 11600 840 20  0001 C CNN
F 1 "+5V" H 11600 840 30  0000 C CNN
	1    11600 750 
	1    0    0    -1  
$EndComp
$Comp
L DIODE D13
U 1 1 4FCEE8F0
P 11400 2500
F 0 "D13" H 11400 2600 40  0000 C CNN
F 1 "1N4007" H 11400 2400 40  0000 C CNN
	1    11400 2500
	0    -1   -1   0   
$EndComp
$Comp
L CHRISJ1_RAV14202 K1
U 1 1 4FCEE789
P 14050 2400
F 0 "K1" H 14300 2050 70  0000 C CNN
F 1 "RAV14202" H 14200 1900 70  0000 C CNN
	1    14050 2400
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_CONN_6 P6
U 1 1 4FCEE772
P 12500 2000
F 0 "P6" V 12450 2000 60  0000 C CNN
F 1 "CHRISJ1_CONN_6" V 12550 2000 60  0000 C CNN
	1    12500 2000
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
