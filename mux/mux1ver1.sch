EESchema Schematic File Version 2  date 28/06/2012 9:20:21 AM
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:chrisj1
LIBS:diy_connectors
LIBS:diy_diodes
LIBS:diy_ics
LIBS:diy_rcl
LIBS:diy_regulators
LIBS:diy_switches
LIBS:diy_transformers
LIBS:diy_transistors
LIBS:diy_triac
LIBS:diy_tubes
LIBS:mux1-cache
EELAYER 25  0
EELAYER END
$Descr A3 16535 11700
encoding utf-8
Sheet 1 1
Title "Multiplexer for IV curve tracer and low power PV panels"
Date "27 jun 2012"
Rev ""
Comp "CSES, CECS, ANU"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 2200 4450
Wire Wire Line
	2200 4350 2200 4450
Connection ~ 1850 8700
Wire Wire Line
	1850 8700 1850 8650
Connection ~ 4250 3350
Wire Wire Line
	4250 3350 4250 2550
Connection ~ 4150 3350
Wire Wire Line
	4150 3450 4150 3350
Wire Wire Line
	2450 8750 2400 8750
Connection ~ 2350 8700
Wire Wire Line
	2350 8750 2350 8700
Wire Wire Line
	2400 8750 2400 8700
Connection ~ 2000 9200
Wire Wire Line
	2000 9150 2000 9200
Wire Wire Line
	2000 8700 2000 8750
Wire Wire Line
	1600 8650 1600 8700
Connection ~ 1950 9200
Wire Wire Line
	1950 9250 1950 9200
Wire Wire Line
	2450 9350 2100 9350
Wire Wire Line
	2100 9350 2100 9200
Wire Wire Line
	2100 9200 1650 9200
Wire Wire Line
	1650 9200 1650 9300
Connection ~ 15150 6800
Wire Wire Line
	14450 6800 15150 6800
Connection ~ 14950 6450
Wire Wire Line
	14950 6450 14450 6450
Connection ~ 15150 4800
Wire Wire Line
	15150 4800 14450 4800
Connection ~ 14850 3750
Wire Wire Line
	14850 3750 14450 3750
Connection ~ 11600 8500
Wire Wire Line
	11600 8500 11400 8500
Wire Wire Line
	11400 8500 11400 8550
Connection ~ 11600 6500
Wire Wire Line
	11600 6500 11400 6500
Wire Wire Line
	11400 6500 11400 6550
Connection ~ 11600 4500
Wire Wire Line
	11600 4500 11400 4500
Wire Wire Line
	11400 4500 11400 4550
Connection ~ 11600 2500
Wire Wire Line
	11600 2500 11400 2500
Wire Wire Line
	11400 2500 11400 2550
Connection ~ 10750 3450
Wire Wire Line
	10750 3450 10600 3450
Wire Wire Line
	10600 3450 10600 3400
Connection ~ 10750 7450
Wire Wire Line
	10750 7450 10600 7450
Connection ~ 10750 9450
Wire Wire Line
	10750 9450 10600 9450
Wire Wire Line
	10600 9450 10600 9400
Connection ~ 11050 9000
Wire Wire Line
	11050 9000 11050 8950
Connection ~ 11050 3000
Wire Wire Line
	11050 3000 11050 2950
Connection ~ 4700 6650
Wire Wire Line
	4700 6650 4900 6650
Connection ~ 4700 7050
Wire Wire Line
	4700 7050 4900 7050
Connection ~ 4700 7450
Wire Wire Line
	4700 7450 4900 7450
Connection ~ 4700 7850
Wire Wire Line
	4700 7850 4900 7850
Wire Wire Line
	4700 8300 4700 6250
Wire Wire Line
	4700 6250 4900 6250
Wire Wire Line
	4900 7750 4150 7750
Wire Wire Line
	4150 6150 4900 6150
Wire Wire Line
	4150 6350 4900 6350
Wire Wire Line
	4150 6550 4900 6550
Wire Wire Line
	4150 7150 4900 7150
Wire Wire Line
	4150 6750 4900 6750
Wire Wire Line
	4150 6950 4900 6950
Wire Wire Line
	4150 7350 4900 7350
Wire Wire Line
	7500 1150 7000 1150
Wire Wire Line
	1600 8100 1600 8150
Wire Wire Line
	4250 2550 3800 2550
Wire Wire Line
	4050 9950 4750 9950
Wire Wire Line
	4750 9950 4750 10700
Wire Wire Line
	4750 10700 9500 10700
Wire Wire Line
	9500 10700 9500 10400
Wire Wire Line
	9500 10400 9350 10400
Wire Wire Line
	2350 9950 2350 10500
Wire Wire Line
	2350 9950 2450 9950
Wire Wire Line
	1950 3800 1950 3350
Connection ~ 3000 3350
Wire Wire Line
	3200 3350 2700 3350
Wire Wire Line
	4650 4400 4650 4450
Wire Wire Line
	4650 4000 4650 3900
Wire Wire Line
	4650 3400 4650 3350
Wire Wire Line
	3000 3450 3000 3350
Connection ~ 3600 4450
Wire Wire Line
	3600 4450 3600 3650
Wire Wire Line
	7950 5000 8400 5000
Wire Wire Line
	7950 4800 8400 4800
Wire Wire Line
	7950 4600 8400 4600
Wire Wire Line
	6450 6350 6850 6350
Wire Wire Line
	7150 6150 7150 6050
Wire Wire Line
	7300 6200 7300 6050
Wire Wire Line
	7150 6650 7150 6550
Wire Wire Line
	7300 6400 7300 6600
Wire Wire Line
	7300 6600 7150 6600
Connection ~ 7150 6600
Wire Wire Line
	7150 6050 7650 6050
Connection ~ 7300 6050
Wire Wire Line
	6450 4750 6850 4750
Wire Wire Line
	7150 4550 7150 4450
Wire Wire Line
	7300 4600 7300 4450
Wire Wire Line
	7150 5050 7150 4950
Wire Wire Line
	7300 4800 7300 5000
Wire Wire Line
	7300 5000 7150 5000
Connection ~ 7150 5000
Wire Wire Line
	7150 4450 7650 4450
Connection ~ 7300 4450
Connection ~ 7300 5250
Wire Wire Line
	7650 5250 7150 5250
Connection ~ 7150 5800
Wire Wire Line
	7150 5800 7300 5800
Wire Wire Line
	7300 5800 7300 5600
Wire Wire Line
	7150 5850 7150 5750
Wire Wire Line
	7300 5400 7300 5250
Wire Wire Line
	7150 5250 7150 5350
Wire Wire Line
	6450 5550 6850 5550
Wire Wire Line
	6450 3950 6850 3950
Wire Wire Line
	7150 3750 7150 3650
Wire Wire Line
	7300 3800 7300 3650
Wire Wire Line
	7150 4250 7150 4150
Wire Wire Line
	7300 4000 7300 4200
Wire Wire Line
	7300 4200 7150 4200
Connection ~ 7150 4200
Wire Wire Line
	7150 3650 7650 3650
Connection ~ 7300 3650
Connection ~ 7300 2850
Wire Wire Line
	7650 2850 7150 2850
Wire Wire Line
	8200 2150 7600 2150
Wire Wire Line
	8200 1950 7600 1950
Wire Wire Line
	8200 1750 7600 1750
Connection ~ 7150 3400
Wire Wire Line
	7150 3400 7300 3400
Wire Wire Line
	7300 3400 7300 3200
Connection ~ 7350 8350
Wire Wire Line
	7350 9200 7350 7650
Wire Wire Line
	7350 7650 7600 7650
Wire Wire Line
	7600 7650 7600 7700
Wire Wire Line
	7000 2450 7000 2350
Wire Wire Line
	13650 8900 11600 8900
Wire Wire Line
	12850 6000 13550 6000
Wire Wire Line
	13550 6000 13550 5650
Wire Wire Line
	13550 5650 13650 5650
Wire Wire Line
	12850 6100 13550 6100
Wire Wire Line
	13550 6100 13550 6350
Wire Wire Line
	13550 6350 13650 6350
Wire Wire Line
	12850 6300 13500 6300
Wire Wire Line
	13500 6300 13500 6500
Wire Wire Line
	13500 6500 14500 6500
Wire Wire Line
	13500 6150 14500 6150
Wire Wire Line
	13500 6150 13500 6200
Wire Wire Line
	13500 6200 12850 6200
Wire Wire Line
	12850 6400 13450 6400
Wire Wire Line
	13450 6400 13450 6700
Wire Wire Line
	13450 6700 13650 6700
Wire Wire Line
	13650 6000 13600 6000
Wire Wire Line
	13600 6000 13600 6750
Wire Wire Line
	13600 6750 13400 6750
Wire Wire Line
	13400 6750 13400 6500
Wire Wire Line
	13400 6500 12850 6500
Wire Wire Line
	11600 6900 13650 6900
Wire Wire Line
	12850 8500 13400 8500
Wire Wire Line
	13400 8500 13400 8750
Wire Wire Line
	13400 8750 13600 8750
Wire Wire Line
	13600 8750 13600 8000
Wire Wire Line
	13600 8000 13650 8000
Wire Wire Line
	13650 8700 13450 8700
Wire Wire Line
	13450 8700 13450 8400
Wire Wire Line
	13450 8400 12850 8400
Wire Wire Line
	12850 8200 13500 8200
Wire Wire Line
	13500 8200 13500 8150
Wire Wire Line
	13500 8150 14500 8150
Wire Wire Line
	13500 8500 14500 8500
Wire Wire Line
	13500 8500 13500 8300
Wire Wire Line
	13500 8300 12850 8300
Wire Wire Line
	13650 8350 13550 8350
Wire Wire Line
	13550 8350 13550 8100
Wire Wire Line
	13550 8100 12850 8100
Wire Wire Line
	13650 7650 13550 7650
Wire Wire Line
	13550 7650 13550 8000
Wire Wire Line
	13550 8000 12850 8000
Wire Wire Line
	12850 4000 13550 4000
Wire Wire Line
	13550 4000 13550 3650
Wire Wire Line
	13550 3650 13650 3650
Wire Wire Line
	12850 4100 13550 4100
Wire Wire Line
	13550 4100 13550 4350
Wire Wire Line
	13550 4350 13650 4350
Wire Wire Line
	12850 4300 13500 4300
Wire Wire Line
	13500 4300 13500 4500
Wire Wire Line
	13500 4500 14500 4500
Wire Wire Line
	13500 4150 14500 4150
Wire Wire Line
	13500 4150 13500 4200
Wire Wire Line
	13500 4200 12850 4200
Wire Wire Line
	12850 4400 13450 4400
Wire Wire Line
	13450 4400 13450 4700
Wire Wire Line
	13450 4700 13650 4700
Wire Wire Line
	13650 4000 13600 4000
Wire Wire Line
	13600 4000 13600 4750
Wire Wire Line
	13600 4750 13400 4750
Wire Wire Line
	13400 4750 13400 4500
Wire Wire Line
	13400 4500 12850 4500
Wire Wire Line
	5950 1950 6400 1950
Wire Wire Line
	5950 1750 6400 1750
Wire Wire Line
	9800 10100 9350 10100
Wire Wire Line
	9800 9900 9350 9900
Wire Wire Line
	9800 9700 9350 9700
Wire Wire Line
	5400 10200 6150 10200
Wire Wire Line
	5400 10000 6150 10000
Wire Wire Line
	5700 10100 6150 10100
Wire Wire Line
	5400 10300 6150 10300
Wire Wire Line
	5400 9900 6150 9900
Wire Wire Line
	7950 9200 7950 8450
Wire Wire Line
	7950 8450 7250 8450
Connection ~ 7950 9200
Connection ~ 6150 9200
Wire Wire Line
	6150 9500 6150 9100
Wire Wire Line
	8400 8650 7250 8650
Wire Wire Line
	7250 8250 8100 8250
Wire Wire Line
	7600 1650 9800 1650
Wire Wire Line
	9800 1650 9800 7700
Wire Wire Line
	9800 7700 10200 7700
Wire Wire Line
	10200 7700 10200 9200
Wire Wire Line
	10200 9200 10300 9200
Wire Wire Line
	7600 1450 10000 1450
Wire Wire Line
	10000 1450 10000 5200
Wire Wire Line
	10000 5200 10300 5200
Wire Wire Line
	11050 8550 11050 8500
Wire Wire Line
	11050 6550 11050 6500
Wire Wire Line
	11050 4550 11050 4500
Wire Wire Line
	11050 2550 11050 2500
Wire Wire Line
	11050 2000 11050 1950
Connection ~ 11600 1950
Wire Wire Line
	11050 1950 11600 1950
Connection ~ 11600 1150
Wire Wire Line
	11600 1150 11350 1150
Wire Wire Line
	15300 1650 15250 1650
Connection ~ 9400 9200
Wire Wire Line
	9400 9200 9400 9500
Wire Wire Line
	9400 9500 9350 9500
Wire Wire Line
	5400 9700 6150 9700
Wire Wire Line
	4050 10050 4650 10050
Wire Wire Line
	4650 10050 4650 10800
Wire Wire Line
	4650 10800 9600 10800
Wire Wire Line
	9600 10800 9600 10300
Wire Wire Line
	9600 10300 9350 10300
Wire Notes Line
	1700 10125 1750 10100
Wire Notes Line
	1750 10100 1700 10075
Wire Notes Line
	1700 10025 1750 10000
Connection ~ 1850 10100
Wire Wire Line
	1200 9800 1850 9800
Wire Wire Line
	1850 9800 1850 10300
Wire Wire Line
	1200 9900 2200 9900
Wire Wire Line
	2200 9900 2200 9850
Wire Wire Line
	2200 9850 2450 9850
Wire Wire Line
	1750 10700 1750 10200
Wire Wire Line
	1750 10200 1200 10200
Connection ~ 11600 6900
Connection ~ 11600 2900
Wire Wire Line
	11600 2900 13650 2900
Wire Notes Line
	11750 6350 11750 6400
Wire Notes Line
	12400 6500 12250 6500
Wire Notes Line
	12250 6500 12250 6550
Wire Notes Line
	12250 6550 11700 6550
Wire Notes Line
	11700 6550 11700 6450
Wire Notes Line
	11700 6450 11750 6400
Wire Notes Line
	11750 6400 11800 6450
Wire Notes Line
	11800 6450 11800 6500
Wire Notes Line
	11800 6500 12200 6500
Wire Notes Line
	12200 6500 12200 6400
Wire Notes Line
	12200 6400 12400 6400
Wire Notes Line
	12100 6450 12100 6300
Wire Notes Line
	12100 6450 11950 6450
Wire Notes Line
	12100 6050 11950 6050
Wire Notes Line
	12100 6050 12100 6200
Wire Notes Line
	12100 6200 12400 6200
Wire Notes Line
	11950 6050 11950 6100
Wire Notes Line
	11950 6100 11900 6150
Wire Notes Line
	11900 6150 12000 6200
Wire Notes Line
	12000 6200 11900 6250
Wire Notes Line
	11900 6250 12000 6300
Wire Notes Line
	12000 6300 11900 6350
Wire Notes Line
	11900 6350 11950 6400
Wire Notes Line
	11950 6400 11950 6450
Wire Notes Line
	12100 6300 12400 6300
Wire Notes Line
	12400 6100 12200 6100
Wire Notes Line
	12200 6100 12200 6000
Wire Notes Line
	12200 6000 11800 6000
Wire Notes Line
	11800 6000 11800 6050
Wire Notes Line
	11800 6050 11750 6100
Wire Notes Line
	11700 6050 11750 6100
Wire Notes Line
	11700 6050 11700 5950
Wire Notes Line
	11700 5950 12250 5950
Wire Notes Line
	12250 5950 12250 6000
Wire Notes Line
	12250 6000 12400 6000
Wire Notes Line
	11750 6100 11750 6150
Wire Notes Line
	11700 6150 11800 6150
Wire Notes Line
	11700 6150 11700 6350
Wire Notes Line
	11700 6350 11800 6350
Wire Notes Line
	11800 6350 11800 6150
Wire Notes Line
	11800 8350 11800 8150
Wire Notes Line
	11800 8350 11700 8350
Wire Notes Line
	11700 8350 11700 8150
Wire Notes Line
	11700 8150 11800 8150
Wire Notes Line
	11750 8150 11750 8100
Wire Notes Line
	12400 8000 12250 8000
Wire Notes Line
	12250 8000 12250 7950
Wire Notes Line
	12250 7950 11700 7950
Wire Notes Line
	11700 7950 11700 8050
Wire Notes Line
	11700 8050 11750 8100
Wire Notes Line
	11750 8100 11800 8050
Wire Notes Line
	11800 8050 11800 8000
Wire Notes Line
	11800 8000 12200 8000
Wire Notes Line
	12200 8000 12200 8100
Wire Notes Line
	12200 8100 12400 8100
Wire Notes Line
	12400 8300 12100 8300
Wire Notes Line
	11950 8400 11950 8450
Wire Notes Line
	11950 8400 11900 8350
Wire Notes Line
	11900 8350 12000 8300
Wire Notes Line
	12000 8300 11900 8250
Wire Notes Line
	11900 8250 12000 8200
Wire Notes Line
	12000 8200 11900 8150
Wire Notes Line
	11900 8150 11950 8100
Wire Notes Line
	11950 8100 11950 8050
Wire Notes Line
	12400 8200 12100 8200
Wire Notes Line
	12100 8200 12100 8050
Wire Notes Line
	12100 8050 11950 8050
Wire Notes Line
	11950 8450 12100 8450
Wire Notes Line
	12100 8450 12100 8300
Wire Notes Line
	12400 8400 12200 8400
Wire Notes Line
	12200 8400 12200 8500
Wire Notes Line
	12200 8500 11800 8500
Wire Notes Line
	11800 8500 11800 8450
Wire Notes Line
	11800 8450 11750 8400
Wire Notes Line
	11700 8450 11750 8400
Wire Notes Line
	11700 8450 11700 8550
Wire Notes Line
	11700 8550 12250 8550
Wire Notes Line
	12250 8550 12250 8500
Wire Notes Line
	12250 8500 12400 8500
Wire Notes Line
	11750 8400 11750 8350
Wire Notes Line
	11750 4350 11750 4400
Wire Notes Line
	12400 4500 12250 4500
Wire Notes Line
	12250 4500 12250 4550
Wire Notes Line
	12250 4550 11700 4550
Wire Notes Line
	11700 4550 11700 4450
Wire Notes Line
	11700 4450 11750 4400
Wire Notes Line
	11750 4400 11800 4450
Wire Notes Line
	11800 4450 11800 4500
Wire Notes Line
	11800 4500 12200 4500
Wire Notes Line
	12200 4500 12200 4400
Wire Notes Line
	12200 4400 12400 4400
Wire Notes Line
	12100 4450 12100 4300
Wire Notes Line
	12100 4450 11950 4450
Wire Notes Line
	12100 4050 11950 4050
Wire Notes Line
	12100 4050 12100 4200
Wire Notes Line
	12100 4200 12400 4200
Wire Notes Line
	11950 4050 11950 4100
Wire Notes Line
	11950 4100 11900 4150
Wire Notes Line
	11900 4150 12000 4200
Wire Notes Line
	12000 4200 11900 4250
Wire Notes Line
	11900 4250 12000 4300
Wire Notes Line
	12000 4300 11900 4350
Wire Notes Line
	11900 4350 11950 4400
Wire Notes Line
	11950 4400 11950 4450
Wire Notes Line
	12100 4300 12400 4300
Wire Notes Line
	12400 4100 12200 4100
Wire Notes Line
	12200 4100 12200 4000
Wire Notes Line
	12200 4000 11800 4000
Wire Notes Line
	11800 4000 11800 4050
Wire Notes Line
	11800 4050 11750 4100
Wire Notes Line
	11700 4050 11750 4100
Wire Notes Line
	11700 4050 11700 3950
Wire Notes Line
	11700 3950 12250 3950
Wire Notes Line
	12250 3950 12250 4000
Wire Notes Line
	12250 4000 12400 4000
Wire Notes Line
	11750 4100 11750 4150
Wire Notes Line
	11700 4150 11800 4150
Wire Notes Line
	11700 4150 11700 4350
Wire Notes Line
	11700 4350 11800 4350
Wire Notes Line
	11800 4350 11800 4150
Wire Notes Line
	11800 2350 11800 2150
Wire Notes Line
	11800 2350 11700 2350
Wire Notes Line
	11700 2350 11700 2150
Wire Notes Line
	11700 2150 11800 2150
Wire Notes Line
	11750 2150 11750 2100
Wire Notes Line
	12400 2000 12250 2000
Wire Notes Line
	12250 2000 12250 1950
Wire Notes Line
	12250 1950 11700 1950
Wire Notes Line
	11700 1950 11700 2050
Wire Notes Line
	11700 2050 11750 2100
Wire Notes Line
	11750 2100 11800 2050
Wire Notes Line
	11800 2050 11800 2000
Wire Notes Line
	11800 2000 12200 2000
Wire Notes Line
	12200 2000 12200 2100
Wire Notes Line
	12200 2100 12400 2100
Wire Notes Line
	12400 2300 12100 2300
Wire Notes Line
	11950 2400 11950 2450
Wire Notes Line
	11950 2400 11900 2350
Wire Notes Line
	11900 2350 12000 2300
Wire Notes Line
	12000 2300 11900 2250
Wire Notes Line
	11900 2250 12000 2200
Wire Notes Line
	12000 2200 11900 2150
Wire Notes Line
	11900 2150 11950 2100
Wire Notes Line
	11950 2100 11950 2050
Connection ~ 15250 2100
Wire Wire Line
	15250 1650 15250 8100
Connection ~ 14950 2450
Wire Wire Line
	14950 8450 14950 1450
Wire Wire Line
	14450 8800 15150 8800
Wire Wire Line
	15250 8100 14450 8100
Wire Wire Line
	14500 8500 14500 8600
Wire Wire Line
	14500 8600 14450 8600
Wire Wire Line
	14450 8250 14500 8250
Wire Wire Line
	14500 8250 14500 8150
Wire Wire Line
	14450 7750 14850 7750
Wire Wire Line
	14950 8450 14450 8450
Wire Wire Line
	14500 6150 14500 6250
Wire Wire Line
	14500 6250 14450 6250
Wire Wire Line
	14450 6600 14500 6600
Wire Wire Line
	14500 6600 14500 6500
Wire Wire Line
	14500 4500 14500 4600
Wire Wire Line
	14500 4600 14450 4600
Wire Wire Line
	14450 4250 14500 4250
Wire Wire Line
	14500 4250 14500 4150
Wire Wire Line
	14950 1450 15300 1450
Wire Wire Line
	14450 2450 14950 2450
Wire Wire Line
	14850 1750 14450 1750
Wire Wire Line
	12850 2500 13400 2500
Wire Wire Line
	13400 2500 13400 2750
Wire Wire Line
	13400 2750 13600 2750
Wire Wire Line
	13600 2750 13600 2000
Wire Wire Line
	13600 2000 13650 2000
Wire Wire Line
	13650 2700 13450 2700
Wire Wire Line
	13450 2700 13450 2400
Wire Wire Line
	13450 2400 12850 2400
Wire Wire Line
	12850 2200 13500 2200
Wire Wire Line
	13500 2200 13500 2150
Wire Wire Line
	13500 2150 14500 2150
Wire Wire Line
	14500 2150 14500 2250
Wire Wire Line
	14500 2250 14450 2250
Wire Wire Line
	14450 2600 14500 2600
Wire Wire Line
	14500 2600 14500 2500
Wire Wire Line
	14500 2500 13500 2500
Wire Wire Line
	13500 2500 13500 2300
Wire Wire Line
	13500 2300 12850 2300
Wire Wire Line
	13650 2350 13550 2350
Wire Wire Line
	13550 2350 13550 2100
Wire Wire Line
	13550 2100 12850 2100
Wire Wire Line
	13650 1650 13550 1650
Wire Wire Line
	13550 1650 13550 2000
Wire Wire Line
	13550 2000 12850 2000
Wire Wire Line
	15250 2100 14450 2100
Wire Wire Line
	15150 2800 14450 2800
Wire Wire Line
	15300 1350 14850 1350
Wire Wire Line
	15300 1550 15150 1550
Wire Wire Line
	14850 1350 14850 7750
Connection ~ 14850 1750
Wire Wire Line
	15150 1550 15150 8800
Connection ~ 15150 2800
Wire Notes Line
	12400 2200 12100 2200
Wire Notes Line
	12100 2200 12100 2050
Wire Notes Line
	12100 2050 11950 2050
Wire Notes Line
	11950 2450 12100 2450
Wire Notes Line
	12100 2450 12100 2300
Wire Notes Line
	12400 2400 12200 2400
Wire Notes Line
	12200 2400 12200 2500
Wire Notes Line
	12200 2500 11800 2500
Wire Notes Line
	11800 2500 11800 2450
Wire Notes Line
	11800 2450 11750 2400
Wire Notes Line
	11700 2450 11750 2400
Wire Notes Line
	11700 2450 11700 2550
Wire Notes Line
	11700 2550 12250 2550
Wire Notes Line
	12250 2550 12250 2500
Wire Notes Line
	12250 2500 12400 2500
Wire Notes Line
	11750 2400 11750 2350
Wire Wire Line
	11600 8900 11600 1000
Wire Wire Line
	11600 4900 13650 4900
Connection ~ 11600 4900
Wire Wire Line
	10750 9500 10750 1150
Wire Wire Line
	10600 3000 13650 3000
Wire Wire Line
	10600 5000 13650 5000
Wire Wire Line
	10600 7000 13650 7000
Wire Wire Line
	10600 9000 13650 9000
Wire Wire Line
	1200 10000 2200 10000
Wire Wire Line
	2200 10000 2200 10050
Wire Wire Line
	2200 10050 2450 10050
Wire Wire Line
	1200 10100 1850 10100
Wire Wire Line
	1850 10300 1200 10300
Wire Notes Line
	1750 9875 1700 9900
Wire Notes Line
	1700 9900 1750 9925
Wire Wire Line
	4050 9850 4850 9850
Wire Wire Line
	4850 9850 4850 10400
Wire Wire Line
	4850 10400 6150 10400
Wire Wire Line
	5400 9600 6150 9600
Wire Wire Line
	9650 9300 9650 9200
Wire Wire Line
	9650 9200 7850 9200
Wire Wire Line
	7450 9200 6150 9200
Wire Wire Line
	10750 1150 10950 1150
Connection ~ 11050 2000
Wire Wire Line
	10300 3200 10100 3200
Wire Wire Line
	10100 3200 10100 1350
Wire Wire Line
	10100 1350 7600 1350
Wire Wire Line
	10300 7200 9900 7200
Wire Wire Line
	9900 7200 9900 1550
Wire Wire Line
	9900 1550 7600 1550
Wire Wire Line
	8400 8550 7250 8550
Connection ~ 7350 9200
Wire Wire Line
	7250 8350 7350 8350
Wire Wire Line
	5700 9800 6150 9800
Wire Wire Line
	9800 9600 9350 9600
Wire Wire Line
	10100 9800 9350 9800
Wire Wire Line
	9800 10000 9350 10000
Wire Wire Line
	9350 10200 9700 10200
Wire Wire Line
	5950 1650 6400 1650
Wire Wire Line
	5950 1850 6400 1850
Wire Wire Line
	7000 1150 7000 1250
Wire Wire Line
	7600 8200 7600 8250
Connection ~ 7600 8250
Wire Wire Line
	7150 3450 7150 3350
Wire Wire Line
	7300 3000 7300 2850
Wire Wire Line
	7150 2850 7150 2950
Wire Wire Line
	8200 1850 7600 1850
Wire Wire Line
	8200 2050 7600 2050
Wire Wire Line
	6450 3150 6850 3150
Wire Wire Line
	8400 4500 8300 4500
Wire Wire Line
	8300 4500 8300 5200
Wire Wire Line
	7950 4700 8400 4700
Wire Wire Line
	7950 4900 8400 4900
Wire Wire Line
	3500 4450 3500 4550
Connection ~ 3500 4450
Wire Wire Line
	3000 4450 3000 3850
Wire Wire Line
	4150 3850 4150 4450
Connection ~ 4150 4450
Wire Wire Line
	1950 3350 2300 3350
Wire Wire Line
	4650 4450 1950 4450
Connection ~ 3000 4450
Wire Wire Line
	1950 4450 1950 4000
Wire Wire Line
	1200 10400 2250 10400
Wire Notes Line
	2050 10375 2100 10400
Wire Notes Line
	2100 10400 2050 10425
Wire Notes Line
	1750 10000 1700 9975
Wire Notes Line
	2100 10525 2050 10500
Wire Notes Line
	2050 10500 2100 10475
Wire Wire Line
	2350 10500 1200 10500
Wire Wire Line
	2450 10150 2250 10150
Wire Wire Line
	2250 10150 2250 10400
Wire Notes Line
	4150 10175 4200 10150
Wire Notes Line
	4200 10150 4150 10125
Wire Notes Line
	4150 10025 4200 10050
Wire Notes Line
	4200 10050 4150 10075
Wire Notes Line
	4200 9975 4150 9950
Wire Notes Line
	4150 9950 4200 9925
Wire Notes Line
	4200 9875 4150 9850
Wire Notes Line
	4150 9850 4200 9825
Wire Wire Line
	4050 10150 4550 10150
Wire Wire Line
	4550 10150 4550 10900
Wire Wire Line
	4550 10900 9700 10900
Wire Wire Line
	9700 10900 9700 10200
Wire Notes Line
	6000 10375 5950 10400
Wire Notes Line
	5950 10400 6000 10425
Wire Notes Line
	9300 10675 9250 10700
Wire Notes Line
	9250 10700 9300 10725
Wire Notes Line
	9250 10825 9300 10800
Wire Notes Line
	9300 10800 9250 10775
Wire Notes Line
	9250 10875 9300 10900
Wire Notes Line
	9300 10900 9250 10925
Wire Notes Line
	9500 10225 9450 10200
Wire Notes Line
	9450 10200 9500 10175
Wire Notes Line
	9400 10425 9450 10400
Wire Notes Line
	9450 10400 9400 10375
Wire Notes Line
	9500 10275 9450 10300
Wire Notes Line
	9450 10300 9500 10325
Wire Wire Line
	2850 3350 2850 2550
Wire Wire Line
	2850 2550 3400 2550
Connection ~ 2850 3350
Wire Wire Line
	8000 1250 8000 1150
Wire Wire Line
	8000 1150 7900 1150
Wire Wire Line
	4900 7550 4150 7550
Wire Wire Line
	4900 7950 4150 7950
Wire Wire Line
	4700 8050 4900 8050
Connection ~ 4700 8050
Wire Wire Line
	4700 7650 4900 7650
Connection ~ 4700 7650
Wire Wire Line
	4700 7250 4900 7250
Connection ~ 4700 7250
Wire Wire Line
	4700 6850 4900 6850
Connection ~ 4700 6850
Wire Wire Line
	4900 6450 4700 6450
Connection ~ 4700 6450
Wire Wire Line
	11050 4950 11050 5000
Connection ~ 11050 5000
Wire Wire Line
	11050 6950 11050 7000
Connection ~ 11050 7000
Wire Wire Line
	10600 7450 10600 7400
Wire Wire Line
	10600 5400 10600 5450
Wire Wire Line
	10600 5450 10750 5450
Connection ~ 10750 5450
Wire Wire Line
	11400 3000 11400 2950
Connection ~ 11400 3000
Wire Wire Line
	11400 5000 11400 4950
Connection ~ 11400 5000
Wire Wire Line
	11400 7000 11400 6950
Connection ~ 11400 7000
Wire Wire Line
	11400 9000 11400 8950
Connection ~ 11400 9000
Wire Wire Line
	11050 8000 11050 7950
Wire Wire Line
	11050 7950 11600 7950
Connection ~ 11600 7950
Wire Wire Line
	11050 4000 11050 3950
Wire Wire Line
	11050 3950 11600 3950
Connection ~ 11600 3950
Wire Wire Line
	15250 4100 14450 4100
Connection ~ 15250 4100
Wire Wire Line
	14950 4450 14450 4450
Connection ~ 14950 4450
Wire Wire Line
	14450 5750 14850 5750
Connection ~ 14850 5750
Wire Wire Line
	14450 6100 15250 6100
Connection ~ 15250 6100
Wire Wire Line
	1950 9650 2450 9650
Connection ~ 2000 8700
Wire Wire Line
	1600 8700 2400 8700
Wire Wire Line
	2350 9150 2450 9150
Wire Wire Line
	4650 3350 4000 3350
Wire Wire Line
	4400 3300 4400 3350
Connection ~ 4400 3350
Wire Wire Line
	11050 6000 11050 5950
Wire Wire Line
	11050 5950 11600 5950
Connection ~ 11600 5950
Wire Wire Line
	2200 3250 2200 3350
Connection ~ 2200 3350
$Comp
L PWR_FLAG #FLG01
U 1 1 4FEAA649
P 1850 8650
F 0 "#FLG01" H 1850 8745 30  0001 C CNN
F 1 "PWR_FLAG" H 1850 8830 30  0000 C CNN
	1    1850 8650
	1    0    0    -1  
$EndComp
NoConn ~ 1200 10600
Text Notes 5500 8050 1    40   ~ 0
Header for expansion
$Comp
L +5V #PWR02
U 1 1 4FEA97A6
P 4150 7950
F 0 "#PWR02" H 4150 8040 20  0001 C CNN
F 1 "+5V" H 4150 8040 30  0000 C CNN
	1    4150 7950
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR03
U 1 1 4FEA97A2
P 4150 7750
F 0 "#PWR03" H 4150 7840 20  0001 C CNN
F 1 "+5V" H 4150 7840 30  0000 C CNN
	1    4150 7750
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR04
U 1 1 4FEA9793
P 4150 7550
F 0 "#PWR04" H 4150 7640 20  0001 C CNN
F 1 "+5V" H 4150 7640 30  0000 C CNN
	1    4150 7550
	0    -1   -1   0   
$EndComp
Text Label 4150 7350 0    40   ~ 0
RA2
$Comp
L CHRISJ1_CONN_20 P3
U 1 1 4FEA9736
P 5250 7100
F 0 "P3" V 5500 7950 60  0000 C CNN
F 1 "CHRISJ1_CONN_20" V 5300 7100 60  0000 C CNN
	1    5250 7100
	1    0    0    -1  
$EndComp
Text Label 4150 6150 0    40   ~ 0
RA5
Text Label 4150 6350 0    40   ~ 0
RA4
Text Label 4150 6550 0    40   ~ 0
RC5
Text Label 4150 6750 0    40   ~ 0
RC4
Text Label 4150 6950 0    40   ~ 0
RC6
Text Label 4150 7150 0    40   ~ 0
RC7
Text Label 5400 10300 0    40   ~ 0
RC7
Text Label 5400 10200 0    40   ~ 0
RC6
Text Label 5400 10000 0    40   ~ 0
RC4
Text Label 5400 9900 0    40   ~ 0
RC5
Text Label 5400 9700 0    40   ~ 0
RA4
Text Label 5400 9600 0    40   ~ 0
RA5
Text Label 10100 9800 2    40   ~ 0
RA2
$Comp
L GND #PWR05
U 1 1 4FEA93BA
P 8000 1250
F 0 "#PWR05" H 8000 1250 30  0001 C CNN
F 1 "GND" H 8000 1180 30  0001 C CNN
	1    8000 1250
	1    0    0    -1  
$EndComp
$Comp
L C C9
U 1 1 4FEA9399
P 7700 1150
F 0 "C9" H 7750 1250 50  0000 L CNN
F 1 "100nF 16V" H 7750 1050 50  0000 L CNN
	1    7700 1150
	0    -1   -1   0   
$EndComp
$Comp
L R R1
U 1 1 4FEA92CF
P 1600 8400
F 0 "R1" V 1680 8400 50  0000 C CNN
F 1 "10R" V 1600 8400 50  0000 C CNN
	1    1600 8400
	1    0    0    -1  
$EndComp
$Comp
L DIODE D2
U 1 1 4FEA91DA
P 3600 2550
F 0 "D2" H 3600 2650 40  0000 C CNN
F 1 "1N4007" H 3600 2450 40  0000 C CNN
	1    3600 2550
	-1   0    0    1   
$EndComp
Text Notes 1300 10200 0    40   ~ 0
GND
Text Notes 1300 10300 0    40   ~ 0
DSR
Text Notes 1300 9800 0    40   ~ 0
DCD
Text Label 1300 10100 0    40   ~ 0
DTR
Text Label 1300 10500 0    40   ~ 0
CTS
Text Label 1300 10400 0    40   ~ 0
RTS
Text Label 1300 9900 0    40   ~ 0
RD
Text Label 1300 10000 0    40   ~ 0
TD
$Comp
L CP_EU C10
U 1 1 4FEA7CF6
P 11150 1150
F 0 "C10" H 11200 1250 50  0000 L CNN
F 1 "100uF 10V" H 11200 1050 50  0000 L CNN
	1    11150 1150
	0    1    1    0   
$EndComp
$Comp
L CP_EU C3
U 1 1 4FEA7CE3
P 2350 8950
F 0 "C3" H 2200 8850 50  0000 L CNN
F 1 "1uF 10V" H 2050 9050 50  0000 L CNN
	1    2350 8950
	1    0    0    1   
$EndComp
$Comp
L CP_EU C6
U 1 1 4FEA7CD7
P 4050 9450
F 0 "C6" H 4100 9550 50  0000 L CNN
F 1 "1uF 10V" H 4100 9350 50  0000 L CNN
	1    4050 9450
	1    0    0    -1  
$EndComp
$Comp
L CP_EU C5
U 1 1 4FEA7CD3
P 4050 8950
F 0 "C5" H 4100 9050 50  0000 L CNN
F 1 "1uF 10V" H 4100 8850 50  0000 L CNN
	1    4050 8950
	1    0    0    -1  
$EndComp
$Comp
L CP_EU C1
U 1 1 4FEA7CD1
P 2000 8950
F 0 "C1" H 1700 9000 50  0000 L CNN
F 1 "100uF 10V" H 1550 8850 50  0000 L CNN
	1    2000 8950
	1    0    0    -1  
$EndComp
$Comp
L CP_EU C2
U 1 1 4FEA7CB2
P 1950 9450
F 0 "C2" H 1800 9350 50  0000 L CNN
F 1 "1uF 10V" H 2000 9350 50  0000 L CNN
	1    1950 9450
	1    0    0    -1  
$EndComp
$Comp
L CP_EU C7
U 1 1 4FEA7CAC
P 4150 3650
F 0 "C7" H 4200 3750 50  0000 L CNN
F 1 "1000uF 10V" H 4200 3550 50  0000 L CNN
	1    4150 3650
	1    0    0    -1  
$EndComp
$Comp
L CP_EU C4
U 1 1 4FEA7C9F
P 3000 3650
F 0 "C4" H 3050 3750 50  0000 L CNN
F 1 "470uF 35V" H 3050 3550 50  0000 L CNN
	1    3000 3650
	1    0    0    -1  
$EndComp
Text Notes 650  4300 0    60   ~ 0
Power input DC 9V - 16V
Text Notes 3200 3100 0    60   ~ 0
Requires heatsink
$Comp
L CHRISJ1_CONN_2 P2
U 1 1 4FEA7B1A
P 1600 3900
F 0 "P2" V 1550 3900 40  0000 C CNN
F 1 "CHRISJ1_CONN_2" V 1650 3900 40  0000 C CNN
	1    1600 3900
	-1   0    0    -1  
$EndComp
$Comp
L DIODE D1
U 1 1 4FEA70AF
P 2500 3350
F 0 "D1" H 2500 3450 40  0000 C CNN
F 1 "1N4007" H 2500 3250 40  0000 C CNN
	1    2500 3350
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 4FEA707B
P 4650 3650
F 0 "R2" V 4730 3650 50  0000 C CNN
F 1 "470R" V 4650 3650 50  0000 C CNN
	1    4650 3650
	1    0    0    -1  
$EndComp
$Comp
L LED D3
U 1 1 4FEA707A
P 4650 4200
F 0 "D3" H 4650 4300 50  0000 C CNN
F 1 "LED" H 4650 4100 50  0000 C CNN
	1    4650 4200
	0    1    1    0   
$EndComp
$Comp
L GND #PWR06
U 1 1 4FEA6F95
P 3500 4550
F 0 "#PWR06" H 3500 4550 30  0001 C CNN
F 1 "GND" H 3500 4480 30  0001 C CNN
	1    3500 4550
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR07
U 1 1 4FEA6F8B
P 4400 3300
F 0 "#PWR07" H 4400 3390 20  0001 C CNN
F 1 "+5V" H 4400 3390 30  0000 C CNN
	1    4400 3300
	1    0    0    -1  
$EndComp
$Comp
L LM7805 U2
U 1 1 4FEA6F44
P 3600 3400
F 0 "U2" H 3750 3204 60  0000 C CNN
F 1 "LM7805" H 3600 3600 60  0000 C CNN
	1    3600 3400
	1    0    0    -1  
$EndComp
Text Notes 8050 5350 0    40   ~ 0
Connector for external relays,\nfor additional PV panels 4,5,6,7,8
Text Label 7950 5000 0    40   ~ 0
RLYDRV8
Text Label 7950 4900 0    40   ~ 0
RLYDRV7
Text Label 7950 4800 0    40   ~ 0
RLYDRV6
Text Label 7950 4700 0    40   ~ 0
RLYDRV5
Text Label 7950 4600 0    40   ~ 0
RLYDRV4
$Comp
L GND #PWR08
U 1 1 4FEA6E57
P 8300 5200
F 0 "#PWR08" H 8300 5200 30  0001 C CNN
F 1 "GND" H 8300 5130 30  0001 C CNN
	1    8300 5200
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_CONN_6 P5
U 1 1 4FEA6E3B
P 8750 4750
F 0 "P5" V 8700 4750 60  0000 C CNN
F 1 "CHRISJ1_CONN_6" V 8800 4750 60  0000 C CNN
	1    8750 4750
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q5
U 1 1 4FEA6D9E
P 7050 6350
F 0 "Q5" H 6850 6200 60  0000 R CNN
F 1 "2N7000" H 7000 6100 60  0000 R CNN
	1    7050 6350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 4FEA6D9D
P 7150 6650
F 0 "#PWR09" H 7150 6650 30  0001 C CNN
F 1 "GND" H 7150 6580 30  0001 C CNN
	1    7150 6650
	1    0    0    -1  
$EndComp
$Comp
L ZENERSMALL D8
U 1 1 4FEA6D9C
P 7300 6300
F 0 "D8" H 7100 6200 40  0000 C CNN
F 1 "15V Zener" H 7300 6200 30  0000 C CNN
	1    7300 6300
	0    -1   -1   0   
$EndComp
Text Label 6450 6350 0    40   ~ 0
SEL8
Text Label 7650 6050 2    40   ~ 0
RLYDRV8
$Comp
L MOSFET_N Q3
U 1 1 4FEA6D91
P 7050 4750
F 0 "Q3" H 6850 4600 60  0000 R CNN
F 1 "2N7000" H 7000 4500 60  0000 R CNN
	1    7050 4750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 4FEA6D90
P 7150 5050
F 0 "#PWR010" H 7150 5050 30  0001 C CNN
F 1 "GND" H 7150 4980 30  0001 C CNN
	1    7150 5050
	1    0    0    -1  
$EndComp
$Comp
L ZENERSMALL D6
U 1 1 4FEA6D8F
P 7300 4700
F 0 "D6" H 7100 4600 40  0000 C CNN
F 1 "15V Zener" H 7300 4600 30  0000 C CNN
	1    7300 4700
	0    -1   -1   0   
$EndComp
Text Label 6450 4750 0    40   ~ 0
SEL6
Text Label 7650 4450 2    40   ~ 0
RLYDRV6
Text Label 7650 5250 2    40   ~ 0
RLYDRV7
Text Label 6450 5550 0    40   ~ 0
SEL7
$Comp
L ZENERSMALL D7
U 1 1 4FEA6D8E
P 7300 5500
F 0 "D7" H 7100 5400 40  0000 C CNN
F 1 "15V Zener" H 7300 5400 30  0000 C CNN
	1    7300 5500
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR011
U 1 1 4FEA6D8D
P 7150 5850
F 0 "#PWR011" H 7150 5850 30  0001 C CNN
F 1 "GND" H 7150 5780 30  0001 C CNN
	1    7150 5850
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q4
U 1 1 4FEA6D8C
P 7050 5550
F 0 "Q4" H 6850 5400 60  0000 R CNN
F 1 "2N7000" H 7000 5300 60  0000 R CNN
	1    7050 5550
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q2
U 1 1 4FEA6D84
P 7050 3950
F 0 "Q2" H 6850 3800 60  0000 R CNN
F 1 "2N7000" H 7000 3700 60  0000 R CNN
	1    7050 3950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 4FEA6D83
P 7150 4250
F 0 "#PWR012" H 7150 4250 30  0001 C CNN
F 1 "GND" H 7150 4180 30  0001 C CNN
	1    7150 4250
	1    0    0    -1  
$EndComp
$Comp
L ZENERSMALL D5
U 1 1 4FEA6D82
P 7300 3900
F 0 "D5" H 7100 3800 40  0000 C CNN
F 1 "15V Zener" H 7300 3800 30  0000 C CNN
	1    7300 3900
	0    -1   -1   0   
$EndComp
Text Label 6450 3950 0    40   ~ 0
SEL5
Text Label 7650 3650 2    40   ~ 0
RLYDRV5
Text Label 7650 2850 2    40   ~ 0
RLYDRV4
Text Label 6450 3150 0    40   ~ 0
SEL4
Text Label 8200 2150 2    40   ~ 0
SEL8
Text Label 8200 2050 2    40   ~ 0
SEL7
Text Label 8200 1950 2    40   ~ 0
SEL6
Text Label 8200 1850 2    40   ~ 0
SEL5
Text Label 8200 1750 2    40   ~ 0
SEL4
Text Label 8200 1650 0    40   ~ 0
SEL3
Text Label 8200 1550 0    40   ~ 0
SEL2
Text Label 8200 1450 0    40   ~ 0
SEL1
Text Label 8200 1350 0    40   ~ 0
SEL0
Text Notes 7650 2300 0    40   ~ 0
Use when no relay selected
NoConn ~ 7600 2250
$Comp
L ZENERSMALL D4
U 1 1 4FEA6B80
P 7300 3100
F 0 "D4" H 7100 3000 40  0000 C CNN
F 1 "15V Zener" H 7300 3000 30  0000 C CNN
	1    7300 3100
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR013
U 1 1 4FEA6B65
P 7150 3450
F 0 "#PWR013" H 7150 3450 30  0001 C CNN
F 1 "GND" H 7150 3380 30  0001 C CNN
	1    7150 3450
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q1
U 1 1 4FEA6AEA
P 7050 3150
F 0 "Q1" H 6850 3000 60  0000 R CNN
F 1 "2N7000" H 7000 2900 60  0000 R CNN
	1    7050 3150
	1    0    0    -1  
$EndComp
Text Label 5700 10100 0    40   ~ 0
CH3
Text Label 9800 10100 2    40   ~ 0
CH2
Text Label 9800 10000 2    40   ~ 0
CH1
Text Label 9800 9900 2    40   ~ 0
CH0
Text Label 5950 1950 0    40   ~ 0
CH3
Text Label 5950 1850 0    40   ~ 0
CH2
Text Label 5950 1750 0    40   ~ 0
CH1
Text Label 5950 1650 0    40   ~ 0
CH0
$Comp
L R R3
U 1 1 4FEA69AE
P 7600 7950
F 0 "R3" V 7680 7950 50  0000 C CNN
F 1 "4k7" V 7600 7950 50  0000 C CNN
	1    7600 7950
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR014
U 1 1 4FEA6935
P 7000 1150
F 0 "#PWR014" H 7000 1240 20  0001 C CNN
F 1 "+5V" H 7000 1240 30  0000 C CNN
	1    7000 1150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 4FEA692B
P 7000 2450
F 0 "#PWR015" H 7000 2450 30  0001 C CNN
F 1 "GND" H 7000 2380 30  0001 C CNN
	1    7000 2450
	1    0    0    -1  
$EndComp
Text Label 12900 6000 0    40   ~ 0
V_minus_2
Text Label 12900 6100 0    40   ~ 0
I_minus_2
Text Label 12900 6200 0    40   ~ 0
RL_minus_2
Text Label 12900 6300 0    40   ~ 0
RL_plus_2
Text Label 12900 6400 0    40   ~ 0
I_plus_2
Text Label 12900 6500 0    40   ~ 0
V_plus_2
Text Label 12900 8500 0    40   ~ 0
V_plus_3
Text Label 12900 8400 0    40   ~ 0
I_plus_3
Text Label 12900 8300 0    40   ~ 0
RL_plus_3
Text Label 12900 8200 0    40   ~ 0
RL_minus_3
Text Label 12900 8100 0    40   ~ 0
I_minus_3
Text Label 12900 8000 0    40   ~ 0
V_minus_3
Text Label 12900 4000 0    40   ~ 0
V_minus_1
Text Label 12900 4100 0    40   ~ 0
I_minus_1
Text Label 12900 4200 0    40   ~ 0
RL_minus_1
Text Label 12900 4300 0    40   ~ 0
RL_plus_1
Text Label 12900 4400 0    40   ~ 0
I_plus_1
Text Label 12900 4500 0    40   ~ 0
V_plus_1
Text Label 12900 2500 0    40   ~ 0
V_plus_0
Text Label 12900 2400 0    40   ~ 0
I_plus_0
Text Label 12900 2300 0    40   ~ 0
RL_plus_0
Text Label 12900 2200 0    40   ~ 0
RL_minus_0
Text Label 12900 2100 0    40   ~ 0
I_minus_0
Text Label 12900 2000 0    40   ~ 0
V_minus_0
Text Label 15250 1700 3    40   ~ 0
V_plus
Text Label 15150 1350 2    40   ~ 0
V_minus
Text Label 15250 1550 2    40   ~ 0
I_plus
Text Label 15000 1450 0    40   ~ 0
I_minus
Text Notes 6400 850  0    40   ~ 0
Hardware decoder prevents damage\ndue to multiple relays being active\neven in the event of software errors.
Text Notes 11800 8650 0    40   ~ 0
PV Panel and\nLoad Resistor
Text Notes 11800 6650 0    40   ~ 0
PV Panel and\nLoad Resistor
Text Notes 11800 4650 0    40   ~ 0
PV Panel and\nLoad Resistor
Text Notes 11800 2650 0    40   ~ 0
PV Panel and\nLoad Resistor
Text Notes 15850 1750 1    40   ~ 0
To IV curve tracer
Text Notes 700  10450 1    40   ~ 0
RS-232 from PC
Text Notes 6750 8850 1    40   ~ 0
In-Circuit Programming
Text Label 9800 9600 2    40   ~ 0
PGD
Text Label 9800 9700 2    40   ~ 0
PGC
Text Label 5700 9800 0    40   ~ 0
MCLR
NoConn ~ 7250 8750
Text Label 8400 8650 2    40   ~ 0
PGC
Text Label 8400 8550 2    40   ~ 0
PGD
Text Label 8100 8250 2    40   ~ 0
MCLR
$Comp
L CHRISJ1_CONN_6 P4
U 1 1 4FEA6269
P 6900 8500
F 0 "P4" V 6850 8500 60  0000 C CNN
F 1 "CHRISJ1_CONN_6" V 6950 8500 60  0000 C CNN
	1    6900 8500
	-1   0    0    -1  
$EndComp
$Comp
L LED D12
U 1 1 4FEA5EB0
P 11050 8750
F 0 "D12" H 11050 8850 50  0000 C CNN
F 1 "LED" H 11050 8650 50  0000 C CNN
	1    11050 8750
	0    1    1    0   
$EndComp
$Comp
L R R7
U 1 1 4FEA5EAF
P 11050 8250
F 0 "R7" V 11130 8250 50  0000 C CNN
F 1 "470R" V 11050 8250 50  0000 C CNN
	1    11050 8250
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 4FEA5EA8
P 11050 6250
F 0 "R6" V 11130 6250 50  0000 C CNN
F 1 "470R" V 11050 6250 50  0000 C CNN
	1    11050 6250
	1    0    0    -1  
$EndComp
$Comp
L LED D11
U 1 1 4FEA5EA7
P 11050 6750
F 0 "D11" H 11050 6850 50  0000 C CNN
F 1 "LED" H 11050 6650 50  0000 C CNN
	1    11050 6750
	0    1    1    0   
$EndComp
$Comp
L LED D10
U 1 1 4FEA5E9F
P 11050 4750
F 0 "D10" H 11050 4850 50  0000 C CNN
F 1 "LED" H 11050 4650 50  0000 C CNN
	1    11050 4750
	0    1    1    0   
$EndComp
$Comp
L R R5
U 1 1 4FEA5E9E
P 11050 4250
F 0 "R5" V 11130 4250 50  0000 C CNN
F 1 "470R" V 11050 4250 50  0000 C CNN
	1    11050 4250
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 4FEA5E7B
P 11050 2250
F 0 "R4" V 11130 2250 50  0000 C CNN
F 1 "470R" V 11050 2250 50  0000 C CNN
	1    11050 2250
	1    0    0    -1  
$EndComp
$Comp
L LED D9
U 1 1 4FEA5DE6
P 11050 2750
F 0 "D9" H 11050 2850 50  0000 C CNN
F 1 "LED" H 11050 2650 50  0000 C CNN
	1    11050 2750
	0    1    1    0   
$EndComp
$Comp
L CHRISJ1_CONN_4 P10
U 1 1 4FCEEF28
P 15650 1500
F 0 "P10" V 15600 1500 50  0000 C CNN
F 1 "CHRISJ1_CONN_4" V 15700 1500 50  0000 C CNN
	1    15650 1500
	1    0    0    -1  
$EndComp
$Comp
L 4028 U3
U 1 1 4FEA5D05
P 7000 1800
F 0 "U3" H 7000 1800 60  0000 C CNN
F 1 "4028" H 6950 1500 60  0000 C CNN
	1    7000 1800
	1    0    0    -1  
$EndComp
$Comp
L C C8
U 1 1 4FEA5A33
P 7650 9200
F 0 "C8" H 7700 9300 50  0000 L CNN
F 1 "100nF 16V" H 7700 9100 50  0000 L CNN
	1    7650 9200
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR016
U 1 1 4FEA5A13
P 9650 9300
F 0 "#PWR016" H 9650 9300 30  0001 C CNN
F 1 "GND" H 9650 9230 30  0001 C CNN
	1    9650 9300
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR017
U 1 1 4FEA5A05
P 6150 9100
F 0 "#PWR017" H 6150 9190 20  0001 C CNN
F 1 "+5V" H 6150 9190 30  0000 C CNN
	1    6150 9100
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR018
U 1 1 4FEA59F9
P 1600 8100
F 0 "#PWR018" H 1600 8190 20  0001 C CNN
F 1 "+5V" H 1600 8190 30  0000 C CNN
	1    1600 8100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 4FE95704
P 1750 10700
F 0 "#PWR019" H 1750 10700 30  0001 C CNN
F 1 "GND" H 1750 10630 30  0001 C CNN
	1    1750 10700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 4FE956D1
P 1650 9300
F 0 "#PWR020" H 1650 9300 30  0001 C CNN
F 1 "GND" H 1650 9230 30  0001 C CNN
	1    1650 9300
	1    0    0    -1  
$EndComp
$Comp
L MAX232 U1
U 1 1 4FE952F5
P 3250 9450
F 0 "U1" H 3250 10300 70  0000 C CNN
F 1 "MAX232" H 3250 8600 70  0000 C CNN
	1    3250 9450
	-1   0    0    -1  
$EndComp
$Comp
L CHRISJ1_CONN_9 P1
U 1 1 4FE9527B
P 850 10200
F 0 "P1" V 800 10200 60  0000 C CNN
F 1 "CHRISJ1_CONN_9" V 900 10200 60  0000 C CNN
	1    850  10200
	-1   0    0    -1  
$EndComp
$Comp
L PIC16F690 U4
U 1 1 4FE94FF4
P 7750 9950
F 0 "U4" H 8450 10550 60  0000 C CNN
F 1 "PIC16F690" H 8250 9350 60  0000 C CNN
	1    7750 9950
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q9
U 1 1 4FE94F33
P 10500 9200
F 0 "Q9" H 10510 9370 60  0000 R CNN
F 1 "2N7000" H 10510 9050 60  0000 R CNN
	1    10500 9200
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q8
U 1 1 4FE94F2B
P 10500 7200
F 0 "Q8" H 10510 7370 60  0000 R CNN
F 1 "2N7000" H 10510 7050 60  0000 R CNN
	1    10500 7200
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q7
U 1 1 4FE94F22
P 10500 5200
F 0 "Q7" H 10510 5370 60  0000 R CNN
F 1 "2N7000" H 10510 5050 60  0000 R CNN
	1    10500 5200
	1    0    0    -1  
$EndComp
$Comp
L DIODE D16
U 1 1 4FE94F13
P 11400 8750
F 0 "D16" H 11400 8850 40  0000 C CNN
F 1 "1N4007" H 11400 8650 40  0000 C CNN
	1    11400 8750
	0    -1   -1   0   
$EndComp
$Comp
L DIODE D15
U 1 1 4FE94F0B
P 11400 6750
F 0 "D15" H 11400 6850 40  0000 C CNN
F 1 "1N4007" H 11400 6650 40  0000 C CNN
	1    11400 6750
	0    -1   -1   0   
$EndComp
$Comp
L DIODE D14
U 1 1 4FE94EFE
P 11400 4750
F 0 "D14" H 11400 4850 40  0000 C CNN
F 1 "1N4007" H 11400 4650 40  0000 C CNN
	1    11400 4750
	0    -1   -1   0   
$EndComp
$Comp
L CHRISJ1_CONN_6 P9
U 1 1 4FE928F7
P 12500 8250
F 0 "P9" V 12450 8250 60  0000 C CNN
F 1 "CHRISJ1_CONN_6" V 12550 8250 60  0000 C CNN
	1    12500 8250
	-1   0    0    -1  
$EndComp
$Comp
L CHRISJ1_RAV14202 K4
U 1 1 4FE928F6
P 14050 8650
F 0 "K4" H 14300 8300 70  0000 C CNN
F 1 "RAV14202" H 14200 8150 70  0000 C CNN
	1    14050 8650
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_RAV14202 K3
U 1 1 4FE928DB
P 14050 6650
F 0 "K3" H 14300 6300 70  0000 C CNN
F 1 "RAV14202" H 14200 6150 70  0000 C CNN
	1    14050 6650
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_CONN_6 P8
U 1 1 4FE928DA
P 12500 6250
F 0 "P8" V 12450 6250 60  0000 C CNN
F 1 "CHRISJ1_CONN_6" V 12550 6250 60  0000 C CNN
	1    12500 6250
	-1   0    0    -1  
$EndComp
$Comp
L CHRISJ1_CONN_6 P7
U 1 1 4FE928A8
P 12500 4250
F 0 "P7" V 12450 4250 60  0000 C CNN
F 1 "CHRISJ1_CONN_6" V 12550 4250 60  0000 C CNN
	1    12500 4250
	-1   0    0    -1  
$EndComp
$Comp
L CHRISJ1_RAV14202 K2
U 1 1 4FE928A7
P 14050 4650
F 0 "K2" H 14300 4300 70  0000 C CNN
F 1 "RAV14202" H 14200 4150 70  0000 C CNN
	1    14050 4650
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG021
U 1 1 4FCEEEDA
P 2200 4350
F 0 "#FLG021" H 2200 4445 30  0001 C CNN
F 1 "PWR_FLAG" H 2200 4530 30  0000 C CNN
	1    2200 4350
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG022
U 1 1 4FCEEED6
P 2200 3250
F 0 "#FLG022" H 2200 3345 30  0001 C CNN
F 1 "PWR_FLAG" H 2200 3430 30  0000 C CNN
	1    2200 3250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR023
U 1 1 4FCEE975
P 10750 9500
F 0 "#PWR023" H 10750 9500 30  0001 C CNN
F 1 "GND" H 10750 9430 30  0001 C CNN
	1    10750 9500
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q6
U 1 1 4FCEE957
P 10500 3200
F 0 "Q6" H 10510 3370 60  0000 R CNN
F 1 "2N7000" H 10510 3050 60  0000 R CNN
	1    10500 3200
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR024
U 1 1 4FCEE911
P 11600 1000
F 0 "#PWR024" H 11600 1090 20  0001 C CNN
F 1 "+5V" H 11600 1090 30  0000 C CNN
	1    11600 1000
	1    0    0    -1  
$EndComp
$Comp
L DIODE D13
U 1 1 4FCEE8F0
P 11400 2750
F 0 "D13" H 11400 2850 40  0000 C CNN
F 1 "1N4007" H 11400 2650 40  0000 C CNN
	1    11400 2750
	0    -1   -1   0   
$EndComp
$Comp
L CHRISJ1_RAV14202 K1
U 1 1 4FCEE789
P 14050 2650
F 0 "K1" H 14300 2300 70  0000 C CNN
F 1 "RAV14202" H 14200 2150 70  0000 C CNN
	1    14050 2650
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_CONN_6 P6
U 1 1 4FCEE772
P 12500 2250
F 0 "P6" V 12450 2250 60  0000 C CNN
F 1 "CHRISJ1_CONN_6" V 12550 2250 60  0000 C CNN
	1    12500 2250
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
