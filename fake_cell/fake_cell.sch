EESchema Schematic File Version 4
LIBS:fake_cell-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 6065C79E
P 5200 3200
F 0 "J2" H 5280 3192 50  0000 L CNN
F 1 "Conn_01x08" H 5280 3101 50  0000 L CNN
F 2 "fake_cell:fake_cell_1" H 5200 3200 50  0001 C CNN
F 3 "~" H 5200 3200 50  0001 C CNN
	1    5200 3200
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J1
U 1 1 6065D295
P 4300 3100
F 0 "J1" H 4218 3617 50  0000 C CNN
F 1 "Conn_01x08" H 4218 3526 50  0000 C CNN
F 2 "fake_cell:fake_cell_1" H 4300 3100 50  0001 C CNN
F 3 "~" H 4300 3100 50  0001 C CNN
	1    4300 3100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4500 2800 5000 2800
Wire Wire Line
	4500 2900 5000 2900
Wire Wire Line
	4500 3000 5000 3000
Wire Wire Line
	4500 3100 5000 3100
Wire Wire Line
	4500 3200 5000 3200
Wire Wire Line
	4500 3300 5000 3300
Wire Wire Line
	4500 3400 5000 3400
Wire Wire Line
	4500 3500 5000 3500
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 62E77D63
P 4550 3900
F 0 "H1" H 4650 3949 50  0000 L CNN
F 1 "MountingHole_Pad" H 4650 3858 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 4550 3900 50  0001 C CNN
F 3 "~" H 4550 3900 50  0001 C CNN
	1    4550 3900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 62E782DF
P 4550 4400
F 0 "H2" H 4450 4357 50  0000 R CNN
F 1 "MountingHole_Pad" H 4450 4448 50  0000 R CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 4550 4400 50  0001 C CNN
F 3 "~" H 4550 4400 50  0001 C CNN
	1    4550 4400
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 62E79559
P 4750 4150
F 0 "H3" V 4704 4300 50  0000 L CNN
F 1 "MountingHole_Pad" V 4795 4300 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_6mm_Pad" H 4750 4150 50  0001 C CNN
F 3 "~" H 4750 4150 50  0001 C CNN
	1    4750 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 4000 4550 4150
Wire Wire Line
	4650 4150 4550 4150
Connection ~ 4550 4150
Wire Wire Line
	4550 4150 4550 4300
Text Label 4600 4150 0    50   ~ 0
gnd
$EndSCHEMATC
