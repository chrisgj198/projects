EESchema Schematic File Version 2  date 3/09/2013 11:52:32 AM
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:chrisj1
LIBS:diy_connectors
LIBS:diy_diodes
LIBS:diy_ics
LIBS:diy_rcl
LIBS:diy_regulators
LIBS:diy_switches
LIBS:diy_transistors
LIBS:ctd_licharger-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "3 sep 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CHRISJ1_LTC4059 U1
U 1 1 51F8579C
P 4800 3050
F 0 "U1" H 4800 2750 60  0000 C CNN
F 1 "LTC4059" H 4800 3250 60  0000 C CNN
F 2 "" H 4800 3050 60  0000 C CNN
F 3 "" H 4800 3050 60  0000 C CNN
	1    4800 3050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 52252CE1
P 5300 3450
F 0 "#PWR01" H 5300 3450 30  0001 C CNN
F 1 "GND" H 5300 3380 30  0001 C CNN
F 2 "" H 5300 3450 60  0000 C CNN
F 3 "" H 5300 3450 60  0000 C CNN
	1    5300 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 3150 5300 3150
Wire Wire Line
	5300 3150 5300 3450
Wire Wire Line
	5150 3250 5300 3250
Connection ~ 5300 3250
$Comp
L GND #PWR02
U 1 1 52252D77
P 4200 3450
F 0 "#PWR02" H 4200 3450 30  0001 C CNN
F 1 "GND" H 4200 3380 30  0001 C CNN
F 2 "" H 4200 3450 60  0000 C CNN
F 3 "" H 4200 3450 60  0000 C CNN
	1    4200 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3300 4200 3450
$Comp
L GND #PWR03
U 1 1 52252E58
P 5600 3450
F 0 "#PWR03" H 5600 3450 30  0001 C CNN
F 1 "GND" H 5600 3380 30  0001 C CNN
F 2 "" H 5600 3450 60  0000 C CNN
F 3 "" H 5600 3450 60  0000 C CNN
	1    5600 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3050 5150 3050
$Comp
L CHRISJ1_R R1
U 1 1 522535D6
P 5600 3250
F 0 "R1" V 5680 3250 50  0000 C CNN
F 1 "1k5" V 5520 3250 50  0000 C CNN
F 2 "" H 5600 3250 60  0000 C CNN
F 3 "" H 5600 3250 60  0000 C CNN
	1    5600 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3150 5600 3050
Wire Wire Line
	5600 3350 5600 3450
$Comp
L CHRISJ1_POLYSWITCH F1
U 1 1 522536C0
P 5850 2950
F 0 "F1" V 5930 2950 50  0000 C CNN
F 1 "1.5A" V 5770 2950 50  0000 C CNN
F 2 "" H 5850 2950 60  0000 C CNN
F 3 "" H 5850 2950 60  0000 C CNN
	1    5850 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	5150 2950 5750 2950
$Comp
L CHRISJ1_C C1
U 1 1 52253995
P 4200 3250
F 0 "C1" V 4280 3250 50  0000 C CNN
F 1 "1uF" V 4120 3250 50  0000 C CNN
F 2 "" H 4200 3250 60  0000 C CNN
F 3 "" H 4200 3250 60  0000 C CNN
	1    4200 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 3200 4200 2950
Connection ~ 4200 2950
Wire Wire Line
	4000 2950 4450 2950
$Comp
L CONN_1 P3
U 1 1 52253A6A
P 6150 2950
F 0 "P3" H 6230 2950 40  0000 L CNN
F 1 "CONN_1" H 6150 3005 30  0001 C CNN
F 2 "" H 6150 2950 60  0000 C CNN
F 3 "" H 6150 2950 60  0000 C CNN
	1    6150 2950
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P4
U 1 1 52253A79
P 6150 3400
F 0 "P4" H 6230 3400 40  0000 L CNN
F 1 "CONN_1" H 6150 3455 30  0001 C CNN
F 2 "" H 6150 3400 60  0000 C CNN
F 3 "" H 6150 3400 60  0000 C CNN
	1    6150 3400
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P1
U 1 1 52253A88
P 3850 2950
F 0 "P1" H 3930 2950 40  0000 L CNN
F 1 "CONN_1" H 3850 3005 30  0001 C CNN
F 2 "" H 3850 2950 60  0000 C CNN
F 3 "" H 3850 2950 60  0000 C CNN
	1    3850 2950
	-1   0    0    -1  
$EndComp
$Comp
L CONN_1 P2
U 1 1 52253A97
P 3850 3350
F 0 "P2" H 3930 3350 40  0000 L CNN
F 1 "CONN_1" H 3850 3405 30  0001 C CNN
F 2 "" H 3850 3350 60  0000 C CNN
F 3 "" H 3850 3350 60  0000 C CNN
	1    3850 3350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4200 3350 4000 3350
Connection ~ 4200 3350
Wire Wire Line
	5950 2950 6000 2950
$Comp
L GND #PWR05
U 1 1 52253AC6
P 5850 3450
F 0 "#PWR05" H 5850 3450 30  0001 C CNN
F 1 "GND" H 5850 3380 30  0001 C CNN
F 2 "" H 5850 3450 60  0000 C CNN
F 3 "" H 5850 3450 60  0000 C CNN
	1    5850 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3450 5850 3400
Wire Wire Line
	5850 3400 6000 3400
Text Label 4050 2950 0    40   ~ 0
VIN
Text Label 5950 2950 0    40   ~ 0
VBAT
Text Label 5250 2950 0    40   ~ 0
VBATINT
Text Label 5300 3050 0    40   ~ 0
PROG
$Comp
L GND #PWR?
U 1 1 5225414D
P 4400 3450
F 0 "#PWR?" H 4400 3450 30  0001 C CNN
F 1 "GND" H 4400 3380 30  0001 C CNN
F 2 "" H 4400 3450 60  0000 C CNN
F 3 "" H 4400 3450 60  0000 C CNN
	1    4400 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3450 4400 3050
Wire Wire Line
	4400 3050 4450 3050
Wire Wire Line
	4450 3150 4400 3150
Connection ~ 4400 3150
$EndSCHEMATC
