EESchema Schematic File Version 2  date 18/10/2012 10:30:47 AM
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:chrisj1
LIBS:diy_tubes
LIBS:diy_connectors
LIBS:diy_diodes
LIBS:diy_ics
LIBS:diy_rcl
LIBS:diy_regulators
LIBS:diy_switches
LIBS:diy_transformers
LIBS:diy_transistors
LIBS:diy_triac
LIBS:pvsim-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title "Sliver PV module simulator"
Date "17 oct 2012"
Rev "1"
Comp "Australian National University"
Comment1 "Chris Jones"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 6050 6350 0    60   ~ 0
11-21 cells\n21-31 cells\n1 - 11 cells
Wire Wire Line
	8700 5700 9250 5700
Connection ~ 7150 1350
Wire Wire Line
	7100 1350 7150 1350
Connection ~ 7150 850 
Wire Wire Line
	7150 900  7150 850 
Connection ~ 6500 850 
Wire Wire Line
	6500 850  6500 1150
Wire Notes Line
	8900 5400 8950 6000
Connection ~ 3850 1350
Wire Wire Line
	3850 1350 4350 1350
Wire Wire Line
	4350 1350 4350 1650
Wire Wire Line
	10300 2350 10500 2150
Wire Wire Line
	10300 2350 9250 2350
Wire Wire Line
	9250 2350 9250 3400
Connection ~ 7500 600 
Wire Wire Line
	2400 600  9700 600 
Wire Wire Line
	9700 600  9700 1300
Connection ~ 3850 850 
Wire Wire Line
	3850 1650 3850 600 
Connection ~ 6200 2600
Wire Wire Line
	6200 2600 6200 2550
Connection ~ 6200 2100
Wire Wire Line
	6200 2000 6200 2150
Wire Wire Line
	7500 1650 7500 600 
Wire Wire Line
	4800 850  4850 850 
Connection ~ 6200 1550
Connection ~ 6200 850 
Wire Wire Line
	6200 850  6200 1600
Wire Wire Line
	6200 1550 6000 1550
Wire Wire Line
	6000 1550 6000 1800
Wire Wire Line
	6000 1800 6050 1800
Connection ~ 7400 6200
Wire Wire Line
	7400 6200 7400 6500
Wire Wire Line
	7400 6500 6000 6500
Connection ~ 5400 6600
Wire Wire Line
	5400 6600 5400 6400
Wire Wire Line
	5400 6400 5600 6400
Wire Wire Line
	10000 3200 10500 3200
Wire Wire Line
	10000 3200 10000 6600
Wire Wire Line
	10000 6600 5000 6600
Wire Wire Line
	5000 6600 5000 6150
Wire Wire Line
	9250 5700 9250 3800
Wire Wire Line
	8300 5200 8100 5200
Wire Wire Line
	8100 5200 8100 3200
Wire Wire Line
	8100 3200 7050 3200
Wire Wire Line
	8300 5400 8000 5400
Wire Wire Line
	8000 5400 8000 3800
Wire Wire Line
	8000 3800 7050 3800
Wire Wire Line
	8300 5600 7900 5600
Wire Wire Line
	7900 5600 7900 4400
Wire Wire Line
	7900 4400 7050 4400
Wire Wire Line
	8300 5800 7800 5800
Wire Wire Line
	7800 5800 7800 5000
Wire Wire Line
	7800 5000 7050 5000
Wire Wire Line
	8300 6000 7700 6000
Wire Wire Line
	7700 6000 7700 5600
Wire Wire Line
	7700 5600 7050 5600
Wire Wire Line
	8300 6200 6400 6200
Wire Wire Line
	8950 1900 8850 1900
Wire Wire Line
	9250 1350 9250 1700
Wire Wire Line
	7350 2000 7250 2000
Wire Wire Line
	7250 2000 7250 1400
Wire Wire Line
	7250 1400 8450 1400
Wire Wire Line
	7850 1900 8450 1900
Connection ~ 4400 3000
Wire Wire Line
	5150 3000 4000 3000
Wire Wire Line
	5150 3000 5150 6200
Wire Wire Line
	5150 6200 6250 6200
Wire Wire Line
	4100 3250 4000 3250
Wire Wire Line
	4000 3250 4000 3000
Wire Wire Line
	5000 3350 5000 3300
Wire Wire Line
	5000 3300 4550 3300
Wire Wire Line
	4550 3300 4550 3550
Wire Wire Line
	4400 3450 4400 3550
Wire Wire Line
	4400 3550 4700 3550
Connection ~ 4550 3550
Wire Wire Line
	4100 5650 4000 5650
Wire Wire Line
	4000 5650 4000 5400
Wire Wire Line
	4000 5400 4550 5400
Wire Wire Line
	5000 5550 5000 5600
Wire Wire Line
	5000 5750 5000 5700
Wire Wire Line
	4550 5400 4550 5600
Wire Wire Line
	5000 5700 4550 5700
Wire Wire Line
	4550 5700 4550 5950
Wire Wire Line
	4550 5600 5000 5600
Wire Wire Line
	4400 5850 4400 5950
Wire Wire Line
	4400 5950 4700 5950
Connection ~ 4550 5950
Wire Wire Line
	4400 5450 4400 5400
Connection ~ 4400 5400
Wire Wire Line
	4100 5050 4000 5050
Wire Wire Line
	4000 5050 4000 4800
Wire Wire Line
	4000 4800 4550 4800
Wire Wire Line
	5000 4950 5000 5000
Wire Wire Line
	5000 5150 5000 5100
Wire Wire Line
	4550 4800 4550 5000
Wire Wire Line
	5000 5100 4550 5100
Wire Wire Line
	4550 5100 4550 5350
Wire Wire Line
	4550 5000 5000 5000
Wire Wire Line
	4400 5250 4400 5350
Wire Wire Line
	4400 5350 4700 5350
Connection ~ 4550 5350
Wire Wire Line
	4400 4850 4400 4800
Connection ~ 4400 4800
Wire Wire Line
	4100 4450 4000 4450
Wire Wire Line
	4000 4450 4000 4200
Wire Wire Line
	4000 4200 4550 4200
Wire Wire Line
	5000 4350 5000 4400
Wire Wire Line
	5000 4550 5000 4500
Wire Wire Line
	4550 4200 4550 4400
Wire Wire Line
	5000 4500 4550 4500
Wire Wire Line
	4550 4500 4550 4750
Wire Wire Line
	4550 4400 5000 4400
Wire Wire Line
	4400 4650 4400 4750
Wire Wire Line
	4400 4750 4700 4750
Connection ~ 4550 4750
Wire Wire Line
	4400 4250 4400 4200
Connection ~ 4400 4200
Wire Wire Line
	4100 3850 4000 3850
Wire Wire Line
	4000 3850 4000 3600
Wire Wire Line
	4000 3600 4550 3600
Wire Wire Line
	5000 3750 5000 3800
Wire Wire Line
	5000 3950 5000 3900
Wire Wire Line
	4550 3600 4550 3800
Wire Wire Line
	5000 3900 4550 3900
Wire Wire Line
	4550 3900 4550 4150
Wire Wire Line
	4550 3800 5000 3800
Wire Wire Line
	4400 4050 4400 4150
Wire Wire Line
	4400 4150 4700 4150
Connection ~ 4550 4150
Wire Wire Line
	4400 3650 4400 3600
Connection ~ 4400 3600
Wire Wire Line
	4400 3050 4400 3000
Wire Wire Line
	5650 3050 5650 3000
Wire Wire Line
	6600 3250 6500 3250
Wire Wire Line
	6500 3250 6500 3000
Wire Wire Line
	6500 3000 7050 3000
Wire Wire Line
	7500 3350 7500 3300
Wire Wire Line
	7050 3000 7050 3200
Wire Wire Line
	7050 3550 7050 3300
Connection ~ 7500 3300
Wire Wire Line
	6900 3450 6900 3550
Wire Wire Line
	6900 3550 7200 3550
Connection ~ 7050 3550
Wire Wire Line
	6900 3050 6900 3000
Connection ~ 6900 3000
Wire Wire Line
	6600 5650 6500 5650
Wire Wire Line
	6500 5650 6500 5400
Wire Wire Line
	6500 5400 7050 5400
Connection ~ 7500 5600
Wire Wire Line
	7500 5550 7500 5600
Wire Wire Line
	7500 5750 7500 5700
Wire Wire Line
	7050 5400 7050 5600
Wire Wire Line
	7050 5700 7650 5700
Wire Wire Line
	7050 5700 7050 5950
Connection ~ 7500 5700
Wire Wire Line
	6900 5850 6900 5950
Wire Wire Line
	6900 5950 7200 5950
Connection ~ 7050 5950
Wire Wire Line
	6900 5450 6900 5400
Connection ~ 6900 5400
Wire Wire Line
	6600 5050 6500 5050
Wire Wire Line
	6500 5050 6500 4800
Wire Wire Line
	6500 4800 7050 4800
Connection ~ 7500 5000
Wire Wire Line
	7500 4950 7500 5000
Wire Wire Line
	7500 5150 7500 5100
Wire Wire Line
	7050 4800 7050 5000
Wire Wire Line
	7050 5350 7050 5100
Connection ~ 7500 5100
Wire Wire Line
	6900 5250 6900 5350
Wire Wire Line
	6900 5350 7200 5350
Connection ~ 7050 5350
Wire Wire Line
	6900 4850 6900 4800
Connection ~ 6900 4800
Wire Wire Line
	6600 4450 6500 4450
Wire Wire Line
	6500 4450 6500 4200
Wire Wire Line
	6500 4200 7050 4200
Connection ~ 7500 4400
Wire Wire Line
	7500 4350 7500 4400
Wire Wire Line
	7500 4550 7500 4500
Wire Wire Line
	7050 4200 7050 4400
Wire Wire Line
	7050 4750 7050 4500
Connection ~ 7500 4500
Wire Wire Line
	6900 4650 6900 4750
Wire Wire Line
	6900 4750 7200 4750
Connection ~ 7050 4750
Wire Wire Line
	6900 4250 6900 4200
Connection ~ 6900 4200
Wire Wire Line
	6600 3850 6500 3850
Wire Wire Line
	6500 3850 6500 3600
Wire Wire Line
	6500 3600 7050 3600
Connection ~ 7500 3800
Wire Wire Line
	7500 3750 7500 3800
Wire Wire Line
	7500 3950 7500 3900
Wire Wire Line
	7050 3600 7050 3800
Wire Wire Line
	7050 4150 7050 3900
Connection ~ 7500 3900
Wire Wire Line
	6900 4050 6900 4150
Wire Wire Line
	6900 4150 7200 4150
Connection ~ 7050 4150
Wire Wire Line
	6900 3650 6900 3600
Connection ~ 6900 3600
Wire Wire Line
	7500 6200 7500 6150
Connection ~ 5650 3600
Wire Wire Line
	5650 3650 5650 3600
Connection ~ 5800 4150
Wire Wire Line
	5950 4150 5650 4150
Wire Wire Line
	5650 4150 5650 4050
Wire Wire Line
	5800 3800 6250 3800
Wire Wire Line
	5800 4150 5800 3900
Wire Wire Line
	5800 3900 6250 3900
Wire Wire Line
	5800 3800 5800 3600
Wire Wire Line
	6250 3900 6250 3950
Wire Wire Line
	6250 3800 6250 3750
Wire Wire Line
	5800 3600 5250 3600
Wire Wire Line
	5250 3600 5250 3850
Wire Wire Line
	5250 3850 5350 3850
Connection ~ 5650 4200
Wire Wire Line
	5650 4250 5650 4200
Connection ~ 5800 4750
Wire Wire Line
	5950 4750 5650 4750
Wire Wire Line
	5650 4750 5650 4650
Wire Wire Line
	5800 4400 6250 4400
Wire Wire Line
	5800 4750 5800 4500
Wire Wire Line
	5800 4500 6250 4500
Wire Wire Line
	5800 4400 5800 4200
Wire Wire Line
	6250 4500 6250 4550
Wire Wire Line
	6250 4400 6250 4350
Wire Wire Line
	5800 4200 5250 4200
Wire Wire Line
	5250 4200 5250 4450
Wire Wire Line
	5250 4450 5350 4450
Connection ~ 5650 4800
Wire Wire Line
	5650 4850 5650 4800
Connection ~ 5800 5350
Wire Wire Line
	5950 5350 5650 5350
Wire Wire Line
	5650 5350 5650 5250
Wire Wire Line
	5800 5000 6250 5000
Wire Wire Line
	5800 5350 5800 5100
Wire Wire Line
	5800 5100 6250 5100
Wire Wire Line
	5800 5000 5800 4800
Wire Wire Line
	6250 5100 6250 5150
Wire Wire Line
	6250 5000 6250 4950
Wire Wire Line
	5800 4800 5250 4800
Wire Wire Line
	5250 4800 5250 5050
Wire Wire Line
	5250 5050 5350 5050
Connection ~ 5650 5400
Wire Wire Line
	5650 5450 5650 5400
Connection ~ 5800 5950
Wire Wire Line
	5950 5950 5650 5950
Wire Wire Line
	5650 5950 5650 5850
Wire Wire Line
	5800 5600 6250 5600
Wire Wire Line
	5800 5950 5800 5700
Wire Wire Line
	5800 5700 6250 5700
Wire Wire Line
	5800 5600 5800 5400
Wire Wire Line
	6250 5700 6250 5750
Wire Wire Line
	6250 5600 6250 5550
Wire Wire Line
	5800 5400 5250 5400
Wire Wire Line
	5250 5400 5250 5650
Wire Wire Line
	5250 5650 5350 5650
Connection ~ 5800 3550
Wire Wire Line
	5950 3550 5650 3550
Wire Wire Line
	5650 3550 5650 3450
Wire Wire Line
	5800 3550 5800 3300
Wire Wire Line
	5800 3300 6250 3300
Wire Wire Line
	6250 3300 6250 3350
Wire Wire Line
	5250 3250 5250 3000
Wire Wire Line
	5250 3250 5350 3250
Wire Wire Line
	6400 6200 6400 3000
Wire Wire Line
	6400 3000 5250 3000
Connection ~ 5650 3000
Connection ~ 7500 6200
Wire Wire Line
	8200 1850 8200 1900
Connection ~ 8200 1900
Wire Wire Line
	8200 1450 8200 1400
Connection ~ 8200 1400
Wire Wire Line
	9250 2100 9250 2150
Wire Wire Line
	8850 1400 9250 1400
Connection ~ 9250 1400
Wire Wire Line
	7650 5700 7650 6100
Wire Wire Line
	7650 6100 8300 6100
Wire Wire Line
	7050 5100 7750 5100
Wire Wire Line
	7750 5100 7750 5900
Wire Wire Line
	7750 5900 8300 5900
Wire Wire Line
	7050 4500 7850 4500
Wire Wire Line
	7850 4500 7850 5700
Wire Wire Line
	7850 5700 8300 5700
Wire Wire Line
	7050 3900 7950 3900
Wire Wire Line
	7950 3900 7950 5500
Wire Wire Line
	7950 5500 8300 5500
Wire Wire Line
	7050 3300 8050 3300
Wire Wire Line
	8050 3300 8050 5300
Wire Wire Line
	8050 5300 8300 5300
Wire Wire Line
	9000 5700 9000 5950
Connection ~ 9000 5700
Wire Wire Line
	9250 2150 10500 2150
Wire Wire Line
	6000 6300 6250 6300
Wire Wire Line
	6250 6300 6250 6150
Connection ~ 6250 6200
Wire Wire Line
	9000 6600 9000 6350
Connection ~ 9000 6600
Wire Wire Line
	9250 950  9250 850 
Wire Wire Line
	9250 850  5700 850 
Wire Wire Line
	4350 850  4400 850 
Wire Wire Line
	5250 850  5300 850 
Wire Wire Line
	3950 850  3850 850 
Connection ~ 3850 600 
Wire Wire Line
	7500 2600 7500 2150
Wire Wire Line
	3850 2050 3850 2600
Wire Wire Line
	9700 1700 9700 2150
Connection ~ 9700 2150
Wire Wire Line
	3850 2600 8700 2600
Wire Wire Line
	8700 2600 8700 3000
Wire Wire Line
	8700 3000 10300 3000
Wire Wire Line
	10300 3000 10500 3200
Connection ~ 7500 2600
Wire Wire Line
	10500 3200 10300 3400
Wire Wire Line
	10300 3400 10200 3400
Wire Wire Line
	10200 3400 10200 6850
Wire Wire Line
	10200 6850 3600 6850
Wire Wire Line
	3600 6850 3600 2600
Wire Wire Line
	3600 2600 2400 2600
Wire Wire Line
	4350 2050 4350 2600
Connection ~ 4350 2600
Wire Notes Line
	8950 4050 9200 3750
Wire Wire Line
	6650 1350 6700 1350
Wire Wire Line
	6500 1550 6500 2100
Wire Wire Line
	6500 2100 6200 2100
Wire Wire Line
	7150 1300 7150 1800
Wire Wire Line
	7150 1800 7350 1800
Wire Notes Line
	6350 1400 4800 1400
Wire Notes Line
	4800 1400 4750 1200
$Comp
L CHRISJ1_SWITCH11 SW1
U 1 1 507629A3
P 8500 5700
F 0 "SW1" H 8600 5050 50  0000 C CNN
F 1 "Cells_fine" H 8700 4950 50  0000 C CNN
	1    8500 5700
	-1   0    0    1   
$EndComp
NoConn ~ 7600 2150
NoConn ~ 7850 1800
NoConn ~ 7600 1650
$Comp
L C C2
U 1 1 5074E0FF
P 7150 1100
F 0 "C2" H 7200 1200 50  0000 L CNN
F 1 "100n" H 7200 1000 50  0000 L CNN
	1    7150 1100
	1    0    0    -1  
$EndComp
$Comp
L R_US R5
U 1 1 5074E0CC
P 6900 1350
F 0 "R5" V 6980 1350 50  0000 C CNN
F 1 "10k" V 6820 1350 50  0000 C CNN
	1    6900 1350
	0    -1   -1   0   
$EndComp
Text Notes 10600 3100 0    60   ~ 0
-
Text Notes 10600 2300 0    60   ~ 0
+
Text Notes 2200 2500 0    60   ~ 0
-
Text Notes 2200 750  0    60   ~ 0
+
Text Notes 4200 2500 1    60   ~ 0
reverse supply protection
Text Notes 4000 700  0    60   ~ 0
Diodes keep op-amp output voltage within active range
Text Notes 6300 1200 2    60   ~ 0
P2 adjusts short cct current 0-250mA\n(represents illumination and/or\nnumber of parallel solar cells.)
Text Notes 9950 1900 1    60   ~ 0
to prevent B-E junction\nreverse breakdown damage\nto BD140 (Q1)
Text Notes 8900 5350 1    60   ~ 0
to prevent B-E junction\nreverse breakdown damage\nto 2N2222 transistors
Text Notes 9300 1950 0    60   ~ 0
Heatsink\nrequired!
Text Notes 7550 1150 0    60   ~ 0
LED indicates compliance failure\n(insufficient supply voltage)
Text Notes 6650 1400 3    60   ~ 0
CW
Text Notes 2100 750  3    60   ~ 0
From DC Lab. Power Supply, 30V max.
Text Notes 10950 2150 3    60   ~ 0
To MPPT or curve tracer
$Comp
L CONN_1 P4
U 1 1 504EBF5B
P 2250 2600
F 0 "P4" H 2330 2600 40  0000 L CNN
F 1 "CONN_1" H 2250 2655 30  0001 C CNN
	1    2250 2600
	-1   0    0    -1  
$EndComp
$Comp
L CONN_1 P1
U 1 1 504EBF53
P 2250 600
F 0 "P1" H 2330 600 40  0000 L CNN
F 1 "CONN_1" H 2250 655 30  0001 C CNN
	1    2250 600 
	-1   0    0    -1  
$EndComp
$Comp
L CONN_1 P5
U 1 1 504EBF4D
P 10650 3200
F 0 "P5" H 10730 3200 40  0000 L CNN
F 1 "CONN_1" H 10650 3255 30  0001 C CNN
	1    10650 3200
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P3
U 1 1 504EBF45
P 10650 2150
F 0 "P3" H 10730 2150 40  0000 L CNN
F 1 "CONN_1" H 10650 2205 30  0001 C CNN
	1    10650 2150
	1    0    0    -1  
$EndComp
$Comp
L 1N400X D7
U 1 1 504EBEE7
P 4350 1850
F 0 "D7" H 4350 1950 50  0000 C CNN
F 1 "1N4001" H 4350 1750 50  0000 C CNN
	1    4350 1850
	0    -1   -1   0   
$EndComp
$Comp
L 1N400X D5
U 1 1 504EBE3D
P 9700 1500
F 0 "D5" H 9700 1600 50  0000 C CNN
F 1 "1N4001" H 9700 1400 50  0000 C CNN
	1    9700 1500
	0    -1   -1   0   
$EndComp
$Comp
L R_US R4
U 1 1 504EBDA4
P 6200 2350
F 0 "R4" V 6280 2350 50  0000 C CNN
F 1 "6k8" V 6120 2350 50  0000 C CNN
	1    6200 2350
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_TL431 U1
U 1 1 504EBD37
P 6200 1800
F 0 "U1" H 6300 1900 50  0000 C CNN
F 1 "TL431" H 6350 1700 40  0000 C CNN
	1    6200 1800
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_SWITCH_CENTREOFF SW2
U 1 1 504EBB68
P 5800 6400
F 0 "SW2" H 5600 6500 50  0000 C CNN
F 1 "cells_coarse" H 5650 6250 50  0000 C CNN
	1    5800 6400
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_TL081 U2
U 1 1 504E95E4
P 7600 1900
F 0 "U2" H 7650 2100 50  0000 L CNN
F 1 "TL081" H 7650 1800 50  0000 L CNN
	1    7600 1900
	1    0    0    -1  
$EndComp
$Comp
L R_US R2
U 1 1 504E8F35
P 8650 1400
F 0 "R2" V 8730 1400 50  0000 C CNN
F 1 "150R" V 8570 1400 50  0000 C CNN
	1    8650 1400
	0    -1   -1   0   
$EndComp
$Comp
L R_US R3
U 1 1 504E8F2B
P 8650 1900
F 0 "R3" V 8730 1900 50  0000 C CNN
F 1 "100R" V 8570 1900 50  0000 C CNN
	1    8650 1900
	0    -1   -1   0   
$EndComp
$Comp
L R_US R1
U 1 1 504E8ECF
P 9250 1150
F 0 "R1" V 9330 1150 50  0000 C CNN
F 1 "10R 3W" V 9150 1150 50  0000 C CNN
	1    9250 1150
	1    0    0    -1  
$EndComp
$Comp
L DIY_TRANSISTORS_PNP Q1
U 1 1 504E8E93
P 9150 1900
F 0 "Q1" H 9300 1700 50  0000 L CNN
F 1 "BD140" H 9550 1800 50  0000 R CNN
	1    9150 1900
	1    0    0    1   
$EndComp
$Comp
L 2N2222 Q6
U 1 1 504E87B3
P 4900 3550
F 0 "Q6" H 5050 3550 50  0000 L CNN
F 1 "2N2222" H 4900 3700 50  0000 R CNN
	1    4900 3550
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q3
U 1 1 504E87B2
P 4300 3250
F 0 "Q3" H 4450 3250 50  0000 L CNN
F 1 "2N2222" H 4300 3400 50  0000 R CNN
	1    4300 3250
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q30
U 1 1 504E87B1
P 4900 5950
F 0 "Q30" H 5050 5950 50  0000 L CNN
F 1 "2N2222" H 4900 6100 50  0000 R CNN
	1    4900 5950
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q27
U 1 1 504E87B0
P 4300 5650
F 0 "Q27" H 4450 5650 50  0000 L CNN
F 1 "2N2222" H 4300 5800 50  0000 R CNN
	1    4300 5650
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q24
U 1 1 504E87AF
P 4900 5350
F 0 "Q24" H 5050 5350 50  0000 L CNN
F 1 "2N2222" H 4900 5500 50  0000 R CNN
	1    4900 5350
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q21
U 1 1 504E87AE
P 4300 5050
F 0 "Q21" H 4450 5050 50  0000 L CNN
F 1 "2N2222" H 4300 5200 50  0000 R CNN
	1    4300 5050
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q18
U 1 1 504E87AD
P 4900 4750
F 0 "Q18" H 5050 4750 50  0000 L CNN
F 1 "2N2222" H 4900 4900 50  0000 R CNN
	1    4900 4750
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q15
U 1 1 504E87AC
P 4300 4450
F 0 "Q15" H 4450 4450 50  0000 L CNN
F 1 "2N2222" H 4300 4600 50  0000 R CNN
	1    4300 4450
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q12
U 1 1 504E87AB
P 4900 4150
F 0 "Q12" H 5050 4150 50  0000 L CNN
F 1 "2N2222" H 4900 4300 50  0000 R CNN
	1    4900 4150
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q9
U 1 1 504E87AA
P 4300 3850
F 0 "Q9" H 4450 3850 50  0000 L CNN
F 1 "2N2222" H 4300 4000 50  0000 R CNN
	1    4300 3850
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q10
U 1 1 504E8738
P 5550 3850
F 0 "Q10" H 5700 3850 50  0000 L CNN
F 1 "2N2222" H 5550 4000 50  0000 R CNN
	1    5550 3850
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q13
U 1 1 504E8737
P 6150 4150
F 0 "Q13" H 6300 4150 50  0000 L CNN
F 1 "2N2222" H 6150 4300 50  0000 R CNN
	1    6150 4150
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q16
U 1 1 504E8736
P 5550 4450
F 0 "Q16" H 5700 4450 50  0000 L CNN
F 1 "2N2222" H 5550 4600 50  0000 R CNN
	1    5550 4450
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q19
U 1 1 504E8735
P 6150 4750
F 0 "Q19" H 6300 4750 50  0000 L CNN
F 1 "2N2222" H 6150 4900 50  0000 R CNN
	1    6150 4750
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q22
U 1 1 504E8734
P 5550 5050
F 0 "Q22" H 5700 5050 50  0000 L CNN
F 1 "2N2222" H 5550 5200 50  0000 R CNN
	1    5550 5050
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q25
U 1 1 504E8733
P 6150 5350
F 0 "Q25" H 6300 5350 50  0000 L CNN
F 1 "2N2222" H 6150 5500 50  0000 R CNN
	1    6150 5350
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q28
U 1 1 504E8732
P 5550 5650
F 0 "Q28" H 5700 5650 50  0000 L CNN
F 1 "2N2222" H 5550 5800 50  0000 R CNN
	1    5550 5650
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q31
U 1 1 504E8731
P 6150 5950
F 0 "Q31" H 6300 5950 50  0000 L CNN
F 1 "2N2222" H 6150 6100 50  0000 R CNN
	1    6150 5950
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q4
U 1 1 504E8730
P 5550 3250
F 0 "Q4" H 5700 3250 50  0000 L CNN
F 1 "2N2222" H 5550 3400 50  0000 R CNN
	1    5550 3250
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q7
U 1 1 504E872F
P 6150 3550
F 0 "Q7" H 6300 3550 50  0000 L CNN
F 1 "2N2222" H 6150 3700 50  0000 R CNN
	1    6150 3550
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q8
U 1 1 504E86AE
P 7400 3550
F 0 "Q8" H 7550 3550 50  0000 L CNN
F 1 "2N2222" H 7400 3700 50  0000 R CNN
	1    7400 3550
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q5
U 1 1 504E86AD
P 6800 3250
F 0 "Q5" H 6950 3250 50  0000 L CNN
F 1 "2N2222" H 6800 3400 50  0000 R CNN
	1    6800 3250
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q32
U 1 1 504E86A7
P 7400 5950
F 0 "Q32" H 7550 5950 50  0000 L CNN
F 1 "2N2222" H 7400 6100 50  0000 R CNN
	1    7400 5950
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q29
U 1 1 504E86A6
P 6800 5650
F 0 "Q29" H 6950 5650 50  0000 L CNN
F 1 "2N2222" H 6800 5800 50  0000 R CNN
	1    6800 5650
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q26
U 1 1 504E86A1
P 7400 5350
F 0 "Q26" H 7550 5350 50  0000 L CNN
F 1 "2N2222" H 7400 5500 50  0000 R CNN
	1    7400 5350
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q23
U 1 1 504E86A0
P 6800 5050
F 0 "Q23" H 6950 5050 50  0000 L CNN
F 1 "2N2222" H 6800 5200 50  0000 R CNN
	1    6800 5050
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q20
U 1 1 504E869B
P 7400 4750
F 0 "Q20" H 7550 4750 50  0000 L CNN
F 1 "2N2222" H 7400 4900 50  0000 R CNN
	1    7400 4750
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q17
U 1 1 504E869A
P 6800 4450
F 0 "Q17" H 6950 4450 50  0000 L CNN
F 1 "2N2222" H 6800 4600 50  0000 R CNN
	1    6800 4450
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q14
U 1 1 504E868C
P 7400 4150
F 0 "Q14" H 7550 4150 50  0000 L CNN
F 1 "2N2222" H 7400 4300 50  0000 R CNN
	1    7400 4150
	1    0    0    -1  
$EndComp
$Comp
L 2N2222 Q11
U 1 1 504E868B
P 6800 3850
F 0 "Q11" H 6950 3850 50  0000 L CNN
F 1 "2N2222" H 6800 4000 50  0000 R CNN
	1    6800 3850
	1    0    0    -1  
$EndComp
$Comp
L 1N400X D8
U 1 1 504E80FA
P 9250 3600
F 0 "D8" H 9250 3700 50  0000 C CNN
F 1 "1N4001" H 9250 3500 50  0000 C CNN
	1    9250 3600
	0    1    1    0   
$EndComp
$Comp
L 1N400X D4
U 1 1 504E80F9
P 5500 850
F 0 "D4" H 5500 950 50  0000 C CNN
F 1 "1N4001" H 5500 750 50  0000 C CNN
	1    5500 850 
	1    0    0    -1  
$EndComp
$Comp
L 1N400X D3
U 1 1 504E80F2
P 5050 850
F 0 "D3" H 5050 950 50  0000 C CNN
F 1 "1N4001" H 5050 750 50  0000 C CNN
	1    5050 850 
	1    0    0    -1  
$EndComp
$Comp
L 1N400X D2
U 1 1 504E80EE
P 4600 850
F 0 "D2" H 4600 950 50  0000 C CNN
F 1 "1N4001" H 4600 750 50  0000 C CNN
	1    4600 850 
	1    0    0    -1  
$EndComp
$Comp
L CP_EU C1
U 1 1 504E7E3B
P 3850 1850
F 0 "C1" H 3900 1950 50  0000 L CNN
F 1 "330uF 35V" H 3350 1700 50  0000 L CNN
	1    3850 1850
	1    0    0    -1  
$EndComp
$Comp
L POT_US P2
U 1 1 504E7D69
P 6500 1350
F 0 "P2" H 6500 1200 50  0000 C CNN
F 1 "100k" H 6500 1270 50  0000 C CNN
	1    6500 1350
	0    1    1    0   
$EndComp
$Comp
L LED D6
U 1 1 504E7CEC
P 8200 1650
F 0 "D6" H 8200 1750 50  0000 C CNN
F 1 "LED" H 8200 1550 50  0000 C CNN
	1    8200 1650
	0    1    1    0   
$EndComp
$Comp
L 1N400X D1
U 1 1 504E7CE2
P 4150 850
F 0 "D1" H 4150 950 50  0000 C CNN
F 1 "1N4001" H 4150 750 50  0000 C CNN
	1    4150 850 
	1    0    0    -1  
$EndComp
$Comp
L 1N4148 D9
U 1 1 504E7CDA
P 9000 6150
F 0 "D9" H 9000 6250 50  0000 C CNN
F 1 "1N4148" H 9000 6050 50  0000 C CNN
	1    9000 6150
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
