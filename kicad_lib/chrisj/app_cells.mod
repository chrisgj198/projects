PCBNEW-LibModule-V1  11/02/2014 22:30:31
# encoding utf-8
Units mm
$INDEX
appcell_2000um_boardpads
appcell_2000um_cellmask
appcell_2000um_solderholes
appcell_2304p65um_asymm_boardpads
appcell_2304p65um_asymm_cellmask
appcell_2304p65um_asymm_solderholes
appcell_asymm
appcell_symm
clearance_hole_2mm
clearance_hole_5mm
head_clearance_M2
tapping_hole_M2
tooling_hole_2mm
tooling_hole_2mm_cu
$EndINDEX
$MODULE appcell_2000um_boardpads
Po 0 0 0 15 52DE08DC 00000000 ~~
Li appcell_2000um_boardpads
Sc 0
AR 
Op 0 0 0
T0 0 4.5 0.7 0.5 0 0.125 N V 21 N "appcell_2000um_asymm"
T1 -1.03 -4.64 1 1 0 0.15 N V 21 N "VAL**"
T2 0.1 -12.78 0.7 0.5 0 0.125 N V 21 N "line centre is blade centre"
DS 4.2 13.58 4.2 -13.58 0.16 21
DS 4.2 -13.58 -4.2 -13.58 0.16 21
DS -4.2 -13.58 -4.2 13.58 0.16 21
DS -4.2 13.58 4.2 13.58 0.16 21
$PAD
Sh "pf8" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1 9
$EndPAD
$PAD
Sh "pd8" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1 9
$EndPAD
$PAD
Sh "ph8" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3 9
$EndPAD
$PAD
Sh "pb8" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3 9
$EndPAD
$PAD
Sh "na7" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4 7
$EndPAD
$PAD
Sh "ng7" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2 7
$EndPAD
$PAD
Sh "nc7" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2 7
$EndPAD
$PAD
Sh "ne7" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 7
$EndPAD
$PAD
Sh "ni9" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4 11
$EndPAD
$PAD
Sh "ng9" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2 11
$EndPAD
$PAD
Sh "nc9" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2 11
$EndPAD
$PAD
Sh "ne9" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 11
$EndPAD
$PAD
Sh "pf2" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1 -9
$EndPAD
$PAD
Sh "pd2" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1 -9
$EndPAD
$PAD
Sh "ph2" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3 -9
$EndPAD
$PAD
Sh "pb2" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3 -9
$EndPAD
$PAD
Sh "na1" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4 -11
$EndPAD
$PAD
Sh "ng1" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2 -11
$EndPAD
$PAD
Sh "nc1" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2 -11
$EndPAD
$PAD
Sh "ne1" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -11
$EndPAD
$PAD
Sh "ni3" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4 -7
$EndPAD
$PAD
Sh "ng3" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2 -7
$EndPAD
$PAD
Sh "nc3" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2 -7
$EndPAD
$PAD
Sh "ne3" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -7
$EndPAD
$PAD
Sh "ne6" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 2
$EndPAD
$PAD
Sh "nc6" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2 2
$EndPAD
$PAD
Sh "ng6" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2 2
$EndPAD
$PAD
Sh "ni6" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4 2
$EndPAD
$PAD
Sh "ne4" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -2
$EndPAD
$PAD
Sh "nc4" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2 -2
$EndPAD
$PAD
Sh "ng4" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2 -2
$EndPAD
$PAD
Sh "na4" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4 -2
$EndPAD
$PAD
Sh "pb5" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3 0
$EndPAD
$PAD
Sh "ph5" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3 0
$EndPAD
$PAD
Sh "pd5" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1 0
$EndPAD
$PAD
Sh "pf5" R 0.4 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1 0
$EndPAD
$EndMODULE appcell_2000um_boardpads
$MODULE appcell_2000um_cellmask
Po 0 0 0 15 52DE097B 00000000 ~~
Li appcell_2000um_cellmask
Sc 0
AR 
Op 0 0 0
T0 0 4.5 0.7 0.5 0 0.125 N V 21 N "appcell_2000um_asymm"
T1 -1.03 -4.64 1 1 0 0.15 N V 21 N "VAL**"
T2 0.1 -12.78 0.7 0.5 0 0.125 N V 21 N "line centre is blade centre"
DS 4.2 13.58 4.2 -13.58 0.16 21
DS 4.2 -13.58 -4.2 -13.58 0.16 21
DS -4.2 -13.58 -4.2 13.58 0.16 21
DS -4.2 13.58 4.2 13.58 0.16 21
$PAD
Sh "pf8" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 1 9
$EndPAD
$PAD
Sh "pd8" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -1 9
$EndPAD
$PAD
Sh "ph8" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 3 9
$EndPAD
$PAD
Sh "pb8" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -3 9
$EndPAD
$PAD
Sh "na7" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -4 7
$EndPAD
$PAD
Sh "ng7" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 2 7
$EndPAD
$PAD
Sh "nc7" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -2 7
$EndPAD
$PAD
Sh "ne7" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 0 7
$EndPAD
$PAD
Sh "ni9" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 4 11
$EndPAD
$PAD
Sh "ng9" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 2 11
$EndPAD
$PAD
Sh "nc9" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -2 11
$EndPAD
$PAD
Sh "ne9" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 0 11
$EndPAD
$PAD
Sh "pf2" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 1 -9
$EndPAD
$PAD
Sh "pd2" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -1 -9
$EndPAD
$PAD
Sh "ph2" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 3 -9
$EndPAD
$PAD
Sh "pb2" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -3 -9
$EndPAD
$PAD
Sh "na1" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -4 -11
$EndPAD
$PAD
Sh "ng1" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 2 -11
$EndPAD
$PAD
Sh "nc1" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -2 -11
$EndPAD
$PAD
Sh "ne1" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 0 -11
$EndPAD
$PAD
Sh "ni3" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 4 -7
$EndPAD
$PAD
Sh "ng3" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 2 -7
$EndPAD
$PAD
Sh "nc3" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -2 -7
$EndPAD
$PAD
Sh "ne3" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 0 -7
$EndPAD
$PAD
Sh "ne6" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 0 2
$EndPAD
$PAD
Sh "nc6" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -2 2
$EndPAD
$PAD
Sh "ng6" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 2 2
$EndPAD
$PAD
Sh "ni6" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 4 2
$EndPAD
$PAD
Sh "ne4" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 0 -2
$EndPAD
$PAD
Sh "nc4" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -2 -2
$EndPAD
$PAD
Sh "ng4" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 2 -2
$EndPAD
$PAD
Sh "na4" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -4 -2
$EndPAD
$PAD
Sh "pb5" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -3 0
$EndPAD
$PAD
Sh "ph5" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 3 0
$EndPAD
$PAD
Sh "pd5" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -1 0
$EndPAD
$PAD
Sh "pf5" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 1 0
$EndPAD
$EndMODULE appcell_2000um_cellmask
$MODULE appcell_2000um_solderholes
Po 0 0 0 15 52DE0930 00000000 ~~
Li appcell_2000um_solderholes
Sc 0
AR 
Op 0 0 0
T0 0 4.5 0.7 0.5 0 0.125 N V 21 N "appcell_2000um_asymm"
T1 -1.03 -4.64 1 1 0 0.15 N V 21 N "VAL**"
T2 0.1 -12.78 0.7 0.5 0 0.125 N V 21 N "line centre is blade centre"
DS 4.2 13.58 4.2 -13.58 0.16 21
DS 4.2 -13.58 -4.2 -13.58 0.16 21
DS -4.2 -13.58 -4.2 13.58 0.16 21
DS -4.2 13.58 4.2 13.58 0.16 21
$PAD
Sh "pf8" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1 9
$EndPAD
$PAD
Sh "pd8" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1 9
$EndPAD
$PAD
Sh "ph8" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3 9
$EndPAD
$PAD
Sh "pb8" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3 9
$EndPAD
$PAD
Sh "na7" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -4 7
$EndPAD
$PAD
Sh "ng7" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2 7
$EndPAD
$PAD
Sh "nc7" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2 7
$EndPAD
$PAD
Sh "ne7" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 7
$EndPAD
$PAD
Sh "ni9" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 4 11
$EndPAD
$PAD
Sh "ng9" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2 11
$EndPAD
$PAD
Sh "nc9" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2 11
$EndPAD
$PAD
Sh "ne9" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 11
$EndPAD
$PAD
Sh "pf2" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1 -9
$EndPAD
$PAD
Sh "pd2" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1 -9
$EndPAD
$PAD
Sh "ph2" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3 -9
$EndPAD
$PAD
Sh "pb2" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3 -9
$EndPAD
$PAD
Sh "na1" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -4 -11
$EndPAD
$PAD
Sh "ng1" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2 -11
$EndPAD
$PAD
Sh "nc1" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2 -11
$EndPAD
$PAD
Sh "ne1" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 -11
$EndPAD
$PAD
Sh "ni3" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 4 -7
$EndPAD
$PAD
Sh "ng3" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2 -7
$EndPAD
$PAD
Sh "nc3" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2 -7
$EndPAD
$PAD
Sh "ne3" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 -7
$EndPAD
$PAD
Sh "ne6" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 2
$EndPAD
$PAD
Sh "nc6" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2 2
$EndPAD
$PAD
Sh "ng6" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2 2
$EndPAD
$PAD
Sh "ni6" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 4 2
$EndPAD
$PAD
Sh "ne4" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 -2
$EndPAD
$PAD
Sh "nc4" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2 -2
$EndPAD
$PAD
Sh "ng4" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2 -2
$EndPAD
$PAD
Sh "na4" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -4 -2
$EndPAD
$PAD
Sh "pb5" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3 0
$EndPAD
$PAD
Sh "ph5" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3 0
$EndPAD
$PAD
Sh "pd5" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1 0
$EndPAD
$PAD
Sh "pf5" C 1.5 1.5 0 0 0
Dr 1.1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1 0
$EndPAD
$EndMODULE appcell_2000um_solderholes
$MODULE appcell_2304p65um_asymm_boardpads
Po 0 0 0 15 52F07D81 00000000 ~~
Li appcell_2304p65um_asymm_boardpads
Sc 0
AR 
Op 0 0 0
T0 0.03 4.69 1 1 0 0.15 N V 21 N "2304.65um"
T1 -1.03 -4.64 1 1 0 0.15 N V 21 N "VAL**"
DS 4.83 13.32 4.83 -13.32 0.01 21
DS 4.83 -13.32 -4.83 -13.32 0.01 21
DS -4.83 -13.32 -4.83 13.32 0.01 21
DS -4.83 13.32 4.83 13.32 0.01 21
$PAD
Sh "pf8" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.152325 9.17
$EndPAD
$PAD
Sh "pd8" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.152325 9.17
$EndPAD
$PAD
Sh "ph8" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.456975 9.17
$EndPAD
$PAD
Sh "pb8" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.456975 9.17
$EndPAD
$PAD
Sh "ni7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.6093 7.17
$EndPAD
$PAD
Sh "na7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.6093 8.17
$EndPAD
$PAD
Sh "ng7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.30465 7.17
$EndPAD
$PAD
Sh "nc7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.30465 7.17
$EndPAD
$PAD
Sh "ne7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 7.17
$EndPAD
$PAD
Sh "ni9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.6093 11.17
$EndPAD
$PAD
Sh "na9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.6093 10.17
$EndPAD
$PAD
Sh "ng9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.30465 11.17
$EndPAD
$PAD
Sh "nc9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.30465 11.17
$EndPAD
$PAD
Sh "ne9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 11.17
$EndPAD
$PAD
Sh "pf2" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.152325 -9.17
$EndPAD
$PAD
Sh "pd2" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.152325 -9.17
$EndPAD
$PAD
Sh "ph2" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.456975 -9.17
$EndPAD
$PAD
Sh "pb2" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.456975 -9.17
$EndPAD
$PAD
Sh "ni1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.6093 -11.17
$EndPAD
$PAD
Sh "na1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.6093 -10.17
$EndPAD
$PAD
Sh "ng1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.30465 -11.17
$EndPAD
$PAD
Sh "nc1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.30465 -11.17
$EndPAD
$PAD
Sh "ne1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -11.17
$EndPAD
$PAD
Sh "ni3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.6093 -7.17
$EndPAD
$PAD
Sh "na3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.6093 -8.17
$EndPAD
$PAD
Sh "ng3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.30465 -7.17
$EndPAD
$PAD
Sh "nc3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.30465 -7.17
$EndPAD
$PAD
Sh "ne3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -7.17
$EndPAD
$PAD
Sh "ne6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 2
$EndPAD
$PAD
Sh "nc6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.30465 2
$EndPAD
$PAD
Sh "ng6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.30465 2
$EndPAD
$PAD
Sh "na6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.6093 1
$EndPAD
$PAD
Sh "ni6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.6093 2
$EndPAD
$PAD
Sh "ne4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -2
$EndPAD
$PAD
Sh "nc4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.30465 -2
$EndPAD
$PAD
Sh "ng4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.30465 -2
$EndPAD
$PAD
Sh "na4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.6093 -1
$EndPAD
$PAD
Sh "ni4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.6093 -2
$EndPAD
$PAD
Sh "pb5" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.456975 0
$EndPAD
$PAD
Sh "ph5" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.456975 0
$EndPAD
$PAD
Sh "pd5" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.152325 0
$EndPAD
$PAD
Sh "pf5" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.152325 0
$EndPAD
$EndMODULE appcell_2304p65um_asymm_boardpads
$MODULE appcell_2304p65um_asymm_cellmask
Po 0 0 0 15 52F07F90 00000000 ~~
Li appcell_2304p65um_asymm_cellmask
Sc 0
AR 
Op 0 0 0
T0 0.03 4.69 1 1 0 0.15 N V 21 N "2304.65um"
T1 -1.03 -4.64 1 1 0 0.15 N V 21 N "VAL**"
DS 4.83 13.32 4.83 -13.32 0.01 21
DS 4.83 -13.32 -4.83 -13.32 0.01 21
DS -4.83 -13.32 -4.83 13.32 0.01 21
DS -4.83 13.32 4.83 13.32 0.01 21
$PAD
Sh "pf8" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 1.152325 9.17
$EndPAD
$PAD
Sh "pd8" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -1.152325 9.17
$EndPAD
$PAD
Sh "ph8" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 3.456975 9.17
$EndPAD
$PAD
Sh "pb8" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -3.456975 9.17
$EndPAD
$PAD
Sh "ni7" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 4.6093 7.17
$EndPAD
$PAD
Sh "na7" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -4.6093 8.17
$EndPAD
$PAD
Sh "ng7" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 2.30465 7.17
$EndPAD
$PAD
Sh "nc7" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -2.30465 7.17
$EndPAD
$PAD
Sh "ne7" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 0 7.17
$EndPAD
$PAD
Sh "ni9" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 4.6093 11.17
$EndPAD
$PAD
Sh "na9" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -4.6093 10.17
$EndPAD
$PAD
Sh "ng9" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 2.30465 11.17
$EndPAD
$PAD
Sh "nc9" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -2.30465 11.17
$EndPAD
$PAD
Sh "ne9" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 0 11.17
$EndPAD
$PAD
Sh "pf2" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 1.152325 -9.17
$EndPAD
$PAD
Sh "pd2" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -1.152325 -9.17
$EndPAD
$PAD
Sh "ph2" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 3.456975 -9.17
$EndPAD
$PAD
Sh "pb2" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -3.456975 -9.17
$EndPAD
$PAD
Sh "ni1" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 4.6093 -11.17
$EndPAD
$PAD
Sh "na1" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -4.6093 -10.17
$EndPAD
$PAD
Sh "ng1" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 2.30465 -11.17
$EndPAD
$PAD
Sh "nc1" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -2.30465 -11.17
$EndPAD
$PAD
Sh "ne1" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 0 -11.17
$EndPAD
$PAD
Sh "ni3" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 4.6093 -7.17
$EndPAD
$PAD
Sh "na3" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -4.6093 -8.17
$EndPAD
$PAD
Sh "ng3" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 2.30465 -7.17
$EndPAD
$PAD
Sh "nc3" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -2.30465 -7.17
$EndPAD
$PAD
Sh "ne3" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 0 -7.17
$EndPAD
$PAD
Sh "ne6" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 0 2
$EndPAD
$PAD
Sh "nc6" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -2.30465 2
$EndPAD
$PAD
Sh "ng6" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 2.30465 2
$EndPAD
$PAD
Sh "na6" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -4.6093 1
$EndPAD
$PAD
Sh "ni6" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 4.6093 2
$EndPAD
$PAD
Sh "ne4" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 0 -2
$EndPAD
$PAD
Sh "nc4" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -2.30465 -2
$EndPAD
$PAD
Sh "ng4" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 2.30465 -2
$EndPAD
$PAD
Sh "na4" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -4.6093 -1
$EndPAD
$PAD
Sh "ni4" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 4.6093 -2
$EndPAD
$PAD
Sh "pb5" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -3.456975 0
$EndPAD
$PAD
Sh "ph5" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 3.456975 0
$EndPAD
$PAD
Sh "pd5" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po -1.152325 0
$EndPAD
$PAD
Sh "pf5" R 0.6 1 0 0 0
Dr 0 0 0
At SMD N 00000001
Ne 0 ""
Po 1.152325 0
$EndPAD
$EndMODULE appcell_2304p65um_asymm_cellmask
$MODULE appcell_2304p65um_asymm_solderholes
Po 0 0 0 15 52F42F44 00000000 ~~
Li appcell_2304p65um_asymm_solderholes
Sc 0
AR 
Op 0 0 0
T0 0.03 4.69 1 1 0 0.15 N V 21 N "2304.65um"
T1 -1.03 -4.64 1 1 0 0.15 N V 21 N "VAL**"
DS 4.83 13.32 4.83 -13.32 0.01 21
DS 4.83 -13.32 -4.83 -13.32 0.01 21
DS -4.83 -13.32 -4.83 13.32 0.01 21
DS -4.83 13.32 4.83 13.32 0.01 21
$PAD
Sh "pf8" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 1.152325 9.17
$EndPAD
$PAD
Sh "pd8" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -1.152325 9.17
$EndPAD
$PAD
Sh "ph8" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 3.456975 9.17
$EndPAD
$PAD
Sh "pb8" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -3.456975 9.17
$EndPAD
$PAD
Sh "ni7" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 4.6093 7.17
$EndPAD
$PAD
Sh "na7" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -4.6093 8.17
$EndPAD
$PAD
Sh "ng7" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 2.30465 7.17
$EndPAD
$PAD
Sh "nc7" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -2.30465 7.17
$EndPAD
$PAD
Sh "ne7" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 0 7.17
$EndPAD
$PAD
Sh "ni9" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 4.6093 11.17
$EndPAD
$PAD
Sh "na9" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -4.6093 10.17
$EndPAD
$PAD
Sh "ng9" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 2.30465 11.17
$EndPAD
$PAD
Sh "nc9" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -2.30465 11.17
$EndPAD
$PAD
Sh "ne9" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 0 11.17
$EndPAD
$PAD
Sh "pf2" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 1.152325 -9.17
$EndPAD
$PAD
Sh "pd2" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -1.152325 -9.17
$EndPAD
$PAD
Sh "ph2" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 3.456975 -9.17
$EndPAD
$PAD
Sh "pb2" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -3.456975 -9.17
$EndPAD
$PAD
Sh "ni1" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 4.6093 -11.17
$EndPAD
$PAD
Sh "na1" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -4.6093 -10.17
$EndPAD
$PAD
Sh "ng1" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 2.30465 -11.17
$EndPAD
$PAD
Sh "nc1" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -2.30465 -11.17
$EndPAD
$PAD
Sh "ne1" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 0 -11.17
$EndPAD
$PAD
Sh "ni3" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 4.6093 -7.17
$EndPAD
$PAD
Sh "na3" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -4.6093 -8.17
$EndPAD
$PAD
Sh "ng3" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 2.30465 -7.17
$EndPAD
$PAD
Sh "nc3" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -2.30465 -7.17
$EndPAD
$PAD
Sh "ne3" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 0 -7.17
$EndPAD
$PAD
Sh "ne6" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 0 2
$EndPAD
$PAD
Sh "nc6" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -2.30465 2
$EndPAD
$PAD
Sh "ng6" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 2.30465 2
$EndPAD
$PAD
Sh "na6" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -4.6093 1
$EndPAD
$PAD
Sh "ni6" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 4.6093 2
$EndPAD
$PAD
Sh "ne4" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 0 -2
$EndPAD
$PAD
Sh "nc4" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -2.30465 -2
$EndPAD
$PAD
Sh "ng4" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 2.30465 -2
$EndPAD
$PAD
Sh "na4" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -4.6093 -1
$EndPAD
$PAD
Sh "ni4" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 4.6093 -2
$EndPAD
$PAD
Sh "pb5" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -3.456975 0
$EndPAD
$PAD
Sh "ph5" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 3.456975 0
$EndPAD
$PAD
Sh "pd5" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po -1.152325 0
$EndPAD
$PAD
Sh "pf5" C 0.9 0.9 0 0 0
Dr 0.85 0 0
At STD N 0000FFFF
Ne 0 ""
Po 1.152325 0
$EndPAD
$EndMODULE appcell_2304p65um_asymm_solderholes
$MODULE appcell_asymm
Po 0 0 0 15 5296E140 00000000 ~~
Li appcell_asymm
Sc 0
AR 
Op 0 0 0
T0 0.03 4.69 1 1 0 0.15 N V 21 N "appcell_symm"
T1 -1.03 -4.64 1 1 0 0.15 N V 21 N "VAL**"
DS 4.83 13.32 4.83 -13.32 0.01 21
DS 4.83 -13.32 -4.83 -13.32 0.01 21
DS -4.83 -13.32 -4.83 13.32 0.01 21
DS -4.83 13.32 4.83 13.32 0.01 21
$PAD
Sh "pf8" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.16 9.17
$EndPAD
$PAD
Sh "pd8" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.16 9.17
$EndPAD
$PAD
Sh "ph8" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.48 9.17
$EndPAD
$PAD
Sh "pb8" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.48 9.17
$EndPAD
$PAD
Sh "ni7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.64 7.17
$EndPAD
$PAD
Sh "na7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.64 8.17
$EndPAD
$PAD
Sh "ng7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.32 7.17
$EndPAD
$PAD
Sh "nc7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.32 7.17
$EndPAD
$PAD
Sh "ne7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 7.17
$EndPAD
$PAD
Sh "ni9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.64 11.17
$EndPAD
$PAD
Sh "na9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.64 10.17
$EndPAD
$PAD
Sh "ng9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.32 11.17
$EndPAD
$PAD
Sh "nc9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.32 11.17
$EndPAD
$PAD
Sh "ne9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 11.17
$EndPAD
$PAD
Sh "pf2" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.16 -9.17
$EndPAD
$PAD
Sh "pd2" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.16 -9.17
$EndPAD
$PAD
Sh "ph2" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.48 -9.17
$EndPAD
$PAD
Sh "pb2" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.48 -9.17
$EndPAD
$PAD
Sh "ni1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.64 -11.17
$EndPAD
$PAD
Sh "na1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.64 -10.17
$EndPAD
$PAD
Sh "ng1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.32 -11.17
$EndPAD
$PAD
Sh "nc1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.32 -11.17
$EndPAD
$PAD
Sh "ne1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -11.17
$EndPAD
$PAD
Sh "ni3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.64 -7.17
$EndPAD
$PAD
Sh "na3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.64 -8.17
$EndPAD
$PAD
Sh "ng3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.32 -7.17
$EndPAD
$PAD
Sh "nc3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.32 -7.17
$EndPAD
$PAD
Sh "ne3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -7.17
$EndPAD
$PAD
Sh "ne6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 2
$EndPAD
$PAD
Sh "nc6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.32 2
$EndPAD
$PAD
Sh "ng6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.32 2
$EndPAD
$PAD
Sh "na6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.64 1
$EndPAD
$PAD
Sh "ni6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.64 2
$EndPAD
$PAD
Sh "ne4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -2
$EndPAD
$PAD
Sh "nc4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.32 -2
$EndPAD
$PAD
Sh "ng4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.32 -2
$EndPAD
$PAD
Sh "na4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.64 -1
$EndPAD
$PAD
Sh "ni4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.64 -2
$EndPAD
$PAD
Sh "pb5" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.48 0
$EndPAD
$PAD
Sh "ph5" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.48 0
$EndPAD
$PAD
Sh "pd5" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.16 0
$EndPAD
$PAD
Sh "pf5" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.16 0
$EndPAD
$EndMODULE appcell_asymm
$MODULE appcell_symm
Po 0 0 0 15 5296E140 00000000 ~~
Li appcell_symm
Sc 0
AR 
Op 0 0 0
T0 0.03 4.69 1 1 0 0.15 N V 21 N "appcell_symm"
T1 -1.03 -4.64 1 1 0 0.15 N V 21 N "VAL**"
DS 4.83 13.32 4.83 -13.32 0.01 21
DS 4.83 -13.32 -4.83 -13.32 0.01 21
DS -4.83 -13.32 -4.83 13.32 0.01 21
DS -4.83 13.32 4.83 13.32 0.01 21
$PAD
Sh "pf8" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.16 9.17
$EndPAD
$PAD
Sh "pd8" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.16 9.17
$EndPAD
$PAD
Sh "ph8" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.48 9.17
$EndPAD
$PAD
Sh "pb8" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.48 9.17
$EndPAD
$PAD
Sh "ni7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.64 7.17
$EndPAD
$PAD
Sh "na7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.64 7.17
$EndPAD
$PAD
Sh "ng7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.32 7.17
$EndPAD
$PAD
Sh "nc7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.32 7.17
$EndPAD
$PAD
Sh "ne7" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 7.17
$EndPAD
$PAD
Sh "ni9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.64 11.17
$EndPAD
$PAD
Sh "na9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.64 11.17
$EndPAD
$PAD
Sh "ng9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.32 11.17
$EndPAD
$PAD
Sh "nc9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.32 11.17
$EndPAD
$PAD
Sh "ne9" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 11.17
$EndPAD
$PAD
Sh "pf2" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.16 -9.17
$EndPAD
$PAD
Sh "pd2" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.16 -9.17
$EndPAD
$PAD
Sh "ph2" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.48 -9.17
$EndPAD
$PAD
Sh "pb2" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.48 -9.17
$EndPAD
$PAD
Sh "ni1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.64 -11.17
$EndPAD
$PAD
Sh "na1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.64 -11.17
$EndPAD
$PAD
Sh "ng1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.32 -11.17
$EndPAD
$PAD
Sh "nc1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.32 -11.17
$EndPAD
$PAD
Sh "ne1" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -11.17
$EndPAD
$PAD
Sh "ni3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.64 -7.17
$EndPAD
$PAD
Sh "na3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.64 -7.17
$EndPAD
$PAD
Sh "ng3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.32 -7.17
$EndPAD
$PAD
Sh "nc3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.32 -7.17
$EndPAD
$PAD
Sh "ne3" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -7.17
$EndPAD
$PAD
Sh "ne6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 2
$EndPAD
$PAD
Sh "nc6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.32 2
$EndPAD
$PAD
Sh "ng6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.32 2
$EndPAD
$PAD
Sh "na6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.64 2
$EndPAD
$PAD
Sh "ni6" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.64 2
$EndPAD
$PAD
Sh "ne4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -2
$EndPAD
$PAD
Sh "nc4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.32 -2
$EndPAD
$PAD
Sh "ng4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.32 -2
$EndPAD
$PAD
Sh "na4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.64 -2
$EndPAD
$PAD
Sh "ni4" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.64 -2
$EndPAD
$PAD
Sh "pb5" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.48 0
$EndPAD
$PAD
Sh "ph5" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.48 0
$EndPAD
$PAD
Sh "pd5" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.16 0
$EndPAD
$PAD
Sh "pf5" R 0.39 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.16 0
$EndPAD
$EndMODULE appcell_symm
$MODULE clearance_hole_2mm
Po 0 0 0 15 52DF623F 00000000 ~~
Li clearance_hole_2mm
Sc 0
AR 
Op 0 0 0
T0 0 -2.5 1 1 0 0.15 N I 21 N "clearance_hole_2mm"
T1 0 3 1 1 0 0.15 N I 21 N "VAL**"
$PAD
Sh "" C 2 2 0 0 0
Dr 2 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE clearance_hole_2mm
$MODULE clearance_hole_5mm
Po 0 0 0 15 52FA09CE 00000000 ~~
Li clearance_hole_5mm
Sc 0
AR 
Op 0 0 0
T0 0 -2.5 1 1 0 0.15 N I 21 N "clearance_hole_5mm"
T1 0 3 1 1 0 0.15 N I 21 N "VAL**"
$PAD
Sh "" C 5 5 0 0 0
Dr 5 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE clearance_hole_5mm
$MODULE head_clearance_M2
Po 0 0 0 15 52E04452 00000000 ~~
Li head_clearance_M2
Sc 0
AR 
Op 0 0 0
T0 0 -2.5 1 1 0 0.15 N I 21 N "head_clearance_M2"
T1 0 3 1 1 0 0.15 N I 21 N "VAL**"
$PAD
Sh "" C 4 4 0 0 0
Dr 4 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE head_clearance_M2
$MODULE tapping_hole_M2
Po 0 0 0 15 52DF6219 00000000 ~~
Li tapping_hole_M2
Sc 0
AR 
Op 0 0 0
T0 0 -2.5 1 1 0 0.15 N I 21 N "tapping_hole_M2"
T1 0 3 1 1 0 0.15 N I 21 N "VAL**"
$PAD
Sh "" C 1.6 1.6 0 0 0
Dr 1.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE tapping_hole_M2
$MODULE tooling_hole_2mm
Po 0 0 0 15 52DF1447 00000000 ~~
Li tooling_hole_2mm
Sc 0
AR 
Op 0 0 0
T0 0 -2.5 1 1 0 0.15 N I 21 N "tooling_hole_2mm"
T1 0 3 1 1 0 0.15 N I 21 N "VAL**"
$PAD
Sh "" C 2 2 0 0 0
Dr 2 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE tooling_hole_2mm
$MODULE tooling_hole_2mm_cu
Po 0 0 0 15 52DF35F0 00000000 ~~
Li tooling_hole_2mm_cu
Sc 0
AR 
Op 0 0 0
T0 0 -2.5 1 1 0 0.15 N I 21 N "tooling_hole_2mm"
T1 0 3 1 1 0 0.15 N I 21 N "VAL**"
$PAD
Sh "" C 2 2 0 0 0
Dr 2 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE tooling_hole_2mm_cu
$EndLIBRARY
