EESchema Schematic File Version 2  date 24/09/2013 3:54:59 PM
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:chrisj1
LIBS:diy_connectors
LIBS:diy_diodes
LIBS:diy_ics
LIBS:diy_rcl
LIBS:diy_regulators
LIBS:diy_switches
LIBS:diy_transistors
LIBS:ctd_licharger-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "24 sep 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR01
U 1 1 52252D77
P 3850 3250
F 0 "#PWR01" H 3850 3250 30  0001 C CNN
F 1 "GND" H 3850 3180 30  0001 C CNN
F 2 "" H 3850 3250 60  0000 C CNN
F 3 "" H 3850 3250 60  0000 C CNN
	1    3850 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 3100 3850 3250
$Comp
L CHRISJ1_R R3
U 1 1 522535D6
P 5700 3250
F 0 "R3" V 5780 3250 50  0000 C CNN
F 1 "10k" V 5620 3250 50  0000 C CNN
F 2 "" H 5700 3250 60  0000 C CNN
F 3 "" H 5700 3250 60  0000 C CNN
	1    5700 3250
	0    -1   -1   0   
$EndComp
$Comp
L CHRISJ1_POLYSWITCH F1
U 1 1 522536C0
P 6000 3650
F 0 "F1" V 6080 3650 50  0000 C CNN
F 1 "1.5A" V 5920 3650 50  0000 C CNN
F 2 "" H 6000 3650 60  0000 C CNN
F 3 "" H 6000 3650 60  0000 C CNN
	1    6000 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	5300 3650 5900 3650
$Comp
L CHRISJ1_C C1
U 1 1 52253995
P 3850 3050
F 0 "C1" V 3930 3050 50  0000 C CNN
F 1 "1uF" V 3770 3050 50  0000 C CNN
F 2 "" H 3850 3050 60  0000 C CNN
F 3 "" H 3850 3050 60  0000 C CNN
	1    3850 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 3000 3850 2750
Connection ~ 3850 2350
Wire Wire Line
	3650 2350 5850 2350
$Comp
L CONN_1 P4
U 1 1 52253A6A
P 6300 3650
F 0 "P4" H 6380 3650 40  0000 L CNN
F 1 "CONN_1" H 6300 3705 30  0001 C CNN
F 2 "" H 6300 3650 60  0000 C CNN
F 3 "" H 6300 3650 60  0000 C CNN
	1    6300 3650
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P5
U 1 1 52253A79
P 6300 3900
F 0 "P5" H 6380 3900 40  0000 L CNN
F 1 "CONN_1" H 6300 3955 30  0001 C CNN
F 2 "" H 6300 3900 60  0000 C CNN
F 3 "" H 6300 3900 60  0000 C CNN
	1    6300 3900
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P1
U 1 1 52253A88
P 3500 2350
F 0 "P1" H 3580 2350 40  0000 L CNN
F 1 "CONN_1" H 3500 2405 30  0001 C CNN
F 2 "" H 3500 2350 60  0000 C CNN
F 3 "" H 3500 2350 60  0000 C CNN
	1    3500 2350
	-1   0    0    -1  
$EndComp
$Comp
L CONN_1 P2
U 1 1 52253A97
P 3500 3150
F 0 "P2" H 3580 3150 40  0000 L CNN
F 1 "CONN_1" H 3500 3205 30  0001 C CNN
F 2 "" H 3500 3150 60  0000 C CNN
F 3 "" H 3500 3150 60  0000 C CNN
	1    3500 3150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3850 3150 3650 3150
Connection ~ 3850 3150
Wire Wire Line
	6100 3650 6150 3650
$Comp
L GND #PWR02
U 1 1 52253AC6
P 6000 3950
F 0 "#PWR02" H 6000 3950 30  0001 C CNN
F 1 "GND" H 6000 3880 30  0001 C CNN
F 2 "" H 6000 3950 60  0000 C CNN
F 3 "" H 6000 3950 60  0000 C CNN
	1    6000 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3950 6000 3900
Wire Wire Line
	6000 3900 6150 3900
Text Label 3700 2350 0    40   ~ 0
VIN
Text Label 6100 3650 0    40   ~ 0
VBAT
Text Label 5400 3650 0    40   ~ 0
VBATINT
$Comp
L GND #PWR03
U 1 1 5225414D
P 4900 4000
F 0 "#PWR03" H 4900 4000 30  0001 C CNN
F 1 "GND" H 4900 3930 30  0001 C CNN
F 2 "" H 4900 4000 60  0000 C CNN
F 3 "" H 4900 4000 60  0000 C CNN
	1    4900 4000
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_LTC4052 U1
U 1 1 5241169D
P 4950 3400
F 0 "U1" H 5050 3050 60  0000 C CNN
F 1 "CHRISJ1_LTC4052" H 4900 3400 60  0000 C CNN
F 2 "~" H 4950 3400 60  0000 C CNN
F 3 "~" H 4950 3400 60  0000 C CNN
	1    4950 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2350 4950 3000
Wire Wire Line
	4900 3800 4900 4000
Connection ~ 4900 3900
$Comp
L GND #PWR04
U 1 1 52411EB4
P 4600 4050
F 0 "#PWR04" H 4600 4050 30  0001 C CNN
F 1 "GND" H 4600 3980 30  0001 C CNN
F 2 "" H 4600 4050 60  0000 C CNN
F 3 "" H 4600 4050 60  0000 C CNN
	1    4600 4050
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_C C2
U 1 1 52411EBB
P 4600 3950
F 0 "C2" V 4680 3950 50  0000 C CNN
F 1 "100nF" V 4520 3950 50  0000 C CNN
F 2 "" H 4600 3950 60  0000 C CNN
F 3 "" H 4600 3950 60  0000 C CNN
	1    4600 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3800 4800 3900
Wire Wire Line
	4800 3900 4900 3900
Wire Wire Line
	5850 3550 5300 3550
Connection ~ 4950 2350
$Comp
L CHRISJ1_C C3
U 1 1 524123D9
P 5350 3850
F 0 "C3" V 5430 3850 50  0000 C CNN
F 1 "47nF" V 5270 3850 50  0000 C CNN
F 2 "" H 5350 3850 60  0000 C CNN
F 3 "" H 5350 3850 60  0000 C CNN
	1    5350 3850
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_C C4
U 1 1 52412412
P 5500 3250
F 0 "C4" V 5580 3250 50  0000 C CNN
F 1 "47nF" V 5420 3250 50  0000 C CNN
F 2 "" H 5500 3250 60  0000 C CNN
F 3 "" H 5500 3250 60  0000 C CNN
	1    5500 3250
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR05
U 1 1 52412424
P 5350 4050
F 0 "#PWR05" H 5350 4050 30  0001 C CNN
F 1 "GND" H 5350 3980 30  0001 C CNN
F 2 "" H 5350 4050 60  0000 C CNN
F 3 "" H 5350 4050 60  0000 C CNN
	1    5350 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3900 5350 4050
Wire Wire Line
	5300 3250 5450 3250
Wire Wire Line
	5550 3250 5600 3250
Wire Wire Line
	5800 3250 5850 3250
Wire Wire Line
	5850 2350 5850 3550
Wire Wire Line
	5350 3800 5350 3250
Connection ~ 5350 3250
Wire Wire Line
	4600 4050 4600 4000
Wire Wire Line
	4600 3900 4600 3850
Wire Wire Line
	4600 3850 4700 3850
Wire Wire Line
	4700 3850 4700 3800
Text Label 4600 3850 0    39   ~ 0
TMR
Text Label 5300 3250 0    39   ~ 0
GATE
$Comp
L CHRISJ1_R R2
U 1 1 5241260E
P 4300 3000
F 0 "R2" V 4380 3000 50  0000 C CNN
F 1 "1k" V 4220 3000 50  0000 C CNN
F 2 "" H 4300 3000 60  0000 C CNN
F 3 "" H 4300 3000 60  0000 C CNN
	1    4300 3000
	-1   0    0    1   
$EndComp
NoConn ~ 4500 3250
Wire Wire Line
	4500 3150 4300 3150
Wire Wire Line
	4300 3150 4300 3100
$Comp
L CONN_1 P3
U 1 1 524126AB
P 4300 2700
F 0 "P3" H 4380 2700 40  0000 L CNN
F 1 "CONN_1" H 4300 2755 30  0001 C CNN
F 2 "" H 4300 2700 60  0000 C CNN
F 3 "" H 4300 2700 60  0000 C CNN
	1    4300 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4300 2900 4300 2850
Text Notes 4100 2800 0    39   ~ 0
to Vin via LED
Connection ~ 5850 3250
$Comp
L CHRISJ1_R R1
U 1 1 52412773
P 3850 2650
F 0 "R1" V 3930 2650 50  0000 C CNN
F 1 "4R7" V 3770 2650 50  0000 C CNN
F 2 "" H 3850 2650 60  0000 C CNN
F 3 "" H 3850 2650 60  0000 C CNN
	1    3850 2650
	-1   0    0    1   
$EndComp
Wire Wire Line
	3850 2550 3850 2350
$Comp
L CHRISJ1_ZENER2 D1
U 1 1 52412964
P 3700 2750
F 0 "D1" V 3600 2850 50  0000 C CNN
F 1 "SMBJ6.0A" V 3700 3000 50  0000 C CNN
F 2 "~" H 3700 2750 60  0000 C CNN
F 3 "~" H 3700 2750 60  0000 C CNN
	1    3700 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3700 2650 3700 2350
Connection ~ 3700 2350
Wire Wire Line
	3700 2850 3700 3150
Connection ~ 3700 3150
$EndSCHEMATC
