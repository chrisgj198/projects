#include <stddef.h>
#include <stdint.h>
#include <xc.h>

/*
Fan PWM pins require 25kHz square wave with variable duty cycle. To start, need >30% duty cycle.
Fan tach outputs produce 2 pulses per revolution, up to 6400rpm typical (106.6 revs/s, 213.3Hz),
duty cycle 40%...60% so 1875us minimum high and low periods.

Hardware pinout (20 pin package):
 1 VDD

19 RA0/ICSPDAT			:OUTPUT		:0	:	ICSP
18 RA1/ICSPCLK			:OUTPUT		:0	:	ICSP
17 RA2/CCP3				:OUTPUT		:1	:	Fan 2 (far end) PWM
 4 RA3/MCLR/VPP			:OUTPUT		:0	:	ICSP
 3 RA4/AN3				:ANALOGUE	:-	:	Thermistor_0
 2 RA5/CCP2/P2A 		:INPUT		:-	:	Fan 2 (far end) tach

13 RB4/AN10				:ANALOGUE	:-	:	Thermistor_5
12 RB5/RX				:INPUT		:-	:	UART Rx signal from PC
11 RB6					:OUTPUT		:0	:	n/c
10 RB7/TX				:OUTPUT		:1	:	UART Tx signal to PC

16 RC0/AN4				:ANALOGUE	:-	:	Thermistor_2
15 RC1/AN5				:ANALOGUE	:-	:	Thermistor_3
14 RC2/AN6				:ANALOGUE	:-	:	Thermistor_4
 7 RC3/AN7/P1C/CCP2/P2A	:OUTPUT		:0	:	To cable, for servo? or potentiometer for fan speed?
 6 RC4					:OUTPUT		:0	:	n/c
 5 RC5/P1A/CCP1			:INPUT		:-	:	Fan 1 (near end) tach
 8 RC6/CCP4				:OUTPUT		:1	:	Fan 1 (near end) PWM
 9 RC7/AN9				:ANALOGUE	:-	:	Thermistor_1

20 VSS
*/

#define VAL_TRISA			0x30	// [0 0 t5 t4 t3 t2 t1 t0] 0 for output 1 for input
#define VAL_TRISB			0x30	// [t7 t6 t5 t4 0 0 0 0] 0 for output 1 for input
#define VAL_TRISC			0xA7	// 0 for output 1 for input
#define VAL_WPUA			0x00	// [0 0 wpu5 wpu4 0 wpu2 wpu1 wpu0] 0 = turn off weak pull-ups
#define VAL_WPUB			0x00	// [wpu7 wpu6 wpu5 wpu4 0 0 0 0] 0 = turn off weak pull-ups
#define VAL_WPUC			0x00	// [wpu7 wpu6 wpu5 wpu4 wpu3 wpu2 wpu1 wpu0] 0 = turn off weak pull-ups
#define VAL_ANSELA			0x10	// 1 for analogue
#define VAL_ANSELB			0x10	// 1 for analogue
#define VAL_ANSELC			0x87	// 1 for analogue
#define VAL_PORTA			0x04	// default values
#define VAL_PORTB			0x80	// default values
#define VAL_PORTC			0x40	// default values
#define PIN_ISRMON LATC4
#define PIN_DEBUG LATB6
#define PIN_SERVO LATC3

// see file:///C:/Program%20Files%20(x86)/Microchip/xc8/v2.10/docs/chips/16f1829.html
// or in MPLAB-X, Window >> Target Memory Views >> Configuration Bits to generate code automatically
#pragma config FOSC = INTOSC , WDTE = OFF, PWRTE = ON, MCLRE = OFF, CP = OFF, CPD = OFF, BOREN = ON, CLKOUTEN = OFF, IESO = OFF, FCMEN = OFF
#pragma config WRT = ALL , PLLEN = ON, STVREN = ON, BORV = HI, LVP = OFF

/* Register: CONFIG1 @ 0x8007
FOSC 	Oscillator Selection
	ECH 	ECH, External Clock, High Power Mode (4-32 MHz): device clock supplied to CLKIN pin
	ECM 	ECM, External Clock, Medium Power Mode (0.5-4 MHz): device clock supplied to CLKIN pin
	ECL 	ECL, External Clock, Low Power Mode (0-0.5 MHz): device clock supplied to CLKIN pin
	INTOSC 	INTOSC oscillator: I/O function on CLKIN pin
	EXTRC 	EXTRC oscillator: External RC circuit connected to CLKIN pin
	HS 	HS Oscillator, High-speed crystal/resonator connected between OSC1 and OSC2 pins
	XT 	XT Oscillator, Crystal/resonator connected between OSC1 and OSC2 pins
	LP 	LP Oscillator, Low-power crystal connected between OSC1 and OSC2 pins
WDTE 	Watchdog Timer Enable
	ON 	WDT enabled
	NSLEEP 	WDT enabled while running and disabled in Sleep
	SWDTEN 	WDT controlled by the SWDTEN bit in the WDTCON register
	OFF 	WDT disabled
PWRTE 	Power-up Timer Enable
	OFF 	PWRT disabled
	ON 	PWRT enabled
MCLRE 	MCLR Pin Function Select
	ON 	MCLR/VPP pin function is MCLR
	OFF 	MCLR/VPP pin function is digital input
CP 	Flash Program Memory Code Protection
	OFF 	Program memory code protection is disabled
	ON 	Program memory code protection is enabled
CPD 	Data Memory Code Protection
	OFF 	Data memory code protection is disabled
	ON 	Data memory code protection is enabled
BOREN 	Brown-out Reset Enable
	ON 	Brown-out Reset enabled
	NSLEEP 	Brown-out Reset enabled while running and disabled in Sleep
	SBODEN 	Brown-out Reset controlled by the SBOREN bit in the BORCON register
	OFF 	Brown-out Reset disabled
CLKOUTEN 	Clock Out Enable
	OFF 	CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin
	ON 	CLKOUT function is enabled on the CLKOUT pin
IESO 	Internal/External Switchover
	ON 	Internal/External Switchover mode is enabled
	OFF 	Internal/External Switchover mode is disabled
FCMEN 	Fail-Safe Clock Monitor Enable
	ON 	Fail-Safe Clock Monitor is enabled
	OFF 	Fail-Safe Clock Monitor is disabled
Register: CONFIG2 @ 0x8008
WRT 	Flash Memory Self-Write Protection
	OFF 	Write protection off
	BOOT 	000h to 1FFh write protected, 200h to 1FFFh may be modified by EECON control
	HALF 	000h to FFFh write protected, 1000h to 1FFFh may be modified by EECON control
	ALL 	000h to 1FFFh write protected, no addresses may be modified by EECON control
PLLEN 	PLL Enable
	ON 	4x PLL enabled
	OFF 	4x PLL disabled
STVREN 	Stack Overflow/Underflow Reset Enable
	ON 	Stack Overflow or Underflow will cause a Reset
	OFF 	Stack Overflow or Underflow will not cause a Reset
BORV 	Brown-out Reset Voltage Selection
	LO 	Brown-out Reset Voltage (Vbor), low trip point selected.
	HI 	Brown-out Reset Voltage (Vbor), high trip point selected.
LVP 	Low-Voltage Programming Enable
	ON 	Low-voltage programming enabled
	OFF 	High-voltage on MCLR/VPP must be used for programming
*/

#define XTAL_FREQ_MHZ 32
#define _XTAL_FREQ (1000000*XTAL_FREQ_MHZ)
#define VAL_OSCCON 0xF0 // Set internal clock frequency 32MHz

#define TIMER2_PERIOD_US 40
#define TIMER2_PRESCALER 4
#define TIMER2_CYCLES ((XTAL_FREQ_MHZ/4/TIMER2_PRESCALER)*TIMER2_PERIOD_US)
#define VAL_PR2 (TIMER2_CYCLES-1)
#define MAX_PWM_VAL (TIMER2_CYCLES*4)
#define VAL_T2CON_OFF 0x01 // [- TxOUTPS<3:0> TMRxON TxCKPS<1:0>]
#define VAL_T2CON_ON 0x05
#define VAL_CCPTMRS 0x00 // uses Timer2 for all PWM channels
#define VAL_CCP3CON 0x0C
#define VAL_CCP4CON 0x0C

#define INTERRUPT_PERIOD_US TIMER2_PERIOD_US

#define BAUD_115200
//#define BAUD_9600
#ifdef BAUD_9600
#define VAL_SPBRGH	0
#define VAL_SPBRGL	51
#define VAL_BRGH	0
#define VAL_BRG16	0
#endif

#ifdef BAUD_115200
// need to service UART Rx every 86us or sooner
#define VAL_SPBRGH	0
#define VAL_SPBRGL	68
#define VAL_BRGH	1
#define VAL_BRG16	1
#endif

#define UART_TX_READY (0!=(PIR1&0x10))

#define ADCON0_THERMISTOR_0	0x0D	//AN3  : [0, CHS<4:0>=00011, GODONE=0, ADON=1]
#define ADCON0_THERMISTOR_1	0x25	//AN9  : [0, CHS<4:0>=01001, GODONE=0, ADON=1]
#define ADCON0_THERMISTOR_2	0x11	//AN4  : [0, CHS<4:0>=00100, GODONE=0, ADON=1]
#define ADCON0_THERMISTOR_3 0x15	//AN5  : [0, CHS<4:0>=00101, GODONE=0, ADON=1]
#define ADCON0_THERMISTOR_4 0x19	//AN6  : [0, CHS<4:0>=00110, GODONE=0, ADON=1]
#define ADCON0_THERMISTOR_5 0x29	//AN10 : [0, CHS<4:0>=01010, GODONE=0, ADON=1]
#define ADCON0_EXTIN 0x29	//AN10 : [0, CHS<4:0>=01010, GODONE=0, ADON=1]
#define ADCON0_GODONE1 0x02
#define VAL_ADCON1 0xA0 // ADFM=1, ADCS<2:0>=010(fosc/32), ADNREF=0(VSS) ADPREF=00(VDD)
#define MAX_ADC_VAL 1023
#define NUM_THERMISTORS 6
#define NUM_AVE 20 // averaging 20 samples every 1ms should reject 50Hz (20ms periodic) interference
#define MAX_ADC_SUM (NUM_AVE * MAX_ADC_VAL)

void set_pwm3_duty_cycle(uint16_t pwm3){
	CCPR3L = pwm3>>2;
	CCP3CON = VAL_CCP3CON | ((pwm3&0x03) << 4);
}
void set_pwm4_duty_cycle(uint16_t pwm4){
	CCPR4L = pwm4>>2;
	CCP4CON = VAL_CCP4CON | ((pwm4&0x03) << 4);
}

#define SERIAL_TX_BUF_SIZE 64 // Must be a power of 2
#define SERIAL_TX_BUF_SIZE_MASK (SERIAL_TX_BUF_SIZE-1)

#define SERIAL_RX_BUF_SIZE 64 // Must be a power of 2
#define SERIAL_RX_BUF_SIZE_MASK (SERIAL_RX_BUF_SIZE-1)

volatile uint8_t txdata[SERIAL_TX_BUF_SIZE];
volatile uint8_t rxdata[SERIAL_RX_BUF_SIZE];
volatile uint8_t tx_index_next_in=0;
volatile uint8_t tx_index_next_out=0;
volatile uint8_t rx_index_next_in=0;
volatile uint8_t rx_index_next_out=0;

void putch(uint8_t ch){
	uint32_t tmp_in_index;

	tmp_in_index = (tx_index_next_in+1)&SERIAL_TX_BUF_SIZE_MASK;
	while(tmp_in_index == tx_index_next_out){ // buffer would become completely full but look empty.
		PIN_DEBUG = 1;
	}
	PIN_DEBUG = 0;
	txdata[tx_index_next_in] = ch;
	tx_index_next_in = tmp_in_index;
}

uint8_t rx_empty(void){
	return (rx_index_next_in == rx_index_next_out) ? 1 : 0;
}

// returns character from buffer. If buffer is empty, returns 0 and doesn't wait.
uint8_t getch_nowait(void){
	uint8_t ch;
	if(rx_empty()){
		return 0;
	} else {
		ch = rxdata[rx_index_next_out];
		rx_index_next_out = (rx_index_next_out+1)&SERIAL_RX_BUF_SIZE_MASK;
		return ch;
	}
}




void task_idle(void);
void task0(void);
void task1(void);
void task2(void);
void task3(void);
void task4(void);
void task5(void);
void task6(void);
void task7(void);
void task8(void);
void task9(void);
void task10(void);
void task11(void);
void task12(void);
void task13(void);
void task14(void);
void task15(void);
void task16(void);
void task_copy0(void);
void task_copy1(void);
void task_copy2(void);
void task_copy3(void);

#define NUM_THERMISTORS 6
#define NUM_AVE 20 // averaging 20 samples every 1ms should reject 50Hz (20ms periodic) interference
uint16_t therm_adc_accum[NUM_THERMISTORS];
uint16_t extinp_accum;
volatile uint16_t isr_therm_sum[NUM_THERMISTORS];
volatile uint16_t isr_extinp_sum;
volatile uint8_t isr_lock = 0;
volatile uint8_t isr_adc_sums_updated = 0;
uint8_t nconversions=0;
uint8_t adc_result_l;
uint8_t adc_result_h;
uint8_t adcon0_val = ADCON0_THERMISTOR_0;
void (*isrtask)(void) = task_idle;


void clear_accums(void){
	uint8_t i;
	for(i=0; i<NUM_THERMISTORS; i++){
		therm_adc_accum[i] = 0;
		extinp_accum = 0;
	}
}

void task_idle(void){
	adcon0_val = ADCON0_THERMISTOR_0;
}
void task0(void){
	adcon0_val = ADCON0_THERMISTOR_0; // Set up acquisition
	isrtask = task1;
}
void task1(void){
	// Acquisition of thermistor 0 was just started.
	adcon0_val = ADCON0_THERMISTOR_0 + ADCON0_GODONE1; // Convert
	isrtask = task2;
}
void task2(void){
	// Conversion of thermistor 0 was just started.
	adcon0_val = ADCON0_THERMISTOR_1; // Set up acquisition
	isrtask = task3;
}
void task3(void){
	// adc_result_h,l holds thermistor 0 result.
	// Acquisition of thermistor 1 was just started.
	therm_adc_accum[0] += ((adc_result_h&0x03)<<8)|adc_result_l;
	adcon0_val = ADCON0_THERMISTOR_1 + ADCON0_GODONE1; // Convert
	isrtask = task4;
}
void task4(void){
	// Conversion of thermistor 1 was just started.
	adcon0_val = ADCON0_THERMISTOR_2; // Set up acquisition
	isrtask = task5;
}
void task5(void){
	// adc_result_h,l holds thermistor 1 result.
	// Acquisition of thermistor 2 was just started.
	therm_adc_accum[1] += ((adc_result_h&0x03)<<8)|adc_result_l;
	adcon0_val = ADCON0_THERMISTOR_2 + ADCON0_GODONE1; // Convert
	isrtask = task6;
}
void task6(void){
	// Conversion of thermistor 2 was just started.
	adcon0_val = ADCON0_THERMISTOR_3; // Set up acquisition
	isrtask = task7;
}
void task7(void){
	// adc_result_h,l holds thermistor 2 result.
	// Acquisition of thermistor 3 was just started.
	therm_adc_accum[2] += ((adc_result_h&0x03)<<8)|adc_result_l;
	adcon0_val = ADCON0_THERMISTOR_3 + ADCON0_GODONE1; // Convert
	isrtask = task8;
}
void task8(void){
	// Conversion of thermistor 3 was just started.
	adcon0_val = ADCON0_THERMISTOR_4; // Set up acquisition
	isrtask = task9;
}
void task9(void){
	// adc_result_h,l holds thermistor 3 result.
	// Acquisition of thermistor 4 was just started.
	therm_adc_accum[3] += ((adc_result_h&0x03)<<8)|adc_result_l;
	adcon0_val = ADCON0_THERMISTOR_4 + ADCON0_GODONE1; // Convert
	isrtask = task10;
}
void task10(void){
	// Conversion of thermistor 4 was just started.
	adcon0_val = ADCON0_THERMISTOR_5; // Set up acquisition
	isrtask = task11;
}
void task11(void){
	// adc_result_h,l holds thermistor 4 result.
	// Acquisition of thermistor 5 was just started.
	therm_adc_accum[4] += ((adc_result_h&0x03)<<8)|adc_result_l;
	adcon0_val = ADCON0_THERMISTOR_5 + ADCON0_GODONE1; // Convert
	isrtask = task12;
}
void task12(void){
	// Conversion of thermistor 5 was just started.
	adcon0_val = ADCON0_EXTIN; // Set up acquisition
	isrtask = task13;
}
void task13(void){
	// adc_result_h,l holds thermistor 5 result.
	// Acquisition of EXTIN was just started.
	therm_adc_accum[5] += ((adc_result_h&0x03)<<8)|adc_result_l;
	adcon0_val = ADCON0_EXTIN + ADCON0_GODONE1; // Convert
	isrtask = task14;
}
void task14(void){
	// Conversion of EXTIN was just started.
	adcon0_val = ADCON0_EXTIN; // No mode conversions this time
	isrtask = task15;
}
void task15(void){
	// adc_result_h,l holds EXTIN result.
	extinp_accum += ((adc_result_h&0x03)<<8)|adc_result_l;
	nconversions++; // all channels converted one more time
	isrtask = task16;
}
void task16(void){
	if(nconversions==NUM_AVE){
		isrtask = task_copy0;
	} else {
		isrtask = task_idle;
	}
}
void task_copy0(void){
	if(!isr_lock){
		isr_therm_sum[0] = therm_adc_accum[0];
		isr_therm_sum[1] = therm_adc_accum[1];
		isr_therm_sum[2] = therm_adc_accum[2];
		isr_therm_sum[3] = therm_adc_accum[3];
		isrtask = task_copy1;
	}
}
void task_copy1(void){
	if(!isr_lock){
		isr_therm_sum[4] = therm_adc_accum[4];
		isr_therm_sum[5] = therm_adc_accum[5];
		isr_extinp_sum = extinp_accum;
		nconversions = 0;
		isrtask = task_copy2;
	}
}
void task_copy2(void){
	if(!isr_lock){
		therm_adc_accum[0]=0;
		therm_adc_accum[1]=0;
		therm_adc_accum[2]=0;
		therm_adc_accum[3]=0;
		isrtask = task_copy3;
	}
}
void task_copy3(void){
	if(!isr_lock){
		therm_adc_accum[4]=0;
		therm_adc_accum[5]=0;
		extinp_accum = 0;
		isr_adc_sums_updated = 1;
		isrtask = task_idle;
	}
}

uint8_t isrtask_counter = 0;

void __interrupt() isr(void){
	uint8_t c;
	uint8_t tmpindex;
	PIN_ISRMON=1;
	adc_result_h = ADRESH;
	adc_result_l = ADRESL;
	ADCON0 = adcon0_val;

	(*isrtask)();


// 40us per isr, 25khz ISR rate, 25 interrupts per 1ms
	if(isrtask_counter == 0){ // every 1ms
		isrtask = task0;
		isrtask_counter = 24;
		PIN_SERVO = 1;
	} else {
		isrtask_counter--;
		PIN_SERVO = 0;
	}


	if(OERR){ // if receive buffer overruns, and UART is hung...
		CREN=0; //reset the UART receiver
		CREN=1;
	}
	if(RCIF){
		c = RCREG;
		tmpindex = (rx_index_next_in+1) & SERIAL_RX_BUF_SIZE_MASK;
		if(tmpindex == rx_index_next_out){ // buffer would become completely full but would appear empty
			// Error - buffer overrun. Drop character, and we could set a flag here to do something about it.
		} else {
			rxdata[rx_index_next_in] = c;
			rx_index_next_in = tmpindex;
		}
	}
	if(tx_index_next_in != tx_index_next_out){ // if tx buffer not empty...
		if(UART_TX_READY){
			TXREG = txdata[tx_index_next_out];
			tx_index_next_out = (tx_index_next_out+1) & SERIAL_TX_BUF_SIZE_MASK;
		}
	}

	TMR2IF = 0;
	PIN_ISRMON=0;
}

void setup_lowlevel(void){
	TRISA  = VAL_TRISA;
	TRISB  = VAL_TRISB;
	TRISC  = VAL_TRISC;

	WPUA   = VAL_WPUA;
	WPUB   = VAL_WPUB;
	WPUC   = VAL_WPUC;
	nWPUEN = 0; // global enable weak pull-ups where selected

	ANSELA  = VAL_ANSELA;
	ANSELB  = VAL_ANSELB;
	ANSELC  = VAL_ANSELC;

 	PORTA=VAL_PORTA;
	PORTB=VAL_PORTB;
	PORTC=VAL_PORTC;

	OSCCON = VAL_OSCCON; // Set internal clock frequency
	__delay_ms(20); // stops EUSART getting into a strange state while clock is settling


	SPBRGH = VAL_SPBRGH;
	SPBRGL = VAL_SPBRGL;
	BRGH   = VAL_BRGH;
	BRG16  = VAL_BRG16;
	SYNC = 0;
	SPEN = 1;
	TXEN = 1;
	CREN = 1; // rx enable

	ADCON1 = VAL_ADCON1;
	ADCON0 = ADCON0_THERMISTOR_0;
	clear_accums();

// Set up timer 1 for the CCPs to capture tacho edges.
// Timer 1 will not interrupt, it will just count up at the rate of instruction cycles.

// Set up timer 2 for PWM, and to generate regular interrupts
	PR2 = VAL_PR2;
	T2CON = VAL_T2CON_OFF;
	T2CON = VAL_T2CON_ON;
	CCPTMRS = VAL_CCPTMRS;
	CCP3CON = VAL_CCP3CON;
	CCP4CON = VAL_CCP4CON;
	set_pwm3_duty_cycle(MAX_PWM_VAL);
	set_pwm4_duty_cycle(MAX_PWM_VAL);

	TMR2IF = 0;
	TMR2IE = 1;
	PEIE = 1; // Peripheral interrupts enabled
	GIE = 1; // Global interrupts enabled
}

void puts(char *s){
	while(*s){
		putch((uint8_t) (*(s++)));
	}
}

void print_dec(uint32_t n){
	uint8_t p, q, d;
	uint32_t m;

	p=0;

	d=10; // will get decremented straight away
	do{
		d--;
		switch(d){
			case 9:
				m = 1000000000;
				break;
			case 8:
				m = 100000000;
				break;
			case 7:
				m = 10000000;
				break;
			case 6:
				m = 1000000;
				break;
			case 5:
				m = 100000;
				break;
			case 4:
				m = 10000;
				break;
			case 3:
				m = 1000;
				break;
			case 2:
				m = 100;
				break;
			case 1:
				m = 10;
				break;
			case 0:	
				m = 1;
				p = 1; // force printing last digit, even if zero
				break;
		}
		q=0;
		while(n >= m){
			n -= m;
			q++;
			p=1;
		}
		if(p){
			putch(q + '0');
		}
	}while(d);
}
void print_dec_hundredths(uint32_t n){
	uint8_t p, q, d;
	uint32_t m;

	p=0;

	d=10; // will get decremented straight away
	do{
		d--;
		switch(d){
			case 9:
				m = 1000000000;
				break;
			case 8:
				m = 100000000;
				break;
			case 7:
				m = 10000000;
				break;
			case 6:
				m = 1000000;
				break;
			case 5:
				m = 100000;
				break;
			case 4:
				m = 10000;
				break;
			case 3:
				m = 1000;
				break;
			case 2:
				m = 100;
				p = 1; // force printing last digits, even if zero
				break;
			case 1:
				putch('.');
				m = 10;
				break;
			case 0:	
				m = 1;
				break;
		}
		q=0;
		while(n >= m){
			n -= m;
			q++;
			p=1;
		}
		if(p){
			putch(q + '0');
		}
	}while(d);
}



const uint16_t t100ths[]=\
{65535,65535,65535,65535,65535,65535,65535,65535,65535,65535,65535,65535,65535,65535,65535,65535,65535,42072,41838,41619,\
41413,41218,41033,40858,40691,40532,40380,40236,40097,39963,39834,39707,39585,39469,39361,39261,39168,39079,38992,38904,38813,38719,\
38623,38527,38433,38343,38259,38179,38103,38030,37960,37892,37826,37760,37695,37630,37567,37505,37444,37383,37324,37267,37210,37154,\
37100,37046,36993,36942,36891,36841,36791,36743,36695,36648,36601,36556,36511,36466,36422,36379,36337,36295,36253,36212,36172,36132,\
36092,36053,36015,35977,35939,35902,35865,35829,35793,35757,35722,35688,35653,35619,35586,35552,35519,35487,35454,35423,35391,35360,\
35329,35298,35268,35238,35208,35178,35149,35120,35091,35063,35035,35007,34979,34951,34924,34897,34870,34844,34817,34791,34765,34739,\
34714,34688,34663,34638,34613,34589,34564,34540,34516,34492,34468,34445,34421,34398,34375,34352,34330,34307,34285,34262,34240,34218,\
34196,34175,34153,34132,34110,34089,34068,34047,34027,34006,33986,33965,33945,33925,33905,33885,33865,33846,33826,33807,33787,33768,\
33749,33730,33711,33692,33673,33655,33636,33618,33600,33581,33563,33545,33527,33509,33492,33474,33456,33439,33421,33404,33387,33370,\
33353,33336,33319,33302,33285,33268,33252,33235,33219,33203,33186,33170,33154,33138,33122,33106,33090,33074,33058,33043,33027,33012,\
32996,32981,32965,32950,32935,32920,32905,32890,32875,32860,32845,32830,32815,32801,32786,32771,32757,32742,32728,32714,32699,32685,\
32671,32657,32643,32628,32614,32601,32587,32573,32559,32545,32532,32518,32504,32491,32477,32464,32450,32437,32423,32410,32397,32384,\
32370,32357,32344,32331,32318,32305,32292,32279,32266,32254,32241,32228,32215,32203,32190,32177,32165,32152,32140,32127,32115,32103,\
32090,32078,32066,32053,32041,32029,32017,32005,31993,31981,31969,31957,31945,31933,31921,31909,31897,31885,31874,31862,31850,31838,\
31827,31815,31804,31792,31780,31769,31757,31746,31735,31723,31712,31700,31689,31678,31667,31655,31644,31633,31622,31611,31600,31588,\
31577,31566,31555,31544,31533,31522,31512,31501,31490,31479,31468,31457,31447,31436,31425,31414,31404,31393,31382,31372,31361,31351,\
31340,31330,31319,31308,31298,31288,31277,31267,31256,31246,31236,31225,31215,31205,31194,31184,31174,31164,31153,31143,31133,31123,\
31113,31103,31093,31082,31072,31062,31052,31042,31032,31022,31012,31002,30992,30983,30973,30963,30953,30943,30933,30923,30913,30904,\
30894,30884,30874,30864,30855,30845,30835,30826,30816,30806,30796,30787,30777,30768,30758,30748,30739,30729,30720,30710,30700,30691,\
30681,30672,30662,30653,30643,30634,30624,30615,30606,30596,30587,30577,30568,30558,30549,30540,30530,30521,30512,30502,30493,30484,\
30474,30465,30456,30447,30437,30428,30419,30410,30401,30391,30382,30373,30364,30355,30345,30336,30327,30318,30309,30300,30291,30282,\
30273,30264,30254,30245,30236,30227,30218,30209,30200,30191,30182,30173,30164,30155,30146,30137,30129,30120,30111,30102,30093,30084,\
30075,30066,30057,30048,30039,30031,30022,30013,30004,29995,29986,29978,29969,29960,29951,29942,29933,29925,29916,29907,29898,29889,\
29881,29872,29863,29854,29846,29837,29828,29819,29811,29802,29793,29784,29776,29767,29758,29750,29741,29732,29723,29715,29706,29697,\
29689,29680,29671,29663,29654,29645,29636,29628,29619,29610,29602,29593,29584,29576,29567,29558,29550,29541,29533,29524,29515,29507,\
29498,29489,29481,29472,29463,29455,29446,29437,29429,29420,29412,29403,29394,29386,29377,29368,29360,29351,29343,29334,29325,29317,\
29308,29299,29291,29282,29274,29265,29256,29248,29239,29230,29222,29213,29205,29196,29187,29179,29170,29161,29153,29144,29135,29127,\
29118,29110,29101,29092,29084,29075,29066,29058,29049,29040,29032,29023,29014,29006,28997,28988,28980,28971,28962,28954,28945,28936,\
28928,28919,28910,28902,28893,28884,28876,28867,28858,28849,28841,28832,28823,28815,28806,28797,28788,28780,28771,28762,28753,28745,\
28736,28727,28718,28710,28701,28692,28683,28674,28666,28657,28648,28639,28630,28622,28613,28604,28595,28586,28577,28568,28560,28551,\
28542,28533,28524,28515,28506,28497,28488,28479,28471,28462,28453,28444,28435,28426,28417,28408,28399,28390,28381,28372,28363,28354,\
28345,28336,28327,28317,28308,28299,28290,28281,28272,28263,28254,28245,28235,28226,28217,28208,28199,28190,28180,28171,28162,28153,\
28143,28134,28125,28116,28106,28097,28088,28078,28069,28060,28050,28041,28032,28022,28013,28003,27994,27984,27975,27965,27956,27946,\
27937,27927,27918,27908,27899,27889,27880,27870,27860,27851,27841,27831,27822,27812,27802,27793,27783,27773,27763,27753,27744,27734,\
27724,27714,27704,27694,27684,27674,27664,27655,27645,27635,27625,27614,27604,27594,27584,27574,27564,27554,27544,27533,27523,27513,\
27503,27492,27482,27472,27461,27451,27441,27430,27420,27409,27399,27388,27378,27367,27357,27346,27336,27325,27314,27304,27293,27282,\
27271,27260,27250,27239,27228,27217,27206,27195,27184,27173,27162,27151,27140,27129,27118,27106,27095,27084,27073,27061,27050,27039,\
27027,27016,27004,26993,26981,26970,26958,26946,26935,26923,26911,26900,26888,26876,26864,26852,26840,26828,26816,26804,26792,26780,\
26768,26755,26743,26731,26718,26706,26694,26681,26668,26656,26643,26631,26618,26605,26592,26579,26567,26554,26541,26527,26514,26501,\
26488,26475,26461,26448,26435,26421,26407,26394,26380,26366,26353,26339,26325,26311,26297,26283,26269,26254,26240,26226,26211,26197,\
26182,26167,26153,26138,26123,26108,26093,26078,26063,26047,26032,26017,26001,25985,25970,25954,25938,25922,25906,25890,25874,25857,\
25841,25824,25808,25791,25774,25757,25740,25723,25706,25688,25671,25653,25636,25618,25600,25582,25564,25545,25527,25508,25489,25470,\
25451,25432,25413,25393,25374,25354,25334,25314,25293,25273,25252,25231,25210,25189,25168,25146,25124,25102,25080,25057,25035,25012,\
24989,24965,24942,24918,24894,24869,24845,24820,24794,24769,24743,24717,24691,24664,24637,24609,24582,24554,24525,24496,24467,24437,\
24407,24376,24345,24314,24282,24249,24216,24182,24148,24113,24078,24042,24005,23968,23930,23891,23851,23810,23769,23727,23683,23639,\
23593,23547,23499,23450,23400,23348,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

// Takes a sum of 20 ADC values each in the range of 0 to 1023
// returns in units of 0.01 Kelvin, so 0 = 0K, 65535 = 655.35K
uint16_t adcsum_to_temperature(uint16_t therm_sum){
	uint16_t i;

	i = (therm_sum+10)/20;
	if(i<0){
		i=0;
	}
	if(i>1023){
		i=1023;
	}

	return t100ths[i];
}


uint16_t fanpower = 0;

void setup(void){
	setup_lowlevel();
	puts("Fan controller\r\nPress 0 to 9 to control fan power.\r\n");
    set_pwm3_duty_cycle(0);
	set_pwm4_duty_cycle(0);
}

void iterate(void){
	uint8_t upd;
	uint16_t therm_sum[NUM_THERMISTORS];
	uint16_t extinp_sum;
	uint8_t i;
	uint8_t c;

	isr_lock = 1;
	upd = isr_adc_sums_updated;
	isr_lock = 0;
	if(upd){
		for(i=0; i<NUM_THERMISTORS; i++){
			isr_lock = 1;
			therm_sum[i] = isr_therm_sum[i];
			isr_lock = 0;
		}
		isr_lock = 1;
		extinp_sum = isr_extinp_sum;
		isr_lock = 0;

		isr_lock = 1;
		isr_adc_sums_updated = 0;
		isr_lock = 0;

#if 0
		for(i=0; i<NUM_THERMISTORS; i++){
			puts("Th");
			print_dec(i);
			puts(":");
			print_dec(therm_sum[i]);
			puts(" \t");
		}
#endif
		puts("EI");
		puts(":");
		print_dec(extinp_sum);
		puts(" \t");
        
        fanpower = (uint16_t) ((uint32_t)extinp_sum * MAX_PWM_VAL / MAX_ADC_SUM);
		set_pwm3_duty_cycle(fanpower);
		set_pwm4_duty_cycle(fanpower);

		puts("Fans");
		puts(":");
		print_dec(fanpower);
		puts("/");
		print_dec(MAX_PWM_VAL);
		puts("\r\n");
	}
	if(!rx_empty()){
		c = getch_nowait();
	}
}


void main(void){
	setup();
	for(;;){
		iterate();
	}
}

