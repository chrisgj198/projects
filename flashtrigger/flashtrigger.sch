EESchema Schematic File Version 4
LIBS:flashtrigger-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Isolator:4N35 U1
U 1 1 5DD35356
P 3300 6900
F 0 "U1" H 3400 7250 50  0000 C CNN
F 1 "4N35" H 3400 7150 50  0000 C CNN
F 2 "Package_DIP:DIP-6_W7.62mm" H 3100 6700 50  0001 L CIN
F 3 "https://www.vishay.com/docs/81181/4n35.pdf" H 3300 6900 50  0001 L CNN
	1    3300 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R2
U 1 1 5DD35544
P 2050 6650
F 0 "R2" V 1845 6650 50  0000 C CNN
F 1 "1k" V 1936 6650 50  0000 C CNN
F 2 "chrisj2:chrisj1_R_0204_4" V 2090 6640 50  0001 C CNN
F 3 "~" H 2050 6650 50  0001 C CNN
	1    2050 6650
	0    1    1    0   
$EndComp
$Comp
L Device:C C8
U 1 1 5DD361DB
P 7650 3150
F 0 "C8" V 7398 3150 50  0000 C CNN
F 1 "220nF" V 7489 3150 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L31.5mm_W20.0mm_P27.50mm_MKS4" H 7688 3000 50  0001 C CNN
F 3 "~" H 7650 3150 50  0001 C CNN
	1    7650 3150
	0    1    1    0   
$EndComp
$Comp
L Triac_Thyristor:TIC126 Q2
U 1 1 5DD3630B
P 7100 3350
F 0 "Q2" H 7188 3396 50  0000 L CNN
F 1 "TIC126" H 7188 3305 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Horizontal_TabDown" H 7200 3275 50  0001 L CIN
F 3 "https://cdn-reichelt.de/documents/datenblatt/A400/TIC126.pdf" H 7100 3350 50  0001 L CNN
	1    7100 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R9
U 1 1 5DD3645A
P 7100 2850
F 0 "R9" H 7168 2896 50  0000 L CNN
F 1 "100k 2W" H 7168 2805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_Power_L50.0mm_W9.0mm_P55.88mm" V 7140 2840 50  0001 C CNN
F 3 "~" H 7100 2850 50  0001 C CNN
	1    7100 2850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5DD36778
P 1050 1900
F 0 "J1" H 970 2117 50  0000 C CNN
F 1 "Capacitor_Bank" H 970 2026 50  0000 C CNN
F 2 "chrisj2:chrisj1_WEIDMULLER_1508060000" H 1050 1900 50  0001 C CNN
F 3 "~" H 1050 1900 50  0001 C CNN
	1    1050 1900
	-1   0    0    1   
$EndComp
Wire Wire Line
	8700 3550 9200 3550
Wire Wire Line
	9200 3550 9200 2100
Wire Wire Line
	9200 2100 9300 2100
Wire Wire Line
	1250 1800 1500 1800
Text Label 2850 1900 0    50   ~ 0
COM
Text Label 2850 1800 0    50   ~ 0
CAP+
Text Label 9200 2700 1    50   ~ 0
TRIGGER_EHT
Wire Wire Line
	7800 3150 7850 3150
Wire Wire Line
	7500 3150 7100 3150
Wire Wire Line
	7100 3150 7100 3200
$Comp
L Diode:1N53xxB D1
U 1 1 5DD3811C
P 1500 3750
F 0 "D1" V 1454 3829 50  0000 L CNN
F 1 "1N5349B" V 1545 3829 50  0000 L CNN
F 2 "Diode_THT:D_DO-201_P15.24mm_Horizontal" H 1500 3575 50  0001 C CNN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/1n5345b.pdf" H 1500 3750 50  0001 C CNN
	1    1500 3750
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R1
U 1 1 5DD38CA8
P 1500 2200
F 0 "R1" H 1568 2246 50  0000 L CNN
F 1 "100k 2W" H 1568 2155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_Power_L50.0mm_W9.0mm_P55.88mm" V 1540 2190 50  0001 C CNN
F 3 "~" H 1500 2200 50  0001 C CNN
	1    1500 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 2050 1500 1800
Wire Wire Line
	1500 3900 1500 5100
Wire Wire Line
	1500 5100 1750 5100
Wire Wire Line
	1500 3600 1500 2400
$Comp
L Device:CP C1
U 1 1 5DD399F8
P 1950 4200
F 0 "C1" H 2068 4246 50  0000 L CNN
F 1 "100uF 16V" H 2068 4155 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.50mm" H 1988 4050 50  0001 C CNN
F 3 "~" H 1950 4200 50  0001 C CNN
	1    1950 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1950 4350 1950 5100
Connection ~ 1500 2400
Wire Wire Line
	1500 2400 1500 2350
Text Notes 2350 1750 0    50   ~ 0
400V
Text Notes 2350 2100 0    50   ~ 0
approx \n4mA
Wire Wire Line
	1950 4050 1950 2400
Wire Wire Line
	7100 2700 7100 1800
Wire Wire Line
	7100 3150 7100 3000
Connection ~ 7100 3150
$Comp
L Device:R_US R7
U 1 1 5DD493F5
P 6600 3450
F 0 "R7" V 6395 3450 50  0000 C CNN
F 1 "100" V 6486 3450 50  0000 C CNN
F 2 "chrisj2:chrisj1_R_0204_4" V 6640 3440 50  0001 C CNN
F 3 "~" H 6600 3450 50  0001 C CNN
	1    6600 3450
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R8
U 1 1 5DD4A1E3
P 6800 3650
F 0 "R8" H 6732 3604 50  0000 R CNN
F 1 "220" H 6732 3695 50  0000 R CNN
F 2 "chrisj2:chrisj1_R_0204_4" V 6840 3640 50  0001 C CNN
F 3 "~" H 6800 3650 50  0001 C CNN
	1    6800 3650
	-1   0    0    1   
$EndComp
Wire Wire Line
	7100 3500 7100 3550
Wire Wire Line
	7100 4150 8850 4150
Connection ~ 8850 4150
Wire Wire Line
	8850 4150 8850 5100
Wire Wire Line
	6800 3800 6800 3850
Wire Wire Line
	6800 3850 7100 3850
Connection ~ 7100 3850
Wire Wire Line
	7100 3850 7100 4150
Wire Wire Line
	7100 3550 7850 3550
Connection ~ 7100 3550
Wire Wire Line
	7100 3550 7100 3850
Wire Wire Line
	6750 3450 6800 3450
Wire Wire Line
	6800 3500 6800 3450
Connection ~ 6800 3450
Wire Wire Line
	6800 3450 6950 3450
$Comp
L Diode:1N4148 D2
U 1 1 5DD4EACA
P 2350 6900
F 0 "D2" V 2250 6900 50  0000 L CNN
F 1 "1N4148" V 2450 6900 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 2350 6725 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 2350 6900 50  0001 C CNN
	1    2350 6900
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5DD53970
P 2700 6900
F 0 "C2" H 2750 7000 50  0000 L CNN
F 1 "10nF" H 2700 6800 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 2738 6750 50  0001 C CNN
F 3 "~" H 2700 6900 50  0001 C CNN
	1    2700 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 7100 2950 7000
Wire Wire Line
	2950 7000 3000 7000
Wire Wire Line
	2700 7100 2700 7050
Wire Wire Line
	2350 7100 2350 7050
Wire Wire Line
	2950 6650 2950 6800
Wire Wire Line
	2950 6800 3000 6800
Wire Wire Line
	2700 6750 2700 6650
Wire Wire Line
	2350 6750 2350 6650
Text Notes 7200 3900 0    50   ~ 0
SCR max leakage 2mA\nrequired trigger current 20mA for 20us\nhold current about 7.5mA at 100degC
$Comp
L Transistor_FET:2N7000 Q1
U 1 1 5DD4D8EB
P 6300 3000
F 0 "Q1" H 6506 3046 50  0000 L CNN
F 1 "2N7000" H 6506 2955 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Wide" H 6500 2925 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7000.pdf" H 6300 3000 50  0001 L CNN
	1    6300 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 1800 7100 1800
Connection ~ 7100 1800
Wire Wire Line
	7100 1800 9300 1800
Wire Wire Line
	6400 2400 6400 2800
Wire Wire Line
	6450 3450 6400 3450
Wire Wire Line
	6400 3450 6400 3200
$Comp
L Timer:ICM7555 U2
U 1 1 5DD5D683
P 4300 3200
F 0 "U2" H 4000 3800 50  0000 C CNN
F 1 "ICM7555" H 4050 3700 50  0000 C CNN
F 2 "chrisj2:chrisj1_DIP8_SS" H 4300 3200 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/icm7/icm7555-56.pdf" H 4300 3200 50  0001 C CNN
	1    4300 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 2800 4300 2750
$Comp
L Device:C C7
U 1 1 5DD70B9E
P 5850 3000
F 0 "C7" V 5598 3000 50  0000 C CNN
F 1 "10nF" V 5689 3000 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 5888 2850 50  0001 C CNN
F 3 "~" H 5850 3000 50  0001 C CNN
	1    5850 3000
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R6
U 1 1 5DD73DB4
P 6050 4450
F 0 "R6" H 5982 4404 50  0000 R CNN
F 1 "10k" H 5982 4495 50  0000 R CNN
F 2 "chrisj2:chrisj1_R_0204_4" V 6090 4440 50  0001 C CNN
F 3 "~" H 6050 4450 50  0001 C CNN
	1    6050 4450
	-1   0    0    1   
$EndComp
$Comp
L Diode:1N4148 D3
U 1 1 5DD7489B
P 6400 4450
F 0 "D3" V 6354 4529 50  0000 L CNN
F 1 "1N4148" V 6445 4529 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6400 4275 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6400 4450 50  0001 C CNN
	1    6400 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	6050 4300 6050 4200
Wire Wire Line
	6050 4600 6050 4700
Wire Wire Line
	6400 4600 6400 4700
Wire Wire Line
	6400 4700 6050 4700
Connection ~ 6050 4700
Wire Wire Line
	6050 4700 6050 5100
Wire Wire Line
	6400 4300 6400 4200
Wire Wire Line
	6400 4200 6050 4200
Connection ~ 6050 4200
Wire Wire Line
	6050 4200 6050 3000
NoConn ~ 4800 3200
$Comp
L Device:C C4
U 1 1 5DD7D9D6
P 3700 3550
F 0 "C4" H 3550 3550 50  0000 C CNN
F 1 "1nF" H 3550 3650 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 3738 3400 50  0001 C CNN
F 3 "~" H 3700 3550 50  0001 C CNN
	1    3700 3550
	-1   0    0    1   
$EndComp
Wire Wire Line
	3800 3400 3750 3400
Wire Wire Line
	3750 3400 3750 2750
Wire Wire Line
	3750 2750 4300 2750
Connection ~ 4300 2750
Wire Wire Line
	4300 2750 4300 2550
Wire Wire Line
	3800 3200 3700 3200
Wire Wire Line
	3700 3200 3700 3400
Wire Wire Line
	3700 3700 3700 3750
Wire Wire Line
	3700 3750 4300 3750
Wire Wire Line
	4300 3600 4300 3750
Connection ~ 4300 3750
Wire Wire Line
	4300 3750 4300 4350
Wire Wire Line
	6000 3000 6050 3000
Connection ~ 6050 5100
Wire Wire Line
	6050 5100 8850 5100
Connection ~ 6050 3000
Wire Wire Line
	6050 3000 6100 3000
Wire Wire Line
	4300 2400 6400 2400
Wire Wire Line
	4300 5100 6050 5100
Wire Wire Line
	4800 3000 5700 3000
Wire Wire Line
	3800 3000 3550 3000
Wire Wire Line
	3550 3000 3550 3500
Wire Wire Line
	3550 3850 4800 3850
Wire Wire Line
	4800 3850 4800 3400
Connection ~ 1950 2400
Wire Wire Line
	1950 2400 1500 2400
Connection ~ 1950 5100
Wire Wire Line
	1950 2400 2200 2400
Wire Wire Line
	1950 5100 4300 5100
$Comp
L Device:R_US R4
U 1 1 5DDBBAA7
P 3350 3500
F 0 "R4" V 3145 3500 50  0000 C CNN
F 1 "100k" V 3236 3500 50  0000 C CNN
F 2 "chrisj2:chrisj1_R_0204_4" V 3390 3490 50  0001 C CNN
F 3 "~" H 3350 3500 50  0001 C CNN
	1    3350 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	3500 3500 3550 3500
Connection ~ 3550 3500
Wire Wire Line
	3550 3500 3550 3850
$Comp
L Device:R_US R3
U 1 1 5DDBE776
P 3000 3000
F 0 "R3" H 3068 3046 50  0000 L CNN
F 1 "10k" H 3068 2955 50  0000 L CNN
F 2 "chrisj2:chrisj1_R_0204_4" V 3040 2990 50  0001 C CNN
F 3 "~" H 3000 3000 50  0001 C CNN
	1    3000 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2850 3000 2400
Connection ~ 3000 2400
Wire Wire Line
	3000 2400 4300 2400
$Comp
L Device:C C3
U 1 1 5DDC13C0
P 3550 4100
F 0 "C3" H 3665 4146 50  0000 L CNN
F 1 "10nF" H 3665 4055 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 3588 3950 50  0001 C CNN
F 3 "~" H 3550 4100 50  0001 C CNN
	1    3550 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3950 3550 3850
Connection ~ 3550 3850
Wire Wire Line
	3550 4250 3550 4350
Wire Wire Line
	3550 4350 4300 4350
Connection ~ 4300 4350
Wire Wire Line
	4300 4350 4300 4400
$Comp
L Device:C C6
U 1 1 5DDC772E
P 4650 2550
F 0 "C6" V 4900 2500 50  0000 L CNN
F 1 "1uF" V 4800 2500 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.2mm_W3.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 4688 2400 50  0001 C CNN
F 3 "~" H 4650 2550 50  0001 C CNN
	1    4650 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 2550 4300 2550
Connection ~ 4300 2550
Wire Wire Line
	4300 2550 4300 2400
Wire Wire Line
	4800 2550 4950 2550
Wire Wire Line
	4950 2550 4950 4400
Wire Wire Line
	4950 4400 4300 4400
Connection ~ 4300 4400
Wire Wire Line
	4300 4400 4300 4550
Wire Wire Line
	3000 3150 3000 3500
Wire Wire Line
	3000 3500 3200 3500
Wire Wire Line
	3000 3500 3000 4550
Wire Wire Line
	3000 4550 4100 4550
Wire Wire Line
	4100 4550 4100 4750
Wire Wire Line
	4100 6900 3600 6900
Connection ~ 3000 3500
Wire Wire Line
	3600 7000 4200 7000
Wire Wire Line
	4200 7000 4200 6800
Wire Wire Line
	4200 4550 4300 4550
Connection ~ 4300 4550
Wire Wire Line
	4300 4550 4300 5100
$Comp
L Device:R_US R5
U 1 1 5DDEA8D9
P 3850 6800
F 0 "R5" V 3645 6800 50  0000 C CNN
F 1 "220k" V 3736 6800 50  0000 C CNN
F 2 "chrisj2:chrisj1_R_0204_4" V 3890 6790 50  0001 C CNN
F 3 "~" H 3850 6800 50  0001 C CNN
	1    3850 6800
	0    1    1    0   
$EndComp
Wire Wire Line
	3700 6800 3650 6800
Wire Wire Line
	4000 6800 4050 6800
Connection ~ 4200 6800
$Comp
L Device:C C5
U 1 1 5DDF2777
P 3850 6450
F 0 "C5" V 4100 6450 50  0000 C CNN
F 1 "1nF" V 4000 6450 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L7.2mm_W2.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 3888 6300 50  0001 C CNN
F 3 "~" H 3850 6450 50  0001 C CNN
	1    3850 6450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4000 6450 4050 6450
Wire Wire Line
	4050 6450 4050 6800
Connection ~ 4050 6800
Wire Wire Line
	4050 6800 4200 6800
Wire Wire Line
	3700 6450 3650 6450
Wire Wire Line
	3650 6450 3650 6800
Connection ~ 3650 6800
Wire Wire Line
	3650 6800 3600 6800
Wire Notes Line
	1450 6200 3300 6200
Wire Notes Line
	3300 6200 3300 7300
Wire Notes Line
	3300 7300 1450 7300
Text Notes 1300 6150 0    50   ~ 0
Maintain creepage & clearance for mains "reinforced" insulation
Wire Wire Line
	1250 1900 8850 1900
Connection ~ 8850 1900
Wire Wire Line
	8850 1900 9300 1900
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 5DD7B6A8
P 2500 4750
F 0 "J3" H 2420 4967 50  0000 C CNN
F 1 "Manual_Trigger" H 2420 4876 50  0000 C CNN
F 2 "chrisj2:chrisj1_WEIDMULLER_1508060000" H 2500 4750 50  0001 C CNN
F 3 "~" H 2500 4750 50  0001 C CNN
	1    2500 4750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2700 4750 4100 4750
Connection ~ 4100 4750
Wire Wire Line
	4100 4750 4100 5750
Wire Wire Line
	2700 4850 4200 4850
Connection ~ 4200 4850
Wire Wire Line
	4200 4850 4200 4550
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5DDB3EC3
P 1750 5100
F 0 "#FLG0101" H 1750 5175 50  0001 C CNN
F 1 "PWR_FLAG" H 1750 5273 50  0000 C CNN
F 2 "" H 1750 5100 50  0001 C CNN
F 3 "~" H 1750 5100 50  0001 C CNN
	1    1750 5100
	-1   0    0    1   
$EndComp
Connection ~ 1750 5100
Wire Wire Line
	1750 5100 1950 5100
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5DDB5233
P 2200 2400
F 0 "#FLG0102" H 2200 2475 50  0001 C CNN
F 1 "PWR_FLAG" H 2200 2573 50  0000 C CNN
F 2 "" H 2200 2400 50  0001 C CNN
F 3 "~" H 2200 2400 50  0001 C CNN
	1    2200 2400
	-1   0    0    1   
$EndComp
Connection ~ 2200 2400
Wire Wire Line
	2200 2400 3000 2400
$Comp
L chrisj1:ZS1052AC(H) T1
U 1 1 5DD827F8
P 8300 3350
F 0 "T1" H 8300 2925 50  0000 C CNN
F 1 "ZS1052AC(H)" H 8300 3016 50  0000 C CNN
F 2 "chrisj2:ZS1052AC(H)" H 8300 3350 50  0001 C CNN
F 3 "~" H 8300 3350 50  0001 C CNN
	1    8300 3350
	1    0    0    1   
$EndComp
Wire Wire Line
	8850 1900 8850 4150
$Comp
L Connector:Screw_Terminal_01x02 J5
U 1 1 5DD88F90
P 8300 2650
F 0 "J5" H 8220 2867 50  0000 C CNN
F 1 "Ext_Tfmr" H 8220 2776 50  0000 C CNN
F 2 "chrisj2:chrisj1_WEIDMULLER_1517260000" H 8300 2650 50  0001 C CNN
F 3 "~" H 8300 2650 50  0001 C CNN
	1    8300 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 2650 7850 2650
Wire Wire Line
	7850 2650 7850 3150
Connection ~ 7850 3150
Wire Wire Line
	7850 3150 7900 3150
Wire Wire Line
	8100 2750 8000 2750
Wire Wire Line
	8000 2750 8000 3450
Wire Wire Line
	8000 3450 7850 3450
Wire Wire Line
	7850 3450 7850 3550
Connection ~ 7850 3550
Wire Wire Line
	7850 3550 7900 3550
$Comp
L chrisj1:AudioJack3_SeparateSW_KunMing J2
U 1 1 5DDF184B
P 1300 6550
F 0 "J2" H 1282 6875 50  0000 C CNN
F 1 "Trigger" H 1282 6784 50  0000 C CNN
F 2 "chrisj2:JH_Kunming_phono_jack" H 1300 6550 50  0001 C CNN
F 3 "~" H 1300 6550 50  0001 C CNN
	1    1300 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 6650 1900 6650
Wire Wire Line
	1500 6450 1550 6450
Wire Wire Line
	1550 6450 1550 7100
NoConn ~ 1500 6900
NoConn ~ 1500 7000
NoConn ~ 1500 7100
NoConn ~ 1500 6550
Text Label 2400 2400 0    50   ~ 0
+12V
Text Label 1700 7100 0    50   ~ 0
ISOTRIG-
Text Label 1600 6650 0    50   ~ 0
ISOTRIG+
Connection ~ 2700 6650
Wire Wire Line
	2700 6650 2950 6650
Connection ~ 2700 7100
Wire Wire Line
	2700 7100 2950 7100
Connection ~ 2350 6650
Wire Wire Line
	2350 6650 2700 6650
Connection ~ 2350 7100
Wire Wire Line
	2350 7100 2700 7100
Wire Wire Line
	2200 6650 2350 6650
Wire Wire Line
	1550 7100 2350 7100
Text Label 3250 4550 0    50   ~ 0
~TRIG
Text Label 6050 3750 1    50   ~ 0
FetGate
Text Label 6750 3450 0    50   ~ 0
SCR_G
Text Label 7250 3150 0    50   ~ 0
SCR_A
Text Label 7850 3000 1    50   ~ 0
TFMR_PRI
Text Label 3700 3850 0    50   ~ 0
Filt~Trig
Text Label 3700 3300 1    50   ~ 0
CV
Connection ~ 1500 1800
Connection ~ 4300 5100
Connection ~ 4300 2400
Text Label 5100 3000 0    50   ~ 0
Out555
Text Label 2750 6650 0    50   ~ 0
OptoLED+
Text Label 3650 6700 1    50   ~ 0
OptoBase
$Comp
L Connector:Screw_Terminal_01x04 J4
U 1 1 5DEDF7C0
P 9500 2000
F 0 "J4" H 9418 1575 50  0000 C CNN
F 1 "Flash_Tube" H 9418 1666 50  0000 C CNN
F 2 "chrisj2:chrisj1_WEIDMULLER_1508260000" H 9500 2000 50  0001 C CNN
F 3 "~" H 9500 2000 50  0001 C CNN
	1    9500 2000
	1    0    0    1   
$EndComp
NoConn ~ 9300 2000
$Comp
L Connector:Screw_Terminal_01x02 J6
U 1 1 5DF27CF6
P 3650 5750
F 0 "J6" H 3570 5967 50  0000 C CNN
F 1 "Opto_Trigger" H 3570 5876 50  0000 C CNN
F 2 "chrisj2:chrisj1_WEIDMULLER_1508060000" H 3650 5750 50  0001 C CNN
F 3 "~" H 3650 5750 50  0001 C CNN
	1    3650 5750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3850 5750 4100 5750
Connection ~ 4100 5750
Wire Wire Line
	4100 5750 4100 6900
Wire Wire Line
	3850 5850 4200 5850
Wire Wire Line
	4200 4850 4200 5850
Connection ~ 4200 5850
Wire Wire Line
	4200 5850 4200 6800
$Comp
L Mechanical:MountingHole H1
U 1 1 5DF41E4C
P 5100 6150
F 0 "H1" H 5200 6196 50  0000 L CNN
F 1 "MountingHole" H 5200 6105 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 5100 6150 50  0001 C CNN
F 3 "~" H 5100 6150 50  0001 C CNN
	1    5100 6150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5DF41F3F
P 5100 6400
F 0 "H2" H 5200 6446 50  0000 L CNN
F 1 "MountingHole" H 5200 6355 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 5100 6400 50  0001 C CNN
F 3 "~" H 5100 6400 50  0001 C CNN
	1    5100 6400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5DF4210E
P 5100 6650
F 0 "H3" H 5200 6696 50  0000 L CNN
F 1 "MountingHole" H 5200 6605 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 5100 6650 50  0001 C CNN
F 3 "~" H 5100 6650 50  0001 C CNN
	1    5100 6650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5DF42310
P 5100 6900
F 0 "H4" H 5200 6946 50  0000 L CNN
F 1 "MountingHole" H 5200 6855 50  0000 L CNN
F 2 "MountingHole:MountingHole_4mm" H 5100 6900 50  0001 C CNN
F 3 "~" H 5100 6900 50  0001 C CNN
	1    5100 6900
	1    0    0    -1  
$EndComp
$EndSCHEMATC
