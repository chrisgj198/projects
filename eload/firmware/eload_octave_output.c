#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#include "aduc7060_cj.h"
#include "lowlevel.h"
#include "lowlevel_private.h"
#include "global_vars.h"
#include "print_functions.h"






#define ADC0_AMPS_PER_LSB 8.925079E-07
#define ADC0_OFFSET (-3105)
double adc0_to_amps(int32_t adc0val){
	// FIXME: should check for overflow here
	return (adc0val - ADC0_OFFSET) * ADC0_AMPS_PER_LSB;
}

#define ADC1_VOLTS_PER_LSB 2.810748E-05
#define ADC1_OFFSET (-3145)
double adc1_to_volts(int32_t adc1val){
	// FIXME: should check for overflow here
	return (adc1val - ADC1_OFFSET) * ADC1_VOLTS_PER_LSB;
}


#define DAC_CODE_0V 1528
#define DAC_LSB_PER_V 660.095654
uint32_t volts_to_dac_code(double vset){
	if(vset < 0) vset = 0;
	return DAC_CODE_0V + ((uint32_t)(0.5 + (vset * DAC_LSB_PER_V)));
}



#if 1
#define STATE_GOTO_VOC 1
#define STATE_MEASURE_VOC 2
#define STATE_TRACE_IV_CURVE 3
#define STATE_DWELL_MPP 4
#define NPOINTS 100
#define DWELL_POINTS 10000
int main(void){
	uint8_t str_buf[80];
	int32_t adc0_result;
	int32_t adc1_result;

	double voc;
	double v, i, p;
	double vmp, imp, pmax;
	uint32_t mpp_dac_code;
	uint32_t dac_code;
	uint32_t dac_max;
	uint32_t dac_min;
	uint32_t dac_step;
	uint32_t dwellcount;
	uint32_t point_index;
	
	double i_sum;
	double v_sum;
	double p_sum;
	uint32_t count = 0;
	uint8_t ch;
	
	setup_chip();
	set_dac_v_range(DAC_V_RANGE_LOW);
	
	state = STATE_GOTO_VOC;
	
	while (1) {

		switch(state){
			case STATE_GOTO_VOC:
			case STATE_MEASURE_VOC:
				break;
//			default:
//				if(check_mosfet_enabled() == 0){ // overcurrent or overtemperature triggered - restart
//					serial_puts("%%resetting...\r\n");
//					state = STATE_GOTO_VOC;
//				}
		}
		
		switch(state){
			case STATE_GOTO_VOC:
				write_mosfet_enabled(0);
				discard_samples(20);
				state = STATE_MEASURE_VOC;
				break;
			case STATE_MEASURE_VOC:
				if (get_adc_results(&adc0_result, &adc1_result)){
					voc = adc1_to_volts(adc1_result);
					dac_max = volts_to_dac_code(voc+1.0);
					dac_min = volts_to_dac_code(0);
					dac_step = (dac_max - dac_min) / NPOINTS;

//					sprintf((char *)str_buf, ",,%f,Volts,(Voc),dac_max=%u,dac_min=%d,dac_step=%u\r\n", voc, dac_max, dac_min, dac_step);
					sprintf((char *)str_buf, "voc=%f;\r\n", voc);
					serial_puts(str_buf);

					mpp_dac_code = 0;
					vmp = 0;
					imp = 0;
					pmax = 0;
					dac_code = dac_max;
					write_dac_value(dac_code);
					write_mosfet_enabled(1);
					state = STATE_TRACE_IV_CURVE;
					//sprintf((char *)str_buf, "voltage=zeros(1,%d);", NPOINTS+1);
					//serial_puts(str_buf);
					point_index = 1;
				}
				break;
			case STATE_TRACE_IV_CURVE:
				if (get_adc_results(&adc0_result, &adc1_result)){
					v = adc1_to_volts(adc1_result);
					i = adc0_to_amps(adc0_result);
					p = v * i;
//					sprintf((char *)str_buf, "%f,Amps,%f,Volts,%u,daclsb\r\n", i, v, dac_code);
					sprintf((char *)str_buf, "current(%d)=%f;\r\nvoltage(%d)=%f;\r\n", point_index, i, point_index, v);
					serial_puts(str_buf);
					point_index++;
					if (p > pmax){
						vmp = v;
						imp = i;
						pmax = p;
						mpp_dac_code = dac_code;
					}
					if(dac_code >= (dac_min + dac_step)){
						dac_code -= dac_step;
						write_dac_value(dac_code);
					} else {
						write_dac_value(dac_max);
						write_mosfet_enabled(0);

						sprintf((char *)str_buf, "%% %f,Amps,%f,Volts,%f,Watts,(MPP found)\r\n", imp, vmp, pmax);
						serial_puts(str_buf);
						sprintf((char *)str_buf, "plot(voltage, current)\r\n");
						serial_puts(str_buf);
						sprintf((char *)str_buf, "power = voltage .* current;\r\npmax=max(power)\r\n");
						serial_puts(str_buf);


						dwellcount = 0;
						
						i_sum = 0;
						v_sum = 0;
						p_sum = 0;
						count = 0;

						state = STATE_DWELL_MPP;
					}
				}
				break;
			case STATE_DWELL_MPP:
				if (get_adc_results(&adc0_result, &adc1_result)){
					v = adc1_to_volts(adc1_result);
					i = adc0_to_amps(adc0_result);
					p = v * i;
					dwellcount++;

					i_sum += i;
					v_sum += v;
					p_sum += p; 
					count += 1;

				}
				if((dwellcount >= DWELL_POINTS) || serial_kbhit()){
					if(serial_kbhit()){
						ch = serial_getch();
					}
					
					sprintf((char *)str_buf, "%% %f,Amps,%f,Volts,%f,Watts,(average)\r\n", i_sum/count, v_sum/count, p_sum/count);
					serial_puts(str_buf);				

					state = STATE_GOTO_VOC;
				}
				break;
			default:
				state = STATE_GOTO_VOC;
		}
	}
	//return 0;
}
#endif

#if 0
int main(void){
	int32_t adc0_result;
	int32_t adc1_result;
	
	int64_t adc0_sum = 0;
	int64_t adc1_sum = 0;
	uint32_t count = 0;
	
	uint8_t str_buf[80];
	uint8_t ch;
	uint32_t dacval = 0;
	uint32_t ocot;
	
	setup_chip();

	dacval = 0;
	set_dac_v_range(DAC_V_RANGE_LOW);
	write_dac_value(dacval);

	serial_puts((uint8_t *)"eload\r\n");
	
	while (1) {
		if (get_adc_results(&adc0_result, &adc1_result)){ // Is there an ADC0 result ready for UART transmission?
			ocot = check_mosfet_enabled();
			adc0_sum += adc0_result;
			adc1_sum += adc1_result;
			count += 1;
			

			
//			dacval += 16;
//			if(dacval > DAC0DAT_DATA_MAXVAL) dacval = 0;
			write_dac_value(dacval);
	//		write_dac_value(0);
			if(count == 10){
//				sprintf((char *)str_buf, "%u\t%"PRId64"\t%"PRId64"\t%u\r\n", dacval, adc0_sum, adc1_sum, ocot);
			sprintf((char *)str_buf, "%u\t%d\t%d\t%u\r\n", dacval, adc0_result, adc1_result, ocot);
				serial_puts(str_buf);
				adc0_sum = 0;
				adc1_sum = 0;
				count = 0;
			}
		}

		if(serial_kbhit()){
			ch = serial_getch();
			
			switch(ch){
				case '0':
					write_mosfet_enabled(0);
					serial_putch('0');
					serial_putch('\r');
					serial_putch('\n');
					adc0_sum = 0;
					adc1_sum = 0;
					count = 0;
					dacval = 0;
					break;
				
				case '1':
					write_mosfet_enabled(1);
					serial_putch('1');
					serial_putch('\r');
					serial_putch('\n');
					adc0_sum = 0;
					adc1_sum = 0;
					count = 0;
					dacval = 0;
					break;
				case 'd':
					dacval += 16;
					sprintf((char *)str_buf, "%u\r\n", dacval);
					serial_puts(str_buf);
					break;
				case 'D':
					dacval += 256;
					sprintf((char *)str_buf, "%u\r\n", dacval);
					serial_puts(str_buf);
					break;
				case 'c':
					dacval -= 16;
					sprintf((char *)str_buf, "%u\r\n", dacval);
					serial_puts(str_buf);
					break;
				case 'C':
					dacval -= 256;
					sprintf((char *)str_buf, "%u\r\n", dacval);
					serial_puts(str_buf);
					break;
				
				default:
					//serial_putch(ch);
					sprintf((char *)str_buf, "%u\t%f Amps \t%f Volts\t%u\r\n", dacval, adc0_to_amps(adc0_result), adc1_to_volts(adc1_result), ocot);
					serial_puts(str_buf);

			}
		}

	}
//	return 0;
}
#endif



