#include <stdint.h>
#include <stdbool.h>

#define BAUDRATE 115200
//#define BAUDRATE 9600

#define SENSE_RESISTANCE_UNDEFINED 0
#define SENSE_RESISTANCE_10_MILLIOHMS 1
#define SENSE_RESISTANCE_100_MILLIOHMS 2
#define SENSE_RESISTANCE_1_OHM 3

#define DAC_V_RANGE_HIGH 0
#define DAC_V_RANGE_LOW 1

uint32_t get_uptime_lsbyte(void);


void serial_putch(uint8_t ch);
uint8_t serial_kbhit(void);
uint8_t serial_getch(void);
	



void setup_chip(void);

void set_dac_v_range(uint32_t v_range);
void write_dac_value(uint32_t val);
uint32_t get_adc_results(int32_t *adc0_result, int32_t *adc1_result);

// checks the overcurrent / overtemperature latch
uint32_t check_mosfet_enabled(void);

// Write new state to the overcurrent / overtemperature latch
// Clear the latch (write 0) to enable the power MOSFET to sink current
// Set the latch (write 1) to make the power MOSFET high impedance
void write_mosfet_enabled(uint32_t state);
void discard_samples(uint32_t nsamples);


