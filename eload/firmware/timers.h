#include <stdint.h>
#include <stdbool.h>

#define NUM_TIMERS 32

void update_uptime(void);
void adjust_uptime_and_timers(void);
void set_timer(uint8_t timernum, uint32_t delay);
void reset_timers(void);
bool timer_is_expired_and_rearm(uint8_t timernum, uint32_t delay);
bool timer_is_expired(uint8_t timernum);
uint32_t time_remaining(uint8_t timernum);
void limit_timer(uint8_t timernum, uint32_t delay);
