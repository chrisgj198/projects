
#include <stdint.h>

#include "lowlevel.h"
#include "print_functions.h"

//print_functions.c:
uint8_t hex2uchar(uint8_t v){
	if(v <= '9'){
		v -= '0';
	}else{
		v &= 0xdf;
		v -= ('A'-10);
	}
	return v;
}

// sends a string to the rs232 port
void serial_puts(const uint8_t *s){
	while(*s){
		serial_putch(*(s++));
	}
}

void serial_printx(uint8_t a){
	a &= 0x0F;
	if(a<10){
		a += '0';
	}else{
		a += ('A'-10);
	}
	serial_putch(a);
}

void serial_printxx(uint8_t a){
	serial_printx(a>>4);
	serial_printx(a);
}
void serial_printxxxx(uint16_t a){
	serial_printx(a>>12);
	serial_printx(a>>8);
	serial_printx(a>>4);
	serial_printx((uint8_t)a);
}
void serial_printxxxxxxxx(uint32_t a){
	serial_printx(a>>28);
	serial_printx(a>>24);
	serial_printx(a>>20);
	serial_printx(a>>16);
	serial_printx(a>>12);
	serial_printx(a>>8);
	serial_printx(a>>4);
	serial_printx(a);
}
void serial_print_bin_uint32(uint32_t a){
	uint32_t i;
	serial_putch('<');

	for(i=0x80000000; i; i>>=1){
		serial_putch((i&a)?'1':'0');
	}
	serial_putch('>');

}
void serial_print_bin_uint8(uint8_t a){
	uint8_t i;

	for(i=0x80; i; i>>=1){
		serial_putch((i&a)?'1':'0');
	}
}
void serial_print_bin_uint16(uint16_t a){
	serial_print_bin_uint8((uint8_t)(a >> 8));
	serial_print_bin_uint8((uint8_t)a);
}

void serial_print_dec_uint8(uint8_t n){
	uint8_t p, q, d;
	uint8_t m;

	p=0;

	for(d=0; d<3; d++){
		m = 100;
		if(d==1)m = 10;
		if(d==2){
			m = 1;
			p = 1;
		}

		q=0;
		while(n >= m){
			n -= m;
			q++;
			p=1;
		}
		if(p){
			serial_putch(q + '0');
		}
	}
}
void serial_print_dec_uint32(uint32_t n){
	uint8_t p, q, d;
	uint32_t m;

	p=0;

	d=10; // will get decremented straight away
	do{
		d--;
		switch(d){
			case 9:
				m = 1000000000;
				break;
			case 8:
				m = 100000000;
				break;
			case 7:
				m = 10000000;
				break;
			case 6:
				m = 1000000;
				break;
			case 5:
				m = 100000;
				break;
			case 4:
				m = 10000;
				break;
			case 3:
				m = 1000;
				break;
			case 2:
				m = 100;
				break;
			case 1:
				m = 10;
				break;
			case 0:	
				m = 1;
				p = 1; // force printing last digit, even if zero
				break;
		}
		q=0;
		while(n >= m){
			n -= m;
			q++;
			p=1;
		}
		if(p){
			serial_putch(q + '0');
		}
	}while(d);
}

#if 0
void serial_print_dec_uint16(uint16_t n){
	uint8_t p, q, d;
	uint16_t m;

	p=0;

	for(d=0; d<5; d++){
		m = 10000;
		if(d==1)m = 1000;
		if(d==2)m = 100;
		if(d==3)m = 10;
		if(d==4){
			m = 1;
			p = 1;
		}

		q=0;
		while(n >= m){
			n -= m;
			q++;
			p=1;
		}
		if(p){
			serial_putch(q + '0');
		}
	}
}
#endif

void serial_print_dec_uint16(uint16_t n){
	serial_print_dec_uint32(n);
}


void serial_printcrlf(void){
	serial_putch('\r');
	serial_putch('\n');
}

uint8_t serial_inpx(void){
	uint8_t v;

	v = serial_getch();
	if(v <= '9'){
		v -= '0';
	}else{
		v &= 0xdf;
		v -= ('A'-10);
	}
	return v;
}

uint8_t serial_inpxx(void){
	uint8_t v;

	v = serial_inpx();
	v <<= 4;
	v += serial_inpx();
	return v;
}

uint16_t serial_inpxxxx(void){
	uint16_t v;
	uint8_t i;

	v = 0;
	for(i=0; i<4; i++){
		v <<= 4;
		v += serial_inpx();
	}
	return v;
}

uint32_t serial_inpxxxxxxxx(void){
	uint32_t v;
	uint8_t i;

	v = 0;
	for(i=0; i<8; i++){
		v <<= 4;
		v += serial_inpx();
	}
	return v;
}
