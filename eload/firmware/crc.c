#include <stdint.h>

void reset_crc16(uint16_t *crcsr){
	*crcsr = 0xFFFF; // initialise to 1s
}

// See http://www.ece.cmu.edu/~koopman/roses/dsn04/koopman04_crc_poly_embedded.pdf for selection of polynomial
void run_crc16_on_byte(uint16_t *crcsr, const uint8_t data){
	const uint16_t gen_poly = 0x90D9; // generator polynomial Koopman notation 0xC86C = 1100100001101100+1, drop leading 1 to get 1001000011011001 =0x90D9
	uint8_t j;

	*crcsr ^= (data << (8 * sizeof(*crcsr) - 8 * sizeof(data)));
	for (j = 0; j<(8 * sizeof(data)); j++){
		if ((*crcsr) & (1 << (8 * sizeof(*crcsr) - 1))){
			*crcsr = ((*crcsr) << 1) ^ gen_poly;
		} else {
			(*crcsr) <<= 1;
		}
	}

}

void run_crc16_on_bits(uint16_t *crcsr, const uint16_t data, const uint8_t width){
	const uint16_t gen_poly = 0x90D9; // generator polynomial Koopman notation 0xC86C = 1100100001101100+1, drop leading 1 to get 1001000011011001 =0x90D9
	uint16_t sr;
	uint8_t j;

	sr = *crcsr;
	sr ^= (data << (8 * sizeof(sr) - width));
	for (j = 0; j<width; j++){
		if (sr & (1 << (8 * sizeof(sr) - 1))){
			sr = (sr << 1) ^ gen_poly;
		} else {
			sr <<= 1;
		}
	}
	*crcsr = sr;
}

uint16_t get_crc16(const uint16_t *crcsr){
	return (*crcsr) ^ 0x5555; // "coset"  - to make strings of all 1 (or all 0) bits not encode to a valid message and CRC for initial state of all 1 (or 0)
}
