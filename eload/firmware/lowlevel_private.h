#include "aduc706x_regbits.h"



#define GP0DAT_INITVAL (\
		GPXDAT_DIR_INP(7) | GPXDAT_OUTBIT_LO(7) | /* P0.7 - */ \
		GPXDAT_DIR_OUT(6) | GPXDAT_OUTBIT_LO(6) | /* P0.6 Output, RS485_TXEN */ \
		GPXDAT_DIR_OUT(5) | GPXDAT_OUTBIT_LO(5) | /* P0.5 Output, I_RANGE_0, Low to select 1 Ohm sense, can be low always. */ \
		GPXDAT_DIR_OUT(4) | GPXDAT_OUTBIT_LO(4) | /* P0.4 Output, Connector pin 3, LED */ \
		GPXDAT_DIR_OUT(3) | GPXDAT_OUTBIT_LO(3) | /* P0.3 Output, DAC_VRANGE selects full scale voltage */ \
		GPXDAT_DIR_INP(2) | GPXDAT_OUTBIT_LO(2) | /* P0.2 Input,  OC_READBACK overcurrent/overtemp latch read-back */ \
		GPXDAT_DIR_OUT(1) | GPXDAT_OUTBIT_LO(1) | /* P0.1 Output, OC_C overcurrent/overtemp latch clock */ \
		GPXDAT_DIR_OUT(0) | GPXDAT_OUTBIT_HI(0) | /* P0.0 Output, OC_D overcurrent/overtemp latch data */ \
		0)


#define GP1DAT_INITVAL (\
		GPXDAT_DIR_INP(7) | GPXDAT_OUTBIT_LO(7) | /* P1.7 - */ \
		GPXDAT_DIR_OUT(6) | GPXDAT_OUTBIT_LO(6) | /* P1.6 Output, Connector pin 7, LED */ \
		GPXDAT_DIR_OUT(5) | GPXDAT_OUTBIT_LO(5) | /* P1.5 Output, Connector pin 6 */ \
		GPXDAT_DIR_OUT(4) | GPXDAT_OUTBIT_LO(4) | /* P1.4 Output, Connector pin 5 */ \
		GPXDAT_DIR_OUT(3) | GPXDAT_OUTBIT_LO(3) | /* P1.3 Output, I_RANGE_1, Low for 100mOhm sense, (and can be low for 10mOhm sense also) */ \
		GPXDAT_DIR_OUT(2) | GPXDAT_OUTBIT_LO(2) | /* P1.2 Output, I_RANGE_2, Low for 10mOhm sense (only) */ \
		GPXDAT_DIR_OUT(1) | GPXDAT_OUTBIT_HI(1) | /* P1.1 Output, TXD */ \
		GPXDAT_DIR_INP(0) | GPXDAT_OUTBIT_HI(0) | /* P1.0 Input,  RXD */ \
		0)

#define GP2DAT_INITVAL (\
		GPXDAT_DIR_INP(7) | GPXDAT_OUTBIT_LO(7) | /* P2.7 - */ \
		GPXDAT_DIR_INP(6) | GPXDAT_OUTBIT_LO(6) | /* P2.6 - */ \
		GPXDAT_DIR_INP(5) | GPXDAT_OUTBIT_LO(5) | /* P2.5 - */ \
		GPXDAT_DIR_INP(4) | GPXDAT_OUTBIT_LO(4) | /* P2.4 - */ \
		GPXDAT_DIR_INP(3) | GPXDAT_OUTBIT_LO(3) | /* P2.3 - */ \
		GPXDAT_DIR_INP(2) | GPXDAT_OUTBIT_LO(2) | /* P2.2 - */ \
		GPXDAT_DIR_OUT(1) | GPXDAT_OUTBIT_LO(1) | /* P2.1/IRQ3/PWM5 Output, Connector pin 8 */ \
		GPXDAT_DIR_OUT(0) | GPXDAT_OUTBIT_LO(0) | /* P2.0/IRQ2/PWM0/EXTCLK Output, Connector pin 4 */ \
		0)


#define RSENSE_CONNECT_10_MILLIOHMS do{GP1CLR = GPX_OUTBIT(2);}while(0)
#define RSENSE_DISCONNECT_10_MILLIOHMS do{GP1SET = GPX_OUTBIT(2);}while(0)
#define RSENSE_CONNECT_100_MILLIOHMS do{GP1CLR = GPX_OUTBIT(3);}while(0)
#define RSENSE_DISCONNECT_100_MILLIOHMS do{GP1SET = GPX_OUTBIT(3);}while(0)
#define RSENSE_CONNECT_1_OHM do{GP0CLR = GPX_OUTBIT(5);}while(0)
#define RSENSE_DISCONNECT_1_OHM do{GP0SET = GPX_OUTBIT(5);}while(0)
#define VRANGE_SELECT_LOW do{GP0CLR = GPX_OUTBIT(3);}while(0)
#define VRANGE_SELECT_HIGH do{GP0SET = GPX_OUTBIT(3);}while(0)

#define LED_0_4_OFF do{GP0CLR = GPX_OUTBIT(4);}while(0)
#define LED_0_4_ON do{GP0SET = GPX_OUTBIT(4);}while(0)

#define LED_1_4_OFF do{GP1CLR = GPX_OUTBIT(4);}while(0)
#define LED_1_4_ON do{GP1SET = GPX_OUTBIT(4);}while(0)

#define LED_1_5_OFF do{GP1CLR = GPX_OUTBIT(5);}while(0)
#define LED_1_5_ON do{GP1SET = GPX_OUTBIT(5);}while(0)

#define LED_1_6_OFF do{GP1CLR = GPX_OUTBIT(6);}while(0)
#define LED_1_6_ON do{GP1SET = GPX_OUTBIT(6);}while(0)

#define LED_2_1_OFF do{GP2CLR = GPX_OUTBIT(1);}while(0)
#define LED_2_1_ON do{GP2SET = GPX_OUTBIT(1);}while(0)

#define OC_D_LOW do{GP0CLR = GPX_OUTBIT(0);}while(0)
#define OC_D_HIGH do{GP0SET = GPX_OUTBIT(0);}while(0)

#define OC_C_LOW do{GP0CLR = GPX_OUTBIT(1);}while(0)
#define OC_C_HIGH do{GP0SET = GPX_OUTBIT(1);}while(0)

#define IS_MOSFET_ENABLED ((GP0DAT & GPX_INPBIT(2)) ? 0 : 1)

#define SERIAL_DRIVER_OFF do{GP0CLR = GPX_OUTBIT(6);}while(0)
#define SERIAL_DRIVER_ON do{GP0SET = GPX_OUTBIT(6);}while(0)


#define DAC_AMP_SETTLING 2 // in ADC sample periods
#define ADC_LATENCY 2 // in ADC sample periods


void service_uart_tx(void);
void serial_start_transmitter(void);



/* FIQ\IRQ Handler Functions */
void IRQ0_FIQ_SOURCE (void);
void IRQ1_SW_INT     (void);
void IRQ3_T0_INT     (void);
void IRQ4_T1_INT     (void);  
void IRQ5_T2_INT     (void);
void IRQ6_T3_INT     (void);
void IRQ10_ADC_INT   (void);
void IRQ11_UART_INT  (void);
void IRQ12_SPIM_INT  (void);
void IRQ13_XIRQ0_INT (void);
void IRQ14_XIRQ1_INT (void);
void IRQ15_I2CM_INT  (void);
void IRQ16_I2CS_INT  (void);
void IRQ17_SPIS_INT  (void);
void IRQ18_XIRQ2_INT (void);
void IRQ19_XIRQ3_INT (void);
void IRQx_NULL_INT   (void);


/* Exception Handlers */
void PAB_Function    (void);
void DAB_Function    (void);
void SWI_Function    (void);
void UNDEF_Function  (void);


typedef void (*tyIntHndlr)(void);  






