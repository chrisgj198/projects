
#include <stdint.h>
#include <stdbool.h>

#include "lowlevel.h"
#include "timers.h"

uint32_t uptime;
uint32_t timer_expiry[NUM_TIMERS];
bool timer_running[NUM_TIMERS];

void update_uptime(void){
	uint8_t tmp_uptime_lsbyte;

	// Check the time least significant byte updated by the ISR, and detect whether it has overflowed

	tmp_uptime_lsbyte = get_uptime_lsbyte();
		// note that uptime_lsbyte is a single byte so the ISR cannot change some of the bits before we copy it
		// and some of the bits after we copy it. A long int would not have this nice property on some processors.

	if(tmp_uptime_lsbyte < ((uint8_t)(uptime))){ // uptime_lsbyte is lower than last time this function ran, so it overflowed
		uptime += 0x00000100; // Account for the overflow of the least significant byte
	}

	uptime = (uptime & 0xFFFFFF00) | tmp_uptime_lsbyte; // Copy in the new least significant byte
}


// timers.c
void adjust_uptime_and_timers(void){
	uint8_t i;
	uint32_t time_to_subtract;

	time_to_subtract = uptime;
	if(time_to_subtract & 0x80000000){
		time_to_subtract &= 0x7FFFFF00; // don't tamper with least significant byte as the ISR handles that, nor the MSB.
		for(i=0; i<NUM_TIMERS; i++){
			timer_expiry[i] -= time_to_subtract;
		}
		uptime -= time_to_subtract;
	}
}

void set_timer(uint8_t timernum, uint32_t delay){
	update_uptime();
	timer_expiry[timernum] = uptime+delay;
	if(delay){
		timer_running[timernum] = 1;
	}else{
		timer_running[timernum] = 0;
	}
}

void reset_timers(void){
	uint8_t i;
	for(i=0; i<NUM_TIMERS; i++){
		set_timer(i, 0);
	}
}

// this function is intended to be used where an event should occur regularly, but the timer is checked sufficiently infrequently that
// it is desired for the next expiry of the timer to be set relative to the expiry time rather than when the expiry was noticed (later).
// This function will not clear timer_running[].
bool timer_is_expired_and_rearm(uint8_t timernum, uint32_t delay){
	update_uptime();
	if (timer_running[timernum]){
		if (uptime > timer_expiry[timernum]){
			timer_expiry[timernum] += delay;
			return 1;
		} else{
			return 0;
		}
	}
	return 1;
}

bool timer_is_expired(uint8_t timernum){
	update_uptime();
	if(timer_running[timernum]){
		if(uptime > timer_expiry[timernum]){
			timer_running[timernum]=0;
			return 1;
		}else{
			return 0;
		}
	}
	return 1;
}

uint32_t time_remaining(uint8_t timernum){
	if(timer_is_expired(timernum)) return 0; // updates uptime for us
	return timer_expiry[timernum] - uptime;
}

// Makes sure the timer will not run for longer than delay,
// but leaves it alone if it would expire before delay anyway.
void limit_timer(uint8_t timernum, uint32_t delay){
	if (time_remaining(timernum) > delay){
		set_timer(timernum, delay);
	}
}

