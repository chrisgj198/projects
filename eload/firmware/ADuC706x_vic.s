;/*****************************************************************************/
;/* STARTUP.S: Startup file for ADI ADuC706x device series                    */
;/*****************************************************************************/
;/* <<< Use Configuration Wizard in Context Menu >>>                          */ 
;/*****************************************************************************/
;/* This file is part of the uVision/ARM development tools.                   */
;/* Copyright (c) 2005-2006 Keil Software. All rights reserved.               */
;/* This software may only be used under the terms of a valid, current,       */
;/* end user licence from KEIL for a compatible version of KEIL software      */
;/* development tools. Nothing else gives you the right to use this software. */
;/*****************************************************************************/
	INCLUDE ADuC7060.def


;/*
; *  The STARTUP.S code is executed after CPU Reset. This file may be 
; *  translated with the following SET symbols. In uVision these SET 
; *  symbols are entered under Options - ASM - Define.
; *
; *  RAM_INTVEC: when set the startup code copies exception vectors 
; *  from on-chip Flash to on-chip RAM and remaps RAM to address 0.
; */


; Standard definitions of Mode bits and Interrupt (I & F) flags in PSRs

Mode_USR        EQU     0x10
Mode_FIQ        EQU     0x11
Mode_IRQ        EQU     0x12
Mode_SVC        EQU     0x13
Mode_ABT        EQU     0x17
Mode_UND        EQU     0x1B
Mode_SYS        EQU     0x1F

I_Bit           EQU     0x80            ; when I bit is set, IRQ is disabled
F_Bit           EQU     0x40            ; when F bit is set, FIQ is disabled


;// <h> Stack Configuration (Stack Sizes in Bytes)
;//   <o0> Undefined Mode      <0x0-0xFFFFFFFF:8>
;//   <o1> Supervisor Mode     <0x0-0xFFFFFFFF:8>
;//   <o2> Abort Mode          <0x0-0xFFFFFFFF:8>
;//   <o3> Fast Interrupt Mode <0x0-0xFFFFFFFF:8>
;//   <o4> Interrupt Mode      <0x0-0xFFFFFFFF:8>
;//   <o5> User/System Mode    <0x0-0xFFFFFFFF:8>
;// </h>

UND_Stack_Size  EQU     0x00000080
SVC_Stack_Size  EQU     0x00000080
ABT_Stack_Size  EQU     0x00000080
FIQ_Stack_Size  EQU     0x00000100
IRQ_Stack_Size  EQU     0x00000100
USR_Stack_Size  EQU     0x00000400

ISR_Stack_Size  EQU     (UND_Stack_Size + SVC_Stack_Size + ABT_Stack_Size + \
                         FIQ_Stack_Size + IRQ_Stack_Size)

                AREA    STACK, NOINIT, READWRITE, ALIGN=3

Stack_Mem       SPACE   USR_Stack_Size
__initial_sp    SPACE   ISR_Stack_Size
Stack_Top


;// <h> Heap Configuration
;//   <o>  Heap Size (in Bytes) <0x0-0xFFFFFFFF>
;// </h>

Heap_Size       EQU     0x00000000

                AREA    HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE   Heap_Size
__heap_limit


                PRESERVE8
                

; Area Definition and Entry Point
;  Startup Code must be linked first at Address at which it expects to run.

                AREA    Reset, CODE, READONLY
                ARM


; Exception Vectors
;  Mapped to Address 0.
;  Absolute addressing mode must be used.
;  Dummy Handlers are implemented as infinite loops which can be modified.

Vectors         LDR     PC, Reset_Addr         
                LDR     PC, Undef_Addr
                LDR     PC, SWI_Addr
                LDR     PC, PAbt_Addr
                LDR     PC, DAbt_Addr
                NOP                            ; Reserved Vector 
                LDR     PC, IRQ_Addr
                LDR     PC, FIQ_Addr

                EXTERN Undef_Handler
                EXTERN SWI_Handler
                EXTERN PAbt_Handler
                EXTERN DAbt_Handler
                EXTERN IRQ_Handler
                EXTERN FIQ_Handler

Reset_Addr      DCD     Reset_Handler
Undef_Addr      DCD     Undef_Handler
SWI_Addr        DCD     SWI_Handler
PAbt_Addr       DCD     PAbt_Handler
DAbt_Addr       DCD     DAbt_Handler
                DCD     0                      ; Reserved Address 
IRQ_Addr        DCD     ADI_IRQ_Nested_Interrupt_Setup
FIQ_Addr        DCD     ADI_FIQ_Nested_Interrupt_Setup


; Reset Handler

                EXPORT  Reset_Handler
Reset_Handler   


; Copy Exception Vectors to Internal RAM and Remap Memory
;  (when Interrupt Vectors are in RAM)

                IF      :DEF:RAM_INTVEC
                ADR     R8, Vectors         ; Source
                LDR     R9, =0x00040000     ; Destination
                LDMIA   R8!, {R0-R7}        ; Load Vectors 
                STMIA   R9!, {R0-R7}        ; Store Vectors 
                LDMIA   R8!, {R0-R7}        ; Load Handler Addresses 
                STMIA   R9!, {R0-R7}        ; Store Handler Addresses 
                LDR     R0, =MMR_BASE
                MOV     R1, #1     
                STR     R1, [R0,#REMAP_OFFSET]          
                ENDIF


; Setup Stack for each mode

                LDR     R0, =Stack_Top

;  Enter Undefined Instruction Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_UND:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #UND_Stack_Size

;  Enter Abort Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_ABT:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #ABT_Stack_Size

;  Enter FIQ Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_FIQ:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #FIQ_Stack_Size

;  Enter IRQ Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_IRQ:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #IRQ_Stack_Size

;  Enter Supervisor Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #SVC_Stack_Size

;  Enter User Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_USR
                IF      :DEF:__MICROLIB

                EXPORT __initial_sp

                ELSE

                MOV     SP, R0
                SUB     SL, SP, #USR_Stack_Size

                ENDIF


; Enter the C code

                IMPORT  __main
                LDR     R0, =__main
                BX      R0


                IF      :DEF:__MICROLIB

                EXPORT  __heap_base
                EXPORT  __heap_limit

                ELSE
; User Initial Stack & Heap
                AREA    |.text|, CODE, READONLY

                IMPORT  __use_two_region_memory
                EXPORT  __user_initial_stackheap
__user_initial_stackheap

                LDR     R0, =  Heap_Mem
                LDR     R1, =(Stack_Mem + USR_Stack_Size)
                LDR     R2, = (Heap_Mem +      Heap_Size)
                LDR     R3, = Stack_Mem
                BX      LR
                ENDIF



; Handle the Nested VIC (IRQ) on the ADUC7060 
                AREA    |.text|, CODE, READONLY
                EXPORT  ADI_IRQ_Nested_Interrupt_Setup
ADI_IRQ_Nested_Interrupt_Setup

;======================== Interrupts disabled!=================================;
;========================     IRQ Mode!       =================================;
        ; This routine consumes 28 bytes of irq stack per nested interrupt
        ; => 224 (8 x 28) due to the 8 maximum interrupt nesting.

        ; This routine consumes 8 bytes of SYS stack per nested interrupt
        ; => 64 (8 x 8) due to the 8 maximum interrupt nesting.

        ; R12 is banked in FIQ mode is handling of this reqister is different
        ; between the IRQ and FIQ handlers

        ; Stack modified registers (potentially callee modified too)
        STMDB   SP!    ,{R0-R3,R12,LR}

        ; Stack the SPSR
        MRS     R0      ,SPSR
        STMDB   SP!    ,{R0}

        ; Retrieve handler for this interrupt
        LDR     R0      ,=IRQVEC
        LDR     R0      ,[R0]
        LDR     R2      ,[R0]

        ; Allow new higher priority IRQs (and FIQs if enabled) in system mode
        MRS     R0      ,CPSR
        BIC     R0      ,R0     ,#I_Bit
        ORR     R1      ,R0     ,#Mode_SYS
        MSR     CPSR_c  ,R1 

;======================== Interrupts enabled! =================================;
;========================     SYS Mode!       =================================;
        ; We need to store the R12,LR of system mode
        STMDB   SP!    ,{R12,LR}

        ; Calling interrupt handler in system mode with interrupts enabled
        MOV     LR,PC
        BX      R2

        ; We need to restore the R12,LR of system ,mode also
        LDMIA   SP!    ,{R12,LR}

        ; Change back to IRQ mode, disabling I interrupts 
        MRS     R0      ,CPSR
        BIC     R0      ,R0     ,#Mode_SYS
        ORR     R0      ,R0     ,#(Mode_IRQ :OR: I_Bit)
        MSR     CPSR_c  ,R0 
;======================== Interrupts disabled!=================================;
;========================     IRQ Mode!       =================================;

        ; Clear the highest priority interrupt by writing 0xFF to IRQSTAN
        MOV      R0     ,#0xFF     
        LDR      R1     ,=IRQSTAN
        STR      R0     ,[R1]

        ; Restore the SPSR
        LDMIA   SP!     ,{R0}
        MSR     SPSR_cxsf    ,R0

        ; Restore modified registers 
        LDMIA   SP!    ,{R0-R3,R12,LR}

        ; Return from interrupt
        SUBS    PC     ,LR,#0x0004
;======================== Interrupts enabled! =================================;


; Handle the Nested VIC (IRQ) on the ADUC7060 
                AREA    |.text|, CODE, READONLY
                EXPORT  ADI_FIQ_Nested_Interrupt_Setup
ADI_FIQ_Nested_Interrupt_Setup
;======================== Interrupts disabled!=================================;
;========================     FIQ Mode!       =================================;
        ; This routine consumes 28 bytes of irq stack per nested interrupt
        ; => 224 (8 x 28) due to the 8 maximum interrupt nesting.

        ; This routine consumes 8 bytes of SYS stack per nested interrupt
        ; => 64 (8 x 8) due to the 8 maximum interrupt nesting.

        ; R12 is banked in FIQ mode is handling of this reqister is different
        ; between the IRQ and FIQ handlers

        ; Stack modified registers (potentially callee modified too)
        STMDB   SP!    ,{R0-R3,LR}

        ; Stack the SPSR
        MRS     R0      ,SPSR
        STMDB   SP!    ,{R0}

        ; Retrieve handler for this interrupt
        LDR     R0      ,=FIQVEC
        LDR     R0      ,[R0]
        LDR     R2      ,[R0]

        ; Allow new higher priority FIQs in system mode
        MRS     R0      ,CPSR
        BIC     R0      ,R0     ,#F_Bit
        ORR     R1      ,R0     ,#Mode_SYS
        MSR     CPSR_c  ,R1 


;======================== Interrupts enabled! =================================;
;========================     SYS Mode!       =================================;
        ; We need to store the R12,LR of system mode also
        STMDB   SP!    ,{R12,LR}

        ; Calling interrupt handler in system mode with interrupts enabled
        MOV     LR,PC
        BX      R2

        ; We need to restore the R12,LR of system ,mode also
        LDMIA   SP!    ,{R12,LR}

        ; Change back to FIQ mode, disabling F interrupts 
        MRS     R0      ,CPSR
        BIC     R0      ,R0     ,#Mode_SYS
        ORR     R0      ,R0     ,#(Mode_FIQ :OR: F_Bit)
        MSR     CPSR_c  ,R0 
;======================== Interrupts disabled!=================================;
;========================     FIQ Mode!       =================================;

        ; Clear the highest priority interrupt by writing 0xFF to FIQSTAN
        MOV      R0     ,#0xFF     
        LDR      R1     ,=FIQSTAN
        STR      R0     ,[R1]


        ; Restore the SPSR
        LDMIA   SP!    ,{R0}
        MSR     SPSR_cxsf    ,R0

        ; Restore modified registers 
        LDMIA   SP!    ,{R0-R3,LR}

        ; Return from interrupt
        SUBS    PC     ,LR,#0x0004
;======================== Interrupts enabled! =================================;

                END
