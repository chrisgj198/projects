#include <stdint.h>

uint8_t hex2uchar(uint8_t v);
void serial_puts(const uint8_t *s);
void serial_printx(uint8_t a);
void serial_printxx(uint8_t a);
void serial_printxxxx(uint16_t a);
void serial_printxxxxxxxx(uint32_t a);
void serial_print_bin_uint32(uint32_t a);
void serial_print_bin_uint8(uint8_t a);
void serial_print_bin_uint16(uint16_t a);
void serial_print_dec_uint8(uint8_t n);
void serial_print_dec_uint32(uint32_t n);
void serial_print_dec_uint16(uint16_t n);
void serial_printcrlf(void);
uint8_t serial_inpx(void);
uint8_t serial_inpxx(void);
uint16_t serial_inpxxxx(void);
uint32_t serial_inpxxxxxxxx(void);

