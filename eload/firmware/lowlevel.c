#include <stdint.h>
#include "aduc7060_cj.h"
#include "lowlevel.h"
#include "lowlevel_private.h"
#include "aduc706x_regbits.h"
#include "timers.h"

volatile uint32_t uptime_lsbyte;


uint32_t get_uptime_lsbyte(void){
	return uptime_lsbyte;
}

void background_tasks(void){
	update_uptime();
}



#define SERIAL_LOG2_TX_BUF_SIZE 5
#define SERIAL_TX_BUF_SIZE (1 << SERIAL_LOG2_TX_BUF_SIZE)
#define SERIAL_TX_BUF_INDEX_MASK (SERIAL_TX_BUF_SIZE - 1)
#define SERIAL_LOG2_RX_BUF_SIZE 5
#define SERIAL_RX_BUF_SIZE (1 << SERIAL_LOG2_RX_BUF_SIZE)
#define SERIAL_RX_BUF_INDEX_MASK (SERIAL_RX_BUF_SIZE - 1)

volatile uint8_t serial_rx_buffer[SERIAL_RX_BUF_SIZE];
volatile uint32_t serial_rx_buffer_in_index = 0; //points to last data put into buffer
volatile uint32_t serial_rx_buffer_out_index = 0; //points to last data taken out of buffer
volatile uint8_t serial_tx_buffer[SERIAL_TX_BUF_SIZE];
volatile uint32_t serial_tx_buffer_in_index = 0; //points to last data put into buffer
volatile uint32_t serial_tx_buffer_out_index = 0; //points to last data taken out of buffer
volatile uint32_t serial_rx_error = 0; // set to nonzero if there is known to have been an error (overrun etc.)
volatile uint32_t serial_tx_state = 0;

// serial_rx_buffer_in_index points to the character that has been most recently put into the buffer.
// serial_rx_buffer_out_index points to the character that has been most recently been read out of the buffer.
//
// If serial_rx_buffer_in_index == serial_rx_buffer_out_index, that means the buffer is empty
// If we are trying to insert a character and incrementing serial_rx_buffer_in_index would make it equal to
// serial_rx_buffer_out_index, then the buffer is too full. This case is an error.
// For the corresponding situation on the Tx, we should wait for the ISR to empty the buffer to make room.

void serial_putch(uint8_t ch){ // will block if buffer gets full
	uint8_t tmp_in_index;

	tmp_in_index = (serial_tx_buffer_in_index + 1) & SERIAL_TX_BUF_INDEX_MASK; // This is where we will put the new character, when we can

	while(serial_tx_buffer_out_index == tmp_in_index){ // while there is no room for more characters in the buffer...
		//Wait for tx buffer to be not full
		background_tasks();
	}
	serial_tx_buffer[tmp_in_index] = ch; // Do this first, and ...
	serial_tx_buffer_in_index = tmp_in_index; // do this AFTER putting the char in the buffer, since ISR may run in between

	serial_start_transmitter();
}

uint32_t serial_is_char_for_uart_tx(void){
	return (serial_tx_buffer_out_index != serial_tx_buffer_in_index)?1:0;
}

uint32_t serial_get_char_for_uart_tx(void){
	uint32_t retval;
	
	if(serial_tx_buffer_out_index != serial_tx_buffer_in_index){ // If the circular buffer isn't empty...
			serial_tx_buffer_out_index = (serial_tx_buffer_out_index + 1) & SERIAL_TX_BUF_INDEX_MASK; // Compute the index of next char to send
			retval = serial_tx_buffer[serial_tx_buffer_out_index]; // send the character
		}else{		// There is nothing to transmit. If the TxEn output is enabled, wait for the Tx to empty and then disable it.
			retval = 0;
		}
	return retval;
}

// returns 1 if there is a rs232 character ready for getch()
uint8_t serial_kbhit(void){
	return (serial_rx_buffer_in_index != serial_rx_buffer_out_index) ? 1 : 0;
}

uint8_t serial_getch(void){
	uint8_t tmp_out_index, ch;

	while (!serial_kbhit()){ // wait for a character to be present
		background_tasks();
	}

	tmp_out_index = (serial_rx_buffer_out_index + 1) & SERIAL_RX_BUF_INDEX_MASK; // This is where we will get the character from

	ch = serial_rx_buffer[tmp_out_index]; // Do this first, and ...
	serial_rx_buffer_out_index = tmp_out_index; // do this AFTER reading the char from the buffer, since ISR may run in between

	return ch;
}

// returns non-zero on an error (e.g. circular buffer full)
uint32_t serial_put_char_into_rx_buf(uint32_t tmpchar){
	uint32_t tmp_in_index;
	tmp_in_index = (serial_rx_buffer_in_index + 1) & SERIAL_RX_BUF_INDEX_MASK; // This is where we will put the new character, if we can
	if(serial_rx_buffer_out_index == tmp_in_index){ // if there is no room for more characters in the buffer...
		return 1;
	}else{ // there is room in the buffer
		serial_rx_buffer[tmp_in_index] = tmpchar; // Put the character into the buffer
		serial_rx_buffer_in_index = tmp_in_index; // and update the index
		return 0;
	}
}














#if 0
void task0(void);
void task1(void);
void (*isrtask)(void) = task0;		// isrtask is a pointer to a function, initially task0.
void task0(void){
	isrtask = &task1;
}
void task1(void){
	isrtask = &task0;
}

//	(*isrtask)();

#endif







void setup_dac(void){

	DAC0CON = 	DAC0CON_DACPD_DACON |
							DAC0CON_DACBUFLP_BUFON |
							DAC0CON_OPAMP_BUFFER |
							DAC0CON_DACBUFBYPASS_BUFFERED | 
							DAC0CON_DACCLK_HCLK |
							DAC0CON_DACCLR_NORMAL |
							DAC0CON_DACMODE_12BIT |
							DAC0CON_INTERPRATE_UCLK32 |
							DAC0CON_RANGE_INT_VREF;
}


void enable_xtalclk(void){
// To switch to an external crystal, users must follow this procedure:
	// 1. Enable the Timer1 interrupt and configure it for a timeout period of >120 �s.
	// 2. Follow the write sequence to the PLLCON register, setting the OSEL bits to [10] and clearing the EXTCLK bit.
	// 3. Force the part into nap mode by following the correct write sequence to the POWCON register.
	// 4. When the part is interrupted from nap mode by the Timer1 interrupt source,
	//    the clock source has switched to the external crystal.

	// T1LD is a 32-bit register that holds the 32-bit value that is loaded into the counter.
	T1LD = 0x80; // 32,768 clock ticks
	
	T1CON = T1CON_T1CLKSEL_32768HZ_OSC | T1CON_T1DIR_DOWN | T1CON_T1EN_ENABLED | T1CON_T1MOD_PERIODIC | T1CON_T1FORMAT_BINARY | T1CON_T1SCALE_CLKDIV1;
	
	IRQEN |= IRQ_TMR1; // Enable Timer1 interrupt source

	// Switch to external crystal
	PLLKEY1 = 0xAA;
	PLLCON = PLLCON_OSEL_EXTERNAL;
	PLLKEY2 = 0x55;

	// Enter nap??? mode
	POWKEY1 = 0x1;
	POWCON0 = POWCON0_MODE_PAUSE | POWCON0_CLK_1280KHZ; //  0x73 (from examples - NOT nap mode!)
	POWKEY2 = 0xF4;

	// Now, how do we deal with the interrupt?
															
}



void set_dac_v_range(uint32_t v_range){
	switch(v_range) {
		case DAC_V_RANGE_LOW:
			VRANGE_SELECT_LOW;
			break;
		default:
		case DAC_V_RANGE_HIGH:
			VRANGE_SELECT_HIGH;
	}
}

volatile uint32_t selected_rsense = SENSE_RESISTANCE_UNDEFINED;
volatile uint32_t adc0_invalid_samples_remaining = ADC_LATENCY;
volatile uint32_t adc1_invalid_samples_remaining = ADC_LATENCY;
volatile int32_t adc0_result_buffer;
volatile int32_t adc1_result_buffer;
volatile uint32_t adc0_result_ready = 0;
volatile uint32_t adc1_result_ready = 0;

void write_dac_value(uint32_t val){
	adc0_invalid_samples_remaining += DAC_AMP_SETTLING + ADC_LATENCY;
	adc1_invalid_samples_remaining += DAC_AMP_SETTLING + ADC_LATENCY;
	DAC0DAT = DAC0DAT_DATA_BIT_PATTERN(val);
}



uint32_t get_adc_results(int32_t *adc0_result, int32_t *adc1_result){
	uint32_t retval;
	
	if(adc0_result_ready && adc1_result_ready){
		*adc0_result = adc0_result_buffer;
		*adc1_result = adc1_result_buffer;
		adc0_result_ready = 0;
		adc1_result_ready = 0;
		retval = 1;
	} else {
		retval = 0;
	}
	return retval;
}

void set_sense_resistor(uint32_t new_rsense){
	if(selected_rsense != new_rsense){
		// the sense resistor has changed, so mark the ADC data as invalid
		adc0_invalid_samples_remaining += ADC_LATENCY;
	}
	selected_rsense = new_rsense;

	switch (selected_rsense) {
		case SENSE_RESISTANCE_1_OHM:
			RSENSE_CONNECT_1_OHM;
			RSENSE_DISCONNECT_10_MILLIOHMS;
			RSENSE_DISCONNECT_100_MILLIOHMS;
			break;
		case SENSE_RESISTANCE_100_MILLIOHMS:
			RSENSE_CONNECT_100_MILLIOHMS;
			RSENSE_CONNECT_1_OHM;
			RSENSE_DISCONNECT_10_MILLIOHMS;
			break;
		case SENSE_RESISTANCE_10_MILLIOHMS:
		default:
			RSENSE_CONNECT_10_MILLIOHMS;
			RSENSE_CONNECT_100_MILLIOHMS;
			RSENSE_CONNECT_1_OHM;
	}
}

// checks the overcurrent / overtemperature latch
uint32_t check_mosfet_enabled(void){
	uint32_t retval;
	
	retval = IS_MOSFET_ENABLED;
	
	
	return retval;
}


// An overcurrent or gross overtemperature will cause hardware comparators to make the MOSFET stop drawing current
// This function can allow the MOSFET to draw current again if the fault has gone away
void write_mosfet_enabled(uint32_t want_mosfet_enabled){

	// The latch is written on a rising edge of oc_c so set it low first
	OC_C_LOW;
	
	// Set up the data input of the flipflop
	if(want_mosfet_enabled){
		OC_D_HIGH; // enable the power MOSFET to sink current 
	} else {
		OC_D_LOW; // make the power MOSFET high impedance
	}
	
	// cause the rising edge that writes the new state into the flipflop
	OC_C_HIGH;
	// Note that if there is an overcurrent or overtemperature, then
	//  the hardware overcurrent and overtemperature comparators will override anything we did here.
	
	adc0_invalid_samples_remaining += DAC_AMP_SETTLING + ADC_LATENCY;
	adc1_invalid_samples_remaining += DAC_AMP_SETTLING + ADC_LATENCY;

}


void discard_samples(uint32_t nsamples){
	adc0_invalid_samples_remaining += nsamples;
	adc1_invalid_samples_remaining += nsamples;
}

void setup_adcs(void){
	
	//	ADCFLT = ADCFILT_CHOPEN_ON | ADCFILT_RAVG2_OFF | ADCFILT_AF_BIT_PATTERN(0x00) | ADCFILT_SF_BIT_PATTERN(0x7F);
	//	ADCFLT = ADCFILT_CHOPEN_ON | ADCFILT_RAVG2_OFF | ADCFILT_AF_BIT_PATTERN(63) | ADCFILT_SF_BIT_PATTERN(31);
//	ADCFLT = ADCFILT_CHOPEN_ON | ADCFILT_RAVG2_OFF | ADCFILT_AF_BIT_PATTERN(0) | ADCFILT_SF_BIT_PATTERN(2); // ~884 Sa/s
	ADCFLT = ADCFILT_CHOPEN_ON | ADCFILT_RAVG2_OFF | ADCFILT_AF_BIT_PATTERN(0) | ADCFILT_SF_BIT_PATTERN(19); // ~133.229 Sa/s
	//	ADCFLT = ADCFILT_CHOPEN_OFF | ADCFILT_RAVG2_OFF | ADCFILT_AF_BIT_PATTERN(0) | ADCFILT_SF_BIT_PATTERN(19); // ???100 Sa/s
	//	ADCFLT = ADCFILT_CHOPEN_ON | ADCFILT_RAVG2_OFF | ADCFILT_AF_BIT_PATTERN(0) | ADCFILT_SF_BIT_PATTERN(19); // ???~33 Sa/s

 	ADCMDE = 		ADCMDE_ADCCLKSEL_512KHZ |
							ADCMDE_ADCLPMEN_NORMAL |
							ADCMDE_ADCLPMCFG_NORMAL |
							ADCMDE_ADCMD_CONT;

  ADC0CON = 	ADC0CON_ADC0EN_ON |
							ADC0CON_ADC0DIAG_OFF |
							ADC0CON_HIGHEXTREF0_DISABLED |
							ADC0CON_AMP_CM_MIDRAIL |
							ADC0CON_ADC0CODE_TWOSCOMP |
							ADC0CON_ADC0CH_ADC0_ADC1_DIFF |
							ADC0CON_ADC0REF_INTERNAL |
							ADC0CON_ADC0PGA_GAIN_16;

	ADCCFG = ADCCFG_GNDSW_EN_OFF | ADCCFG_ADC0ACCEN_DISABLED | ADCCFG_ADC0CMPEN_DISABLED | ADCCFG_ADC0OREN_DISABLED | ADCCFG_GNDSW_RES_EN_SHORT | ADCCFG_ADCRCEN_DISABLED;

	ADC1CON = 	ADC1CON_ADC1EN_ON |
							ADC1CON_ADC1DIAG_OFF |
							ADC1CON_HIGHEXTREF1_DISABLED |
							ADC1CON_ADC1CODE_TWOSCOMP |
							ADC1CON_ADC1CH_ADC6_ADC7_DIFF |
							ADC1CON_ADC1REF_INTERNAL |
							ADC1CON_BUF_BYPASS_NEGBUF_ACTIVE |
							ADC1CON_BUF_BYPASS_POSBUF_ACTIVE |
							ADC1CON_DIGGAIN_1;
}


volatile uint32_t shadow_etbei_enabled = 0;

#if (BAUDRATE == 115200)
	#define TXEN_10240KHZ_CYCLES (1+10UL*10240000UL/115200UL)
#endif
#if (BAUDRATE == 9600)
	#define TXEN_10240KHZ_CYCLES (1+9UL*10240000UL/9600UL)
#endif

#define IS_UART_TX_EMPTY_INTERRUPT_ENABLED (shadow_etbei_enabled)
#define ENABLE_UART_TX_EMPTY_INTERRUPT do{COMIEN0 |= COMIEN0_ETBEI_ENABLED; shadow_etbei_enabled=1;}while(0)
#define DISABLE_UART_TX_EMPTY_INTERRUPT do{COMIEN0 &= (~COMIEN0_ETBEI_ENABLED); shadow_etbei_enabled=0;}while(0)
#define ENABLE_UART_RX_NOT_EMPTY_INTERRUPT do{COMIEN0 |= COMIEN0_ERBFI_ENABLED; shadow_etbei_enabled=1;}while(0)
#define DISABLE_UART_RX_NOT_EMPTY_INTERRUPT do{COMIEN0 &= (~COMIEN0_ERBFI_ENABLED); shadow_etbei_enabled=0;}while(0)
#define T3CON_VAL_SET (T3CON_T3CAPEN_DISABLE | T3CON_T3CAPSEL_BIT_PATTERN(0) | T3CON_T3CLKSEL_10240KHZ | T3CON_T3DIR_DOWN | T3CON_T3EN_ENABLE | T3CON_T3MOD_FREE_RUN | T3CON_T3SCALE_DIV_1)
#define T3CON_VAL_OFF (T3CON_T3CAPEN_DISABLE | T3CON_T3CAPSEL_BIT_PATTERN(0) | T3CON_T3CLKSEL_10240KHZ | T3CON_T3DIR_DOWN | T3CON_T3EN_DISABLE | T3CON_T3MOD_FREE_RUN | T3CON_T3SCALE_DIV_1)
#define IRQ_TXEN_TIMER IRQ_TMR3
#define DISABLE_TXEN_TIMER do{T3CON = T3CON_VAL_OFF;}while(0)
#define DISABLE_TXEN_TIMER_INTERRUPT do{IRQEN &= (~IRQ_TXEN_TIMER);}while(0)
#define ENABLE_TXEN_TIMER_INTERRUPT do{IRQEN |= (IRQ_TXEN_TIMER);}while(0)
#define SET_TXEN_TIMER do{T3LD = TXEN_10240KHZ_CYCLES; T3CON = T3CON_VAL_SET;}while(0)
#define IS_UART_TX_FULL ((COMSTA0 & COMSTA0_THRE)?0:1)
#define PUT_DATA_TO_UART_TX(x) do{COMTX = x;}while(0)
#define TX_SHIFT_REGISTER_IS_EMPTY ((COMSTA0 & COMSTA0_TEMT)?1:0)

void check_uart_errors(void){
	if(COMSTA0 & (COMSTA0_FE | COMSTA0_OE)){ // Framing or overrun error detected
		serial_rx_error = 1;
	}
}

// gets called by serial_putch(), in case transmitter is idle and will not generate interrupts.
void serial_start_transmitter(void){
	uint32_t ch;
	
	if(IS_UART_TX_EMPTY_INTERRUPT_ENABLED){
		// Don't do anything - the character will get put out on the next Tx Empty interrupt.
	} else {
		DISABLE_TXEN_TIMER_INTERRUPT;
		DISABLE_TXEN_TIMER;
		SERIAL_DRIVER_ON;
		if(IS_UART_TX_FULL){
			// Error - this shouldn't happen...
		} else {
			ch=serial_get_char_for_uart_tx();
			PUT_DATA_TO_UART_TX(ch);
		}
		ENABLE_UART_TX_EMPTY_INTERRUPT;
	}
}

// Gets called on every uart interrupt caused because the Tx is not full.
void service_uart_tx(void){
	if(IS_UART_TX_FULL){
		//shouldn't happen because we should only get interrupts when the buffer becomes empty
		// log error here
	} else {
		if(serial_is_char_for_uart_tx()){ // If the software circular buffer contains a character to transmit
			PUT_DATA_TO_UART_TX(serial_get_char_for_uart_tx());
		}else{ // There is nothing to transmit.
			// set the timer so that we get an interrupt around the time when the character has finished being
			//			transmitted onto the wires
			SET_TXEN_TIMER;
			DISABLE_UART_TX_EMPTY_INTERRUPT;
			ENABLE_TXEN_TIMER_INTERRUPT;
		}
	}
}


//	DBG_P_0_5_ON;
//	DBG_P_0_5_OFF;
//	LED_1_4_ON;
//	DBG_P_1_3_ON;
//	DBG_P_1_3_OFF;
//	LED_1_4_OFF;
//	DBG_P_0_5_OFF;
//	LED_1_5_ON;
//	DBG_P_0_0_ON;
//	DBG_P_0_0_OFF;
//	DBG_P_1_3_OFF;
//	LED_1_5_OFF;
//	LED_1_6_OFF;
//	DBG_P_1_3_ON;
//	LED_1_6_ON;
//	LED_2_1_ON;
//	DBG_P_1_2_ON;
//	DBG_P_1_2_OFF;
//	LED_2_1_OFF;


void service_txen_timer_int(void){
	DISABLE_TXEN_TIMER_INTERRUPT;
	DISABLE_TXEN_TIMER;
	while(!TX_SHIFT_REGISTER_IS_EMPTY){
		// Do nothing whilst waiting for the character to finish transmitting.
		// This shouldn't take long (or any time at all) if the timer was set for long enough.
	}
	SERIAL_DRIVER_OFF;
}

void service_uart_rx(void){
	uint32_t tmpchar;

	if(COMSTA0 & COMSTA0_DR){ // if a character is ready...
		tmpchar = COMRX; // Get the character out of the Rx FIFO, even if we have nowhere to put it. At least unclog the Rx.
		if(serial_put_char_into_rx_buf(tmpchar)){
			serial_rx_error = 1;
		}
	}
}

void	IRQ_Handler(void) __irq {
}

void	FIQ_Handler(void) __irq {
}

void	SWI_Handler(void) __irq {
}

void	Undef_Handler(void)__irq {
}

void	PAbt_Handler(void) __irq {
}

void	DAbt_Handler(void) __irq {
}

void IRQ0_FIQ_SOURCE (void) {
   while(1); // Infinite loop until we provide an actual handler
}

void IRQ1_SW_INT     (void) {
   while(1); // Infinite loop until we provide an actual handler
}

void IRQ3_T0_INT     (void) {
   T0CLRI = 0xFF;
}

void IRQ4_T1_INT     (void) {
   // Clear the interrupt
   T1CLRI = 0xFF;
}

void IRQ5_T2_INT     (void) {
   // Clear the interrupt
   T2CLRI = 0xFF;
}

void IRQ6_T3_INT     (void) {
  // Clear the interrupt
  T3CLRI = 0xFF;
	service_txen_timer_int();
}

void IRQ10_ADC_INT   (void) {
	uint32_t tmp_irqsta;
	int32_t adc0_dat_tmp;
	int32_t adc1_dat_tmp;

	tmp_irqsta = IRQSTA;
	if (tmp_irqsta & IRQ_ADC) {
	 	adc1_dat_tmp = ADC1DAT;
	 	adc0_dat_tmp = ADC0DAT;

		if(adc0_invalid_samples_remaining){
			adc0_invalid_samples_remaining--;
		}else{
			adc0_result_buffer = adc0_dat_tmp;
			adc0_result_ready = 1;
		}
		if(adc1_invalid_samples_remaining){
			adc1_invalid_samples_remaining--;
		}else{
			adc1_result_buffer = adc1_dat_tmp;
			adc1_result_ready = 1;
		}
		
	} else {

	}
}

void IRQ11_UART_INT  (void) {
	uint32_t tmp_irqsta;
	uint8_t tmp_comiid0;
	volatile uint32_t dummy_u32;

	tmp_irqsta = IRQSTA;
	if (tmp_irqsta & IRQ_UART){
		tmp_comiid0 = COMIID0;
		switch(tmp_comiid0){
			case COMIID0_LINE_STATUS:		// Line status interrupt; clear by: Read COMSTA0
				dummy_u32 = COMSTA0;
				break;
			case COMIID0_RX_BUF_FULL:		// Receive buffer full interrupt; Clear by: Read COMRX
				service_uart_rx();
				break;
			case COMIID0_TX_BUF_EMPTY:	// Transmit buffer empty interrupt; Clear by: Write data to COMTX or read COMIID0
				service_uart_tx();
				break;
			case COMIID0_MODEM_STATUS:	// Modem status interrupt; Clear by: Read COMSTA1 register
				dummy_u32 = COMSTA1;
				break;
			case COMIID0_NO_INTERRUPT:
			default:
				break;			
		} 	
	}
}

void IRQ12_SPIM_INT  (void) {
   while(1); // Infinite loop until we provide an actual handler
}

void IRQ13_XIRQ0_INT (void) {
   // Clear an edge level interrupt
   IRQCLRE = IRQCLRE_IRQ0CLRI;
}

void IRQ14_XIRQ1_INT (void) {
   // Clear an edge level interrupt
   IRQCLRE = IRQCLRE_IRQ1CLRI;
}

void IRQ15_I2CM_INT  (void) {
   while(1); // Infinite loop until we provide an actual handler
}

void IRQ16_I2CS_INT  (void) {
   while(1); // Infinite loop until we provide an actual handler
}

void IRQ17_SPIS_INT  (void) {
   while(1); // Infinite loop until we provide an actual handler
}

void IRQ18_XIRQ2_INT (void) {
   // Clear an edge level interrupt
   IRQCLRE = IRQCLRE_IRQ2CLRI;
}

void IRQ19_XIRQ3_INT (void) {
   // Clear an edge level interrupt
   IRQCLRE = IRQCLRE_IRQ3CLRI;
}

void IRQx_NULL_INT   (void) {
   // This should never be called
   while(1); // Infinite loop until we provide an actual handler
}

const tyIntHndlr IntHndlrTable[32] __attribute__ ((aligned (128)))  = {
   IRQ0_FIQ_SOURCE,             /* 0  - Any FIQ active         */
   IRQ1_SW_INT    ,             /* 1  - Software               */
   IRQx_NULL_INT  ,             /* 2  - Unused                 */
   IRQ3_T0_INT    ,             /* 3  - Timer 0                */
   IRQ4_T1_INT    ,             /* 4  - Timer 1                */
   IRQ5_T2_INT    ,             /* 5  - Timer 2                */
   IRQ6_T3_INT    ,             /* 6  - Timer 3                */
   IRQx_NULL_INT  ,             /* 7  - Unused                 */
   IRQx_NULL_INT  ,             /* 8  - Unused                 */
   IRQx_NULL_INT  ,             /* 9  - Unused                 */
   IRQ10_ADC_INT  ,             /* 10 - ADC Interrupt          */
   IRQ11_UART_INT ,             /* 11 - UART interrupt         */
   IRQ12_SPIM_INT  ,            /* 12 - SPI Master interrupt   */
   IRQ13_XIRQ0_INT,             /* 13 - External interrupt 0   */
   IRQ14_XIRQ1_INT,             /* 14 - External interrupt 1   */
   IRQ15_I2CM_INT ,             /* 15 - I2C Master interrupt   */
   IRQ16_I2CS_INT ,             /* 16 - I2C Slave interrupt    */
   IRQ17_SPIS_INT ,             /* 17 - SPI Slave interrupt    */
   IRQ18_XIRQ2_INT,             /* 18 - External interrupt 2   */
   IRQ19_XIRQ3_INT,             /* 19 - External interrupt 3   */
   IRQx_NULL_INT  ,             /* 20 - Unused                 */
   IRQx_NULL_INT  ,             /* 21 - Unused                 */
   IRQx_NULL_INT  ,             /* 22 - Unused                 */
   IRQx_NULL_INT  ,             /* 23 - Unused                 */
   IRQx_NULL_INT  ,             /* 24 - Unused                 */
   IRQx_NULL_INT  ,             /* 25 - Unused                 */
   IRQx_NULL_INT  ,             /* 26 - Unused                 */
   IRQx_NULL_INT  ,             /* 27 - Unused                 */
   IRQx_NULL_INT  ,             /* 28 - Unused                 */
   IRQx_NULL_INT  ,             /* 29 - Unused                 */
   IRQx_NULL_INT  ,             /* 30 - Unused                 */
   IRQx_NULL_INT  ,             /* 31 - Unused                 */
};




/*
https://ez.analog.com/message/155973#155973

For clarification,  the ADC0 and ADC1 on a ADuC706x have a Master (ADC0) - Slave(ADC1) relationship.

So after startup or any change on the control- or mode-MMRs, ADC1 is always automatically synchronized to the ADC0 cycle.

So normally in continuous mode you should always get both interrupts at the same time, if both ADCs are running.

So if ADC0 is running, you may not need to to enable the ADC1 interrupt, as this can be handled as well in the ADC0 interrupt.

There is anyway only one ADC interrupt signal.

https://ez.analog.com/message/156340#156340


Datasheet:
ADC0DAT holds the 24-bit conversion result from the primary ADC.
The ADC does not update ADC0DAT if the ADC0 conversion result ready bit (ADCSTA[0]) is set.
A read of ADC0DAT by the MCU clears all asserted ready flags (ADCSTA[1:0]).

ADC1DAT holds the 24-bit conversion result from the auxiliary ADC.
The ADC does not update ADC1DAT if the ADC0 conversion result ready bit (ADCSTA[1]) is set.

https://ez.analog.com/message/20194#20194
ADC0 has priority over ADC1
If ADC0CON is reconfigured then ADC1CON is reset and resynchronised.
If ADC1CON is reconfigured then ADC0CON is not reset.
If ADCFLT is written to then both ADCs are reset.




*/


void setup_chip(void){
	POWKEY1 = POWKEY1VAL;
	POWCON0 = POWCON0_MODE_ACTIVE | POWCON0_CLK_10240KHZ;   // Set core to max CPU speed of 10.24Mhz
	POWKEY2 = POWKEY2VAL;

	POWKEY3 = POWKEY3VAL;
	POWCON1 = POWCON1_PWM_DISABLED | POWCON1_UART_ENABLED | POWCON1_I2CSPI_DISABLED;
	POWKEY4 = POWKEY4VAL;

	GP0CON0 = 	GP0CON0_P0_6_GPIO |
							GP0CON0_P0_5_GPIO |
							GP0CON0_P0_4_GPIO |
							GP0CON0_P0_3_GPIO |
							GP0CON0_P0_2_GPIO |
							GP0CON0_P0_1_GPIO |
							GP0CON0_P0_0_GPIO;

	GP1CON = 		GP1CON_P1_6_GPIO |
							GP1CON_P1_5_GPIO |
							GP1CON_P1_4_GPIO |
							GP1CON_P1_3_GPIO |
							GP1CON_P1_2_GPIO |
							GP1CON_P1_1_SOUT |
							GP1CON_P1_0_SIN;  // Select UART functionality for P1.0/P1.1

	GP2CON = 	GP2CON_P2_1_GPIO |
						GP2CON_P2_0_GPIO;

	GP0DAT = GP0DAT_INITVAL;
	GP1DAT = GP1DAT_INITVAL;
	GP2DAT = GP2DAT_INITVAL;

	COMCON0 = 	COMCON0_DLAB_ENABLE_COMDIV |
							COMCON0_BRK_NORMAL |
							COMCON0_SP_NORMAL |
							COMCON0_EPS_ODD |
							COMCON0_PEN_NO_PARITY |
							COMCON0_STOP_1BIT |
							COMCON0_WLS_8BITS;			// Enable access to COMDIV registers

#if (BAUDRATE == 9600)
	COMDIV0 = COMDIV0VAL_9600;
	COMDIV1 = COMDIV1VAL_9600;
	COMDIV2 = COMDIV2VAL_9600;
#endif
#if (BAUDRATE == 115200)
	COMDIV0 = COMDIV0VAL_115200;
	COMDIV1 = COMDIV1VAL_115200;
	COMDIV2 = COMDIV2VAL_115200;
#endif

	COMCON0 = COMCON0_STOP_2BITS | COMCON0_WLS_8BITS;
	
	COMIEN0 = COMIEN0_ERBFI_ENABLED;	 // Enable UART interrupts when Rx not empty
	
	ADCMSKI = ADCMSKI_ADC0RDY_INTEN;	// Enable ADC0 result ready interrupt source


	setup_dac();
	setup_adcs();
	

   // Let the VIC know about our vector table
   IRQBASE           = (unsigned long)(((unsigned long)IntHndlrTable) >> 7);

   // Enable nesting and prioritisation of IRQ and FIQ interrupts
   IRQCONN           = IRQCONN_ENFIQN_ENABLE | IRQCONN_ENIRQN_ENABLE;

   // Set priorities
   IRQP0             = IRQP0_T3PI_BIT_PATTERN(3) |
                       IRQP0_T2PI_BIT_PATTERN(5) |
                       IRQP0_T1PI_BIT_PATTERN(4) |
                       IRQP0_T0PI_BIT_PATTERN(2) |
											 IRQP0_SWINTP_BIT_PATTERN(0);

   IRQP1             = IRQP1_I2CMPI_BIT_PATTERN(0) | 
	                     IRQP1_IRQ1PI_BIT_PATTERN(0) | 
	                     IRQP1_IRQ0PI_BIT_PATTERN(1) | 
	                     IRQP1_SPIMPI_BIT_PATTERN(0) | 
	                     IRQP1_UARTPI_BIT_PATTERN(0) |
                       IRQP1_ADCPI_BIT_PATTERN(0);

   IRQP2             = IRQP2_IRQ3PI_BIT_PATTERN(0) |
                       IRQP2_IRQ2PI_BIT_PATTERN(0) |
											 IRQP2_SPISPI_BIT_PATTERN(0) |
											 IRQP2_I2CSPI_BIT_PATTERN(0);

//  T0LD            = 0x300;       // Enable Timer 1
//  T0CON           = 0x4C0;       // Periodic, HCLK, div/1
 
//FIQEN = IRQ_TMR0 | IRQ_TMR3; 
	IRQEN = IRQ_ADC | IRQ_UART;

	set_sense_resistor(SENSE_RESISTANCE_10_MILLIOHMS);
	set_dac_v_range(DAC_V_RANGE_HIGH);
	write_mosfet_enabled(0);
}



// FIXME:
// Bug noticed: Timer 3 seems to continue to generate interrupts when its interrupt is disabled, unless the timer is also disabled
// Bug noticed: Timer 3 interrupt should have higher priority than UART interrupt but it seems like UART interrupt can interrupt timer ISR.


