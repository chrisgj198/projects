#ifndef ADUC706X_REGBITS_H
#define ADUC706X_REGBITS_H



// DAC0CON
//	<15:10>	Reserved
//	<9> 	DACPD
#define DAC0CON_DACPD_DACOFF	0x0200	//	Set to 1 to power down DAC output (DAC output is tristated).
#define DAC0CON_DACPD_DACON 	0x0000	//	Clear this bit to enable the DAC.
//
//	<8> 	DACBUFLP
#define DAC0CON_DACBUFLP_BUFOFF	0x0100	//	Set to 1 to place the DAC output buffer in low power mode.
#define DAC0CON_DACBUFLP_BUFON	0x0000	//	Clear this bit to enable the DAC buffer.
//
//	<7> OPAMP
#define DAC0CON_OPAMP_OPAMP 	0x0080	//	Set to 1 to place the DAC output buffer in op amp mode.
#define DAC0CON_OPAMP_BUFFER	0x0000	//	Clear this bit to enable the DAC output buffer for normal DAC operation.
//
//	<6> DACBUFBYPASS
#define DAC0CON_DACBUFBYPASS_BYPASS 	0x0040	//	Set to 1 to bypass the output buffer and send the DAC output directly to the output pin.
#define DAC0CON_DACBUFBYPASS_BUFFERED	0x0000	//	Clear this bit to buffer the DAC output.
//
//	<5> DACCLK
#define DAC0CON_DACCLK_HCLK		0x0000	//	Cleared to 0 to update the DAC on the negative edge of HCLK.
#define DAC0CON_DACCLK_TIMER0	0x0020	//	Set to 1 to update the DAC on the negative edge of Timer0.
//
//	<4> DACCLR
#define DAC0CON_DACCLR_NORMAL	0x0010	//	Set to 1 for normal DAC operation.
#define DAC0CON_DACCLR_RESET	0x0000	//	Set to 0 to clear the DAC output and to set DAC0DAT to 0.
//				Writing to this bit has an immediate effect on the DAC output.
//
//	<3> DACMODE
#define DAC0CON_DACMODE_16BIT	0x0008	//	Set to 1 to enable the DAC in 16-bit interpolation mode.
#define DAC0CON_DACMODE_12BIT	0x0000	//	Set to 0 to enable the DAC in normal 12-bit mode.
//
//	<2> Rate
//				Used with interpolation mode.
#define DAC0CON_INTERPRATE_UCLK16	0x0008	//	Set to 1 to configure the interpolation clock as UCLK/16.
#define DAC0CON_INTERPRATE_UCLK32	0x0000	//	Set to 0 to configure the interpolation clock as UCLK/32.
//
//	<1:0> DAC range bits
#define DAC0CON_RANGE_VSS_VDD	0x0003	//	[11] = 0 V to AVDD range.
#define DAC0CON_RANGE_EXT_REF2IN	0x0002	//	[10] = ADC5/EXT_REF2IN- to ADC4/EXT_REF2IN+.
#define DAC0CON_RANGE_VREFM_VREFP	0x0001	//	[01] = VREF- to VREF+.
#define DAC0CON_RANGE_INT_VREF	0x0000	//	[00] = 0 V to VREF (1.2 V) range. Internal reference source.
//

//	DAC0DAT
// <31:28> Reserved
// <27:16> 12-bit data for DAC0.
// <15:12> Extra four bits used in interpolation mode.
	#define DAC0DAT_DATA_MASK 0x0000FFFF
	#define DAC0DAT_DATA_LSBPOS 12
	#define DAC0DAT_DATA_BIT_PATTERN(x) ((((uint32_t)(x)) & DAC0DAT_DATA_MASK) << DAC0DAT_DATA_LSBPOS)
	#define DAC0DAT_DATA_MAXVAL (DAC0DAT_DATA_MASK)
// <11:0> Reserved



	//T1CON
	//	<15:11>	Reserved.
	//	<10: 9>	T1CLKSEL
	//						Clock source select.
	#define T1CON_T1CLKSEL_32768HZ_OSC 0x0000 //	[00] = 32.768 kHz oscillator.
	#define T1CON_T1CLKSEL_10240KHZ_DIVCD 0x0200 //	[01] = 10.24 MHz/CD.
	#define T1CON_T1CLKSEL_XTALI_OSC 0x0400 //	[10] = XTALI.
	#define T1CON_T1CLKSEL_10240KHZ 0x0600 //	[11] = 10.24 MHz.
	//
	//	<8>   	T1DIR
	//						Count up.
	//						Set by user for Timer1 to count up.
	//						Cleared by user for Timer1 to count down (default).
	#define T1CON_T1DIR_UP 0x0100
	#define T1CON_T1DIR_DOWN 0x0000
	//
	//	<7>   	T1EN
	//						Timer1 enable bit.
	//						Set by user to enable Timer1.
	//						Cleared by user to disable Timer1 (default).
	#define T1CON_T1EN_ENABLED 0x0080
	#define T1CON_T1EN_DISABLED 0x0000
	//
	//	<6>			T1MOD
	//						Timer1 mode.
	//						Set by user to operate in periodic mode.
	//						Cleared by user to operate in free running mode (default).
	#define T1CON_T1MOD_PERIODIC 0x0040
	#define T1CON_T1MOD_FREERUN 0x0000
	//
	//	<5:4>  	T1FORMAT
	//						Format.
	#define T1CON_T1FORMAT_BINARY 0x0000	//	[00] = binary (default).
	#define T1CON_T1FORMAT_RESERVED 0x0010	//	[01] = reserved.
	#define T1CON_T1FORMAT_23HMSH 0x0020	//	[10] = hours:minutes:seconds:hundredths (23 hours to 0 hours). This is only valid with a 32 kHz clock.
	#define T1CON_T1FORMAT_255HMSH 0x0030	//	[11] = hours:minutes:seconds:hundredths (255 hours to 0 hours). This is only valid with a 32 kHz clock.
	//
	//	<3:0>		T1SCALE
	//						Prescaler.
	#define T1CON_T1SCALE_CLKDIV1     0x0000	//	[0000] = source clock/1 (default).
	#define T1CON_T1SCALE_CLKDIV16    0x0004	//	[0100] = source clock/16.
	#define T1CON_T1SCALE_CLKDIV256   0x0008	//	[1000] = source clock/256. This setting should be used in conjunction with Timer1 in the format hours:minutes:seconds:hundredths. See Format 10 and Format 11 listed with Bits[5:4] in this table (Table 81).
	#define T1CON_T1SCALE_CLKDIV32768 0x000F	//	[1111] = source clock/32,768.
	//
	//T1CON = 0xC0; // Periodic mode, enable timer, 32,768 Hz clock/1




	//IRQEN
#define IRQ_ANY    	0x00000001	//	<0> 	All interrupts OR�ed (FIQ only), This bit is set if any FIQ is active
#define IRQ_SW     	0x00000002	//	<1> 	Software interrupt, User programmable interrupt source
	//	<2> 	Undefined, This bit is not used
#define IRQ_TMR0   	0x00000008	//	<3> 	Timer0, General-Purpose Timer0
#define IRQ_TMR1   	0x00000010	//	<4> 	Timer1 or wake-up timer, General-Purpose Timer1 or wake-up timer
#define IRQ_TMR2   	0x00000020	//	<5> 	Timer2 or watchdog timer, General-Purpose Timer2 or watchdog timer
#define IRQ_TMR3   	0x00000040	//	<6> 	Timer3 or STI timer, General-Purpose Timer3
	//	<7> 	Undefined, This bit is not used
	//	<8> 	Undefined, This bit is not used
	//	<9> 	Undefined, This bit is not used
#define IRQ_ADC   	0x00000400	//	<10> 	ADC, ADC interrupt source bit
#define IRQ_UART  	0x00000800	//	<11>	UART, UART interrupt source bit
#define IRQ_SPI   	0x00001000	//	<12>	SPI, SPI interrupt source bit
#define IRQ_XIRQ0 	0x00002000	//	<13>	XIRQ0 (GPIO IRQ0), External Interrupt 0
#define IRQ_XIRQ1 	0x00004000	//	<14>	XIRQ1 (GPIO IRQ1), External Interrupt 1
#define IRQ_I2CM  	0x00008000	//	<15>	I2C master IRQ, I2C master interrupt source bit
#define IRQ_I2CS  	0x00010000	//	<16>	I2C slave IRQ, I2C slave interrupt source bit
#define IRQ_PWM   	0x00020000	//	<17>	PWM, PWM trip interrupt source bit
#define IRQ_XIRQ2  	0x00040000	//	<18>	XIRQ2 (GPIO IRQ2), External Interrupt 2
#define IRQ_XIRQ3 	0x00080000	//	<19>	XIRQ3 (GPIO IRQ3), External Interrupt 3

	//IRQCONN
	// <31:2> Reserved
//<1> ENFIQN
// Setting this bit to 1 enables nesting of FIQ interrupts. Clearing this bit means no nesting or prioritization of FIQs is allowed.
#define IRQCONN_ENFIQN_ENABLE  0x00000002
#define IRQCONN_ENFIQN_DISABLE 0x00000000
//<0> ENIRQN
// Setting this bit to 1 enables nesting of IRQ interrupts. Clearing this bit means no nesting or prioritization of IRQs is allowed.
#define IRQCONN_ENIRQN_ENABLE  0x00000001
#define IRQCONN_ENIRQN_DISABLE 0x00000000

// IRQP0
// <31:27> Reserved
// <26:24> T3PI : A priority level of 0 to 7 can be set for Timer3.
#define IRQP0_T3PI_MASK 0x07
#define IRQP0_T3PI_LSBPOS 24
#define IRQP0_T3PI_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP0_T3PI_MASK) << IRQP0_T3PI_LSBPOS)
// <23> Reserved
// <22:20> T2PI : A priority level of 0 to 7 can be set for Timer2.
#define IRQP0_T2PI_MASK 0x07
#define IRQP0_T2PI_LSBPOS 20
#define IRQP0_T2PI_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP0_T2PI_MASK) << IRQP0_T2PI_LSBPOS)
// <19> Reserved
// <18:16> T1PI : A priority level of 0 to 7 can be set for Timer1.
#define IRQP0_T1PI_MASK 0x07
#define IRQP0_T1PI_LSBPOS 16
#define IRQP0_T1PI_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP0_T1PI_MASK) << IRQP0_T1PI_LSBPOS)
// <15> Reserved
// <14:12> T0PI : A priority level of 0 to 7 can be set for Timer0.
#define IRQP0_T0PI_MASK 0x07
#define IRQP0_T0PI_LSBPOS 12
#define IRQP0_T0PI_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP0_T0PI_MASK) << IRQP0_T0PI_LSBPOS)
// <11:7> Reserved
// <6:4> SWINTP : A priority level of 0 to 7 can be set for the software interrupt source.
#define IRQP0_SWINTP_MASK 0x07
#define IRQP0_SWINTP_LSBPOS 4
#define IRQP0_SWINTP_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP0_SWINTP_MASK) << IRQP0_SWINTP_LSBPOS)
// <3:0> Reserved : Interrupt 0 cannot be prioritized

// IRQP1
// <31> Reserved
// <30:28> I2CMPI : A priority level of 0 to 7 can be set for I2C master.
#define IRQP1_I2CMPI_MASK 0x07
#define IRQP1_I2CMPI_LSBPOS 28
#define IRQP1_I2CMPI_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP1_I2CMPI_MASK) << IRQP1_I2CMPI_LSBPOS)
// <27> Reserved
// <26:24> IRQ1PI : A priority level of 0 to 7 can be set for IRQ1.
#define IRQP1_IRQ1PI_MASK 0x07
#define IRQP1_IRQ1PI_LSBPOS 24
#define IRQP1_IRQ1PI_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP1_IRQ1PI_MASK) << IRQP1_IRQ1PI_LSBPOS)
// <23> Reserved
// <22:20> IRQ0PI : A priority level of 0 to 7 can be set for IRQ0.
#define IRQP1_IRQ0PI_MASK 0x07
#define IRQP1_IRQ0PI_LSBPOS 20
#define IRQP1_IRQ0PI_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP1_IRQ0PI_MASK) << IRQP1_IRQ0PI_LSBPOS)
// <19> Reserved
// <18:16> SPIMPI : A priority level of 0 to 7 can be set for SPI master.
#define IRQP1_SPIMPI_MASK 0x07
#define IRQP1_SPIMPI_LSBPOS 16
#define IRQP1_SPIMPI_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP1_SPIMPI_MASK) << IRQP1_SPIMPI_LSBPOS)
// <15> Reserved
// <14:12> UARTPI : A priority level of 0 to 7 can be set for UART.
#define IRQP1_UARTPI_MASK 0x07
#define IRQP1_UARTPI_LSBPOS 12
#define IRQP1_UARTPI_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP1_UARTPI_MASK) << IRQP1_UARTPI_LSBPOS)
// <11> Reserved
// <10:8> ADCPI : A priority level of 0 to 7 can be set for the ADC interrupt source.
#define IRQP1_ADCPI_MASK 0x07
#define IRQP1_ADCPI_LSBPOS 8
#define IRQP1_ADCPI_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP1_ADCPI_MASK) << IRQP1_ADCPI_LSBPOS)
// <7:0> Reserved

// IRQP2
// <31:15> Reserved
// <14:12> IRQ3PI : A priority level of 0 to 7 can be set for IRQ3.
#define IRQP2_IRQ3PI_MASK 0x07
#define IRQP2_IRQ3PI_LSBPOS 12
#define IRQP2_IRQ3PI_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP2_IRQ3PI_MASK) << IRQP2_IRQ3PI_LSBPOS)
// <11> Reserved
// <10:8> IRQ2PI : A priority level of 0 to 7 can be set for IRQ2.
#define IRQP2_IRQ2PI_MASK 0x07
#define IRQP2_IRQ2PI_LSBPOS 8
#define IRQP2_IRQ2PI_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP2_IRQ2PI_MASK) << IRQP2_IRQ2PI_LSBPOS)
// <7> Reserved
// <6:4> SPISPI : A priority level of 0 to 7 can be set for SPI slave.
#define IRQP2_SPISPI_MASK 0x07
#define IRQP2_SPISPI_LSBPOS 4
#define IRQP2_SPISPI_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP2_SPISPI_MASK) << IRQP2_SPISPI_LSBPOS)
// <3> Reserved
// <2:0> I2CSPI : A priority level of 0 to 7 can be set for I2C slave.
#define IRQP2_I2CSPI_MASK 0x07
#define IRQP2_I2CSPI_LSBPOS 0
#define IRQP2_I2CSPI_BIT_PATTERN(x) ((((uint32_t)(x)) & IRQP2_I2CSPI_MASK) << IRQP2_I2CSPI_LSBPOS)


//IRQCLRE
//<31:20> Reserved : These bits are reserved and should not be written to.
//<19> IRQ3CLRI : A 1 must be written to this bit in the IRQ3 interrupt service routine to clear an edge triggered IRQ3 interrupt.
#define IRQCLRE_IRQ3CLRI 0x00040000
//<18> IRQ2CLRI : A 1 must be written to this bit in the IRQ2 interrupt service routine to clear an edge triggered IRQ2 interrupt.
#define IRQCLRE_IRQ2CLRI 0x00020000
//<17:15> Reserved
//<14> IRQ1CLRI : A 1 must be written to this bit in the IRQ1 interrupt service routine to clear an edge triggered IRQ1 interrupt.
#define IRQCLRE_IRQ1CLRI 0x00004000
//<13> IRQ0CLRI : A 1 must be written to this bit in the IRQ0 interrupt service routine to clear an edge triggered IRQ0 interrupt.
#define IRQCLRE_IRQ0CLRI 0x00002000
//<12:0> Reserved


//PLLCON
	//	<7:3>	Reserved, These bits must always be set to 0.
	//	<2>  	EXTCLK
#define PLLCON_EXTCLK_ENABLED 	0x04 //				Set this bit to 1 to select external clock input from P2.0.
#define PLLCON_EXTCLK_DISABLED	0x00 	//				Clear this bit to disable the external clock.
	//	<1:0>	OSEL
	//				Oscillator selection bits.
#define PLLCON_OSEL_INTERNAL 0x00	//				[00] = internal 32,768 Hz oscillator.
	//				[01] = internal 32,768 Hz oscillator.
#define PLLCON_OSEL_EXTERNAL 0x02		//				[10] = external crystal.
	//				[11] = internal 32,768 Hz oscillator.




	//	POWCON0
	//	<7> 	Reserved, set to 0
	//	<6> 	XPD
	//				XTAL power-down.
	//				Cleared by user to power down the external crystal circuitry.
	//				Set by user to enable the external crystal circuitry.
	//	<5> 	PLLPD
	//				PLL power-down.
	//				Timer peripherals power down if driven from the PLL output clock.
	//				Timers driven from an active clock source remain in normal power mode.
	//				This bit is cleared to 0 to power down the PLL.
	//				The PLL cannot be powered down if either the core or peripherals are enabled;
	//				Bit 3, Bit 4, and Bit 5 must be cleared simultaneously.
	//				Set by default, and set by hardware on a wake-up event.
	//	<4> 	PPD
	//				Peripherals power-down.
	//				The peripherals that are powered down by this bit are as follows:
	//					SRAM, Flash/EE memory and GPIO interfaces, and SPI/I2C and UART serial ports.
	//				Cleared to power down the peripherals.
	//				The peripherals cannot be powered down if the core is enabled;
	//				Bit 3 and Bit 4 must be cleared simultaneously.
	//				Set by default and/or by hardware on a wake-up event.
	//				Wake-up timer (Timer1) can remain active.
	//	<3> 	COREPD
	//				Core power-down.
	//				If user code powers down the MCU, include a dummy MCU cycle
	//				after the power-down command is written to POWCON0.
	//				Cleared to power down the ARM core.
	//				Set by default and set by hardware on a wake-up event.
	//	<2:0> 	CD[2:0]
	//				Core clock depends on CD setting:
	#define POWCON0_CLK_10240KHZ 0x00	//				[000] = 10.24 MHz
	#define POWCON0_CLK_5120KHZ  0x01	//				[001] = 5.12 MHz
	#define POWCON0_CLK_2560KHZ  0x02	//				[010] = 2.56 MHz
	#define POWCON0_CLK_1280KHZ  0x03	//				[011] = 1.28 MHz [default value]
	#define POWCON0_CLK_640KHZ   0x04	//				[100] = 640 kHz
	#define POWCON0_CLK_320KHZ   0x05	//				[101] = 320 kHz
	#define POWCON0_CLK_160KHZ   0x06	//				[110] = 160 kHz
	#define POWCON0_CLK_80KHZ    0x07	//				[111] = 80 kHz
	#define POWCON0_MODE_ACTIVE 0x78	// powcon0[6:3] = 1111
	#define POWCON0_MODE_PAUSE  0x70	// powcon0[6:3] = 1110
	#define POWCON0_MODE_NAP    0x60	// powcon0[6:3] = 1100
	#define POWCON0_MODE_SLEEP  0x40	// powcon0[6:3] = 1000
	#define POWCON0_MODE_STOP   0x00	// powcon0[6:3] = 0000

	#define POWKEY1VAL 0x1
	#define POWKEY2VAL 0xF4




	// POWCON1
	//	<15:9> 	Reserved
	//				Must always be set to 0.
	//	<8> 	PWMOFF
	//				PWM power-down bit.
	#define POWCON1_PWM_ENABLED  0x0100 	// Set by user to 1 to enable the PWM block.
	//				This bit is set by default.
	#define POWCON1_PWM_DISABLED 0x0000 	// Cleared by user to 0 to power down the PWM block.
	//	<7:6>	Reserved
	//				Reserved bits. Always clear these bits to 0.
	//	<5> 	UARTOFF
	//				UART power-down bit.
	#define POWCON1_UART_ENABLED  0x0020	// Set by user to 1 to enable the UART block.
	//				This bit is set by default.
	#define POWCON1_UART_DISABLED 0x0000	// Cleared by user to 0 to power down the UART block.
	//	<4:3> 	Reserved
	//				Reserved bits. Always clear these bits to 0.
	//	<2> 	I2CSPIOFF
	//				I2C/SPI power-down bit.
	#define POWCON1_I2CSPI_ENABLED  0x0004	// Set by user to 1 to enable the I2C/SPI blocks.
	//				This bit is set by default.
	#define POWCON1_I2CSPI_DISABLED 0x0000	// Cleared by user to 0 to power down the I2C/SPI blocks.
	//	<1:0> 	Reserved
	//				Reserved Bits. Always clear these bits to 0.
	
	#define POWKEY3VAL 0x76
	#define POWKEY4VAL 0xB1




	// GPXCON
	//	<31:30>	Reserved.
	//	<29:28>	Reserved.
	//	<27:26>	Reserved.
	//	<25:24>	Selects the function of the P0.6/RTS and P1.6/PWM pins.
	//	<23:22>	Reserved.
	//	<21:20>	Selects the function of the P0.5/CTS and P1.5/PWM3 pins.
	//	<19:18>	Reserved.
	//	<17:16>	Selects the function of the P0.4/IRQ0/PWM1 and P1.4/PWM2 pins.
	//	<15:14>	Reserved.
	//	<13:12>	Selects the function of the P0.3/MOSI/SDA and P1.3/TRIP pins.
	//	<11:10>	Reserved.
	//	<9:8>  	Selects the function of the P0.2/MISO and P1.2/SYNC pins.
	//	<7:6>  	Reserved.
	//	<5:4>  	Selects the function of the P0.1/SCLK/SCL, P1.1/SOUT, and P2.1/IRQ3/PWM5 pins.
	//	<3:2>  	Reserved.
	//	<1:0>  	Selects the function of the P0.0/SS, P1.0/IRQ1/SIN/T0, P2.0/IRQ2/PWM0/EXTCLK pins.

	// GP0CON0
	//	<31:30>	Reserved.
	//	<29:28>	Reserved.
	//	<27:26>	Reserved.
	#define GP0CON0_P0_6_GPIO			0x00000000	//	<25:24>	Selects the function of the P0.6/RTS pin
	#define GP0CON0_P0_6_RTS 			0x01000000
	//	<23:22>	Reserved.
	#define GP0CON0_P0_5_GPIO			0x00000000	//	<21:20>	Selects the function of the P0.5/CTS
	#define GP0CON0_P0_5_CTS			0x00100000
	//	<19:18>	Reserved.
	#define GP0CON0_P0_4_GPIO			0x00000000	//	<17:16>	Selects the function of the P0.4/IRQ0/PWM1
	#define GP0CON0_P0_4_PWM1			0x00010000
	//	<15:14>	Reserved.
	#define GP0CON0_P0_3_GPIO			0x00000000	//	<13:12>	Selects the function of the P0.3/MOSI/SDA
	#define GP0CON0_P0_3_MOSI			0x00001000
	//	<11:10>	Reserved.
	#define GP0CON0_P0_2_GPIO			0x00000000	//	<9:8>  	Selects the function of the P0.2/MISO
	#define GP0CON0_P0_2_MISO			0x00000100	
	//	<7:6>  	Reserved.
	#define GP0CON0_P0_1_GPIO			0x00000000	//	<5:4>  	Selects the function of the P0.1/SCLK/SCL
	#define GP0CON0_P0_1_SCLK			0x00000010	
	//	<3:2>  	Reserved.
	#define GP0CON0_P0_0_GPIO			0x00000000	//	<1:0>  	Selects the function of the P0.0/SS
	#define GP0CON0_P0_0_SSB			0x00000001	

	// GP1CON
	//	<31:30>	Reserved.
	//	<29:28>	Reserved.
	//	<27:26>	Reserved.
	#define GP1CON_P1_6_GPIO			0x00000000	//	<25:24>	Selects the function of the P0.6/RTS and P1.6/PWM pins.
	#define GP1CON_P1_6_PWM4			0x01000000
	//	<23:22>	Reserved.
	#define GP1CON_P1_5_GPIO			0x00000000	//	<21:20>	Selects the function of the P0.5/CTS and P1.5/PWM3 pins.
	#define GP1CON_P1_5_PWM3			0x00100000
	//	<19:18>	Reserved.
	#define GP1CON_P1_4_GPIO			0x00000000	//	<17:16>	Selects the function of the P0.4/IRQ0/PWM1 and P1.4/PWM2 pins.
	#define GP1CON_P1_4_PWM2			0x00010000
	//	<15:14>	Reserved.
	#define GP1CON_P1_3_GPIO			0x00000000	//	<13:12>	Selects the function of P1.3/TRIP
	#define GP1CON_P1_3_TRIP			0x00001000
	//	<11:10>	Reserved.
	#define GP1CON_P1_2_GPIO			0x00000000	//	<9:8>  	Selects the function of P1.2/SYNC
	#define GP1CON_P1_2_SYNC			0x00000100
	//	<7:6>  	Reserved.
	#define GP1CON_P1_1_GPIO			0x00000000	//	<5:4>  	Selects the function of P1.1/SOUT
	#define GP1CON_P1_1_SOUT			0x00000010
	//	<3:2>  	Reserved.
	#define GP1CON_P1_0_GPIO_IRQ1	0x00000000	//	<1:0>  	Selects the function of P1.0/IRQ1/SIN/T0
	#define GP1CON_P1_0_SIN      	0x00000001

	// GP2CON
	//	<31:6>  	Reserved.
	#define GP2CON_P2_1_GPIO			0x00000000	//	<5:4>  	Selects the function of the P2.1/IRQ3/PWM5 pins.
	#define GP2CON_P2_1_PWM5			0x00000010
	//	<3:2>  	Reserved.
	#define GP2CON_P2_0_GPIO			0x00000000	//	<1:0>  	Selects the function of the P2.0/IRQ2/PWM0/EXTCLK pins.
	#define GP2CON_P2_0_PWM0			0x00000001

	//GPxDAT
	//	<31:24>	Direction of the data.
	//					Set to 1 by user to configure the GPIO pin as an output.
	//					Cleared to 0 by user to configure the GPIO pin as an input.
	//	<23:16>	Port x data output.
	//	<15:8>	Reflect the state of Port x pins at reset (read only).
	//	<7:0>  	Port x data input (read only).

	//GP0DAT
	//	<31:24>	Direction of the data.
	//					Set to 1 by user to configure the GPIO pin as an output.
	//					Cleared to 0 by user to configure the GPIO pin as an input.
	#define GPXDAT_DIR_INP(x)    	0x00000000
	#define GPXDAT_DIR_OUT(x)    	(0x01000000 << (x))
	
	//	<23:16>	Port x data output.
	#define GPXDAT_OUTBIT_LO(x)   	0x00000000
	#define GPXDAT_OUTBIT_HI(x)   	(0x00010000 << (x))
	#define GPX_OUTBIT(x) 				(0x00010000 << (x))
	//	<15:8>	Reflect the state of Port x pins at reset (read only).
	//	<7:0>  	Port x data input (read only).
	#define GPX_INPBIT(x) 				(0x00000001 << (x))

	//GPxSET
	//	<31:24>	Reserved.
	//	<23:16>	Data Port x set bit.
	//						Set to 1 by user to set bit on Port x; also sets the corresponding bit in the GPxDAT MMR.
	//						Cleared to 0 by user; does not affect the data output.
	//	<15:0> 	Reserved.

	//GPxCLR
	//	<31:24>	Reserved.
	//	<23:16> Data Port x clear bit.
	//					Set to 1 by user to clear the bit on Port x; also clears the corresponding bit in the GPxDAT MMR.
	//					Cleared to 0 by user; does not affect the data output.
	//	<15:0>	Reserved.
	
	//GPxPAR
	//	<31:15> Reserved.
	//	<23:16>	GPL[7:0] General I/O port pin functionality lock registers.
	//					GPL[7:0] = 0, normal operation.
	//					GPL[7:0] = 1, for each GPIO pin, if this bit is set, writing to the corresponding bit in GPxCON or GPxDAT register bit has no effect.
	//	<15:8>	GPDS[7:0] Drive strength configuration. This bit is configurable.
	//					GPDS[x] = 0, maximum source current is 2 mA.
	//					GPDS[x] = 1, maximum source current is 4 mA.
	//	<7:0>		GPPD[7:0] Pull-Up Disable Port x[7:0].
	//					GPPD[x] = 0, pull-up resistor is active.
	//					GPPD[x] = 1, pull-up resistor is disabled.
	
	//	GP0CON1
	//	<7:2>	Reserved These bits must always be set to 0.
	//	<1>  	SPII2CSEL
	//				This bit configures the P0.0 to P0.3 functions in I2C or SPI mode. Note that Bit 0 of GP0CON1 must be set to 0 for this bit to work.
	//				To select the P0.0, P0.1, P0.2, and P0.3 functions in SPI mode, clear this bit to 0.
	//				To select the P0.0, P0.1, P0.2, and P0.3 functions in I2C mode, set this bit to 1.
	//				This bit is cleared by default.
	//	<0>  	ADCSEL
	//				This bit configures the P0.0 to P0.3 functions as GPIO pins or as ADC input pins.
	//				To enable P0.0, P0.1, P0.2 and P0.3 functions as ADC inputs, set this bit to 1.
	//				To enable P0.0, P0.1, P0.2, and P0.3 functions as digital I/O, clear this bit to 0.
	//				This bit is cleared by default.
	//GP0KEY1 = 0x07;
	//GP0CON1 = ...;
	//GP0KEY2 = 0x13;

	//COMCON0
	//	<7>	DLAB
	//	Divisor latch access.
	#define COMCON0_DLAB_ENABLE_COMDIV 0x80	//	Set by user to enable access to the COMDIV0 and COMDIV1 registers.
	#define COMCON0_DLAB_ENABLE_COMRX_COMTX 0x00	//	Cleared by user to disable access to COMDIV0 and COMDIV1 and enable access to COMRX, COMTX, and COMIEN0.
	//
	//	<6> BRK
	#define COMCON0_BRK_FORCE 0x40	//	Set by user to force transmit to 0.
	#define COMCON0_BRK_NORMAL 0x00	//	Cleared to operate in normal mode.
	//
	//	<5> SP
	//	Stick parity. Set by user to force parity to defined values.
	#define COMCON0_SP_FORCED_PARITY 0x20	//	1 if EPS = 1 and PEN = 1, 0 if EPS = 0 and PEN = 1.
	#define COMCON0_SP_NORMAL 0x00
	//
	//	<4> EPS
	#define COMCON0_EPS_EVEN 0x10	//	Set for even parity.
	#define COMCON0_EPS_ODD 0x00	//	Cleared for odd parity.
	//
	//	<3> PEN
	//	Parity enable bit.
	#define COMCON0_PEN_PARITY_ON 0x08	//	Set by user to transmit and check the parity bit.
	#define COMCON0_PEN_NO_PARITY 0x00	//	Cleared by user for no parity transmission or checking.
	//
	//	<2> Stop
	#define COMCON0_STOP_2BITS 0x04	//	Set by user to transmit 1.5 or 2 stop bits if the word length is 5 bits or 6,7,8 bits.
	#define COMCON0_STOP_1BIT 0x00	//	Cleared by user to generate one stop bit in the transmitted data.
	//
	//	<1:0> WLS
	//	Word length select.
	#define COMCON0_WLS_5BITS 0x00	//	[00] = 5 bits.
	#define COMCON0_WLS_6BITS 0x01	//	[01] = 6 bits.
	#define COMCON0_WLS_7BITS 0x02	//	[10] = 7 bits.
	#define COMCON0_WLS_8BITS 0x03	//	[11] = 8 bits.

// COMCON1
// <7:5> Reserved bits. Not used.
// <4>
#define COMCON1_LOOPBACK_TXHI 0x10 // Loopback. Set by user to enable loopback mode. In loopback mode, the transmit pin is forced high.
#define COMCON1_LOOPBACK_OFF 0x00
// <3:2> Reserved bits. Not used.
// <1> RTS
#define COMCON1_RTS_LOW 0x02 // Request to send. Set by user to force the RTS output to 0.
#define COMCON1_RTS_HIGH 0x00 // Cleared by user to force the RTS output to 1.
// <0> DTR Data terminal ready.
#define COMCON1_DTR_LOW 0x01 // Set by user to force the DTR output to 0.
#define COMCON1_DTR_HIGH 0x00 // Cleared by user to force the DTR output to 1.

// COMSTA0
// <7> Reserved
// <6> TEMT : COMTX and shift register empty status bit.
#define COMSTA0_TEMT 0x40
//            This bit indicates that the data has been transmitted, that is, no more data is present in the shift register.
//						Set automatically if COMTX and the shift register are empty. Cleared automatically when writing to COMTX.
//
// <5> THRE : COMTX empty status bit.
#define COMSTA0_THRE 0x20
// Set automatically if COMTX is empty. COMTX can be written as soon as this bit is set;
// the previous data might not have been transmitted yet and can still be present in the shift register.
// Cleared automatically when writing to COMTX.
//
// <4> BI : Break indicator.
#define COMSTA0_BI 0x10
//  Set when P1.0/IRQ1/SIN/T0 pin is held low for more than the maximum word length. Cleared automatically.
//
// <3> FE : Framing error. Set when the stop bit is invalid. Cleared automatically.
#define COMSTA0_FE 0x08
//
// <2> PE : Parity error. Set when a parity error occurs. Cleared automatically.
#define COMSTA0_PE 0x04
//
// <1> OE Overrun error. Set automatically if data is overwritten before being read. Cleared automatically.
#define COMSTA0_OE 0x02
//
// <0> DR Data ready. Set automatically when COMRX is full. Cleared by reading COMRX.
#define COMSTA0_DR 0x01



	//COMDIV0 = least significant byte of Divisor Latch, DL

	//COMDIV1 = most significant byte of Divisor Latch, DL
	
	//COMDIV2
	//	<15> FBEN Fractional baud rate generator enable bit.
	#define COMDIV2_FBEN_FRAC_ON 0x8000	// Set by user to enable the fractional baud rate generator.
	#define COMDIV2_FBEN_FRAC_OFF 0x0000 // Cleared by user to generate the baud rate using the standard 450 UART baud rate generator.
	//	<14:13> Reserved.
	//	<12:11> FBM[1:0] = M. If FBM = 0, M = 4
	#define COMDIV2_FBM_MASK 0x03
	#define COMDIV2_FBM_LSBPOS 11
	#define COMDIV2_FBM_BIT_PATTERN(x) ((((uint16_t)(x)) & COMDIV2_FBM_MASK) << COMDIV2_FBM_LSBPOS)
	//	<10:0> FBN[10:0] = N
	#define COMDIV2_FBN_MASK 0x07FF
	#define COMDIV2_FBN_LSBPOS 0
	#define COMDIV2_FBN_BIT_PATTERN(x) ((((uint16_t)(x)) & COMDIV2_FBN_MASK) << COMDIV2_FBN_LSBPOS)

  #define COMDIV0VAL_9600 (0x21)
	#define COMDIV1VAL_9600 (0x00)
	#define COMDIV2VAL_9600 (COMDIV2_FBEN_FRAC_OFF | COMDIV2_FBM_BIT_PATTERN(0) | COMDIV2_FBN_BIT_PATTERN(0))

	// In Frac mode, Baudrate = 10.24MHz / (16*DL*2*(M+N/2048))
	// for 115200 baud, DL=0x2, M=1, N=796
  #define COMDIV0VAL_115200 (0x02)
	#define COMDIV1VAL_115200 (0x00)
	#define COMDIV2VAL_115200 (COMDIV2_FBEN_FRAC_ON | COMDIV2_FBM_BIT_PATTERN(1) | COMDIV2_FBN_BIT_PATTERN(796))
	
	
		//COMIEN0
	//	<7:4>	Reserved. Not used.
	//	<3> 	EDSSI	Modem status interrupt enable bit.
	#define COMIEN0_ESSDI_ENABLED 0x08	//				Set by user to enable generation of an interrupt if any of COMSTA0[3:1] are set.
	#define COMIEN0_ESSDI_DISABLED 0x00	//				Cleared by user.
	//	<2> 	ELSI	Receive status interrupt enable bit.
	#define COMIEN0_ELSI_ENABLED 0x04	//				Set by user to enable generation of an interrupt if any of the COMSTA0[3:1] register bits are set.
	#define COMIEN0_ELSI_DISABLED 0x00	//				Cleared by user.
	//	<1> 	ETBEI	Enable transmit buffer empty interrupt.
	#define COMIEN0_ETBEI_ENABLED 0x02	//				Set by user to enable an interrupt when the buffer is empty during a transmission; that is, when COMSTA0[5] is set.
	#define COMIEN0_ETBEI_DISABLED 0x00	//				Cleared by user.
	//	<0> 	ERBFI	Enable receive buffer full interrupt.
	#define COMIEN0_ERBFI_ENABLED 0x01	//				Set by user to enable an interrupt when the buffer is full during a reception.
	#define COMIEN0_ERBFI_DISABLED 0x00	//				Cleared by user.

	
//COMIID0
#define COMIID0_NO_INTERRUPT 	0x01	// value 001 : No interrupt
#define COMIID0_LINE_STATUS  	0x06	// value 110 : Line status interrupt; clear by: Read COMSTA0
#define COMIID0_RX_BUF_FULL  	0x04	// value 100 : Receive buffer full interrupt; Clear by: Read COMRX
#define COMIID0_TX_BUF_EMPTY 	0x02	// value 010 : Transmit buffer empty interrupt; Clear by: Write data to COMTX or read COMIID0
#define COMIID0_MODEM_STATUS 	0x00	// value 000 : Modem status interrupt; Clear by: Read COMSTA1 register


	
	
		// ADCMSKI
	// bit 7 not used
	#define ADCMSKI_ADC0ATHEX_INTEN 0x40	// bit 6 ADC0ATHEX_INTEN when set to 1 enables an interrupt when the ADC0ATHEX bit in the ADCSTA register is set
	// bit 5 not used
	#define ADCMSKI_ADC0THEX_INTEN 0x10	// bit 4 ADC0THEX_INTEN When set to 1 enables an interrupt when the ADC0THEX bit in the ADCSTA register is set.
	#define ADCMSKI_ADC0OVR_INTEN 0x08	// bit 3 ADC0OVR_INTEN when set to 1 enables an interrupt when the ADC0OVR bit in the ADCSTA register is set.
	// bit 2 not used
	#define ADCMSKI_ADC1RDY_INTEN 0x02	// bit 1 ADC1RDY_INTEN when set to 1 enables an interrupt when the ADC1RDY bit in the ADCSTA register is set.
	#define ADCMSKI_ADC0RDY_INTEN 0x01	// bit 0 ADC0RDY_INTEN when set to 1 enables an interrupt when the ADC0RDY bit in the ADCSTA register is set




	//ADCFLT
	// <15> 	CHOPEN
	//			Chop enable. Enable system chopping of all active ADCs.
	//			When this bit is set, the ADC has very low offset errors and drift, but the ADC output rate
	//			is reduced by a factor of 3 if AF = 0 (see sinc3 decimation factor, Bits[6:0] in this table).
	//			If AF > 0, then the ADC output update rate is the same with chop on or off.
	//			When chop is enabled, the settling time is two output periods.
	#define  ADCFILT_CHOPEN_ON  	0x8000
	#define  ADCFILT_CHOPEN_OFF 	0x0000
	//
	// <14> 	RAVG2
	//			Running average-by-2 enable bit.
	//			Set by user to enable a running-average-by-2 function, reducing ADC noise.
	//			This function is automatically enabled when chopping is active.
	//			It is an optional feature when chopping is inactive, and if enabled (when chopping is inactive), does
	//			not reduce the ADC output rate but does increase the settling time by one conversion period.
	//			Cleared by user to disable the running average function.
	#define ADCFILT_RAVG2_ON  	0x4000
	#define ADCFILT_RAVG2_OFF 	0x0000
	//
	// <13:8>	AF[5:0]
	//			Averaging factor (AF).
	//			The values written to these bits are used to implement a programmable first-order sinc3 post filter.
	//			The averaging factor can further reduce ADC noise at the expense of output rate as described in Bits[6:0]
	//			(sinc3 decimation factor) in this table.
	#define ADCFILT_AF_MASK 0x3F
	#define ADCFILT_AF_LSBPOS 8
	#define ADCFILT_AF_BIT_PATTERN(x) ((((uint16_t)(x)) & ADCFILT_AF_MASK) << ADCFILT_AF_LSBPOS)
	//
	// <7>		NOTCH2
	//			Sinc3 modify. Set by user to modify the standard sinc3 frequency response to increase the filter
	//			stop-band rejection by approximately 5 dB. This is achieved by inserting a second notch (NOTCH2) at
	//			fNOTCH2 = 1.333 � fNOTCH
	//			where fNOTCH is the location of the first notch in the response.
	#define ADCFILT_NOTCH2_ENABLE 0x80
	//
	// <6:0>	SF[6:0]
	//			Sinc3 decimation factor (SF)
	//			The value (SF) written in these bits controls the oversampling (decimation factor) of the sinc3 filter.
	//			The output rate from the sinc3 filter is given by fADC = (512,000/([SF + 1] � 64)) Hz
	//			when the chop bit (Bit 15, chop enable) = 0 and the averaging factor (AF) = 0.
	//			This is valid for all SF values <= 125.
	//			For SF = 126, fADC is forced to 60 Hz.
	//			For SF = 127, fADC is forced to 50 Hz.
	#define ADCFILT_SF_MASK 0x7F
	#define ADCFILT_SF_LSBPOS 0
	#define ADCFILT_SF_BIT_PATTERN(x) (((x) & ADCFILT_SF_MASK) << ADCFILT_SF_LSBPOS)

	
	
	
		//ADCMDE
	// bit 7 : ADCCLKSEL
	#define ADCMDE_ADCCLKSEL_512KHZ 0x80 //	1  forADCCLK = 512 kHz
	#define ADCMDE_ADCCLKSEL_131KHZ 0x00 //	0 for ADCCLK = 131 kHz
	//
	// bit 6 : not used
	//
	// bit 5 : ADCLPMEN
	#define ADCMDE_ADCLPMEN_LOWPWR 0x20 // Must be 1 in low power mode.
	#define ADCMDE_ADCLPMEN_NORMAL 0x00 // This bit has no effect if ADCMDE[4:3] = 00
	//
	// bits <4:3> ADCLPMCFG[1:0]
	#define ADCMDE_ADCLPMCFG_NORMAL 0x00 // [00],[10]=normal mode
	#define ADCMDE_ADCLPMCFG_LOWPWR 0x08 // [01]=low power mode
	#define ADCMDE_ADCLPMCFG_LOWPWR_PGAOFF 0x18 // [11]=low power mode and PGA off
	//
	// bit <2:0> = 0 : ADCMD[2:0]
	#define ADCMDE_ADCMD_OFF 0x00 //	[000] = ADC power-down mode. All ADC circuits and the input amplifier are powered down.
	#define ADCMDE_ADCMD_CONT 0x01 //	[001] = ADC continuous conversion mode. Any enabled ADC continuously converts at fADC. ADCxRDY must be cleared to enable new data to be written to ADC0DAT/ADC1DAT.
	#define ADCMDE_ADCMD_SINGLE 0x02 //	[010] = ADC single conversion mode. In this mode, any enabled ADC performs a single conversion. The ADC enters idle mode when the single shot conversion is complete.
	//			A single conversion takes two to three ADC clock cycles, depending on the chop mode.
	#define ADCMDE_ADCMD_IDLE 0x03 // [011] = ADC idle mode. In this mode, the ADC is fully powered on but is held in reset. The part enters this mode after calibration.
	#define ADCMDE_ADCMD_OFFSET_CAL 0x04 //	[100] = ADC self-offset calibration. In this mode, an offset calibration is performed on any enabled ADC
	//			using an internally generated 0 V. The calibration is carried out at the user-programmed ADC settings;
	//			therefore, as with a normal single ADC conversion, it takes two to three ADC conversion cycles before
	//			a fully settled calibration result is ready. The calibration result is automatically written to
	//			the ADCxOF MMR of the respective ADC. The ADC returns to idle mode, and the calibration and conversion
	//			ready status bits are set at the end of an offset calibration cycle.
	//			Note: Always use ADC0 for single-ended self-calibration cycles on the primary ADC.
	//			Always use ADC0/ADC1 when self-calibrating for a differential input to the primary ADC.
	#define ADCMDE_ADCMD_GAIN_CAL 0x05 //	[101] = ADC self-gain calibration. In this mode, a gain calibration against an internal reference voltage is
	//			performed on all enabled ADCs. A gain calibration is a two-stage process and takes twice the time of
	//			an offset calibration. The calibration result is automatically written to the ADCxGN MMR of the
	//			respective ADC. The ADC returns to idle mode and the calibration and conversion ready status bits
	//			are set at the end of a gain calibration cycle. An ADC self-gain calibration should only be carried
	//			out on the primary channel ADC. Note that self-gain calibration works only when the gain = 1;
	//			do not use it when the gain > 1.
	#define ADCMDE_ADCMD_ZERO_CAL 0x06 //	[110] = ADC system zero-scale calibration. In this mode, a zero-scale calibration is performed on enabled ADC
	//			channels against an external zero-scale voltage driven at the ADC input pins.
	//			To do this, short the channel externally.
	#define ADCMDE_ADCMD_FULLSCALE_CAL 0x07 //	[111] = ADC system full-scale calibration. In this mode, a full-scale calibration is performed on enabled ADC
	//			channels against an external full-scale voltage driven at the ADC input pins. The ADCxGN register is
	//			updated after a full-scale calibration sequence




	//ADC0CON
	// <15> ADC0EN Primary channel ADC enable.
	#define ADC0CON_ADC0EN_ON  				0x8000 //		This bit is set to 1 by user code to enable the primary ADC.
	#define ADC0CON_ADC0EN_OFF 				0x0000 //		Clearing this bit to 0 powers down the primary ADC and resets the respective ADC ready bit in the ADCSTA MMR
	//
	// <14:13> ADC0DIAG[1:0] Diagnostic current source enable bits.
	#define ADC0CON_ADC0DIAG_OFF 			0x0000 //		[00] = current sources off.
	#define ADC0CON_ADC0DIAG_POSINP 	0x2000 //		[01] = enables a 50 �A current source on the selected positive input (for example, ADC0).
	#define ADC0CON_ADC0DIAG_NEGINP 	0x4000 //		[10] = enables a 50 �A current source on the selected negative input (for example, ADC1).
	#define ADC0CON_ADC0DIAG_BOTHINP 	0x6000 //		[11] = enables a 50 �A current source on both selected inputs (for example, ADC0 and ADC1).
	//
	// <12> HIGHEXTREF0
	#define ADC0CON_HIGHEXTREF0_ENABLED		0x1000 // This bit must be set high if the external reference for ADC0 exceeds 1.35 V. This results in the reference source being divided by 2.
	#define ADC0CON_HIGHEXTREF0_DISABLED	0x0000 //	Clear this bit when using the internal reference or an external reference of less than 1.35 V.
	//
	// <11> AMP_CM
	#define ADC0CON_AMP_CM_MIDRAIL	0x0800 // Set to 1 to set the PGA output common-mode voltage to AVDD/2.
	#define ADC0CON_AMP_CM_INPUTCM	0x0000 // Set to 0 to set the PGA output common-mode voltage to the PGA input common-mode voltage level.
	//
	// <10> ADC0CODE Primary channel ADC output coding.
	#define ADC0CON_ADC0CODE_UNIPOLAR	0x0400 //		Set to 1 to configure primary ADC output coding as unipolar.
	#define ADC0CON_ADC0CODE_TWOSCOMP 0x0000 //		Set to 0 to configure primary ADC output coding as twos complement.
	//
	// <9:6> ADC0CH[3:0] Primary channel ADC input select.
	#define ADC0CON_ADC0CH_ADC0_ADC1_DIFF 		0x0000 //		[0000] = ADC0/ADC1 (differential mode).
	#define ADC0CON_ADC0CH_ADC0_ADC5_SE   		0x0040 //		[0001] = ADC0/ADC5 (single-ended mode).
	#define ADC0CON_ADC0CH_ADC1_ADC5_SE   		0x0080 //		[0010] = ADC1/ADC5 (single-ended mode).
	#define ADC0CON_ADC0CH_REFP_REFM      		0x00C0 //		[0011] = VREF+, VREF-. Note: This is the reference selected by the ADC0REF bits.
	#define ADC0CON_ADC0CH_RESERVED4      		0x0100 //		[0100] = Not used. This bit combination is reserved for future functionality and should not be written.
	#define ADC0CON_ADC0CH_ADC2_ADC3_DIFF 		0x0140 //		[0101] = ADC2/ADC3 (differential mode).
	#define ADC0CON_ADC0CH_ADC2_ADC5_SE   		0x0180 //		[0110] = ADC2/ADC5 (single-ended mode).
	#define ADC0CON_ADC0CH_ADC3_ADC5_SE   		0x01C0 //		[0111] = ADC3/ADC5 (single-ended mode).
	#define ADC0CON_ADC0CH_SHORT_ADC1     		0x0200 //		[1000] = internal short to ADC1.
	#define ADC0CON_ADC0CH_SHORT2_ADC1    		0x0240 //		[1001] = internal short to ADC1.
	//
	// <5:4> ADC0REF[1:0] Primary channel ADC reference select.
	#define ADC0CON_ADC0REF_INTERNAL    	0x0000 //		[00] = internal reference selected. In low power mode, the reference selection is controlled by ADCMDE[5].
	#define ADC0CON_ADC0REF_EXT_VREF    	0x0010 //		[01] = external reference inputs (VREF+, VREF-) selected. Set the HIGHEXTREF0 if reference exceeds 1.3 V.
	#define ADC0CON_ADC0REF_EXT_REF2IN  	0x0020 //		[10] = auxiliary external reference inputs (ADC4/EXT_REF2IN+, ADC5/EXT_REF2IN-) selected.
	#define ADC0CON_ADC0REF_VDD_GND_DIV2	0x0030 //		[11] = (AVDD, AGND) divide-by-two selected.
	//
	// <3:0> ADC0PGA[3:0]
	//		Primary channel ADC gain select. Note, nominal primary ADC full-scale input voltage = (VREF/gain).
	#define ADC0CON_ADC0PGA_GAIN_1  	0x0000 //		[0000] = ADC0 gain of 1. Buffer of negative input is bypassed.
	#define ADC0CON_ADC0PGA_GAIN_2  	0x0001 //		[0001] = ADC0 gain of 2.
	#define ADC0CON_ADC0PGA_GAIN_4  	0x0002 //		[0010] = ADC0 gain of 4 (default value). Enables the in-amp.
	#define ADC0CON_ADC0PGA_GAIN_8  	0x0003 //		[0011] = ADC0 gain of 8.
	#define ADC0CON_ADC0PGA_GAIN_16 	0x0004 //		[0100] = ADC0 gain of 16.
	#define ADC0CON_ADC0PGA_GAIN_32 	0x0005 //		[0101] = ADC0 gain of 32.
	#define ADC0CON_ADC0PGA_GAIN_64 	0x0006 //		[0110] = ADC0 gain of 64 (maximum PGA gain setting).
	#define ADC0CON_ADC0PGA_GAIN_128	0x0007 //		[0111] = ADC0 gain of 128 (extra gain implemented digitally).
	#define ADC0CON_ADC0PGA_GAIN_256	0x0008 //		[1000] = ADC0 gain of 256.
	#define ADC0CON_ADC0PGA_GAIN_512	0x0009 //		[1001] = ADC0 gain of 512.
	//		[1XXX] = ADC0 gain is undefined.




// ADC1CON
	//<15>  ADC1EN Auxiliary channel ADC enable
	#define ADC1CON_ADC1EN_ON  				0x8000 //
	#define ADC1CON_ADC1EN_OFF 				0x0000 //
	//
	//<14:13> ADC1DIAG[1:0] Diagnostic current source enable bits.
	//	This is the same current source as that used on ADC0DIAG[1:0].
	//	The ADCs cannot enable the diagnostic current sources at the same time.
	#define ADC1CON_ADC1DIAG_OFF 			0x0000 //		[00]= current sources off.
	#define ADC1CON_ADC1DIAG_POSINP 	0x2000 //		[01] = enables a 50 �A current source on selected positive input (for example, ADC2).
	#define ADC1CON_ADC1DIAG_NEGINP 	0x4000 //		[10] = enables a 50 � A current source on selected negative input (for example, ADC3).
	#define ADC1CON_ADC1DIAG_BOTHINP 	0x6000 //		[11] = enables a 50 � A current source on both selected inputs (for example, ADC2 and ADC3).
	//
	//<12> HIGHEXTREF1
	#define ADC1CON_HIGHEXTREF1_ENABLED		0x1000 //	This bit must be set high if the external reference for ADC1 exceeds 1.35 V.
	//	This results in the reference source being divided by 2.
	#define ADC1CON_HIGHEXTREF1_DISABLED	0x0000 //	Clear this bit when using the internal reference or an external reference of less than 1.35 V.
	//
	//<11> ADC1CODE
	//	Auxiliary channel ADC output coding.
	#define ADC1CON_ADC1CODE_UNIPOLAR	0x0800 //	This bit is set to 1 by user code to configure auxiliary ADC output coding as unipolar.
	#define ADC1CON_ADC1CODE_TWOSCOMP 0x0000 //	This bit is cleared to 0 by user code to configure auxiliary ADC output coding as twos complement.
	//
	//<10:7> ADC1CH[3:0]
	//	Auxiliary channel ADC input select.
	//	Note: Single-ended channels are selected with respect to ADC5. Bias ADC5 to a minimum level of 0.1 V.
	#define ADC1CON_ADC1CH_ADC2_ADC3_DIFF			0x0000 //	[0000] = ADC2/ADC3 (differential mode).
	#define ADC1CON_ADC1CH_ADC4_ADC5_DIFF			0x0080 //	[0001] = ADC4/ADC5 (differential mode).
	#define ADC1CON_ADC1CH_ADC6_ADC7_DIFF			0x0100 //	[0010] = ADC6/ADC7 (differential mode).
	#define ADC1CON_ADC1CH_ADC8_ADC9_DIFF			0x0180 //	[0011] = ADC8/ADC9 (differential mode).
	#define ADC1CON_ADC1CH_ADC2_ADC5_SE				0x0200 //	[0100] = ADC2/ADC5 (single-ended mode).
	#define ADC1CON_ADC1CH_ADC3_ADC5_SE				0x0280 //	[0101] = ADC3/ADC5 (single-ended mode).
	#define ADC1CON_ADC1CH_ADC4_ADC5_SE				0x0300 //	[0110] = ADC4/ADC5 (single-ended mode).
	#define ADC1CON_ADC1CH_ADC6_ADC5_SE				0x0380 //	[0111] = ADC6/ADC5 (single-ended mode).
	#define ADC1CON_ADC1CH_ADC7_ADC5_SE				0x0400 //	[1000] = ADC7/ADC5 (single-ended mode).
	#define ADC1CON_ADC1CH_ADC8_ADC5_SE				0x0480 //	[1001] = ADC8/ADC5 (single-ended mode).
	#define ADC1CON_ADC1CH_ADC9_ADC5_SE				0x0500 //	[1010] = ADC9/ADC5 (single-ended mode).
	#define ADC1CON_ADC1CH_THEROMETER					0x0580 //	[1011] = internal temperature sensor+/internal temperature sensor-.
	#define ADC1CON_ADC1CH_REFP_REFM					0x0600 //	[1100] = VREF+, VREF-. Note: This is the reference selected by the ADC1REF bits.
	#define ADC1CON_ADC1CH_DACOUT							0x0680 //	[1101] = DAC_OUT/AGND.
	#define ADC1CON_ADC1CH_RESERVED14					0x0700 //	[1110] = undefined.
	#define ADC1CON_ADC1CH_SHORT_ADC3					0x0780 //	[1111] = internal short to ADC3.
	//
	// <6:4> ADC1REF[2:0]	Auxiliary channel ADC reference select.
	#define ADC1CON_ADC1REF_INTERNAL    			0x0000 //		[000] = internal reference selected.
		// In ADC low power mode, the voltage reference selection is controlled by ADCMODE[5].
	#define ADC1CON_ADC1REF_EXT_VREF  		  	0x0010 //		[001] = external reference inputs (VREF+, VREF-) selected.
		// Set the HIGHEXTREF1 bit if reference voltage exceeds 1.3 V.
	#define ADC1CON_ADC1REF_EXT_REF2IN		  	0x0020 //		[010] = auxiliary external reference inputs (ADC4/EXT_REF2IN+, ADC5/EXT_REF2IN-) selected.
		// Set the HIGHEXTREF1 bit if reference voltage exceeds 1.35 V.
	#define ADC1CON_ADC1REF_VDD_GND_DIV2	  	0x0030 //		[011] = (AVDD, AGND) divide-by-2 selected.
		// If this configuration is selected, the HIGHEXTREF1 bit is set automatically.
	#define ADC1CON_ADC1REF_VDD_ADC3	  			0x0040 //		[100] = (AVDD, ADC3). ADC3 can be used as the negative input terminal for the reference source.
	//		[101] to [111] = reserved.
	//
	// <3:2> BUF_BYPASS[1:0]	Buffer bypass.
	#define ADC1CON_BUF_BYPASS_NEGBUF_ACTIVE	0x0000 
	#define ADC1CON_BUF_BYPASS_NEGBUF_BYPASS	0x0004 
	#define ADC1CON_BUF_BYPASS_POSBUF_ACTIVE	0x0000 
	#define ADC1CON_BUF_BYPASS_POSBUF_BYPASS	0x0008 
	//		[00] = full buffer on. Both positive and negative buffer inputs active.
	//		[01] = negative buffer is bypassed, positive buffer is on.
	//		[10] = negative buffer is on, positive buffer is bypassed.
	//		[11] = full buffer bypass. Both positive and negative buffer inputs are off.
	//
	// <1:0> Digital gain. Select for auxiliary ADC inputs.
	#define ADC1CON_DIGGAIN_1									0x0000	//		[00] = ADC1 gain = 1.
	#define ADC1CON_DIGGAIN_2									0x0001	//		[01] = ADC1 gain = 2.
	#define ADC1CON_DIGGAIN_4									0x0002	//		[10] = ADC1 gain = 4.
	#define ADC1CON_DIGGAIN_8									0x0003	//		[11] = ADC1 gain = 8.




// ADCCFG
//	<7> GNDSW_EN
//		Analog ground switch enable.
#define ADCCFG_GNDSW_EN_ON 0x80//		Set to 1 to connect the external GND_SW pin to an internal analog ground reference point.
//		This bit can be used to connect and disconnect external circuits and components to ground under program control
//		and thereby minimize dc current consumption when the external circuit or component is not being used.
//		This bit is used in conjunction with ADCCFG[1] to select a 20 kO resistor to ground.
#define ADCCFG_GNDSW_EN_OFF 0x00 //		When this bit is cleared, the analog ground switch is disconnected from the external pin.
//
//	<6:5> ADC0ACCEN[1:0]
//		Primary channel (32-bit) accumulator enable.
#define ADCCFG_ADC0ACCEN_DISABLED 0x00
//		[00] = accumulator disabled and reset to 0. The accumulator must be disabled for a full ADC conversion
//			(ADCSTA[0] set twice) before the accumulator can be re-enabled to ensure that the accumulator is reset.
#define ADCCFG_ADC0ACCEN_ACTIVE_POSITIVE 0x20
//		[01] = accumulator active. Positive current values are added to the accumulator total; the accumulator can
//			overflow if allowed to run for >65,535 conversions. Negative current values are subtracted from the
//			accumulator total; the accumulator is clamped to a minimum value of 0.
#define ADCCFG_ADC0ACCEN_ACTIVE_BIPOLAR 0x40
//		[10] = accumulator active. Same as [01] except that there is no clamp. Positive current values are added to the
//			accumulator total; the accumulator can overflow if allowed to run for >65,535 conversions. The absolute
//			values of negative current are subtracted from the accumulator total; the accumulator in this mode continues
//			to accumulate negatively, below 0.
#define ADCCFG_ADC0ACCEN_ACTIVE_WITHCOMPARATOR 0x60
//		[11] = accumulator and comparator active. This causes an ADC0 interrupt if ADCMSKI[6] is set.
//
//	<4:3> ADC0CMPEN[1:0]
//		Primary ADC comparator enable bits.
#define ADCCFG_ADC0CMPEN_DISABLED 0x00
//		[00] = comparator disabled.
#define ADCCFG_ADC0CMPEN_COMP_IMMEDIATE 0x08
//		[01] = comparator active. Interrupt asserted if absolute value of ADC0 conversion result |I| >= ADC0TH.
#define ADCCFG_ADC0CMPEN_COMP_INC_RST 0x10
//		[10] = comparator count mode active. Interrupt asserted if absolute value of ADC0 conversion result
//				|I| >= ADC0TH for the number of ADC0THC conversions.
//				A conversion value |I| < ADC0TH resets the threshold counter value (ADC0THV) to 0.
#define ADCCFG_ADC0CMPEN_COMP_INC_DEC 0x18
//		[11] = comparator count mode active, interrupt asserted if absolute value of ADC0 conversion result
//				|I| >= ADC0TH for the number of ADC0THC conversions.
//				A conversion value |I| < ADC0TH decrements the threshold counter value (ADC0THV) toward 0.
//
//	<2> ADC0OREN
//		ADC0 overrange enable.
#define ADCCFG_ADC0OREN_ENABLED 0x04 //		Set by the user to enable a coarse comparator on the primary channel ADC.
//		If the reading is grossly (>30% approximate) overrange for the active gain setting,
//		the overrange bit in the ADCSTA MMR is set. The ADC reading must be outside this range for
//		greater than 125 �s for the flag to be set. Do not use this feature in ADC low power mode.
#define ADCCFG_ADC0OREN_DISABLED 0x00
//
//	<1> GNDSW_RES_EN
#define ADCCFG_GNDSW_RES_EN_20K 0x02 //		Set to 1 to enable a 20 kO resistor in series with the ground switch.
#define ADCCFG_GNDSW_RES_EN_SHORT 0x00//		Clear this bit to disable this resistor.
//
//	<0> ADCRCEN
//		ADC result counter enable.
#define ADCCFG_ADCRCEN_ENABLED 0x01 //		Set by user to enable the result count mode. ADC interrupts occur if ADC0RCR = ADC0RCV.
#define ADCCFG_ADCRCEN_DISABLED 0x00 //		Cleared to disable the result counter. ADC interrupts occur after every conversion.



#define T_CAPSEL_DEFAULT 0 // Reserved
#define T_CAPSEL_TIMER0 1 // Timer0
#define T_CAPSEL_TIMER1 2 // Timer1 or wake-up timer
#define T_CAPSEL_TIMER2 3 // Timer2 or watchdog timer
#define T_CAPSEL_TIMER3 4 // Timer3
// 5 Reserved
// 6 Reserved
// 7 Reserved
#define T_CAPSEL_ADC 8 // ADC
#define T_CAPSEL_ UART 9 // UART
#define T_CAPSEL_SPI 10 // SPI
#define T_CAPSEL_XIRQ0 11 // XIRQ0
#define T_CAPSEL_XIRQ1 12 // XIRQ1
#define T_CAPSEL_I2C_MASTER 13 // I2C master
#define T_CAPSEL_I2C_SLAVE 14 // I2C slave
#define T_CAPSEL_PWM 15 // PWM
#define T_CAPSEL_XIRQ2 16 // XIRQ2 (GPIO IRQ2)
#define T_CAPSEL_XIRQ3 17 // XIRQ3 (GPIO IRQ3)

//T0LD

//T0CLRI

//T0VAL

//T0CAP

//T0CON
// <31:24> T0PVAL : 8-bit postscaler.
#define T0CON_T0PVAL_MASK 0xFF
#define T0CON_T0PVAL_LSBPOS 24
#define T0CON_T0PVAL_BIT_PATTERN(x) (((x) & T0CON_T0PVAL_MASK) << T0CON_T0PVAL_LSBPOS)
//By writing to these eight bits, a value is written to the postscaler. Writing 0 is interpreted as a 1.
//By reading these eight bits, the current value of the counter is read.
//
// <23> T0PEN : Timer0 enable postscaler.
#define T0CON_T0PEN_ENABLED 0x00800000 // Set to enable the Timer0 postscaler.
// If enabled, interrupts are generated after T0CON[31:24] periods as defined by T0LD.
#define T0CON_T0PEN_DISABLED 0x00000000 // Cleared to disable the Timer0 postscaler.
//
// <22:20> Reserved. These bits are reserved and should be written as 0 by user code.
//
// <19> T0PCF : Postscaler compare flag; read only.
#define T0CON_T0PCF 0x00080000 
// Set if the number of Timer0 overflows is equal to the number written to the postscaler.
//
// <18> T0SRCI : Timer0 interrupt source.
#define T0CON_T0SRCI_POSTSCALER 0x00040000 // Set to select interrupt generation from the postscaler counter.
#define T0CON_T0SRCI_DIRECT     0x00040000 // Cleared to select interrupt generation directly from Timer0.
//
// <17> T0CAPEN : Event enable bit.
#define T0CON_T0CAPEN_ENABLED  0x00020000 // Set by user to enable time capture of an event.
#define T0CON_T0CAPEN_DISABLED 0x00000000 // Cleared by user to disable time capture of an event.
//
// <16:12> T0CAPSEL : Event Select Bits[17:0]. The events are described in Table 78.
#define T0CON_T0CAPSEL_MASK 0x1F
#define T0CON_T0CAPSEL_LSBPOS 12
#define T0CON_T0CAPSEL_BIT_PATTERN(x) (((x) & T0CON_T0CAPSEL_MASK) << T0CON_T0CAPSEL_LSBPOS)
//
// <11> Reserved bit.
//
// <10:9> T0CLKSEL : Clock select.
#define T0CON_T0CLKSEL_32768HZ        0x00000000 // [00] = 32.768 kHz.
#define T0CON_T0CLKSEL_10240KHZ_DIV_CD 0x00000200 // [01] = 10.24 MHz/CD.
#define T0CON_T0CLKSEL_10240KHZ        0x00000400 // [10] = 10.24 MHz.
#define T0CON_T0CLKSEL_P1_0           0x00000600 // [11] = P1.0.
//
// <8> T0DIR : Count up.
#define T0CON_T0DIR_UP   0x00000100 // Set by user for Timer0 to count up.
#define T0CON_T0DIR_DOWN 0x00000000 // Cleared by user for Timer0 to count down (default).
//
// <7> T0EN : Timer0 enable bit.
#define T0CON_T0EN_ENABLED  0x00000080 // Set by user to enable Timer0.
#define T0CON_T0EN_DISABLED 0x00000000 // Cleared by user to disable Timer0 (default).
//
// <6> T0MOD : Timer0 mode.
#define T0CON_T0MOD_PERIODIC 0x00000040 // Set by user to operate in periodic mode.
#define T0CON_T0MOD_FREE_RUN 0x00000000 // Cleared by user to operate in free running mode (default).
//
// <5:4> T0FORMAT : Format.
#define T0CON_T0FORMAT_BINARY   0x00000000 // [00] = binary (default).
#define T0CON_T0FORMAT_RESERVED 0x00000010 // [01] = reserved.
#define T0CON_T0FORMAT_HHMSH    0x00000020 // [10] = hours:minutes:seconds:hundredths (23 hours to 0 hours).
#define T0CON_T0FORMAT_HHHMSH   0x00000030 // [11] = hours:minutes:seconds:hundredths (255 hours to 0 hours).
//
// <3:0> T0SCALE : Prescaler.
#define T0CON_T0SCALE_DIV_1     0x00000000   // [0000] = source clock/1 (default).
#define T0CON_T0SCALE_DIV_16    0x00000004   // [0100] = source clock/16.
#define T0CON_T0SCALE_DIV_256   0x00000008   // [1000] = source clock/256.
#define T0CON_T0SCALE_DIV_32768 0x0000000F   // [1111] = source clock/32,768. Note that all other values are undefined.

// T1CON
//
// <15:11> Reserved.
//
// <10:9> T1CLKSEL : Clock source select.
#define T1CON_T1CLKSEL_32768HZ        0x0000 // [00] = 32.768 kHz oscillator.
#define T1CON_T1CLKSEL_10240KHZ_DIV_CD 0x0200 // [01] = 10.24 MHz/CD.
#define T1CON_T1CLKSEL_XTALI          0x0400 // [10] = XTALI.
#define T1CON_T1CLKSEL_10240KHZ        0x0600 // [11] = 10.24 MHz.
//
// <8> T1DIR : Count up.
#define T1CON_T1DIR_UP                0x0100 // Set by user for Timer1 to count up.
#define T1CON_T1DIR_DOWN              0x0000 // Cleared by user for Timer1 to count down (default).
//
// <7> T1EN : Timer1 enable bit.
#define T1CON_T1EN_ENABLED            0x0080 // Set by user to enable Timer1.
#define T1CON_T1EN_DISABLED           0x0000 // Cleared by user to disable Timer1 (default).
//
// <6> T1MOD : Timer1 mode.
#define T1CON_T1MOD_PERIODIC          0x0040 // Set by user to operate in periodic mode.
#define T1CON_T1MOD_FREE_RUN          0x0000 // Cleared by user to operate in free running mode (default).
//
// <5:4> T1FORMAT : Format
#define T1CON_T1FORMAT_BINARY         0x0000 // [00] = binary (default).
#define T1CON_T1FORMAT_RESERVED       0x0010 // [01] = reserved.
#define T1CON_T1FORMAT_HHMSH          0x0020 // [10] = hours:minutes:seconds:hundredths (23 hours to 0 hours). This is only valid with a 32 kHz clock.
#define T1CON_T1FORMAT_HHHMSH         0x0030 // [11] = hours:minutes:seconds:hundredths (255 hours to 0 hours). This is only valid with a 32 kHz clock.
//
// <3:0> T1SCALE : Prescaler
#define T1CON_T1SCALE_DIV_1           0x0000 // [0000] = source clock/1 (default).
#define T1CON_T1SCALE_DIV_16          0x0004 // [0100] = source clock/16.
#define T1CON_T1SCALE_DIV_256         0x0008 // [1000] = source clock/256. This setting should be used with the formats HHMSH and HHHMSH
#define T1CON_T1SCALE_DIV_32768       0x000F // [1111] = source clock/32,768

//T2CON
//
// <15:9> Reserved. These bits are reserved and should be written as 0 by user code.
//
// <8> T2DIR : Count up/count down enable.
#define T2CON_T2DIR_UP   0x0100 // Set by user code to configure Timer2 to count up.
#define T2CON_T2DIR_DOWN 0x0000 // Cleared by user code to configure Timer2 to count down.
//
// <7> T2EN : Timer2 enable.
#define T2CON_T2EN_ENABLED  0x0080 // Set by user code to enable Timer2.
#define T2CON_T2EN_DISABLED 0x0000 // Cleared by user code to disable Timer2.
//
// <6> T2MOD : Timer2 operating mode.
#define T2CON_T2MOD_PERIODIC 0x0040 // Set by user code to configure Timer2 to operate in periodic mode.
#define T2CON_T2MOD_FREE_RUN 0x0000 // // Cleared by user to configure Timer2 to operate in free running mode.
//
// <5> WDOGMDEN : Watchdog timer mode enable.
#define T2CON_WDOGMDEN_ENABLED  0x0020 // Set by user code to enable watchdog mode.
#define T2CON_WDOGMDEN_DISABLED 0x0000 // Cleared by user code to disable watchdog mode.
//
// <4> Reserved. This bit is reserved and should be written as 0 by user code.
//
// <3:2> T2SCALE : Timer2 clock (32.768 kHz) prescaler.
#define T2CON_T2SCALE_32768HZ  0x0000 // 00 = 32.768 kHz (default).
#define T2CON_T2SCALE_DIV_16   0x0004 // 01 = source clock/16.
#define T2CON_T2SCALE_DIV_256  0x0008 // 10 = source clock/256.
#define T2CON_T2SCALE_RESERVED 0x000C // 11 = reserved.
//
// <1> WDOGENI : Watchdog timer IRQ enable.
#define T2CON_WDOGENI_INTERRUPT 0x0002 // Set by user code to produce an IRQ instead of a reset when the watchdog reaches 0.
#define T2CON_WDOGENI_RESET     0x0000 // Cleared by user code to disable the IRQ option.
//
// <0> T2PDOFF : Stop Timer2 when power-down is enabled.
#define T2CON_T2PDOFF_POWERDOWN  0x0001 // Set by user code to stop Timer2 when the peripherals are powered down using Bit 4 in the POWCON0 MMR.
#define T2CON_T2PDOFF_KEEP_ON    0x0000 // Cleared by user code to enable Timer2 when the peripherals are powered down using Bit 4 in the POWCON0 MMR.

//T3CON
//
// <31:18> Reserved
//
// <17> T3CAPEN : Event enable bit
#define T3CON_T3CAPEN_ENABLE  0x00020000 // Set by user to enable time capture of an event.
#define T3CON_T3CAPEN_DISABLE 0x00000000 // Cleared by user to disable time capture of an event.
//
// <16:12> T3CAPSEL : Event select range, 0 to 17. The events are described in Table 78.
#define T3CON_T3CAPSEL_MASK 0x1F
#define T3CON_T3CAPSEL_LSBPOS 12
#define T3CON_T3CAPSEL_BIT_PATTERN(x) (((x) & T3CON_T3CAPSEL_MASK) << T3CON_T3CAPSEL_LSBPOS)
//
// <11> Reserved
//
// <10:9> T3CLKSEL : Clock select
#define T3CON_T3CLKSEL_32768HZ        0x00000000 // [00] = 32.768 kHz oscillator.
#define T3CON_T3CLKSEL_10240KHZ_DIV_CD 0x00000200 // [01] = 10.24 MHz/CD
#define T3CON_T3CLKSEL_10240KHZ        0x00000400 // [10] = 10.24 MHz.
#define T3CON_T3CLKSEL_RESERVED        0x00000600 // [11] = reserved.
//
// <8> T3DIR : Count up
#define T3CON_T3DIR_UP                0x00000100 // Set by user for Timer3 to count up.
#define T3CON_T3DIR_DOWN              0x00000000 // Cleared by user for Timer3 to count down (default).
//
// <7> T3EN : Timer3 enable bit
#define T3CON_T3EN_ENABLE             0x00000080 // Set by user to enable Timer3.
#define T3CON_T3EN_DISABLE            0x00000000 // Cleared by user to disable Timer3 (default).
//
// <6> T3MOD : Timer3 mode
#define T3CON_T3MOD_PERIODIC          0x00000040 // Set by user to operate in periodic mode.
#define T3CON_T3MOD_FREE_RUN          0x00000000 // Cleared by user to operate in free running mode (default mode)
//
// <5:4> Reserved
//
// <3:0> T3SCALE : Prescaler
#define T3CON_T3SCALE_DIV_1           0x00000000 // [0000] = source clock/1 (default).
#define T3CON_T3SCALE_DIV_16          0x00000004 // [0100] = source clock/16.
#define T3CON_T3SCALE_DIV_256         0x00000008 // [1000] = source clock/256.
#define T3CON_T3SCALE_DIV_32768       0x0000000F // [1111] = source clock/32,768.




#endif
