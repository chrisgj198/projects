EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:chrisj1
LIBS:chrisj_power
LIBS:eload_prog-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Constant Voltage Electronic Load for PV / IV Curve Tracer"
Date "24 jun 2015"
Rev "0"
Comp "ANU"
Comment1 "licenced under CERN OHL V1.2 or later"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CHRISJ1_R R5
U 1 1 540EBE35
P 5400 2900
F 0 "R5" V 5250 2900 50  0000 C CNN
F 1 "330" V 5320 2900 50  0000 C CNN
F 2 "SM0603_Resistor" H 5400 2900 60  0001 C CNN
F 3 "~" H 5400 2900 60  0000 C CNN
	1    5400 2900
	0    1    1    0   
$EndComp
Text Label 5450 2200 0    60   ~ 0
RS485_RXD
Text Label 6150 2900 0    60   ~ 0
RS485_TXD
$Comp
L CHRISJ1_R R6
U 1 1 540ED2B3
P 6100 2200
F 0 "R6" V 5950 2200 50  0000 C CNN
F 1 "10k" V 6020 2200 50  0000 C CNN
F 2 "SM0603_Resistor" H 6100 2200 60  0001 C CNN
F 3 "~" H 6100 2200 60  0000 C CNN
	1    6100 2200
	0    1    1    0   
$EndComp
Text Notes 3750 1700 0    60   ~ 0
Run LEDs at approx 3.2mA
$Comp
L CHRISJ1_H11L1M U2
U 1 1 540F0ECD
P 4950 3000
F 0 "U2" H 4825 3200 40  0000 C CNN
F 1 "H11L1M" H 5000 2775 40  0000 C CNN
F 2 "DIP-6__300" H 5050 3200 29  0001 C CNN
F 3 "" H 4925 3000 60  0000 C CNN
	1    4950 3000
	-1   0    0    -1  
$EndComp
Text Notes 5450 2300 0    60   ~ 0
idle state high
Text Notes 6150 3000 0    60   ~ 0
idle state high
Text Notes 3750 1800 0    60   ~ 0
LEDs are off in idle state, and output of optoisolator is high
$Comp
L CHRISJ1_R R4
U 1 1 540F492D
P 3750 2200
F 0 "R4" V 3850 2200 50  0000 C CNN
F 1 "1k" V 3700 2300 50  0000 C CNN
F 2 "SM0603_Resistor" H 3750 2200 60  0001 C CNN
F 3 "~" H 3750 2200 60  0000 C CNN
	1    3750 2200
	0    1    1    0   
$EndComp
$Comp
L CHRISJ1_C C1
U 1 1 540F5D7A
P 4250 2900
F 0 "C1" H 4350 2800 50  0000 C CNN
F 1 "100nF" H 4400 2700 50  0000 C CNN
F 2 "c_0402" H 4250 2900 60  0001 C CNN
F 3 "~" H 4250 2900 60  0000 C CNN
	1    4250 2900
	-1   0    0    -1  
$EndComp
$Comp
L CHRISJ1_R R3
U 1 1 540F65A8
P 3700 3350
F 0 "R3" V 3760 3310 50  0000 C CNN
F 1 "3k3" V 3825 3330 50  0000 C CNN
F 2 "SM0603_Resistor" H 3700 3350 60  0001 C CNN
F 3 "~" H 3700 3350 60  0000 C CNN
	1    3700 3350
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_74HC1G04 U3
U 1 1 540F7883
P 5900 2900
F 0 "U3" H 5750 2750 60  0000 C CNN
F 1 "74HC1G14" H 5550 2650 60  0000 C CNN
F 2 "chrisj1_SOT353" H 5900 2900 60  0001 C CNN
F 3 "~" H 5900 2900 60  0000 C CNN
	1    5900 2900
	-1   0    0    -1  
$EndComp
$Comp
L DVDD #PWR01
U 1 1 54177E11
P 6300 2100
F 0 "#PWR01" H 6300 2050 20  0001 C CNN
F 1 "DVDD" H 6300 2200 50  0000 C CNN
F 2 "~" H 6300 2100 60  0000 C CNN
F 3 "~" H 6300 2100 60  0000 C CNN
	1    6300 2100
	1    0    0    -1  
$EndComp
Text Label 9900 2650 2    60   ~ 0
TDO
Text Label 9900 2750 2    60   ~ 0
TDI
Text Label 9900 2850 2    60   ~ 0
TCK
Text Label 9900 2450 2    60   ~ 0
P2_1
Text Label 9900 1950 2    60   ~ 0
P0_4
Text Label 9900 2050 2    60   ~ 0
EXTCLK
Text Label 9900 3050 2    60   ~ 0
TMS
Text Label 9900 2550 2    60   ~ 0
~BM
Text Label 9900 2950 2    60   ~ 0
~RST
$Comp
L DVDD #PWR02
U 1 1 5434F87E
P 9450 1800
F 0 "#PWR02" H 9450 1750 20  0001 C CNN
F 1 "DVDD" H 9450 1900 50  0000 C CNN
F 2 "~" H 9450 1800 60  0000 C CNN
F 3 "~" H 9450 1800 60  0000 C CNN
	1    9450 1800
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR03
U 1 1 5434F884
P 9450 1500
F 0 "#PWR03" H 9450 1550 20  0001 C CNN
F 1 "VSS" H 9450 1400 50  0000 C CNN
F 2 "~" H 9450 1500 60  0000 C CNN
F 3 "~" H 9450 1500 60  0000 C CNN
	1    9450 1500
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_CONN_16 P4
U 1 1 5434F8E0
P 10350 2500
F 0 "P4" H 10150 1650 60  0000 C CNN
F 1 "DEBUG" H 10100 1550 60  0000 C CNN
F 2 "pin_array_8x2" H 10350 2300 60  0001 C CNN
F 3 "~" H 10350 2300 60  0000 C CNN
	1    10350 2500
	1    0    0    -1  
$EndComp
NoConn ~ 5250 3100
Text Label 9900 3150 2    60   ~ 0
RS485_RXD
Text Label 9900 3250 2    60   ~ 0
RS485_TXD
Text Label 9900 2150 2    60   ~ 0
P1_4
Text Label 9900 2250 2    60   ~ 0
P1_5
Text Label 9900 2350 2    60   ~ 0
P1_6
$Comp
L DVDD #PWR04
U 1 1 5572EFAA
P 5850 2750
F 0 "#PWR04" H 5850 2700 20  0001 C CNN
F 1 "DVDD" H 5850 2850 50  0000 C CNN
F 2 "~" H 5850 2750 60  0000 C CNN
F 3 "~" H 5850 2750 60  0000 C CNN
	1    5850 2750
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR05
U 1 1 5572F110
P 5850 3150
F 0 "#PWR05" H 5850 3200 20  0001 C CNN
F 1 "VSS" H 5850 3050 50  0000 C CNN
F 2 "~" H 5850 3150 60  0000 C CNN
F 3 "~" H 5850 3150 60  0000 C CNN
	1    5850 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 2900 6400 2900
Wire Wire Line
	5250 2200 6000 2200
Wire Wire Line
	6200 2200 6300 2200
Wire Wire Line
	6300 2200 6300 2100
Wire Wire Line
	9900 1950 10000 1950
Wire Wire Line
	9900 2050 10000 2050
Wire Wire Line
	10000 2250 9900 2250
Wire Wire Line
	10000 2350 9900 2350
Wire Wire Line
	10000 2450 9900 2450
Wire Wire Line
	10000 2550 9900 2550
Wire Wire Line
	10000 2650 9900 2650
Wire Wire Line
	10000 2750 9900 2750
Wire Wire Line
	10000 2850 9900 2850
Wire Wire Line
	10000 2950 9900 2950
Wire Wire Line
	10000 3050 9900 3050
Wire Wire Line
	9900 2150 10000 2150
Wire Wire Line
	10000 3150 9900 3150
Wire Wire Line
	10000 3250 9900 3250
Wire Wire Line
	9450 1800 9450 1850
Wire Wire Line
	9300 1850 10000 1850
Wire Wire Line
	10000 1750 9700 1750
Wire Wire Line
	9700 1750 9700 1450
Wire Wire Line
	9700 1450 9300 1450
Wire Wire Line
	9450 1450 9450 1500
Wire Wire Line
	5500 2900 5550 2900
Wire Wire Line
	5250 2900 5300 2900
Wire Wire Line
	5250 3000 5600 3000
Wire Wire Line
	5600 3000 5600 3100
Wire Wire Line
	5600 3100 5850 3100
Wire Wire Line
	5850 3050 5850 3150
Connection ~ 5850 3100
$Comp
L PC817 U1
U 1 1 557304D2
P 4900 2300
F 0 "U1" H 4687 2478 40  0000 C CNN
F 1 "PC817" H 5050 2115 40  0000 C CNN
F 2 "DIP4" H 4736 2125 29  0000 C CNN
F 3 "" H 4900 2300 60  0000 C CNN
	1    4900 2300
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR06
U 1 1 557305E4
P 5350 2450
F 0 "#PWR06" H 5350 2500 20  0001 C CNN
F 1 "VSS" H 5350 2350 50  0000 C CNN
F 2 "~" H 5350 2450 60  0000 C CNN
F 3 "~" H 5350 2450 60  0000 C CNN
	1    5350 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 2400 5350 2400
Wire Wire Line
	5350 2400 5350 2450
Wire Wire Line
	4250 2100 4250 2850
Wire Wire Line
	4250 3000 4550 3000
Wire Wire Line
	4250 3500 4250 2950
$Comp
L CHRISJ1_R R7
U 1 1 557316CA
P 6100 2700
F 0 "R7" V 5950 2700 50  0000 C CNN
F 1 "100k" V 6020 2700 50  0000 C CNN
F 2 "SM0603_Resistor" H 6100 2700 60  0001 C CNN
F 3 "~" H 6100 2700 60  0000 C CNN
	1    6100 2700
	-1   0    0    1   
$EndComp
$Comp
L DVDD #PWR07
U 1 1 557316D5
P 6100 2550
F 0 "#PWR07" H 6100 2500 20  0001 C CNN
F 1 "DVDD" H 6100 2650 50  0000 C CNN
F 2 "~" H 6100 2550 60  0000 C CNN
F 3 "~" H 6100 2550 60  0000 C CNN
	1    6100 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 2600 6100 2550
Wire Wire Line
	6100 2900 6100 2800
Connection ~ 6100 2900
$Comp
L CHRISJ1_CONN_6 P1
U 1 1 557317C8
P 1950 2750
F 0 "P1" H 2050 2350 60  0000 C CNN
F 1 "TTL-232R-3V3" H 1750 2250 60  0000 C CNN
F 2 "~" H 1950 2750 60  0000 C CNN
F 3 "~" H 1950 2750 60  0000 C CNN
	1    1950 2750
	-1   0    0    -1  
$EndComp
Text Label 2350 2600 0    60   ~ 0
CTS
Text Label 2350 2900 0    60   ~ 0
RXD_PC
Text Label 2350 3000 0    60   ~ 0
RTS
Wire Wire Line
	2300 2600 2800 2600
Wire Wire Line
	2800 2600 2800 3000
Wire Wire Line
	2800 3000 2300 3000
Text Label 2350 2500 0    60   ~ 0
GND_PC
Text Label 2350 2700 0    60   ~ 0
5V_PC
Wire Wire Line
	2900 3500 4450 3500
Wire Wire Line
	2900 3500 2900 2500
Wire Wire Line
	2900 2500 2300 2500
Connection ~ 4250 3000
Wire Wire Line
	3000 2700 2300 2700
Wire Wire Line
	3600 2100 3600 2200
Wire Wire Line
	3000 2100 4250 2100
Wire Wire Line
	4550 2900 4350 2900
Wire Wire Line
	4350 2900 4350 2800
Wire Wire Line
	4350 2800 4250 2800
Connection ~ 4250 2800
$Comp
L CHRISJ1_R R2
U 1 1 5573199C
P 3550 3050
F 0 "R2" V 3610 3010 50  0000 C CNN
F 1 "1k8" V 3675 3030 50  0000 C CNN
F 2 "SM0603_Resistor" H 3550 3050 60  0001 C CNN
F 3 "~" H 3550 3050 60  0000 C CNN
	1    3550 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 3500 3700 3450
Connection ~ 3700 3500
Wire Wire Line
	4550 3100 4350 3100
Wire Wire Line
	4350 3100 4350 3200
Wire Wire Line
	4350 3200 2700 3200
Wire Wire Line
	2700 3200 2700 2900
Wire Wire Line
	2700 2900 2300 2900
Wire Wire Line
	3700 3250 3700 3200
Connection ~ 3700 3200
Wire Wire Line
	3600 2200 3650 2200
Wire Wire Line
	3850 2200 4550 2200
Wire Wire Line
	4450 3500 4450 2400
Wire Wire Line
	4450 2400 4550 2400
Connection ~ 4250 3500
$Comp
L CHRISJ1_2N7002 Q1
U 1 1 5572E9DB
P 3800 2800
F 0 "Q1" H 3850 3000 60  0000 R CNN
F 1 "BSS138LT1G" H 4250 3100 60  0000 R CNN
F 2 "~" H 3800 2800 60  0000 C CNN
F 3 "~" H 3800 2800 60  0000 C CNN
	1    3800 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2200 3900 2650
Connection ~ 3900 2200
Wire Wire Line
	3900 2950 3900 3500
Connection ~ 3900 3500
$Comp
L CHRISJ1_R R1
U 1 1 5572EB30
P 3050 2800
F 0 "R1" V 3150 2800 50  0000 C CNN
F 1 "1k" V 3250 2800 50  0000 C CNN
F 2 "SM0603_Resistor" H 3050 2800 60  0001 C CNN
F 3 "~" H 3050 2800 60  0000 C CNN
	1    3050 2800
	0    1    1    0   
$EndComp
Text Label 2350 2800 0    60   ~ 0
TXD_PC
Wire Wire Line
	2950 2800 2300 2800
Wire Wire Line
	3150 2800 3700 2800
Wire Wire Line
	3000 2700 3000 2100
Connection ~ 3600 2100
$Comp
L CHRISJ1_BAV99 D2
U 1 1 5572EC94
P 3350 2650
F 0 "D2" H 3350 2450 40  0000 C CNN
F 1 "BAV99" H 3350 2500 40  0000 C CNN
F 2 "~" H 3320 2700 60  0000 C CNN
F 3 "~" H 3320 2700 60  0000 C CNN
	1    3350 2650
	1    0    0    1   
$EndComp
Wire Wire Line
	3450 2650 3500 2650
Wire Wire Line
	3500 2650 3500 2800
Connection ~ 3500 2800
Wire Wire Line
	3100 2700 3250 2700
Wire Wire Line
	3200 2700 3200 3500
Connection ~ 3200 3500
Wire Wire Line
	3200 2600 3250 2600
Wire Wire Line
	3200 2100 3200 2600
Connection ~ 3200 2100
$Comp
L CHRISJ1_ZENER2 D1
U 1 1 5572EBEE
P 3100 2550
F 0 "D1" V 3400 2800 50  0000 C CNN
F 1 "SMBJ5.0" V 3300 2850 50  0000 C CNN
F 2 "~" H 3100 2550 60  0000 C CNN
F 3 "~" H 3100 2550 60  0000 C CNN
	1    3100 2550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3100 2650 3100 2700
Connection ~ 3200 2700
Wire Wire Line
	3100 2450 3100 2400
Wire Wire Line
	3100 2400 3200 2400
Connection ~ 3200 2400
Text Notes 2000 3700 0    60   ~ 0
Need to provide full-duplex serial adapter for\nprogramming, because bootloader of\nADuC7060 does not enable/disable RS485\nTX driver, so RS485 can't be used.
$Comp
L CHRISJ1_PUSHBUTTON SW1
U 1 1 5572EDCF
P 5500 3900
F 0 "SW1" H 5650 3950 50  0000 C CNN
F 1 "RESET" H 5700 3850 50  0000 C CNN
F 2 "~" H 5500 3900 60  0000 C CNN
F 3 "~" H 5500 3900 60  0000 C CNN
	1    5500 3900
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_PUSHBUTTON SW2
U 1 1 5572EDEB
P 6200 3900
F 0 "SW2" H 6350 3950 50  0000 C CNN
F 1 "BM" H 6350 3850 50  0000 C CNN
F 2 "~" H 6200 3900 60  0000 C CNN
F 3 "~" H 6200 3900 60  0000 C CNN
	1    6200 3900
	1    0    0    -1  
$EndComp
Text Label 5400 3650 2    60   ~ 0
~RST
Text Label 6150 3650 2    60   ~ 0
~BM
Wire Wire Line
	5400 3650 5500 3650
Wire Wire Line
	5500 3650 5500 3750
Wire Wire Line
	6150 3650 6200 3650
Wire Wire Line
	6200 3650 6200 3750
$Comp
L VSS #PWR08
U 1 1 5572EEBE
P 5500 4150
F 0 "#PWR08" H 5500 4200 20  0001 C CNN
F 1 "VSS" H 5500 4050 50  0000 C CNN
F 2 "~" H 5500 4150 60  0000 C CNN
F 3 "~" H 5500 4150 60  0000 C CNN
	1    5500 4150
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR09
U 1 1 5572EEC4
P 6200 4150
F 0 "#PWR09" H 6200 4200 20  0001 C CNN
F 1 "VSS" H 6200 4050 50  0000 C CNN
F 2 "~" H 6200 4150 60  0000 C CNN
F 3 "~" H 6200 4150 60  0000 C CNN
	1    6200 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 4150 5500 4050
Wire Wire Line
	6200 4150 6200 4050
$Comp
L CHRISJ1_LED D0_4
U 1 1 5572EF8F
P 7450 3900
F 0 "D0_4" H 7450 4000 50  0000 C CNN
F 1 "LED" H 7450 3800 50  0000 C CNN
F 2 "~" H 7450 3900 60  0000 C CNN
F 3 "~" H 7450 3900 60  0000 C CNN
	1    7450 3900
	0    1    1    0   
$EndComp
$Comp
L CHRISJ1_LED D1_4
U 1 1 5572EFA1
P 7750 3900
F 0 "D1_4" H 7750 4000 50  0000 C CNN
F 1 "LED" H 7750 3800 50  0000 C CNN
F 2 "~" H 7750 3900 60  0000 C CNN
F 3 "~" H 7750 3900 60  0000 C CNN
	1    7750 3900
	0    1    1    0   
$EndComp
$Comp
L CHRISJ1_LED D1_5
U 1 1 5572EFA7
P 8050 3900
F 0 "D1_5" H 8050 4000 50  0000 C CNN
F 1 "LED" H 8050 3800 50  0000 C CNN
F 2 "~" H 8050 3900 60  0000 C CNN
F 3 "~" H 8050 3900 60  0000 C CNN
	1    8050 3900
	0    1    1    0   
$EndComp
$Comp
L CHRISJ1_LED D1_6
U 1 1 5572EFAD
P 8350 3900
F 0 "D1_6" H 8350 4000 50  0000 C CNN
F 1 "LED" H 8350 3800 50  0000 C CNN
F 2 "~" H 8350 3900 60  0000 C CNN
F 3 "~" H 8350 3900 60  0000 C CNN
	1    8350 3900
	0    1    1    0   
$EndComp
$Comp
L CHRISJ1_LED D2_1
U 1 1 5572EFB3
P 8650 3900
F 0 "D2_1" H 8650 4000 50  0000 C CNN
F 1 "LED" H 8650 3800 50  0000 C CNN
F 2 "~" H 8650 3900 60  0000 C CNN
F 3 "~" H 8650 3900 60  0000 C CNN
	1    8650 3900
	0    1    1    0   
$EndComp
Text Label 7400 3300 2    60   ~ 0
P0_4
Text Label 7700 3300 2    60   ~ 0
P1_4
Text Label 8000 3300 2    60   ~ 0
P1_5
Text Label 8300 3300 2    60   ~ 0
P1_6
Text Label 8600 3300 2    60   ~ 0
P2_1
$Comp
L CHRISJ1_R R_0_4
U 1 1 5572EFC0
P 7450 3550
F 0 "R_0_4" V 7300 3550 50  0000 C CNN
F 1 "330" V 7370 3550 50  0000 C CNN
F 2 "SM0603_Resistor" H 7450 3550 60  0001 C CNN
F 3 "~" H 7450 3550 60  0000 C CNN
	1    7450 3550
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_R R_1_4
U 1 1 5572EFC6
P 7750 3550
F 0 "R_1_4" V 7600 3550 50  0000 C CNN
F 1 "330" V 7670 3550 50  0000 C CNN
F 2 "SM0603_Resistor" H 7750 3550 60  0001 C CNN
F 3 "~" H 7750 3550 60  0000 C CNN
	1    7750 3550
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_R R_1_5
U 1 1 5572EFCC
P 8050 3550
F 0 "R_1_5" V 7900 3550 50  0000 C CNN
F 1 "330" V 7970 3550 50  0000 C CNN
F 2 "SM0603_Resistor" H 8050 3550 60  0001 C CNN
F 3 "~" H 8050 3550 60  0000 C CNN
	1    8050 3550
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_R R_1_6
U 1 1 5572EFD2
P 8350 3550
F 0 "R_1_6" V 8200 3550 50  0000 C CNN
F 1 "330" V 8270 3550 50  0000 C CNN
F 2 "SM0603_Resistor" H 8350 3550 60  0001 C CNN
F 3 "~" H 8350 3550 60  0000 C CNN
	1    8350 3550
	1    0    0    -1  
$EndComp
$Comp
L CHRISJ1_R R_2_1
U 1 1 5572EFD8
P 8650 3550
F 0 "R_2_1" V 8500 3550 50  0000 C CNN
F 1 "330" V 8570 3550 50  0000 C CNN
F 2 "SM0603_Resistor" H 8650 3550 60  0001 C CNN
F 3 "~" H 8650 3550 60  0000 C CNN
	1    8650 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3300 7400 3300
Wire Wire Line
	7450 2800 7450 3450
Wire Wire Line
	7750 3300 7700 3300
Wire Wire Line
	7750 2800 7750 3450
Wire Wire Line
	8000 3300 8050 3300
Wire Wire Line
	8050 3050 8050 3450
Wire Wire Line
	8300 3300 8350 3300
Wire Wire Line
	8350 3000 8350 3450
Wire Wire Line
	8600 3300 8650 3300
Wire Wire Line
	8650 2950 8650 3450
Wire Wire Line
	8650 3700 8650 3650
Wire Wire Line
	8350 3700 8350 3650
Wire Wire Line
	8050 3700 8050 3650
Wire Wire Line
	7750 3700 7750 3650
Wire Wire Line
	7450 3700 7450 3650
$Comp
L VSS #PWR010
U 1 1 5572F4E3
P 7450 4150
F 0 "#PWR010" H 7450 4200 20  0001 C CNN
F 1 "VSS" H 7450 4050 50  0000 C CNN
F 2 "~" H 7450 4150 60  0000 C CNN
F 3 "~" H 7450 4150 60  0000 C CNN
	1    7450 4150
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR011
U 1 1 5572F4E9
P 7750 4150
F 0 "#PWR011" H 7750 4200 20  0001 C CNN
F 1 "VSS" H 7750 4050 50  0000 C CNN
F 2 "~" H 7750 4150 60  0000 C CNN
F 3 "~" H 7750 4150 60  0000 C CNN
	1    7750 4150
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR012
U 1 1 5572F4EF
P 8050 4150
F 0 "#PWR012" H 8050 4200 20  0001 C CNN
F 1 "VSS" H 8050 4050 50  0000 C CNN
F 2 "~" H 8050 4150 60  0000 C CNN
F 3 "~" H 8050 4150 60  0000 C CNN
	1    8050 4150
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR013
U 1 1 5572F4F5
P 8350 4150
F 0 "#PWR013" H 8350 4200 20  0001 C CNN
F 1 "VSS" H 8350 4050 50  0000 C CNN
F 2 "~" H 8350 4150 60  0000 C CNN
F 3 "~" H 8350 4150 60  0000 C CNN
	1    8350 4150
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR014
U 1 1 5572F4FB
P 8650 4150
F 0 "#PWR014" H 8650 4200 20  0001 C CNN
F 1 "VSS" H 8650 4050 50  0000 C CNN
F 2 "~" H 8650 4150 60  0000 C CNN
F 3 "~" H 8650 4150 60  0000 C CNN
	1    8650 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 4150 7450 4100
Wire Wire Line
	7750 4150 7750 4100
Wire Wire Line
	8050 4150 8050 4100
Wire Wire Line
	8350 4150 8350 4100
Wire Wire Line
	8650 4150 8650 4100
Connection ~ 7450 3300
Connection ~ 7750 3300
Connection ~ 8050 3300
Connection ~ 8350 3300
Connection ~ 8650 3300
$Comp
L VSS #PWR015
U 1 1 5572FA3F
P 8950 2950
F 0 "#PWR015" H 8950 3000 20  0001 C CNN
F 1 "VSS" H 8950 2850 50  0000 C CNN
F 2 "~" H 8950 2950 60  0000 C CNN
F 3 "~" H 8950 2950 60  0000 C CNN
	1    8950 2950
	1    0    0    -1  
$EndComp
Text Notes 7450 4400 0    60   ~ 0
Be very careful if attaching a grounded oscilloscope\nto this board! This microcontroller VSS connection is\nNOT at the same voltage as the analogue ground!\nIt is also not at the voltage of the PV negative terminal!\nDamage could result if the PV and oscilloscope are\nboth connected at the same time, unless one is floating.
Text Notes 7100 1850 0    60   ~ 0
LEDs for debugging\nand probe points to allow software timing\nto be monitored with an oscilloscope
Text Notes 4800 4400 0    60   ~ 0
To enter bootloader:\nHold down BM and pulse Reset\nThen click START in ARMWSD program on PC
$Comp
L PWR_FLAG #FLG016
U 1 1 557A3974
P 9300 1450
F 0 "#FLG016" H 9300 1545 30  0001 C CNN
F 1 "PWR_FLAG" H 9300 1630 30  0000 C CNN
F 2 "" H 9300 1450 60  0000 C CNN
F 3 "" H 9300 1450 60  0000 C CNN
	1    9300 1450
	0    -1   -1   0   
$EndComp
$Comp
L PWR_FLAG #FLG017
U 1 1 557A3983
P 9300 1850
F 0 "#FLG017" H 9300 1945 30  0001 C CNN
F 1 "PWR_FLAG" H 9300 2030 30  0000 C CNN
F 2 "" H 9300 1850 60  0000 C CNN
F 3 "" H 9300 1850 60  0000 C CNN
	1    9300 1850
	0    -1   -1   0   
$EndComp
Connection ~ 9450 1450
Connection ~ 9450 1850
NoConn ~ 9900 2050
NoConn ~ 9900 2650
NoConn ~ 9900 2750
NoConn ~ 9900 2850
NoConn ~ 9900 3050
$Comp
L CHRISJ1_CONN_16 P2
U 1 1 557A43B8
P 8200 2450
F 0 "P2" V 8500 3100 60  0000 C CNN
F 1 "PROBE_POINTS" V 8400 2850 60  0000 C CNN
F 2 "~" H 8200 2250 60  0000 C CNN
F 3 "~" H 8200 2250 60  0000 C CNN
	1    8200 2450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7550 2800 7550 2850
Wire Wire Line
	7550 2850 7450 2850
Connection ~ 7450 2850
Wire Wire Line
	7650 2800 7650 2850
Wire Wire Line
	7650 2850 7750 2850
Connection ~ 7750 2850
Wire Wire Line
	7850 2800 7850 2850
Wire Wire Line
	7850 2850 7950 2850
Wire Wire Line
	7950 2800 7950 3050
Wire Wire Line
	7950 3050 8050 3050
Connection ~ 7950 2850
Wire Wire Line
	8050 2800 8050 2850
Wire Wire Line
	8050 2850 8150 2850
Wire Wire Line
	8150 2800 8150 3000
Wire Wire Line
	8150 3000 8350 3000
Connection ~ 8150 2850
Wire Wire Line
	8250 2800 8250 2850
Wire Wire Line
	8250 2850 8350 2850
Wire Wire Line
	8350 2800 8350 2950
Wire Wire Line
	8350 2950 8650 2950
Connection ~ 8350 2850
Wire Wire Line
	8450 2800 8450 2850
Wire Wire Line
	8450 2850 8950 2850
Wire Wire Line
	8950 2800 8950 2950
Wire Wire Line
	8550 2800 8550 2850
Connection ~ 8550 2850
Wire Wire Line
	8650 2800 8650 2850
Connection ~ 8650 2850
Wire Wire Line
	8750 2800 8750 2850
Connection ~ 8750 2850
Wire Wire Line
	8850 2800 8850 2850
Connection ~ 8850 2850
Connection ~ 8950 2850
Wire Wire Line
	3550 2950 3550 2100
Connection ~ 3550 2100
Wire Wire Line
	3550 3150 3550 3200
Connection ~ 3550 3200
$Comp
L CHRISJ1_C C2
U 1 1 557A836C
P 9600 1650
F 0 "C2" H 9450 1700 50  0000 C CNN
F 1 "100nF" H 9350 1650 50  0000 C CNN
F 2 "c_0402" H 9600 1650 60  0001 C CNN
F 3 "~" H 9600 1650 60  0000 C CNN
	1    9600 1650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9600 1700 9600 1850
Connection ~ 9600 1850
Wire Wire Line
	9600 1600 9600 1450
Connection ~ 9600 1450
$EndSCHEMATC
