EESchema Schematic File Version 4
LIBS:phone_supply-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C2
U 1 1 5F491690
P 4250 3500
F 0 "C2" H 4365 3546 50  0000 L CNN
F 1 "1uF" H 4365 3455 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7.2mm_W5.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 4288 3350 50  0001 C CNN
F 3 "~" H 4250 3500 50  0001 C CNN
	1    4250 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R2
U 1 1 5F491966
P 4000 2200
F 0 "R2" H 4068 2246 50  0000 L CNN
F 1 "56" H 4068 2155 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4040 2190 50  0001 C CNN
F 3 "~" H 4000 2200 50  0001 C CNN
	1    4000 2200
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC557 Q2
U 1 1 5F4922FD
P 6400 2700
F 0 "Q2" H 6591 2746 50  0000 L CNN
F 1 "BC557" H 6591 2655 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 6600 2625 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC557.pdf" H 6400 2700 50  0001 L CNN
	1    6400 2700
	1    0    0    1   
$EndComp
$Comp
L Diode:1N4148 D3
U 1 1 5F492800
P 4500 2400
F 0 "D3" H 4500 2616 50  0000 C CNN
F 1 "1N4148" H 4500 2525 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_DO-35_SOD27_P7.62mm_Horizontal" H 4500 2225 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4500 2400 50  0001 C CNN
	1    4500 2400
	0    1    1    0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5F4932F9
P 3250 2350
F 0 "D2" H 3243 2566 50  0000 C CNN
F 1 "Green" H 3243 2475 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 3250 2350 50  0001 C CNN
F 3 "~" H 3250 2350 50  0001 C CNN
	1    3250 2350
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 5F4939CF
P 7000 3500
F 0 "J3" H 7080 3542 50  0000 L CNN
F 1 "Handset2" H 7080 3451 50  0000 L CNN
F 2 "Terminal_Blocks:TerminalBlock_bornier-3_P5.08mm" H 7000 3500 50  0001 C CNN
F 3 "~" H 7000 3500 50  0001 C CNN
	1    7000 3500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5F493FDC
P 8300 2400
F 0 "H1" H 8400 2446 50  0000 L CNN
F 1 "MountingHole" H 8400 2355 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_4mm" H 8300 2400 50  0001 C CNN
F 3 "~" H 8300 2400 50  0001 C CNN
	1    8300 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:Polyfuse F1
U 1 1 5F491BFE
P 1750 1900
F 0 "F1" V 1975 1900 50  0000 C CNN
F 1 "Polyfuse" V 1884 1900 50  0000 C CNN
F 2 "Capacitors_ThroughHole:C_Disc_D7.5mm_W4.4mm_P5.00mm" H 1800 1700 50  0001 L CNN
F 3 "~" H 1750 1900 50  0001 C CNN
	1    1750 1900
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP C1
U 1 1 5F4922A2
P 2450 2750
F 0 "C1" H 2568 2796 50  0000 L CNN
F 1 "470u 25V" H 2568 2705 50  0000 L CNN
F 2 "Capacitors_ThroughHole:CP_Radial_D10.0mm_P5.00mm" H 2488 2600 50  0001 C CNN
F 3 "~" H 2450 2750 50  0001 C CNN
	1    2450 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 5F499CAA
P 1000 2800
F 0 "J1" H 1080 2842 50  0000 L CNN
F 1 "DC In" H 1080 2751 50  0000 L CNN
F 2 "Terminal_Blocks:TerminalBlock_bornier-3_P5.08mm" H 1000 2800 50  0001 C CNN
F 3 "~" H 1000 2800 50  0001 C CNN
	1    1000 2800
	-1   0    0    -1  
$EndComp
NoConn ~ 1200 2800
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5F49E9E9
P 2550 1900
F 0 "#FLG01" H 2550 1975 50  0001 C CNN
F 1 "PWR_FLAG" H 2550 2073 50  0000 C CNN
F 2 "" H 2550 1900 50  0001 C CNN
F 3 "~" H 2550 1900 50  0001 C CNN
	1    2550 1900
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4007 D1
U 1 1 5F49F7CD
P 2150 1900
F 0 "D1" H 2150 2116 50  0000 C CNN
F 1 "1N4007" H 2150 2025 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_DO-41_SOD81_P10.16mm_Horizontal" H 2150 1725 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 2150 1900 50  0001 C CNN
	1    2150 1900
	-1   0    0    1   
$EndComp
Wire Wire Line
	1200 2700 1350 2700
Wire Wire Line
	1350 2700 1350 1900
Wire Wire Line
	1350 1900 1600 1900
Wire Wire Line
	1900 1900 2000 1900
$Comp
L power:GND #PWR01
U 1 1 5F4AA17C
P 1350 4050
F 0 "#PWR01" H 1350 3800 50  0001 C CNN
F 1 "GND" H 1355 3877 50  0000 C CNN
F 2 "" H 1350 4050 50  0001 C CNN
F 3 "" H 1350 4050 50  0001 C CNN
	1    1350 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 2900 1350 2900
Wire Wire Line
	1350 2900 1350 4000
Wire Wire Line
	1350 4000 2450 4000
Wire Wire Line
	2450 4000 2450 2900
Connection ~ 1350 4000
Wire Wire Line
	1350 4000 1350 4050
Wire Wire Line
	2450 2600 2450 1900
Wire Wire Line
	2450 1900 2300 1900
$Comp
L Device:R_US R1
U 1 1 5F4B192F
P 3250 3300
F 0 "R1" H 3318 3346 50  0000 L CNN
F 1 "680" H 3318 3255 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3290 3290 50  0001 C CNN
F 3 "~" H 3250 3300 50  0001 C CNN
	1    3250 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3450 3250 4000
Wire Wire Line
	3250 4000 2450 4000
Connection ~ 2450 4000
Wire Wire Line
	3250 3150 3250 2700
Wire Wire Line
	3250 2200 3250 1900
Wire Wire Line
	3250 1900 2550 1900
Connection ~ 2450 1900
Connection ~ 2550 1900
Wire Wire Line
	2550 1900 2450 1900
$Comp
L Transistor_BJT:BC557 Q1
U 1 1 5F4B2921
P 3900 2700
F 0 "Q1" H 4091 2746 50  0000 L CNN
F 1 "BC557" H 4091 2655 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 4100 2625 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC557.pdf" H 3900 2700 50  0001 L CNN
	1    3900 2700
	1    0    0    1   
$EndComp
Wire Wire Line
	4000 2500 4000 2350
Wire Wire Line
	4000 1900 4000 2050
Wire Wire Line
	3250 1900 4000 1900
Connection ~ 3250 1900
Wire Wire Line
	3700 2700 3250 2700
Connection ~ 3250 2700
Wire Wire Line
	3250 2700 3250 2500
Wire Wire Line
	4500 2550 4500 3050
Wire Wire Line
	4500 3050 4000 3050
Wire Wire Line
	4000 3050 4000 2900
Wire Wire Line
	4500 2250 4500 1900
Wire Wire Line
	4500 1900 4000 1900
Connection ~ 4000 1900
Wire Wire Line
	6200 2700 3700 2700
Connection ~ 3700 2700
$Comp
L Device:R_US R6
U 1 1 5F4CE24B
P 6500 2200
F 0 "R6" H 6568 2246 50  0000 L CNN
F 1 "56" H 6568 2155 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6540 2190 50  0001 C CNN
F 3 "~" H 6500 2200 50  0001 C CNN
	1    6500 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2500 6500 2350
Wire Wire Line
	6500 1900 6500 2050
Wire Wire Line
	6500 1900 4500 1900
$Comp
L Diode:1N4148 D7
U 1 1 5F4D3BE4
P 7000 2400
F 0 "D7" H 7000 2616 50  0000 C CNN
F 1 "1N4148" H 7000 2525 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_DO-35_SOD27_P7.62mm_Horizontal" H 7000 2225 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 7000 2400 50  0001 C CNN
	1    7000 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	7000 2550 7000 3050
Wire Wire Line
	7000 2250 7000 1900
Wire Wire Line
	7000 3050 6500 3050
Wire Wire Line
	6500 3050 6500 2900
Wire Wire Line
	6500 1900 7000 1900
Connection ~ 6500 1900
Connection ~ 4500 1900
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 5F4E95DA
P 3700 3500
F 0 "J2" H 3780 3542 50  0000 L CNN
F 1 "Handset1" H 3780 3451 50  0000 L CNN
F 2 "Terminal_Blocks:TerminalBlock_bornier-3_P5.08mm" H 3700 3500 50  0001 C CNN
F 3 "~" H 3700 3500 50  0001 C CNN
	1    3700 3500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3900 3500 4000 3500
Wire Wire Line
	4000 3500 4000 3050
Connection ~ 4000 3050
Wire Wire Line
	3900 3600 4000 3600
Wire Wire Line
	4000 3600 4000 4000
Wire Wire Line
	4000 4000 3250 4000
Connection ~ 3250 4000
NoConn ~ 3900 3400
Wire Wire Line
	6800 3600 6500 3600
Wire Wire Line
	6500 3600 6500 4000
Wire Wire Line
	6500 4000 5600 4000
Connection ~ 4000 4000
Wire Wire Line
	6500 3050 6500 3500
Wire Wire Line
	6500 3500 6800 3500
Connection ~ 6500 3050
NoConn ~ 6800 3400
$Comp
L Device:C C3
U 1 1 5F4F8E00
P 6250 3500
F 0 "C3" H 6365 3546 50  0000 L CNN
F 1 "1uF" H 6365 3455 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7.2mm_W5.5mm_P5.00mm_FKS2_FKP2_MKS2_MKP2" H 6288 3350 50  0001 C CNN
F 3 "~" H 6250 3500 50  0001 C CNN
	1    6250 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 3500 4100 3500
Connection ~ 4000 3500
Wire Wire Line
	6500 3500 6400 3500
Connection ~ 6500 3500
$Comp
L Device:R_US R3
U 1 1 5F4FAA6B
P 4600 3500
F 0 "R3" H 4668 3546 50  0000 L CNN
F 1 "100" H 4668 3455 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4640 3490 50  0001 C CNN
F 3 "~" H 4600 3500 50  0001 C CNN
	1    4600 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R5
U 1 1 5F4FB887
P 5900 3500
F 0 "R5" H 5968 3546 50  0000 L CNN
F 1 "100" H 5968 3455 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5940 3490 50  0001 C CNN
F 3 "~" H 5900 3500 50  0001 C CNN
	1    5900 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R4
U 1 1 5F4FBD6E
P 5600 3750
F 0 "R4" H 5668 3796 50  0000 L CNN
F 1 "1k8" H 5668 3705 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5640 3740 50  0001 C CNN
F 3 "~" H 5600 3750 50  0001 C CNN
	1    5600 3750
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4148 D5
U 1 1 5F4FC54A
P 5250 3750
F 0 "D5" H 5250 3966 50  0000 C CNN
F 1 "1N4148" H 5250 3875 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_DO-35_SOD27_P7.62mm_Horizontal" H 5250 3575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5250 3750 50  0001 C CNN
	1    5250 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	4450 3500 4400 3500
Wire Wire Line
	6050 3500 6100 3500
$Comp
L Diode:1N4148 D4
U 1 1 5F50ECA1
P 5050 3750
F 0 "D4" H 5050 3966 50  0000 C CNN
F 1 "1N4148" H 5050 3875 50  0000 C CNN
F 2 "Diodes_ThroughHole:D_DO-35_SOD27_P7.62mm_Horizontal" H 5050 3575 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5050 3750 50  0001 C CNN
	1    5050 3750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4750 3500 5050 3500
Wire Wire Line
	5600 3600 5600 3500
Connection ~ 5600 3500
Wire Wire Line
	5600 3500 5750 3500
Wire Wire Line
	5250 3600 5250 3500
Connection ~ 5250 3500
Wire Wire Line
	5250 3500 5600 3500
Wire Wire Line
	5050 3600 5050 3500
Connection ~ 5050 3500
Wire Wire Line
	5050 3500 5250 3500
Wire Wire Line
	5050 3900 5050 4000
Connection ~ 5050 4000
Wire Wire Line
	5050 4000 4000 4000
Wire Wire Line
	5250 3900 5250 4000
Connection ~ 5250 4000
Wire Wire Line
	5250 4000 5050 4000
Wire Wire Line
	5600 3900 5600 4000
Connection ~ 5600 4000
Wire Wire Line
	5600 4000 5250 4000
$Comp
L Mechanical:MountingHole H2
U 1 1 5F52D49D
P 8300 2650
F 0 "H2" H 8400 2696 50  0000 L CNN
F 1 "MountingHole" H 8400 2605 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_4mm" H 8300 2650 50  0001 C CNN
F 3 "~" H 8300 2650 50  0001 C CNN
	1    8300 2650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5F52D751
P 8300 2900
F 0 "H3" H 8400 2946 50  0000 L CNN
F 1 "MountingHole" H 8400 2855 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_4mm" H 8300 2900 50  0001 C CNN
F 3 "~" H 8300 2900 50  0001 C CNN
	1    8300 2900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5F52DA54
P 8300 3150
F 0 "H4" H 8400 3196 50  0000 L CNN
F 1 "MountingHole" H 8400 3105 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_4mm" H 8300 3150 50  0001 C CNN
F 3 "~" H 8300 3150 50  0001 C CNN
	1    8300 3150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
